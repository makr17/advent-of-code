use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let mut elves = vec![];
    let mut calories = 0;
    for s in input.lines() {
	let cals = match s {
	    Ok(val) => val,
	    Err(e) => {
		println!("Error: {}", e);
		"0".to_string()
	    }
	};
	if cals.is_empty() {
	    elves.push(calories);
	    calories = 0;
	    continue;
	}
	calories += cals.parse::<u32>().unwrap();
    }
    elves.push(calories);
    elves.sort();
    //println!("{:?}", elves);
    Ok(*elves.last().unwrap())
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 24000);
    Ok(())
}
