use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashMap;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(mut input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    let mut line = String::new();
    let len = input.read_line(&mut line)?;
    println!("{}", line);
    let chars: Vec<char> = line.chars().collect();
    let mut head = 0;
    for idx in 13..chars.len() {
	// build a hashmap to count unique characters
	let unique: HashMap<char, bool> = chars[idx-13..=idx]
	    .iter()
	    .copied()
	    .map(|x| (x, true))
	    .collect();
	if unique.len() == 14 {
	    // problem indexes from 1, we index from 0
	    head = idx + 1;
	    break;
	}
    }
    if head == 0 {
	head = len;
    }
    Ok(head)
}

#[test]
fn test_input() -> io::Result<()> {
    let file0 = File::open("../test0.txt")?;
    let reader0 = BufReader::new(file0);
    assert_eq!(process(reader0).unwrap(), 19);
    
    let file1 = File::open("../test1.txt")?;
    let reader1 = BufReader::new(file1);
    assert_eq!(process(reader1).unwrap(), 23);

    let file2 = File::open("../test2.txt")?;
    let reader2 = BufReader::new(file2);
    assert_eq!(process(reader2).unwrap(), 23);
    
    let file3 = File::open("../test3.txt")?;
    let reader3 = BufReader::new(file3);
    assert_eq!(process(reader3).unwrap(), 29);

    let file4 = File::open("../test4.txt")?;
    let reader4 = BufReader::new(file4);
    assert_eq!(process(reader4).unwrap(), 26);

    Ok(())
}
