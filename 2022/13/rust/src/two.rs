use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::cmp::{max, Ordering};

use serde_json::{Value};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> std::result::Result<usize, Box::<dyn Error>> {
    let mut lines: Vec<String> = input.lines()
	.map(|l| l.unwrap())
	.filter(|l| !l.is_empty())
	.collect();
    lines.push("[[2]]".to_string());
    lines.push("[[6]]".to_string());
    lines.sort_by(
	|a, b|
	{
	    //println!("a: {}\nb: {}", a, b);
	    match compare(&serde_json::from_str(a).unwrap(),
			  &serde_json::from_str(b).unwrap(), 0) {
		Some(true) => Ordering::Less,
		_ => Ordering::Greater,
	    }
	}
    );

    let divs: Vec<usize> = lines.iter().enumerate()
	.filter(|(_i, p)| *p == "[[2]]" || *p == "[[6]]")
	.map(|(i, _p)| i+1)
	.collect();
    //println!("{:?}", divs);
    let composite = divs[0] * divs[1];
    
    Ok(composite)
}

fn compare (v1: &Value, v2: &Value, _depth: usize) -> Option<bool> {
    // sort out mixed types
    #[allow(unused)]
    let mut left = Value::Null;
    #[allow(unused)]
    let mut right = Value::Null;
    if v1.is_array() && v2.is_number() {
	left = v1.clone();
	right = Value::Array(vec![v2.clone()]);
    }
    else if v1.is_number() && v2.is_array() {
	left = Value::Array(vec![v1.clone()]);
	right = v2.clone();
    }
    else {
	left = v1.clone();
	right = v2.clone();
    }
    
    if left.is_array() {
	let max = max(
	    left.as_array().unwrap().len(),
	    right.as_array().unwrap().len()
	);
	for idx in 0..max {
	    if idx >= left.as_array().unwrap().len() {
		// left ran out first, correctly-ordered
		return Some(true);
	    }
	    else if idx >= right.as_array().unwrap().len() {
		// right ran out first, not correctly ordered
		return Some(false);
	    }
	    // compare values in idx order
	    let same = compare(&left[idx], &right[idx], _depth + 1);
	    match same {
		// had a definitive result from the recurse call, return it
		Some(b) => return Some(b),
		// same so far, keep going
		None => continue,
	    };
	}
    }
    else if left.is_number() {
	return match left.as_u64().cmp(&right.as_u64()) {
	    Ordering::Greater => {
		Some(false)
	    },
	    Ordering::Less => {
		Some(true)
	    },
	    Ordering::Equal => None,
	};
    }
    None
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 140);
    Ok(())
}

