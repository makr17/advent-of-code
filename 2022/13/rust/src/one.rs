use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::cmp::{max, Ordering};
use std::collections::VecDeque;

use serde_json::{Value};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> std::result::Result<usize, Box::<dyn Error>> {
    let lines: Vec<String> = input.lines().map(|l| l.unwrap()).collect();
    let mut sum = 0;
    let mut pair = 1;
    let mut deq = VecDeque::from(lines);
    while !deq.is_empty() {
	let one = deq.remove(0).unwrap();
	let two = deq.remove(0).unwrap();
	let _dummy = deq.remove(0);

	let left = serde_json::from_str(&one)?;
	let right = serde_json::from_str(&two)?;
	
	println!("== Pair {} ==", pair);
	let check = compare(&left, &right, 0);
	// correctly-ordered or same
	if check == Some(true) {
	    println!("*** well-ordered, pair={}", pair);
	    sum += pair;
	}	

	pair += 1;
    }

    Ok(sum)
}

fn compare (v1: &Value, v2: &Value, depth: usize) -> Option<bool> {
    let depth_string: String = vec![' '; depth].iter().collect();
    println!("{}- Compare {} vs {}", depth_string, v1, v2);
    // sort out mixed types
    #[allow(unused)]
    let mut left = Value::Null;
    #[allow(unused)]
    let mut right = Value::Null;
    if v1.is_array() && v2.is_number() {
	left = v1.clone();
	right = Value::Array(vec![v2.clone()]);
	println!(
	    "{}- Mixed types, convert {} to {}",depth_string, v2, right
	);
    }
    else if v1.is_number() && v2.is_array() {
	left = Value::Array(vec![v1.clone()]);
	println!("{}- Mixed types, convert {} to {}", depth_string, v1, left);
	right = v2.clone();
    }
    else {
	left = v1.clone();
	right = v2.clone();
    }
    
    if left.is_array() {
	let max = max(
	    left.as_array().unwrap().len(),
	    right.as_array().unwrap().len()
	);
	for idx in 0..max {
	    if idx >= left.as_array().unwrap().len() {
		// left ran out first, correctly-ordered
		println!("{} - Left side ran out of items, so inputs are in the right order", depth_string);
		return Some(true);
	    }
	    else if idx >= right.as_array().unwrap().len() {
		// right ran out first, not correctly ordered
		println!("{} - Right side ran out of items, so inputs are not in the right order", depth_string);
		return Some(false);
	    }
	    // compare values in idx order
	    let same = compare(&left[idx], &right[idx], depth + 1);
	    match same {
		// had a definitive result from the recurse call, return it
		Some(b) => return Some(b),
		// same so far, keep going
		None => continue,
	    };
	}
    }
    else if left.is_number() {
	return match left.as_u64().cmp(&right.as_u64()) {
	    Ordering::Greater => {
		println!("{} - Right side is smaller, so inputs are not in the right order", depth_string);
		Some(false)
	    },
	    Ordering::Less => {
		println!("{} - Left side is smaller, so inputs are in the right order", depth_string);
		Some(true)
	    },
	    Ordering::Equal => None,
	};
    }
    None
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 13);
    Ok(())
}

