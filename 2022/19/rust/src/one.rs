use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::{HashMap, VecDeque};

use regex::Regex;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Blueprint {
    id: usize,
    costs: HashMap<String, HashMap<String, u32>>,
}
impl Blueprint {
    fn empty(id: usize) -> Self {
	Blueprint {
	    id,
	    HashMap::new(),
	}
    }
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    let name_re = Regex::new(
	r"Each (\w+) robot costs"
    ).unwrap();
    let recipe_re = Regex::new(
	r"(\d+) (\w+)"
    ).unwrap();
    let mut blueprints: Vec<Blueprint> = vec![];
    for l in input.lines() {
	let line = l.unwrap();
	let pieces: Vec<&str> = line.split(':').collect();
	let id_pieces: Vec<&str> = pieces[0].split(' ').collect();
	let id = id_pieces[1].parse::<usize>().unwrap();
	let mut blueprint = Blueprint::empty(id);
	for piece in pieces[1].split('.') {
	    if piece.is_empty() {
		continue;
	    }
	    let name_cap = name_re.captures(&piece).unwrap();
	    //println!("{}", &name_cap[1]);
	    let mut cost = HashMap::new();
	    //println!("{}", name);
	    for recipe_cap in recipe_re.captures_iter(&piece) {
		//println!("{:?}", recipe_cap);
		cost.insert(
		    recipe_cap[2],
		    recipe_cap[1].parse::<u32>().unwrap()
		);
	    }
	    blueprint.costs.insert(name_cap[1].to_string(), cost);
	}
	println!("{:?}", blueprint);
	blueprints.push(blueprint);
    }

    let best: HashMap<Blueprint, usize> = blueprints.iter()
	.map(|b| (*b, optimal(b, 24)))
	.collect();

    let mut score = 0;
    for (b, count) in best.iter() {
	score += b.id * count;
    }
    
    Ok(score)
}

fn optimal(blueprint: &Blueprint, minute: u32) -> usize {
    let mut queue = VecDeque::new();
    queue.push_back(
	( 0, // step
	  HashMap::new(), // inventory
	  HashMap::from([("ore", 1)]), // robots
	  HashMap::new() // built
	)
    );
    let mut cache: HashMap<u32, usize> = HashMap::new();
    while !queue.is_empty() {
	let (step, inventory, robots, built) = queue.pop_front().unwrap();
	step += 1;
	// out of time
	if step > minute {
	    break;
	}
	let &hwm = cache.get(&step).unwrap_or(&0);
	// update inventory for all current robots
	for (name, count) in robots.iter() {
	    *inventory.get_mut(name).unwrap() += count;
	}
	// add potential new robot
	for (name, count) in built.iter() {
	    *robots.entry(name).or_insert(0) += count;
	}
	// what can we build?
	for (name, cost) in blueprints.costs.iter() {
	    let mut build = true;
	    let next_inv = inventory.clone();
	    for (resource, count) cost.iter() {
		if inventory.entry(resource).or_insert(0) < count {
		    build = false;
		    break; // can't build it yet
		}
		else {
		    *next_inv.entry(resource).or_insert(0) -= count;
		}
	    }
	    if !build {
		continue;
	    }
	    // queue up each build possibility
	    queue.push_back(
		( step,
		  next_inv,
		  robots.clone(),
		  HashMap::from([(name, 1)])
		)
	    );
	}
	// also queue an empty build possibility
	queue.push_back(
	    ( step,
	      inventory.clone(),
	      robots.clone(),
	      HashMap::new()
	    )
	);
	let geodes = match inventory.get("geode") {
	    Some(val) => val,
	    None => &0,
	};
	if geodes > &hwm {
	    *cache.entry(step).or_insert(geodes) = geodes;
	}
    }
    let best = match cache.get(minutes) {
	Some(val) => val,
	None => &0,
    };
    *best
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 33);
    Ok(())
}
