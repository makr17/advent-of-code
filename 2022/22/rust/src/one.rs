use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashMap;

use regex::Regex;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Debug, PartialEq)]
enum Space {
    Open,
    Wall,
    Unknown,
}

#[derive(Clone, Copy, Debug, Hash, PartialOrd, Ord, PartialEq, Eq)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug, PartialEq)]
enum Move {
    Forward,
    Turn,
}

fn process(input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut board: HashMap<Point, Space> = HashMap::new();
    let mut build_board = true;
    let re = Regex::new(r"(\d+|[RL])").unwrap();
    let mut moves: Vec<(Move, i32)> = vec![];
    for (y,l) in input.lines().enumerate() {
	let line = l.unwrap();
	if line.is_empty() {
	    build_board = false;
	}
	if build_board {
	    for (x,c) in line.chars().enumerate() {
		let space = match c {
		    '.' => Space::Open,
		    '#' => Space::Wall,
		    _ => Space::Unknown,
		};
		if space == Space::Unknown {
		    continue;
		}
		// board indexes from 1, iterator from 0
		board.insert(Point { x: x as i32 + 1, y: y as i32 + 1 }, space);
	    }
	}
	else {
	    for cap in re.captures_iter(&line) {
		let m: (Move, i32) = match &cap[1] {
		    "R" => (Move::Turn, 1),
		    "L" => (Move::Turn, -1),
		    _ => (Move::Forward, cap[1].parse::<i32>().unwrap()),
		};
		moves.push(m);
	    }
	    //println!("{:?}", moves);
	}
    }

    //println!("{:?}", board);

    // find starting position
    let mut facing: i32 = 0;
    let mut rownums: Vec<i32> = board.iter()
	.map(|(p, _s)| p.y)
	.collect();
    rownums.sort();
    let row = *rownums.first().unwrap();
    let mut colnums: Vec<i32> = board.iter()
	.filter(|(p, s)| p.y == row && s == &&Space::Open)
	.map(|(p, _s)| p.x)
	.collect();
    colnums.sort();
    let col = *colnums.first().unwrap();
    let mut pos = Point { x: col, y: row  };
    println!("{:?}", pos);

    for m in moves.into_iter() {
	if m.0 == Move::Forward {
	    for _shift in 1..=m.1 {
		// build next position
		let mut newpos = pos;
		match facing {
		    0 => newpos.x += 1,
		    1 => newpos.y += 1,
		    2 => newpos.x += -1,
		    3 => newpos.y += -1,
		    _ => continue,
		}
		if !board.contains_key(&newpos) {
		    // wrapping off the edge
		    // find the next point on the other side
		    newpos = find_space(&newpos, facing, &board);
		}
		if board.get(&newpos).unwrap() == &Space::Wall {
		    // hit a wall, movement stops
		    break;
		}
		pos = newpos;
	    }
	}
	else if m.0 == Move::Turn {
	    facing = (facing + m.1).rem_euclid(3);
	}
    }

    Ok(1000 * pos.y + 4 * pos.x + facing)
}

fn find_space(pos: &Point, facing: i32, board: &HashMap<Point, Space>) -> Point {
    let mut row: Vec<Point> = board.iter()
	.filter(|(p, _s)| p.y == pos.y)
	.map(|(p, _s)| *p)
	.collect();
    row.sort();
    let mut col: Vec<Point> = board.iter()
	.filter(|(p, _s)| p.x == pos.x)
	.map(|(p, _s)| *p)
	.collect();
    col.sort();
    let point = match facing {
	0 => *row.first().unwrap(),
	1 => *col.first().unwrap(),
	2 => *row.last().unwrap(),
	3 => *col.last().unwrap(),
	_ => Point { x: 0, y: 0 }, // shouldn't get here
    };
    point
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 6032);
    Ok(())
}
