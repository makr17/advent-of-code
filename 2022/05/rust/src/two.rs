use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use regex::Regex;

fn main() -> io::Result<()> {
    // hardcode initial state, I don't have the patience to write a parser
    let mut stacks: Vec<Vec<char>> = vec![];
    // dummy at index 0
    stacks.insert(0, vec![]);
    // built from
    //   head -8 input.txt |cut N-N
    // for each column
    // so reversing to git into proper stack order
    let mut vec1 = vec!['W','P','G','Z','V','S','B'];
    vec1.reverse();
    stacks.insert(1, vec1);
    let mut vec2 = vec!['F','Z','C','B','V','J'];
    vec2.reverse();
    stacks.insert(2, vec2);
    let mut vec3 = vec!['C','D','Z','N','H','M','L','V'];
    vec3.reverse();
    stacks.insert(3, vec3);
    let mut vec4 = vec!['B','J','F','P','Z','M','D','L'];
    vec4.reverse();
    stacks.insert(4, vec4);
    let mut vec5 = vec!['H','Q','B','J','G','C','F','V'];
    vec5.reverse();
    stacks.insert(5, vec5);
    let mut vec6 = vec!['B','L','S','T','Q','F','G'];
    vec6.reverse();
    stacks.insert(6, vec6);
    let mut vec7 = vec!['V','Z','C','G','L'];
    vec7.reverse();
    stacks.insert(7, vec7);
    let mut vec8 = vec!['G','L','N'];
    vec8.reverse();
    stacks.insert(8, vec8);
    let mut vec9 = vec!['C','H','F','J'];
    vec9.reverse();
    stacks.insert(9, vec9);

    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader, stacks);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>, mut stacks: Vec<Vec<char>>) -> Result<String, Box::<dyn Error>> {
    let re = Regex::new(r"move (\d+) from (\d) to (\d)").unwrap();
    for l in input.lines() {
	let line = l.unwrap();
	if line.is_empty() {
	    break;
	}
	let caps = re.captures(&line).unwrap();
	// what we're doing
	let count = caps.get(1).map(|x| x.as_str().parse::<usize>().unwrap()).unwrap();
	let from = caps.get(2).map(|x| x.as_str().parse::<usize>().unwrap()).unwrap();
	let to = caps.get(3).map(|x| x.as_str().parse::<usize>().unwrap()).unwrap();
	let slen = stacks[from].len();
	// working around multiple mutable borrows
	let tmp = stacks[from].clone();
	let (head, tail) = tmp.split_at(slen - count);
	let mut piece: Vec<char> = tail.to_owned();
	stacks[to].append(&mut piece);
	let remains: Vec<char> = head.to_owned();
	stacks[from] = remains;
    }
    //println!("{:?}", stacks);
    let mut result = String::new();
    // leave out the dummy when we construct the return
    for s in stacks[1..].iter() {
	result.push(*s.last().unwrap());
    }

    Ok(result)
}

#[test]
fn test_input() -> io::Result<()> {
    // hardcode initial state, I don't have the patience to write a parser
    let mut stacks: Vec<Vec<char>> = vec![];
    // dummy at index 0
    stacks.insert(0, vec![]);
    let mut vec1: Vec<char> = vec!['N','Z'];
    vec1.reverse();
    stacks.insert(1, vec1);
    let mut vec2: Vec<char> = vec!['D','C','M'];
    vec2.reverse();
    stacks.insert(2, vec2);
    let mut vec3: Vec<char> = vec!['P'];
    vec3.reverse();
    stacks.insert(3, vec3);
    
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader, stacks).unwrap(), "MCD".to_string());
    Ok(())
}
