use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use regex::Regex;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let re = Regex::new(r"(\d+)-(\d+),(\d+)-(\d+)").unwrap();
    let mut contains = 0;
    for line in input.lines() {
	let pairs = line.unwrap();

	let caps = re.captures(&pairs).unwrap();
	let elf1start = caps.get(1).map(|x| x.as_str().parse::<u32>().unwrap());
	let elf1end = caps.get(2).map(|x| x.as_str().parse::<u32>().unwrap());
	let elf2start = caps.get(3).map(|x| x.as_str().parse::<u32>().unwrap());
	let elf2end = caps.get(4).map(|x| x.as_str().parse::<u32>().unwrap());

	if elf1start <= elf2start && elf1end >= elf2end || elf2start <= elf1start && elf2end >= elf1end {
	    contains += 1;
	}
    }
    Ok(contains)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 2);
    Ok(())
}
