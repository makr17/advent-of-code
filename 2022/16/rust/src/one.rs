use std::cmp::Reverse;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashMap;

use pathfinding::prelude::yen;
use regex::Regex;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Valve {
    name: String,
    rate: u32,
    open: bool,
    connects: Vec<String>,
}
impl Valve {
    fn successors(&self, valves: &HashMap<String, Valve>) -> Vec<(Valve, u32)> {
	self.connects.iter()
	    .map(|n| (valves.get(n).unwrap().clone(), 1))
	    .collect()
    }

    fn best_path(&self, target: &Valve, valves: &HashMap<String, Valve>, step: u32) -> (Vec<Valve>, u32, u32) {
	let paths = yen(
	    self,
	    |v: &Valve| v.successors(&valves),
	    |v: &Valve| v == target,
	    15  // input has 15 non-zero-flow valves
	);
	// splice in average flow rate opportunity along the path
	let mut values: Vec<(Vec<Valve>, u32, u32)> = paths.into_iter()
	    .filter(|(_p, l)| *l <= 30 - step)
	    .map(|(p, l)| {
		// leave self out of path value, we're already here
		let rates: Vec<u32> = p.iter()
		    .filter(|v| *v != self && !v.open)
		    .map(|v| v.rate)
		    .collect();
		let mut value: u32 = 0;
		// the value is the compound released pressure based on when we'll get there
		for (i,r) in rates.iter().enumerate() {
		    let val = (30 as i64 - step as i64 - i as i64) * *r as i64;
		    if val > 0 {
			value += val as u32;
		    }
		}
		(p, l, value/l)
	    })
	    .collect();
	values.sort_unstable_by_key(|v| Reverse(v.2));
	/*
	let debug: Vec<(Vec<String>, u32)> = values.iter()
	    .map(|(p, _l, val)| {
		let names: Vec<String> = p.iter()
		    .map(|v| v.name.clone())
		    .collect();
		(names, *val)
	    })
	    .collect();
	println!("{}->{}: {:?}", self.name, target.name, debug);
	 */
	match values.first() {
	    Some(val) => val.clone(),
	    None => (vec![], 0, 0),
	}
    }
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    // parse input and build structure for valves and paths
    // the optional 's' for plural/singular is annoying, but manageable
    let re = Regex::new(r"Valve (\w+) has flow rate=(\d+); tunnel(s{0,1}) lead(s{0,1}) to valve(s{0,1}) (.+)").unwrap();
    let mut valves: HashMap<String, Valve> = HashMap::new();
    for l in input.lines() {
	let line = l.unwrap();
	let caps = re.captures(&line).unwrap();
	let name = caps[1].to_string();
	let rate = caps[2].parse::<u32>().unwrap();
	let paths: Vec<String> = caps[6]
	    .split(", ")
	    .map(|s| s.to_string())
	    .collect();
	let v = Valve {
	    name: name.clone(),
	    rate,
	    open: false,
	    connects: paths,
	};
	valves.insert(name, v);
    }
    
    // don't care about valves with flow rate of 0, they can't help
    let mut released = 0;
    let mut step = 1;
    let mut current = valves.get("AA").unwrap().clone();
    while step <= 30 {
	// best value path for each remaining open valve
	let mut remaining: Vec<(Vec<Valve>, u32, u32)> = valves.iter()
	    .filter(|(_n, v)| **v != current && !v.open && v.rate > 0)
	    .map(|(_n, v)| current.best_path(&v, &valves, step))
	    .collect();
	// sort by potential value
	remaining.sort_unstable_by_key(|v| Reverse(v.2));
	if remaining.is_empty() || remaining.first().unwrap().0.is_empty() {
	    // stay put, nothing left to do
	    released += take_step(step, &valves);
	    step += 1;
	}
	else {
	    let(path, _len, _value) = remaining.first().unwrap();
	    let target = path.last().unwrap();
	    let debug: Vec<(String, u32)> = path[1..].iter()
		.map(|v| (v.name.clone(), v.rate))
		.collect();
	    println!("{}->{}: {:?}", current.name, target.name, debug);
	    // walk the path
	    // skip first node in path, that is current
	    for (i, v) in path[1..].iter().enumerate() {
		if step > 30 {
		    break;
		}
		// set our current position
		current = valves.get(&v.name).unwrap().clone();
		released += take_step(step, &valves);
		step += 1;
		println!("You move to valve {}", v.name);
		// open opportunistically if we have the chance
		// calculate the cost of delaying the rest of the path by one move
		let cost = path[i+1..].iter()
		    .map(|v| v.rate)
		    .sum::<u32>();
		let opportunity = v.rate;
		if !current.open
		    && current.rate > 0
		    && step <= 30 {
			if current == *target {
			    released += take_step(step, &valves);
			    println!("You open valve {} ({})", current.name, current.rate);
			    valves.get_mut(&current.name).unwrap().open = true;
			    step += 1;
			}
			else {
			    println!(
				"Considering {}: cost={} vs {}",
				v.name, cost, opportunity
			    );
			    if cost < opportunity {
				released += take_step(step, &valves);
				println!("You open valve {} ({})", current.name, current.rate);
				valves.get_mut(&current.name).unwrap().open = true;
				step += 1;
			    }
			}
		    }
	    }
	}
    }
    
    Ok(released)
}

fn take_step(step: u32, valves: &HashMap<String, Valve>) -> u32 {
    println!("== Minute {} ==", step);
    let open: Vec<Valve> = valves.iter()
	.filter(|(_n, v)| v.open)
	.map(|(_n, v)| v.clone())
	.collect();
    let released = open.iter()
	.map(|v| v.rate)
	.sum();
    let mut names: Vec<String> = open.iter()
	.map(|v| v.name.clone())
	.collect();
    names.sort();
    println!("Valves {:?} are open, releasing {} pressure.", names, released);
    released
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 1651);
    Ok(())
}

