use std::cmp::{min, max};
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashSet;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone,Copy,Debug,Hash,PartialEq,Eq)]
struct Point (usize, usize);
impl Point {
    fn path(&self, other: &Point) -> Vec<Point> {
	if self.0 == other.0 {
	    return (min(self.1, other.1) ..= max(self.1, other.1))
		.map(|y| Point (self.0, y))
		.collect()
	}
	else if self.1 == other.1 {
	    return (min(self.0, other.0)  ..= max(self.0, other.0))
		.map(|x| Point (x, self.1))
		.collect()
	}
	// to make the type signature happy
	// but we really shouldn't get here given the problem spec
	vec![]
    }
}

fn process(input: BufReader<File>) -> std::result::Result<usize, Box::<dyn Error>> {
    let source = Point(500, 0);

    let mut grid: HashSet<Point> = HashSet::new();

    let mut ymax = 0;
    let mut xy: Vec<usize> = vec![];
    for l in input.lines() {
	let line = l.unwrap();
	let points: Vec<Point> = line
	    .split(" -> ")
	    .map(
		|s| {
		    xy = s
			.split(',')
			.map(|n| n.parse::<usize>().unwrap())
			.collect();
		    Point (xy[0], xy[1])
		}
	    )
	    .collect();
	for idx in 1..points.len() {
	    let path = points[idx-1].path(&points[idx]);
	    for p in path.iter() {
		grid.insert(*p);
		if p.1 > ymax {
		    ymax = p.1;
		}
	    }
	}
    }
    // 1 more means resting on the floor
    ymax += 1;

    // start adding sand
    let mut count = 0;
    loop {
	let mut sand = source;
	for y in 1..=ymax {
	    if !grid.contains(&Point(sand.0, y)) {
		// drop straight down if we can
		sand.1 = y;
	    }
	    else if !grid.contains(&Point(sand.0 - 1, y)) {
		// diagonally left if we can
		sand.1 = y;
		sand.0 -= 1;
	    }
	    else if !grid.contains(&Point(sand.0 + 1, y)) {
		// diagonally right if we can
		sand.1 = y;
		sand.0 += 1;
	    }
	    else {
		// came to rest, add the grain to the grid
		grid.insert(sand);
		break;
	    }
	}
	grid.insert(sand);
	count += 1;
	if grid.contains(&source) {
	    // blocked
	    break;
	}
    }

    Ok(count)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 93);
    Ok(())
}

