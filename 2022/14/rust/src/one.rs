use std::cmp::{min, max};
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone,Copy,Debug)]
struct Point (usize, usize);
impl Point {
    fn path(&self, other: &Point) -> Vec<Point> {
	if self.0 == other.0 {
	    return (min(self.1, other.1) ..= max(self.1, other.1))
		.map(|y| Point (self.0, y))
		.collect()
	}
	else if self.1 == other.1 {
	    return (min(self.0, other.0)  ..= max(self.0, other.0))
		.map(|x| Point (x, self.1))
		.collect()
	}
	// to make the type signature happy
	// but we really shouldn't get here given the problem spec
	vec![]
    }
}

fn process(input: BufReader<File>) -> std::result::Result<usize, Box::<dyn Error>> {
    /*
    I did a bit of fiddling with the input to precalculate the bounds
      $ cut -f1 -d',' <sizing.txt |sort -n |head -1
      426
      $ cut -f1 -d',' <sizing.txt |sort -n |tail -1
      506
      $ cut -f2 -d',' <sizing.txt |sort -n |head -1
      13
      $ cut -f2 -d',' <sizing.txt |sort -n |tail -1
      158
    But the _test_ input has y values as small as 4
     */
    let xmin = 426 - 1; // pad x by one for fall-off
    let xmax = 506 + 1;
    let ymin = 0; // because of the source
    let ymax = 158 + 1; // pad so we can tell when we're into the abyss

    let source = Point(500 - xmin, 0 - ymin);

    // build the grid
    let mut grid: Vec<Vec<char>> = vec![];
    for _idx in ymin..=ymax {
	grid.push(vec![' '; xmax-xmin]);
    }
    grid[source.1][source.0] = '+';
    // and populate from our input
    let mut xy: Vec<usize> = vec![];
    for l in input.lines() {
	let line = l.unwrap();
	let points: Vec<Point> = line
	    .split(" -> ")
	    .map(
		|s| {
		    xy = s
			.split(',')
			.map(|n| n.parse::<usize>().unwrap())
			.collect();
		    Point (xy[0] - xmin, xy[1] - ymin) // scale points by min
		}
	    )
	    .collect();
	for idx in 1..points.len() {
	    let path = points[idx-1].path(&points[idx]);
	    for p in path.iter() {
		grid[p.1][p.0] = '#';
	    }
	}
    }

    /*
    for r in grid.iter() {
	let row: String = r.iter().collect();
	println!("{}", row);
    }
    */

    // start adding sand
    let mut count = 0;
    loop {
	let mut sand = source;
	for y in 1..=ymax {
	    if sand.1 >= ymax {
		// falling off the edge
		break;
	    }
	    if grid[y][sand.0] == ' ' {
		// drop straight down if we can
		sand.1 = y;
	    }
	    else if grid[y][sand.0 - 1] == ' ' {
		// diagonally left if we can
		sand.1 = y;
		sand.0 -= 1;
	    }
	    else if grid[y][sand.0 + 1] == ' ' {
		// diagonally right if we can
		sand.1 = y;
		sand.0 += 1;
	    }
	    else {
		// came to rest
		grid[sand.1][sand.0] = 'o';
		break;
	    }
	}
	if sand.1 >= ymax {
	    // fell off the edge into the abyss
	    break;
	}
	else {
	    count += 1;
	}
    }

    Ok(count)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 24);
    Ok(())
}

