use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader, Write};

use std::collections::HashSet;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

static DEBUG: bool = false;
fn dbg(out: String) {
    if DEBUG {
	println!("{}", out);
    }
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
struct Point(i32, i32);

fn move_left(rock: &HashSet<Point>, area: &HashSet<Point>) -> HashSet<Point> {
    // already at the left wall?
    let blocked: Vec<Point> = rock.iter()
        .filter(|p| p.0 == 0)
	.map(|p| *p)
        .collect();
    if blocked.is_empty() {
        // check to see if existing stones are where we need to be
        let new: HashSet<Point> = rock.iter()
    	    .map(|p| Point(p.0 - 1, p.1))
    	    .collect();
        if new.is_disjoint(area) {
	    dbg(format!("Jet of gas pushes rock left"));
    	    return new;
        }
    }
    dbg(format!("Jet of gas pushes rock left, but nothing happens"));
    return rock.clone();
}

fn move_right(rock: &HashSet<Point>, area: &HashSet<Point>) -> HashSet<Point> {
    // already at the right wall?
    let blocked: Vec<Point> = rock.iter()
        .filter(|p| p.0 == 6)
	.map(|p| *p)
        .collect();
    if blocked.is_empty() {
        // check to see if existing stones are there
        let new: HashSet<Point> = rock.iter()
    	    .map(|p| Point(p.0 + 1, p.1))
    	    .collect();
        if new.is_disjoint(area) {
	    dbg(format!("Jet of gas pushes rock right"));
    	    return new;
        }
    }
    dbg(format!("Jet of gas pushes rock right, but nothing happens"));
    return rock.clone();
}

fn drop(rock: &HashSet<Point>, area: &HashSet<Point>) -> Option<HashSet<Point>> {
    let new: HashSet<Point> = rock.iter()
        .map(|p| Point(p.0, p.1 - 1))
        .collect();
    if new.is_disjoint(area) {
	dbg(format!("Rock falls 1 unit"));
        return Some(new);
    }
    dbg(format!("Rock comes to a rest"));
    None
}

fn process(mut input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut line = String::new();
    let _len = input.read_line(&mut line).unwrap();
    // almost forgot to discard the trailing CR
    let wind: Vec<char> = line.trim().chars().collect();
    // initial _x_ positioning
    // y will need to be set when the rock appears
    let rocks: Vec<HashSet<Point>> = vec![
	// dash
	HashSet::from([
	    Point(2,0), Point(3,0), Point(4,0), Point(5,0)
	]),
	// plus
	HashSet::from([
	                Point(3,2),
	    Point(2,1), Point(3,1), Point(4,1),
	                Point(3,0)
	]),
	// backwards L
	HashSet::from([
	                            Point(4,2),
	                            Point(4,1),
	    Point(2,0), Point(3,0), Point(4,0)
	]),
	// pipe
	HashSet::from([
	    Point(2,3),
	    Point(2,2),
	    Point(2,1),
	    Point(2,0)
	]),
	// block
	HashSet::from([
	    Point(2,1), Point(3,1),
	    Point(2,0), Point(3,0)
	])
    ];

    // fill in the floor at y=0
    let mut area: HashSet<Point> = HashSet::from([
	Point(0,0), Point(1,0), Point(2,0), Point(3,0), Point(4,0), Point(5,0), Point(6,0)
    ]);
    // let's start dropping rocks
    let mut idx = 0;
    for count in 0..2022 {
	let mut max: Vec<i32> = area.iter()
	    .map(|p| p.1)
	    .collect();
	max.sort();
	let max_y = max.last().unwrap();
	let mut rock: HashSet<Point> = rocks[count % rocks.len()].iter()
	    .map(|p| Point(p.0, p.1 + max_y + 4))
	    .collect();
	//visualize(&rock, &area);
	loop {
	    visualize(&rock, &area);
	    dbg(format!("idx: {} => {}", idx, idx % wind.len()));
	    rock = match wind[idx % wind.len()] {
		'<' => move_left(&rock, &area),
		'>' => move_right(&rock, &area),
		_ => rock, // but really shouldn't happen
	    };
	    idx += 1;
	    visualize(&rock, &area);
	    match drop(&rock, &area) {
		Some(r) => rock = r,
		None => {
		    // add rock to area
		    for p in rock.drain() {
			area.insert(p);
		    }
		    // and on to the next
		    break;
		}
	    };
	}
	visualize(&rock, &area);
    }

    let mut max: Vec<i32> = area.iter()
	.map(|p| p.1)
	.collect();
    max.sort();
    let max_y = max.last().unwrap();
    Ok(*max_y)
}

fn visualize(rock: &HashSet<Point>, area: &HashSet<Point>) {
    if !DEBUG {
	return;
    }
    let mut ys: Vec<i32> = rock.union(area)
	.map(|p| p.1)
	.collect();
    ys.sort();
    let max_y = ys.last().unwrap();

    for y in (0..=*max_y).rev() {
	print!("|");
	for x in 0..7 {
	    let p = Point(x,y);
	    if rock.contains(&p) {
		print!("@");
	    }
	    else if area.contains(&p) {
		print!("#");
	    }
	    else {
		print!(".");
	    }
	}
	println!("|");
    }
    println!("");
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 3068);
    Ok(())
}
