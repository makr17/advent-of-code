use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone,Debug)]
struct Node {
    parent: Option<usize>,
    name: String,
    size: Option<i64>,
    children: Vec<usize>,
}
impl Node {
    fn size (&self, nodes: &Vec<Node>) -> i64 {
	match self.size {
	    Some(x) => x,
	    None => self.children
		.iter()
		.map(|idx| nodes[*idx].size(nodes))
		.sum()
	}
    }
}

fn process(input: BufReader<File>) -> Result<i64, Box::<dyn Error>> {
    let mut nodes: Vec<Node> = vec![];
    let root = Node {
	parent: None,
	name: "/".to_owned(),
	size: None,
	children: vec![],
    };
    nodes.push(root);
    let mut parent_idx = 0;
    for l in input.lines() {
	let line = l.unwrap();
	//println!("{}", line);
	let pieces: Vec<&str> = line.split(' ').collect();
	match pieces[0] {
	    "$" => {
		if pieces[1] == "cd" {
		    match pieces[2] {
			"/" => {
			    // back to the rood
			    parent_idx = 0;
			},
			".." => {
			    // up a level
			    // set parent_idx to our parent's parent
			    parent_idx = nodes[parent_idx].parent.unwrap_or(0);
			},
			_ => {
			    // down a level
			    // find the node with given name
			    let matches: Vec<(usize, &Node)> = nodes
				.iter()
				.enumerate()
				.filter(|(idx, _)| nodes[parent_idx].children.contains(idx))
				.filter(|(_, x)| x.name == pieces[2])
				.collect();
			    (parent_idx, _) = matches[0];
			}
		    };
		}
		// ignore "ls" command
		// only care about the output that follow
	    },
	    "dir" => {
		// new dir node
		let node = Node {
		    parent: Some(parent_idx),
		    name: pieces[1].to_owned(),
		    size: None,
		    children: vec![],
		};
		// put the new node in the storage vec
		nodes.push(node);
		// and put the index in our parent's child vec
		let idx = nodes.len() - 1;
		nodes[parent_idx].children.push(idx);
	    },
	    _ => {
		// new file node
		let node = Node {
		    parent: Some(parent_idx),
		    name: pieces[1].to_owned(),
		    size: Some(pieces[0].parse::<i64>()?),
		    children: vec![],
		};
		nodes.push(node);
		let idx = nodes.len() - 1;
		nodes[parent_idx].children.push(idx);
	    },
	};
    }
    let target = -70000000 + nodes[0].size(&nodes) + 30000000;
    println!("target is {} bytes", target);
    let mut sizes: Vec<i64> = nodes
        .iter()
        .filter(|x| x.size.is_none()) // only dirs
        .map(|x| x.size(&nodes)) // extract aggregate sizes
        .filter(|x| x >= &target) // min size
        .collect();
    sizes.sort();

    Ok(sizes[0])
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 24933642);
    Ok(())
}
