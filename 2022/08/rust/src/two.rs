use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    let lines: Vec<String> = input.lines().map(|x| x.unwrap()).collect();
    // calculate size of grid
    let ymax = lines.len();
    let xmax = lines[0].len();
    // build the grid
    let mut forest_raw: Vec<u32> = lines
	.join("")
	.chars()
	.map(|x| x.to_digit(10).unwrap())
	.collect();
    let mut forest_base: Vec<_> = forest_raw
	.as_mut_slice()
	.chunks_mut(xmax)
	.collect();
    // final 2d array: &mut [&mut [_]]
    let forest = forest_base.as_mut_slice();

    let mut max_score = 0;
    for x in 1..xmax-1 {
	for y in 1..ymax-1 {
	    let height = forest[y][x];
	    let up: Vec<u32> = (0..y).rev()
		.map(|idx| forest[idx][x])
		.collect();
	    let mut up_score = 0;
	    for val in up {
		up_score += 1;
		if val >= height {
		    break;
		}
	    }
	    let down: Vec<u32> = (y+1..ymax)
		.map(|idx| forest[idx][x])
		.collect();
	    let mut down_score = 0;
	    for val in down {
		down_score += 1;
		if val >= height {
		    break;
		}
	    }	    
	    let left: Vec<u32> = (0..x).rev()
		.map(|idx| forest[y][idx])
		.collect();
	    let mut left_score = 0;
	    for val in left {
		left_score += 1;
		if val >= height {
		    break;
		}
	    }	    
	    let right: Vec<u32> = (x+1..xmax)
		.map(|idx| forest[y][idx])
		.collect();
	    let mut right_score = 0;
	    for val in right {
		right_score += 1;
		if val >= height {
		    break;
		}
	    }	    
	    let score = up_score * down_score * left_score * right_score;
	    if score > max_score {
		max_score = score;
		/*
		println!("({},{}): {}", x, y, score);
		println!(
		    "up={}, down={}, left={}, right={}",
		    up_score, down_score, left_score, right_score
	        );
		*/
	    }
	}
    }
    
    Ok(max_score)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 8);
    Ok(())
}
