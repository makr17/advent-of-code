use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    let lines: Vec<String> = input.lines().map(|x| x.unwrap()).collect();
    // calculate size of grid
    let ymax = lines.len();
    let xmax = lines[0].len();
    // build the grid
    let mut forest_raw: Vec<u32> = lines
	.join("")
	.chars()
	.map(|x| x.to_digit(10).unwrap())
	.collect();
    let mut forest_base: Vec<_> = forest_raw
	.as_mut_slice()
	.chunks_mut(xmax)
	.collect();
    // final 2d array: &mut [&mut [_]]
    let forest = forest_base.as_mut_slice();

    // the outer "shell" is defacto visible
    let mut visible = 2 * (xmax -1) + 2 * (ymax - 1);
    // now check interior trees
    for x in 1..xmax-1 {
	for y in 1..ymax-1 {
	    let height = forest[y][x];
	    //println!("({},{}): {}", x, y, height);
	    let up: Vec<u32> = (0..y)
		.map(|idx| forest[idx][x])
		.filter(|n| *n >= height)
		.collect();
	    if up.is_empty() {
		//println!("visible from below");
		visible += 1;
		continue;
	    }
	    let down: Vec<u32> = (y+1..ymax)
		.map(|idx| forest[idx][x])
		.filter(|n| *n >= height)
		.collect();
	    if down.is_empty() {
		//println!("visible from below");
		visible += 1;
		continue;
	    }
	    let left: Vec<u32> = (0..x)
		.map(|idx| forest[y][idx])
		.filter(|n| *n >= height)
		.collect();
	    if left.is_empty() {
		//println!("visible from left");
		visible += 1;
		continue;
	    }
	    let right: Vec<u32> = (x+1..xmax)
		.map(|idx| forest[y][idx])
		.filter(|n| *n >= height)
		.collect();
	    if right.is_empty() {
		//println!("visible from right");
		visible += 1;
		continue;
	    }
	}
    }
    
    Ok(visible)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 21);
    Ok(())
}
