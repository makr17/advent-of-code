use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use std::collections::HashMap;
use std::collections::HashSet;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(mut input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    // initialize priority mapping
    let mut prios: HashMap<char, u32> = HashMap::new();
    let mut pri = 0;
    for c in 'a'..='z' {
	pri += 1;
	prios.insert(c, pri);
    }
    for c in 'A'..='Z' {
	pri += 1;
	prios.insert(c, pri);
    }

    let mut sum = 0;

    // take lines three at at a time
    loop {
	let mut first = String::new();
	let mut second = String::new();
	let mut third = String::new();
	let l1 = input.read_line(&mut first)?;
	if l1 == 0 {
	    break;
	}
	let _l2 = input.read_line(&mut second)?;
	let _l3 = input.read_line(&mut third)?;

	// input strings into hashsets, one per line
	let sack1: HashSet<char> = first.trim().chars().collect();
	let sack2: HashSet<char> = second.trim().chars().collect();
	let sack3: HashSet<char> = third.trim().chars().collect();
	//println!("first: {}", first);
	//println!("sack1 {:?}", sack1);
	//println!("second: {}", second);
	//println!("sack2 {:?}", sack2);
	// intersect the first two sacks into a new hashset
	let inter: HashSet<char> = sack1.intersection(&sack2).copied().collect();
	//println!("inter {:?}", inter);
	//println!("third: {}", third);
	//println!("sack3 {:?}", sack3);
	// then intersect that with the third to find the common element
	let badge: Vec<char> = inter.intersection(&sack3).copied().collect();
	//println!("{:?}", badge);
	sum += prios.get(&badge[0]).unwrap();
    }

    Ok(sum)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 70);
    Ok(())
}
