use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use std::collections::HashMap;
use std::collections::HashSet;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    // initialize priority mapping
    let mut prios: HashMap<char, u32> = HashMap::new();
    let mut pri = 0;
    for c in 'a'..='z' {
	pri += 1;
	prios.insert(c, pri);
    }
    for c in 'A'..='Z' {
	pri += 1;
	prios.insert(c, pri);
    }

    let mut sum = 0;
    // iterate through the input
    for line in input.lines() {
	let mut top: HashSet<char> = HashSet::new();
	let mut bottom: HashSet<char> = HashSet::new();
	let sack = line.unwrap();
	let len = sack.len();
	//println!("{}", sack);
	for (idx, item) in sack.chars().enumerate() {
	    //println!("{}: {}", idx, item);
	    if idx < len/2 {
		top.insert(item);
	    }
	    else {
		bottom.insert(item);
	    }
	}
	//println!("{:?}: {:?}, {:?}", top.intersection(&bottom), top, bottom);
	let intersect = top.intersection(&bottom);
	for item in intersect {
	    let prio = prios.get(item).unwrap();
	    //println!("{}: {}", item, prio);
	    sum += prio;
	}
    }
    Ok(sum)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 157);
    Ok(())
}
