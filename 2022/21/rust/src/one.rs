use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashMap;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

type Monkey = String;
#[derive(Debug)]
enum Op {
    Plus,
    Minus,
    Times,
    Divide,
    Unknown,
}
type Formula = (Op, Monkey, Monkey);

fn process(input: BufReader<File>) -> Result<i64, Box::<dyn Error>> {
    // read the input into hashmaps by name
    let mut numbers: HashMap<Monkey, i64> = HashMap::new();
    let mut formulas: HashMap<Monkey, Formula> = HashMap::new();
    for l in input.lines() {
	let line = l.unwrap();
	let lr: Vec<String> = line.split(": ").map(|s| s.to_string()).collect();
	let m: Monkey = lr[0].clone();
	let value = lr[1].parse::<i64>();
	if value.is_ok() {
	    numbers.insert(m, value.unwrap());
	}
	else {
	    let pieces: Vec<String> = lr[1].split(' ').map(|s| s.to_string()).collect();
	    let ml = pieces[0].clone();
	    let mr = pieces[2].clone();
	    let op = match pieces[1].as_str() {
		"+" => Op::Plus,
		"-" => Op::Minus,
		"*" => Op::Times,
		"/" => Op::Divide,
		_ => Op::Unknown,
	    };
	    let f: Formula = (op, ml, mr);
	    formulas.insert(m, f);
	}
    }

    Ok(value(&String::from("root"), &numbers, &formulas))
}

fn value(m: &Monkey, n: &HashMap<Monkey, i64>, f: &HashMap<Monkey, Formula>) -> i64 {
    // if monkey is simple num then we're done for this branch
    let num = n.get(m);
    if num.is_some() {
	return *num.unwrap();
    }
    // otherwise, look in formulas
    let formula = f.get(m);
    match f.get(m) {
	Some((Op::Plus, l, r)) => value(l, n, f) + value(r, n, f),
	Some((Op::Minus, l, r)) => value(l, n, f) - value(r, n, f),
	Some((Op::Times, l, r)) => value(l, n, f) * value(r, n, f),
	Some((Op::Divide, l, r)) => value(l, n, f) / value(r, n, f),
	_ => {
	    println!("something is very wrong: {:?}", formula);
	    1
	}
    }
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 152);
    Ok(())
}
