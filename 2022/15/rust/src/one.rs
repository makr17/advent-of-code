use std::cmp::min;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::{HashMap, HashSet};

use regex::Regex;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader, 2000000);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone,Copy,Debug,Hash,PartialEq,Eq)]
struct Point (i32, i32);
impl Point {
    fn distance(&self, other: &Point) -> u32 {
	((self.0 - other.0).abs() + (self.1 - other.1).abs()) as u32
    }
}

fn process(input: BufReader<File>, rownum: i32) -> std::result::Result<usize, Box::<dyn Error>> {
    let mut sensors: HashMap<Point, Point> = HashMap::new();
    let re = Regex::new(r"Sensor at x=([\d-]+), y=([\d-]+): closest beacon is at x=([\d-]+), y=([\d-]+)").unwrap();
    for l in input.lines() {
	let line = l.unwrap();
	let caps = re.captures(&line).unwrap();
	let sensor = Point(
	    caps[1].parse::<i32>().unwrap(),
	    caps[2].parse::<i32>().unwrap()
	);
	let beacon = Point(
	    caps[3].parse::<i32>().unwrap(),
	    caps[4].parse::<i32>().unwrap()
	);
	sensors.insert(sensor, beacon);
    }

    // calculate range of x
    let mut xs: Vec<i32> = sensors.iter()
	.map(|(s, b)| min(s.0, b.0))
	.collect();
    xs.sort();
    let min_x = xs.first().unwrap();
    let max_x = xs.last().unwrap();
    // build a hashset of our beacons for a quick check later
    // position occupied by a known beacon clearly can't be the one we're after
    let beacons: HashSet<Point> = sensors.iter()
	.map(|(_s, b)| b)
	.copied()
	.collect();
    let mut count = 0;
    let scale = max_x - min_x;
    for x in *min_x-scale..=*max_x+scale {
	let p = Point(x, rownum);
	for (s, b) in sensors.iter() {
	    if beacons.contains(&p) {
		// position already holds a beacon
		continue;
	    }
	    let d = s.distance(&p);
	    let b_d = s.distance(b);
	    if d <= b_d {
		// within sensor range for sensor s
		count += 1;
		// no need to keep looking, this disqualifies the position
		break;
	    }
	}
    }

    Ok(count)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader, 10).unwrap(), 26);
    Ok(())
}

