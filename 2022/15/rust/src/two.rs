use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashMap;

use regex::Regex;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader, 4000000);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone,Copy,Debug,Hash,PartialEq,Eq)]
struct Point (i64, i64);
impl Point {
    fn distance(&self, other: &Point) -> i64 {
	(self.0 - other.0).abs() + (self.1 - other.1).abs()
    }
}

fn process(input: BufReader<File>, max_xy: i64) -> std::result::Result<i64, Box::<dyn Error>> {
    let mut sensors: HashMap<Point, Point> = HashMap::new();
    let re = Regex::new(r"Sensor at x=([\d-]+), y=([\d-]+): closest beacon is at x=([\d-]+), y=([\d-]+)").unwrap();
    for l in input.lines() {
	let line = l.unwrap();
	let caps = re.captures(&line).unwrap();
	let sensor = Point(
	    caps[1].parse::<i64>().unwrap(),
	    caps[2].parse::<i64>().unwrap()
	);
	let beacon = Point(
	    caps[3].parse::<i64>().unwrap(),
	    caps[4].parse::<i64>().unwrap()
	);
	sensors.insert(sensor, beacon);
    }

    // map from sensor point to beacon range
    let ranges: HashMap<Point, i64> = sensors.iter()
	.map(|(s,b)| (*s, s.distance(b)))
	.collect();
    //println!("{:?}", ranges);
    let mut freq = 0;
    // _unique_ solution means it must be adjacent to one or more sensor regions
    // for each sensor, build bounding manhattan-distance diamond around it for d
    // trace/test positions just outside that bounding region
    for (s, r) in ranges.iter() {
	let mut test = *s;
	test.0 -= r+1; // start left corner of diamond
	// walk up and to the right
	while freq == 0 && test.1 <= s.1 + r + 1 {
	    if test_point(&test, s, &ranges, max_xy) {
		// point is clear of all sensors
		freq = test.0 * 4000000 + test.1;
	    }
	    test.0 -= 1;
	    test.1 += 1;
	}
	// now down and to the right
	while freq == 0 && test.0 <= s.0 + r + 1 {
	    if test_point(&test, s, &ranges, max_xy) {
		// point is clear of all sensors
		freq = test.0 * 4000000 + test.1;
	    }
	    test.0 += 1;
	    test.1 -= 1;
	}
	// now down and to the left
	while freq == 0 && test.1 >= s.1 - r - 1 {
	    if test_point(&test, s, &ranges, max_xy) {
		// point is clear of all sensors
		freq = test.0 * 4000000 + test.1;
	    }
	    test.0 -= 1;
	    test.1 -= 1;
	}
	// finally up and to the left
	while freq == 0 && test.0 >= s.0 - r - 1 {
	    if test_point(&test, s, &ranges, max_xy) {
		// point is clear of all sensors
		freq = test.0 * 4000000 + test.1;
	    }
	    test.0 -= 1;
	    test.1 += 1;
	}
	if freq > 0 {
	    break;
	}
    }

    Ok(freq)
}

fn test_point (test: &Point, sensor: &Point, ranges: &HashMap<Point, i64>, max_xy: i64) -> bool {
    // short-circuit if the test is out-of-bounds
    if test.0 < 0 || test.1 < 0 || test.0 > max_xy || test.1 > max_xy {
	return false;
    }
    let mut success = true;
    for (point, d) in ranges.iter() {
	if point == sensor {
	    // skip current sensor
	    continue;
	}
	if test.distance(point) <= *d {
	    success = false;
	    break;
	}
    }
    if success {
	println!("{:?}", test);
    }
    success
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader, 20).unwrap(), 56000011);
    Ok(())
}

