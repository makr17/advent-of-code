# 2022 AoC

Only in rust this year, mainly due to time constraints.
Plus I'm writing a _lot_ more python for work, don't really need the practice.

I'm likely doing a lot more with `unwrap()` than
I would in a real-world application.  Having _some_ control over the input
(or at least over _variability_ of input) helps in cutting corners on the
way to an answer.

I only got 36 stars this year:
* missed 16
* missed second half of 17
* missed 19
* missed second half of 21
* missed 22-25
