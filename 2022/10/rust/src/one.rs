use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<i64, Box::<dyn Error>> {
    let mut check: Vec<i64> = vec![220, 180, 140, 100, 60, 20];
    let mut cycles: i64 = 0;
    let mut sum: i64 = 0;
    let mut xreg: i64 = 1;

    for l in input.lines() {
	let line = l.unwrap();
	//println!("{}: {}: {}", cycles, xreg, line);
	let pieces: Vec<&str> = line.split(' ').collect();
	cycles += match pieces[0] {
	    "noop" => 1,
	    "addx" => 2,
	    _ => 0,
	};
	if check.len() > 0 && cycles >= check.last().unwrap().to_owned() {
	    let point = check.pop().unwrap();
	    //println!("### {}: {}  {} => {} ###", point, xreg, sum, point * xreg);
	    sum += point * xreg;
	}
	if pieces[0] == "addx" {
	    xreg += pieces[1].parse::<i64>().unwrap();
	}
    }
    
    Ok(sum)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 13140);
    Ok(())
}
