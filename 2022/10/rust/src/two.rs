use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader, Write};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<i64, Box::<dyn Error>> {
    let mut cycles: i64 = 0;
    let mut xreg: i64 = 1;

    let mut rows: Vec<String> = vec![];
    let mut row: Vec<char> = vec![];
    sprite_position(xreg);
    for l in input.lines() {
	let line = l.unwrap();
	cycles += 1;

	println!("Start cycle {}: begin executing {}", cycles, line);
	let pixel = (cycles % 40) - 1;
	println!("During cycle {}: CRT draws pixel in position {}", cycles, pixel);
	if xreg - 1 <= pixel % 40 && xreg + 1 >= pixel {
	    row.push('#');
	}
	else {
	    row.push('.');
	}
	let cur: String = row.iter().collect();
	println!("Current CRT row: {}", cur);
	println!();

	if cycles % 40 == 0 {
	    let row_string: String = row.iter().collect();
	    rows.push(row_string);
	    row = vec![];
	}
	
	let pieces: Vec<&str> = line.split(' ').collect();
	if pieces[0] == "addx" {
	    cycles += 1;
	    let pixel = (cycles % 40) - 1;
	    println!("During cycle {}: CRT draws pixel in position {}", cycles, pixel);
	    if xreg - 1 <= pixel % 40 && xreg + 1 >= pixel {
		row.push('#');
	    }
	    else {
		row.push('.');
	    }
	    let cur: String = row.iter().collect();
	    println!("Current CRT row: {}", cur);
	    
	    xreg += pieces[1].parse::<i64>().unwrap();
	    println!("End of cycle {}: finish executing {} (Register X is now {})", cycles, line, xreg);
	    sprite_position(xreg);

	    if cycles % 40 == 0 {
		let row_string: String = row.iter().collect();
		rows.push(row_string);
		row = vec![];
	    }
	}

	println!();
    }

    for r in rows {
	println!("{}", r);
    }

    Ok(0)
}

fn sprite_position(xreg: i64) {
    print!("Sprite position: ");
    for _idx in 0..xreg-1 {
	print!(".");
    }
    print!("###");
    for _idx in xreg+2..40 {
	print!(".");
    }
    print!("\n");
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 1);
    Ok(())
}
