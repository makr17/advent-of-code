use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader, Write};

use std::collections::HashSet;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone,Copy,Debug,Eq,PartialEq,Hash)]
struct Point {
    x: i32,
    y: i32,
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    let mut positions: HashSet<Point> = HashSet::new();
    // starting positions
    // (0,0) is bottom-left
    let mut head = Point { x: 0, y: 0 };
    let mut tail = head.clone();
    positions.insert(tail);

    for l in input.lines() {
	let line = l.unwrap();
	//println!("== {} ==", line);
	let parts: Vec<&str> = line.split(' ').collect();
	let dir = parts[0];
	let num = parts[1].parse::<i32>().unwrap();
	for _idx in 0..num {
	    // move the head
	    match dir {
		"U" => head.y += 1,
		"D" => head.y -= 1,
		"L" => head.x -= 1,
		"R" => head.x += 1,
		_ => println!("don't know how to: {}", dir),
	    };
	    // compare to the tail
	    // and possibly move the tail
	    if head.x == tail.x && (head.y - tail.y).abs() > 1 {
		// same row, move up/down to follow
		tail.y += (head.y - tail.y)/(head.y - tail.y).abs();
	    }
	    else if head.y == tail.y && (head.x - tail.x).abs() > 1 {
		// some column, move left/right to follow
		tail.x += (head.x - tail.x)/(head.x - tail.x).abs();
	    }
	    else if (head.x - tail.x).abs() > 1 || (head.y - tail.y).abs() > 1 {
		// move diagonally
		tail.x += (head.x - tail.x)/(head.x - tail.x).abs();
		tail.y += (head.y - tail.y)/(head.y - tail.y).abs();
	    }
	    
	    // log tail position
	    positions.insert(tail);

	    //visualize(head, tail);
	}
    }
    
    Ok(positions.len())
}

#[allow(dead_code)]
fn visualize(head: Point, tail: Point) {
    let mut xs: Vec<i32> = vec![head.x, tail.x, 5];
    let mut ys: Vec<i32> = vec![head.y, tail.y, 5];
    xs.sort();
    ys.sort();
    let max_x = xs.last().unwrap();
    let max_y = ys.last().unwrap();
    println!("{:?}, {:?}", head, tail);
    for y in (0..=*max_y).rev() {
	for x in 0..=*max_x {
	    if x == head.x && y == head.y {
		print!("H");
	    }
	    else if x == tail.x && y == tail.y {
		print!("T");
	    }
	    else {
		print!(".");
	    }
	}
	print!("\n");
    }
    print!("\n");
    io::stdout().flush().unwrap();
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 13);
    Ok(())
}
