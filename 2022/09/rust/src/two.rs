use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader, Write};

use std::collections::HashSet;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone,Copy,Debug,Eq,PartialEq,Hash)]
struct Point {
    x: i32,
    y: i32,
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    let mut positions: HashSet<Point> = HashSet::new();
    // starting positions
    // (0,0) is bottom-left
    let mut rope: Vec<Point> = vec![Point { x: 0, y: 0 }; 10];
    positions.insert(rope.last().unwrap().clone());

    for l in input.lines() {
	let line = l.unwrap();
	//println!("== {} ==", line);
	let parts: Vec<&str> = line.split(' ').collect();
	let dir = parts[0];
	let num = parts[1].parse::<i32>().unwrap();
	for _idx in 0..num {
	    // move the head
	    match dir {
		"U" => rope[0].y += 1,
		"D" => rope[0].y -= 1,
		"L" => rope[0].x -= 1,
		"R" => rope[0].x += 1,
		_ => println!("don't know how to: {}", dir),
	    };
	    // now work our way down the rope
	    for idx in 1..10 {
		// compare to the tail
		// and possibly move the rope[idx]
		if rope[idx-1].x == rope[idx].x && (rope[idx-1].y - rope[idx].y).abs() > 1 {
		    // same row, move up/down to follow
		    rope[idx].y += (rope[idx-1].y - rope[idx].y)/(rope[idx-1].y - rope[idx].y).abs();
		}
		else if rope[idx-1].y == rope[idx].y && (rope[idx-1].x - rope[idx].x).abs() > 1 {
		    // some column, move left/right to follow
		    rope[idx].x += (rope[idx-1].x - rope[idx].x)/(rope[idx-1].x - rope[idx].x).abs();
		}
		else if (rope[idx-1].x - rope[idx].x).abs() > 1 || (rope[idx-1].y - rope[idx].y).abs() > 1 {
		    // move diagonally
		    rope[idx].x += (rope[idx-1].x - rope[idx].x)/(rope[idx-1].x - rope[idx].x).abs();
		    rope[idx].y += (rope[idx-1].y - rope[idx].y)/(rope[idx-1].y - rope[idx].y).abs();
		}
	    }

	    // log tail position
	    positions.insert(rope.last().unwrap().clone());

	    //visualize(rope.clone());
	}
    }
    
    Ok(positions.len())
}

#[allow(dead_code)]
fn visualize(rope: Vec<Point>) {
    let mut xs: Vec<i32> = rope.iter().copied().map(|p| p.x).collect();
    xs.push(5);
    let mut ys: Vec<i32> = rope.iter().copied().map(|p| p.x).collect();
    ys.push(5);
    xs.sort();
    ys.sort();
    let max_x = xs.last().unwrap();
    let max_y = ys.last().unwrap();
    for y in (0..=*max_y).rev() {
	for x in 0..=*max_x {
	    let mut taken = false;
	    for idx in 0..rope.len() {
		if x == rope[idx].x && y == rope[idx].y {
		    print!("{}", idx);
		    taken = true;
		    break;
		}
	    }
	    if !taken {
		print!(".");
	    }
	}
	print!("\n");
    }
    print!("\n");
    io::stdout().flush().unwrap();
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test2.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 36);
    Ok(())
}
