use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

static DEBUG: bool = false;
fn dbg(out: String) {
    if DEBUG {
	println!("{}", out);
    }
}

static KEY: i64 = 811589153;

fn process(input: BufReader<File>) -> Result<i64, Box::<dyn Error>> {
    // (idx, n): original index position for process order, and the value
    let mut ring: Vec<(usize,i64)> = input.lines()
	.map(|l| l.unwrap().parse::<i64>().unwrap() * KEY)
	.enumerate()
	.collect();

    let len = ring.len();
    let ilen = len as i64;
    for _ in 0..10 {
	for opos in 0..len {
	    let m: Vec<(usize, &(usize, i64))> = ring.iter()
		.enumerate()
		.filter(|(_,(idx, _))| idx == &opos)
		.collect();
	    let (i, (_, n)) = m.first().unwrap();
	    let idx = (n.rem_euclid(ilen-1) + *i as i64).rem_euclid(ilen-1);

	    dbg(format!("  moving {} from {} to {}", n, i, idx));

	    let swap = ring.remove(*i);
	    ring.insert(idx as usize, swap);
	
	    let debug: Vec<i64> = ring.iter().map(|(_,n)| *n).collect();
	    dbg(format!("  {:?}", debug));
	    dbg(format!(""));
	}
    }
    // find zero
    let zero: Vec<(usize, &(usize, i64))> = ring.iter()
	.enumerate()
	.filter(|(_,(_, n))| n == &0)
	.collect();
    let (idx, _) = zero.first().unwrap();
    dbg(format!("0 idx == {}", idx));
    // then take our values from the ring, indexed from zero
    for n in [1000, 2000, 3000] {
	dbg(format!("{}: {}: {}", idx + n, (idx +n)%len, ring[(idx+n)%len].1));
    }
    Ok(ring[(idx+1000)%len].1 + ring[(idx+2000)%len].1 + ring[(idx+3000)%len].1)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 1623178306);
    Ok(())
}
