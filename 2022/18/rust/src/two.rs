use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashSet;

use pathfinding::prelude::dijkstra;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
struct Point {
    x: i32,
    y: i32,
    z: i32
}
impl Point {
    fn new(components: &Vec<i32>) -> Self {
	Point { x: components[0], y: components[1], z: components[2], }
    }

    fn adjacents(&self) -> Vec<Point> {
	vec![
	    Point { x: self.x - 1, y: self.y, z: self.z },
	    Point { x: self.x + 1, y: self.y, z: self.z },
	    Point { x: self.x, y: self.y - 1, z: self.z },
	    Point { x: self.x, y: self.y + 1, z: self.z },
	    Point { x: self.x, y: self.y, z: self.z - 1 },
	    Point { x: self.x, y: self.y, z: self.z + 1 }
	]
    }

    fn empty_adjacents(&self, points: &HashSet<Point>) -> Vec<Point> {
	let empty: Vec<Point> = self.adjacents().iter()
	    .filter(|p| !points.contains(p))
	    .copied()
	    .collect();
	empty
    }

    fn weighted_empty_adjacents(&self, points: &HashSet<Point>) -> Vec<(Point, u32)> {
	let empty = self.empty_adjacents(points);
	let weighted = empty.into_iter()
	    .map(|p| (p, 1))
	    .collect();
	weighted
    }
    
    fn adjacent_count(&self, points: &HashSet<Point>) -> usize {
	self.adjacents().iter()
	    .map(|p| match points.contains(p) {
		true => 1,
		false => 0,
	    })
	    .sum()
    }

    fn exterior_surface(points: &HashSet<Point>) -> usize {
	let mut area = 0;
	let corner = Point::new(&vec![0,0,0]);
	for point in points.iter() {
	    area += 6;
	    area -= point.adjacent_count(&points);
	    // test empty neighbors to see if a path is possible to the corner
	    // if not, then the point is interior to the solid
	    for test in point.empty_adjacents(&points).iter() {
		match dijkstra(
		    test,
		    |p| p.weighted_empty_adjacents(&points),
		    |p| *p == corner
		) {
		    Some((_p, _l)) => continue,  // exterior
		    None => area -= 1,  // interior, no path out
		};
	    }
	}
	area
    }
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    // build the volume
    let mut solid: HashSet<Point> = HashSet::new();
    for l in input.lines() {
	let line = l.unwrap();
	let components = line.split(',')
	    .map(|d| d.parse::<i32>().unwrap())
	    .collect();
	solid.insert(Point::new(&components));
    }
    Ok(Point::exterior_surface(&solid))
}



#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 58);
    Ok(())
}
