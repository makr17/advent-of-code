use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
struct Point {
    x: i32,
    y: i32,
    z: i32
}
impl Point {
    fn new(components: &Vec<i32>) -> Self {
	Point { x: components[0], y: components[1], z: components[2], }
    }
    fn adjacent(&self, other: &Point) -> bool {
	let mut same = 0;
	let mut next = 0;
	if self.x == other.x {
	    same += 1;
	}
	else if (self.x - other.x).abs() == 1 {
	    next += 1;
	}
	if self.y == other.y {
	    same += 1;
	}
	else if (self.y - other.y).abs() == 1 {
	    next += 1;
	}
	if self.z == other.z {
	    same += 1;
	}
	else if (self.z - other.z).abs() == 1 {
	    next += 1;
	}

	if same == 2 && next == 1 {
	    return true;
	}
	false
    }
}
#[test]
fn test_adjacent() -> () {
    let p1 = Point::new(&vec![1,1,1]);
    let p2 = Point::new(&vec![2,1,1]);
    assert_eq!(p1.adjacent(&p2), true);
    let p3 = Point::new(&vec![3,1,1]);
    assert_eq!(p1.adjacent(&p3), false);
}

fn process(input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut points: Vec<Point> = vec![];
    for l in input.lines() {
	let line = l.unwrap();
	let components = line.split(',')
	    .map(|d| d.parse::<i32>().unwrap())
	    .collect();
	points.push(Point::new(&components));
    }
    
    let mut sides = 0;

    for p in points.iter() {
	//println!("{:?}", p);
	sides += 6;
	for p2 in points.iter().filter(|t| *t != p) {
	    //println!("  {:?}, {}", p2, p.adjacent(&p2));
	    if p.adjacent(&p2) {
		sides -= 1;
	    }
	}
    }

    Ok(sides)
}



#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 64);
    Ok(())
}
