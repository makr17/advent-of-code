use std::error::Error;
use std::io;

fn main() -> io::Result<()> {
    let mut monkeys: Vec<Monkey> = vec![];
    monkeys.push( // 0
	Monkey {
	    items: vec![96, 60, 68, 91, 83, 57, 85],
	    operation: |w: u64| -> u64 { w * 2 },
	    test: |w: u64| -> bool { w % 17 == 0 },
	    t: 2,
	    f: 5,
	    inspections: 0,
	}
    );
    monkeys.push( // 1
	Monkey {
	    items: vec![75, 78, 68, 81, 73, 99],
	    operation: |w: u64| -> u64 { w + 3 },
	    test: |w: u64| -> bool { w % 13 == 0 },
	    t: 7,
	    f: 4,
	    inspections: 0,
	}
    );
    monkeys.push( // 2
	Monkey {
	    items: vec![69, 86, 67, 55, 96, 69, 94, 85],
	    operation: |w: u64| -> u64 { w + 6 },
	    test: |w: u64| -> bool { w % 19 == 0 },
	    t: 6,
	    f: 5,
	    inspections: 0,
	}
    );
    monkeys.push( // 3
	Monkey {
	    items: vec![88, 75, 74, 98, 80],
	    operation: |w: u64| -> u64 { w + 5 },
	    test: |w: u64| -> bool { w % 7 == 0 },
	    t: 7,
	    f: 1,
	    inspections: 0,
	}
    );
    monkeys.push( // 4
	Monkey {
	    items: vec![82],
	    operation: |w: u64| -> u64 { w + 8 },
	    test: |w: u64| -> bool { w % 11 == 0 },
	    t: 0,
	    f: 2,
	    inspections: 0,
	}
    );
    monkeys.push( // 5
	Monkey {
	    items: vec![72, 92, 92],
	    operation: |w: u64| -> u64 { w * 5 },
	    test: |w: u64| -> bool { w % 3 == 0 },
	    t: 6,
	    f: 3,
	    inspections: 0,
	}
    );
    monkeys.push( // 6
	Monkey {
	    items: vec![74, 61],
	    operation: |w: u64| -> u64 { w * w },
	    test: |w: u64| -> bool { w % 2 == 0 },
	    t: 3,
	    f: 1,
	    inspections: 0,
	}
    );
    monkeys.push( // 7
	Monkey {
	    items: vec![76, 86, 83, 55],
	    operation: |w: u64| -> u64 { w + 4 },
	    test: |w: u64| -> bool { w % 5 == 0 },
	    t: 4,
	    f: 0,
	    inspections: 0,
	}
    );

    let val = process(monkeys);
    println!("{}", val.unwrap());
    Ok(())
}

#[derive(Debug)]
struct Monkey {
    items: Vec<u64>,
    operation: fn(u64) -> u64,
    test: fn(u64) -> bool,
    t: usize,
    f: usize,
    inspections: u64,
}

fn process(mut monkeys: Vec<Monkey>) -> Result<u64, Box::<dyn Error>> {
    let mut monkeys_items: Vec<Vec<u64>> = monkeys.iter()
	.map(|m| m.items.clone())
	.collect();
    for _round in 0..20 {
	for idx in 0..monkeys.len() {
	    let mut monkey = &mut monkeys[idx];
    	    let items = monkeys_items[idx].clone();
    	    monkeys_items[idx].clear();
    	    for item in items.into_iter() {
		//println!("  Monkey inspects an item with with a worry level of {}", item);
    		let mut new_item = (monkey.operation)(item);
		//println!("  New worry level is {}", new_item);
		new_item = new_item / 3; 
		//println!("  Monkey gets bored with item. Worry level is divided by 3 to {}", new_item);
		let throw = match (monkey.test)(new_item) {
		    true => monkey.t,
		    false => monkey.f,
		};
		//println!("  Test is {}", (monkey.test)(new_item));
		//println!("  Item with worry level {} is throw to monkey {}", new_item, throw);
		monkeys_items[throw].push(new_item);
    		monkey.inspections += 1;
    	    }
        }
    }

    monkeys.sort_unstable_by_key(|m| m.inspections);

    let m1 = monkeys.pop().unwrap();
    let m2 = monkeys.pop().unwrap();

    Ok(m1.inspections * m2.inspections)
}

#[test]
fn test_input() -> io::Result<()> {
    let mut monkeys: Vec<Monkey> = vec![];
    monkeys.push(
	Monkey {
	    items: vec![79, 98],
	    operation: |w: u64| -> u64 { w * 19 },
	    test: |w: u64| -> bool { w % 23 == 0 },
	    t: 2,
	    f: 3,
	    inspections: 0,
	}
    );
    monkeys.push(
	Monkey {
	    items: vec![54, 65, 75, 74],
	    operation: |w: u64| -> u64 { w + 6 },
	    test: |w: u64| -> bool { w % 19 == 0 },
	    t: 2,
	    f: 0,
	    inspections: 0,
	}
    );
    monkeys.push(
	Monkey {
	    items: vec![79, 60, 97],
	    operation: |w: u64| -> u64 { w * w },
	    test: |w: u64| -> bool { w % 13 == 0 },
	    t: 1,
	    f: 3,
	    inspections: 0,
	}
    );
    monkeys.push(
	Monkey {
	    items: vec![74],
	    operation: |w: u64| -> u64 { w + 3 },
	    test: |w: u64| -> bool { w % 17 == 0 },
	    t: 0,
	    f: 1,
	    inspections: 0,
	}
    );

    assert_eq!(process(monkeys).unwrap(), 10605);
    Ok(())
}
