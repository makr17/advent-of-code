use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use pathfinding::prelude::dijkstra;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
// (x, y)
struct Pos(usize, usize);

impl Pos {
    /*
    fn distance(&self, other: &Pos) -> u32 {
	(self.0.abs_diff(other.0) + self.1.abs_diff(other.1)) as u32
    }
     */

    fn successors(&self, elev: &Vec<Vec<i64>>) -> Vec<(Pos, u32)> {
	let &Pos(x, y) = self;
	let mut succ: Vec<(Pos, u32)> = vec![];
	// x,y as i64 so we can safely subtract and abs()
	let ix = x as i64;
	let iy = y as i64;
	for tx in [ix-1, ix+1] {
	    if tx >= 0 && tx < elev[y].len() as i64 {
		let cx = tx as usize;
		if elev[y][cx] - elev[y][x] == 1 || elev[y][x] - elev[y][cx] >= 0 {
		    succ.push( ( Pos(cx, y), 1 ) );
		}
	    }
	}
	for ty in [iy-1, iy+1] {
	    if ty >= 0 && ty < elev.len() as i64 {
		let cy = ty as usize;
		if elev[cy][x] - elev[y][x] == 1 || elev[y][x] - elev[cy][x] >= 0 {
		    succ.push( ( Pos(x, cy), 1 ) );
		}
	    }
	}
	
	//println!("{:?}: {:?}", self, succ);
	succ
    }
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    // build the elevation grid from input
    let mut goal = Pos(0, 0);
    let elev: Vec<Vec<i64>> = input.lines()
	.map(|l| l.unwrap())
	.enumerate()
	.map(|(by, l)| l.chars()
	     .enumerate()
	     .map(|(bx, c)|
		  match c {
		      'S' => {
			  'a' as i64
		      },
		      'E' => {
			  goal.0 = bx;
			  goal.1 = by;
			  'z' as i64
		      },
		      _ => c as i64,		  }
	     )
	     .collect()
	)
	.collect();
    
    //println!("pathing from {:?} to {:?}", start, goal);

    // I _think_ could probably do something with starting at E
    // goal condition being elev[y][x] = 'a' as i64 or similar
    // but this runs quickly enough
    // don't need to think about inverting neighbor criteria
    let mut shortest: u32 = 100_000_000;
    for y in 0..elev.len() {
	for x in 0..elev[y].len() {
	    if elev[y][x] == 'a' as i64 {
		let p = Pos(x,y);
		let result = dijkstra(
		    &p,
		    |p| p.successors(&elev),
		    |p| *p == goal
		);
		// handle the case where there is no path from chosen 'a'
		let len = match result {
		    Some((_p,l)) => l,
		    None => 100_000_000,
		};
		if len < shortest {
		    shortest = len;
		}
	    }
	}
    }
    
    Ok(shortest)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 29);
    Ok(())
}

