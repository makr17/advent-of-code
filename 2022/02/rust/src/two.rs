use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use phf::phf_map;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

static POINTS: phf::Map<&str, u32> = phf_map! {
    "A" => 1,
    "B" => 2,
    "C" => 3,
};

const PLAYS: phf::Map<&str, &str> = phf_map! {
    "A X" => "C",
    "A Y" => "A",
    "A Z" => "B",
    "B X" => "A",
    "B Y" => "B",
    "B Z" => "C",
    "C X" => "B",
    "C Y" => "C",
    "C Z" => "A",
};

static ROUNDS: phf::Map<&str, u32> = phf_map! {
    "X" => 0,
    "Y" => 3,
    "Z" => 6,
};

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let mut score = 0;
    for line in input.lines() {
	let s = line.unwrap();
	let round: Vec<&str> = s.split(' ').collect();
	//println!("{:?}", round);
	let play = PLAYS.get(s.as_str()).unwrap();
	score += POINTS.get(play).unwrap(); // points for the play
	score += ROUNDS.get(round[1]).unwrap(); // point result of round
    }
    Ok(score)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 12);
    Ok(())
}
