use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use phf::phf_map;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

static POINTS: phf::Map<&'static str, u32> = phf_map! {
    "X" => 1,
    "Y" => 2,
    "Z" => 3,
};

const BEATS: phf::Map<&str, u32> = phf_map! {
    "A X" => 3,
    "A Y" => 6,
    "A Z" => 0,
    "B X" => 0,
    "B Y" => 3,
    "B Z" => 6,
    "C X" => 6,
    "C Y" => 0,
    "C Z" => 3,
};

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let mut score = 0;
    for line in input.lines() {
	let s = line.unwrap();
	let plays: Vec<&str> = s.split(' ').collect();
	//println!("{:?}", plays);
	score += POINTS.get(plays[1]).unwrap(); // points for my play
	score += BEATS.get(s.as_str()).unwrap(); // points for the round
    }
    Ok(score)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 15);
    Ok(())
}
