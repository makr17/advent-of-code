# Advent of Code

My personal [Advent of Code](https://adventofcode.com/) solutions.

Bucketed with a directory per-year, and a directory per-day from there.
Generally I set things up with the test(s) and input at the day level,
with one or more language implementations below that.

I give myself a loose deadline of finishing by the end of December.
That gives me some time after the 25th, but not open-ended.
As such, most years (as yet, all but 2024) are incomplete.

2017 and 2018 were stored in a different (keybase/git) repo,
I pulled them into this repo when I rediscovered them.

I missed 2019 and 2020, though I don't remember why.  Travel probably.

