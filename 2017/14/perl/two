#!/usr/bin/env perl

use strict;
use warnings;

use constant SIZE => 256;

my $topkey = shift // 'ljoxqyyw';

my $used = 0;
my @rows = [];
for ( my $row = 0; $row < 128; $row++ ) {
  my $key = $topkey . '-' . $row;
  my $hash = hash($key);
  #warn length $hash, "\t", $hash, "\n";
  my $binary = unpack ('B*', pack ('H*', $hash));
  my @cols = split(//, $binary);
  push @rows, \@cols;
}

my %nodes;
my %edges;
for ( my $i = 0; $i < scalar @rows; $i++ ) {
  for ( my $j = 0; $j < scalar @{$rows[$i]}; $j++ ) {
    next unless $rows[$i][$j];
    $nodes{"$i:$j"} = 1;
    push @{$edges{"$i:$j"}}, ($i-1).':'.$j if $rows[$i-1][$j];
    push @{$edges{"$i:$j"}}, ($i+1).':'.$j if $rows[$i+1][$j];
    push @{$edges{"$i:$j"}}, $i.':'.($j-1) if $rows[$i][$j-1];
    push @{$edges{"$i:$j"}}, $i.':'.($j+1) if $rows[$i][$j+1];
  }
}

#warn scalar keys %nodes,"\n";
#warn scalar keys %edges,"\n";

no warnings 'recursion';
my @groups;
while ( my($start) = sort { rand } keys %nodes ) {
  my $group = walk(\%edges, $start);
  push @groups, $group;
  delete @nodes{keys %$group};
}

print scalar @groups, " partitions\n";

exit;

sub hash {
  my $input = shift;
  
  my @segments = map { ord $_ } split(//, $input);
  push @segments, (17, 31, 73, 47, 23);
  # 64 rounds of knot "hash"
  my $idx = 0;
  my $skip = 0;
  my @ring = (0 .. SIZE-1);
  for ( my $i = 0; $i < 64; $i++ ) {
    foreach my $len (@segments) {
      swap(\@ring, $idx, $len);
      $idx = ($idx + $len + $skip++) % SIZE;
    }
  }

  #then XOR chunks of 16 together
  my $hash;
  for ( my $i = 0; $i < SIZE; $i += 16 ) {
    my $val = $ring[$i];
    for ( my $j = 1; $j < 16; $j++ ) {
      $val ^= $ring[$i+$j];
    }
    $hash .= sprintf("%X", $val);
  }

  return lc $hash;
}

sub swap {
  my($ring, $idx, $len) = @_;
  for (my $i = 0; $i < $len/2; $i++) {
    my($a, $b) = (($idx + $i) % SIZE, ($idx + $len - $i - 1) % SIZE);
    @$ring[$a, $b] = @$ring[$b, $a];
  }
}

sub walk {
  my($edges, $start, $seen) = @_;
  $seen //= {};
  $seen->{$start} = 1;
  foreach my $node ( @{$edges{$start}} ) {
    next if $seen->{$node};
    walk($edges, $node, $seen);
  }
  return $seen;
}
