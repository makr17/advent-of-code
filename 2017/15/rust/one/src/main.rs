struct Generator {
    current: u64,
    factor:  u64
}
impl Generator {
    fn next (&mut self) -> u16 {
        let next = ( self.current * self.factor ) % 2147483647;
        self.current = next;
        let ret = ( next & 0xffff ) as u16;
        return ret;
    }
}

fn main() {
    let mut gen_a = Generator { current: 634, factor: 16807 };
    let mut gen_b = Generator { current: 301, factor: 48271 };
    let mut matches = 0;
    for _i in 0 .. 40_000_000 {
        if gen_a.next() == gen_b.next() { matches += 1 }
    }
    println!("{}", matches);
}
