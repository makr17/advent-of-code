use std::io;
use std::io::prelude::*;

extern crate regex;
use regex::Regex;

fn main() {
    let mut line = vec!["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p"];

    let stdin = io::stdin();
    let mut buffer = String::new();
    let mut handle = stdin.lock();
    handle.read_to_string(&mut buffer);
    let moves: Vec<&str> = buffer.trim_right().split(",").collect();
    let re = Regex::new(r"^(\w)(\w+)(?:/(\w+))?$").unwrap();
    for m in moves {
        let cap: Vec<&str> = re.captures_iter(m).collect();
        line = match cap[1] {
            's' => spin(line, cap[2]),
            'x' => exchange(line, cap[2], cap[3]),
            'p' => partner(line, cap[2], cap[3]),
            _   => line
        }
    }
    println!("{:?}", line);
}

fn spin (mut line: Vec<&str>, count: u8) -> Vec<&str> {
    for i in 0..count {
        let foo = line.pop().unwrap();
        line.insert(0, foo);
    }
    return line;
}

fn exchange (mut line: Vec<&str>, a: usize, b: usize) -> Vec<&str> {
    line.swap(a, b);
    return line;
}

fn partner (mut line: Vec<&str>, a: &str, b: &str) -> Vec<&str> {
    let ia = find(&line, a);
    let ib = find(&line, b);
    line.swap(ia, ib);
    return line;
}

fn find(line: &Vec<&str>, a: &str) -> usize {
    for i in 0 .. line.len() {
        if line[i] == a { return i }
    }
    return 0;
}