use std::io;
use std::io::prelude::*;
use std::collections::HashMap;

struct Line<'a> {
    vector: Vec<&'a str>
}
impl<'a> Line<'a> {
    fn reg(&self) -> &str {
        return self.vector[0];
    }
    fn op(&self) -> &str {
        return self.vector[1];
    }
    fn value(&self) -> i32 {
        return self.vector[2].parse::<i32>().unwrap();
    }
    fn treg(&self) -> &str {
        return self.vector[4];
    }
    fn top(&self) -> &str {
        return self.vector[5];
    }
    fn tval(&self) -> i32 {
        return self.vector[6].parse::<i32>().unwrap();
    }
}

fn main () {
    let stdin = io::stdin();
    let mut regs: HashMap<&str, i32> = HashMap::new();
    for line in stdin.lock().lines() {
        let text = line.unwrap().to_string();
        let ops: Line = Line { vector: text.split_whitespace().collect() };
        // initialize the registers if we haven't seen them yet
        let reg = ops.reg().to_string();
        if !regs.contains_key(&reg) {
            regs.insert(&reg, 0);
        }
        let treg = ops.treg().to_string();
        if !regs.contains_key(&treg) {
            regs.insert(&treg, 0);
        }
        let tval = *regs.get(&treg).unwrap();
        let test: bool = match ops.top() {
            ">"  => { tval >  ops.tval() },
            ">=" => { tval >= ops.tval() },
            "<"  => { tval <  ops.tval() },
            "<=" => { tval <= ops.tval() },
            "==" => { tval == ops.tval() },
            "!=" => { tval != ops.tval() },
            op   => { println!("unknown op {}", op); false }
        };
        if test {
            let val = regs.get_mut(&reg).unwrap();
            match ops.op() {
                "inc" => {
                    *val = *val + ops.value()
                },
                "dec" => {
                    *val = *val - ops.value()
                },
                op => { println!("unknown op {}", op); }
            }
        }
    }
    let mut values: Vec<&i32> = regs.values().collect();
    values.sort();
    let biggest = values.last().unwrap();
    println!("{}", biggest);

}
