use std::io;
use std::io::prelude::*;
use std::str;

fn main() {
    let stdin = io::stdin();
    let mut sum: u32 = 0;
    for line in stdin.lock().lines() {
        sum += process(line.unwrap());
    }
    println!("{}", sum);
}

#[test]
fn it_works() {
    assert_eq!(process("5 9 2 8".to_string()), 4);
    assert_eq!(process("9 4 7 3".to_string()), 3);
    assert_eq!(process("3 8 6 5".to_string()), 2);
}

fn process(line: String) -> u32 {
    let v: Vec<_> = line.split_whitespace().collect();
    // convert vec of strings to vec of u32
    let mut nums: Vec<u32> = vec![];
    let size = v.len();
    for i in 0 .. size {
        let digit = v[i].parse::<u32>().unwrap();
        nums.push(digit);
    }
    // sort and reverse the vec
    nums.sort();
    // not sure why this ends up as ref, but going with it...
    let sorted: Vec<&u32> = nums.iter().rev().collect();
    //println!("{:?}", sorted);
    for i in 0 .. size {
        for j in i + 1 .. size {
            //println!("{} / {} = {}", sorted[i], sorted[j], sorted[i]/sorted[j]);
            if sorted[i] % sorted[j] == 0 {
                return sorted[i] / sorted[j];
            }
        }
    }
    return 0;
}