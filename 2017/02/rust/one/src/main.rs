use std::io;
use std::io::prelude::*;
use std::str;

fn main() {
    let stdin = io::stdin();
    let mut sum: u32 = 0;
    for line in stdin.lock().lines() {
        sum += process(line.unwrap());
    }
    println!("{}", sum);
}

#[test]
fn it_works() {
    assert_eq!(process("5 1 9 5".to_string()), 8);
    assert_eq!(process("7 5 3".to_string()), 4);
    assert_eq!(process("2 4 6 8".to_string()), 6);
}

fn process(line: String) -> u32 {
    let v: Vec<_> = line.split_whitespace().collect();
    // convert vec of strings to vec of u32
    let mut nums: Vec<u32> = vec![];
    for i in 0 .. v.len() {
        let digit = v[i].parse::<u32>().unwrap();
        nums.push(digit);
    }
    nums.sort();
    //println!("{:?}", nums);
    let csum = nums.last().unwrap() - nums.first().unwrap();
    //println!("{}", csum);
    return csum;
}