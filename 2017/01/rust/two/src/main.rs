use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let sum = process(line.unwrap());
        println!("{}", sum);
    }
}

#[test]
fn it_works() {
    assert_eq!(process("1212".to_string()), 6);
    assert_eq!(process("1221".to_string()), 0);
    assert_eq!(process("123425".to_string()), 4);
    assert_eq!(process("123123".to_string()), 12);
    assert_eq!(process("12131415".to_string()), 4);
}

fn process(line: String) -> u32 {
    let v: Vec<_> = line.chars().collect();
    let mut sum: u32  = 0;
    let size = v.len();
    let offset = size/2;
    for i in 0..size {
        let curr = i;
        let next = (i + offset) % size;
        //println!("curr {} next {} first {} second {}",
        //         curr, next,
        //         v[curr].to_digit(10).unwrap(), v[next].to_digit(10).unwrap());
        if v[curr] == v[next] {
            sum += v[curr].to_digit(10).unwrap();
        }
    }
    
    return sum;
}
