use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let sum = process(line.unwrap());
        println!("{}", sum);
    }
}

#[test]
fn it_works() {
    assert_eq!(process("1122".to_string()), 3);
    assert_eq!(process("1111".to_string()), 4);
    assert_eq!(process("1234".to_string()), 0);
    assert_eq!(process("91212129".to_string()), 9);
}

fn process(line: String) -> u32 {
    let v: Vec<_> = line.chars().collect();
    let mut sum: u32  = 0;
    let size = v.len();
    for i in 0..size {
        let curr = i;
        let next = (i + 1) % size;
        //println!("curr {} next {} first {} second {}",
        //         curr, next,
        //         v[curr].to_digit(10).unwrap(), v[next].to_digit(10).unwrap());
        if v[curr] == v[next] {
            sum += v[curr].to_digit(10).unwrap();
        }
    }
    
    return sum;
}
