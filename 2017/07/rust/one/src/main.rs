use std::io;
use std::io::prelude::*;
use std::collections::HashMap;

extern crate regex;
use regex::Regex;

fn main() {
    let stdin = io::stdin();
    let mut progs = HashMap::new();
    let mut children = HashMap::new();
    let pw = Regex::new(r"^(\w+)\s+\((\d+)\)(?:\s->\s.(.*))?").unwrap();
    let comma = Regex::new(r",").unwrap();
    for line in stdin.lock().lines() {
        let text = line.unwrap();
        for cap in pw.captures_iter(&text) {
            progs.insert((&cap[1]).to_string(), (&cap[2]).to_string());
            println!("{:?}", cap);
            let kids: Vec<&str> = match &cap[3] {
                None => vec![],
                Some(tail) => {
                    comma.replace_all(tail, "");
                    tail.split_whitespace().collect()
                }
            };
        }
    }
    println!("{:?}", children);
    println!("{:?}", progs);
}
