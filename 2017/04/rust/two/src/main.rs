use std::io;
use std::io::prelude::*;
use std::collections::HashMap;

fn main() {
    let stdin = io::stdin();
    let mut sum: u32 = 0;
    for line in stdin.lock().lines() {
        sum += process(line.unwrap());
    }
    println!("{}", sum);
}

#[test]
fn it_works() {
    assert_eq!(process("abcde fghij".to_string()), 1);
    assert_eq!(process("abcde xyz ecdab".to_string()), 0);
    assert_eq!(process("a ab abc abd abf abj".to_string()), 1);
    assert_eq!(process("iiii oiii ooii oooi oooo".to_string()), 1);
    assert_eq!(process("oiii ioii iioi iiio".to_string()), 0);
}

fn process(line: String) -> u32 {
    let words: Vec<_> = line.split_whitespace().collect();
    let mut uniq = HashMap::new();
    let count = words.len();
    for word in words {
        let mut chars: Vec<_> = word.chars().collect();
        chars.sort();
        let s: String = chars.into_iter().collect();
        if !uniq.contains_key(&s) {
             uniq.insert(s, 1);
        }
    }
    if count == uniq.len() {
        return 1;
    }
    return 0;
}
