use std::io;
use std::io::prelude::*;
use std::collections::HashMap;

fn main() {
    let stdin = io::stdin();
    let mut sum: u32 = 0;
    for line in stdin.lock().lines() {
        sum += process(line.unwrap());
    }
    println!("{}", sum);
}

#[test]
fn it_works() {
    assert_eq!(process("aa bb cc dd ee".to_string()), 1);
    assert_eq!(process("aa bb cc dd aa".to_string()), 0);
    assert_eq!(process("aa bb cc dd aaa".to_string()), 1);
}

fn process(line: String) -> u32 {
    let words: Vec<_> = line.split_whitespace().collect();
    let mut uniq = HashMap::new();
    for word in &words {
        if !uniq.contains_key(word) {
            uniq.insert(word, 1);
        }
    }
    if words.len() == uniq.len() {
        return 1;
    }
    return 0;
}
