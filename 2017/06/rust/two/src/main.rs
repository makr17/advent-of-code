use std::io;
use std::io::prelude::*;
use std::collections::HashMap;

struct Ret {
    count: u32,
    blocks: Vec<u32>
}

fn main() {
    let stdin = io::stdin();
    let mut blocks: Vec<u32> = vec![];
    for line in stdin.lock().lines() {
        blocks = line.unwrap().split_whitespace().map(|x| x.parse::<u32>().unwrap()).collect();
        break;
    }
    let count = process(blocks);
    println!("{}", count);
}

#[test]
fn it_works() {
    let blocks = vec![0, 2, 7, 0];
    assert_eq!(process(blocks), 4);
}

fn process (blocks: Vec<u32>) -> u32 {
    let ret = shim(blocks);
    let sec = shim(ret.blocks);
    return sec.count;
}

fn shim(mut blocks: Vec<u32>) -> Ret {
    let mut seen = HashMap::new();
    let fp = fingerprint(blocks.to_vec());
    seen.insert(fp, 1);
    let mut count = 0;
    let size = blocks.len();
    loop {
        // find the biggest block
        let mut sorted = blocks.to_vec();
        sorted.sort();
        let biggest = sorted.last().unwrap();
        // find the index for that biggest value
        let mut idx = 0;
        for i in 0 .. size {
            if blocks[i] == *biggest { idx = i; break };
        }
        // zero it out
        let mut pool = blocks[idx];
        blocks[idx] = 0;
        // and redistribute the contents
        while pool > 0 {
            idx = (idx + 1) % size;
            blocks[idx] = blocks[idx] + 1;
            pool = pool - 1;
        }
        // fingerprint result
        count = count + 1;
        let fp = fingerprint(blocks.to_vec());
        //println!("{}", fp);
        //println!("{:?}", seen);
        if seen.contains_key(&fp) { break };
        seen.insert(fp, 1);
    }
    return Ret { count: count, blocks: blocks };
}

fn fingerprint(blocks: Vec<u32>) -> String {
    let stringy: Vec<String> = blocks.iter().map(|x| x.to_string()).collect();
    let fp: String = stringy.join(".");
    return fp;
}
