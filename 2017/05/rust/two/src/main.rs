use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();
    let mut ops: Vec<i32> = vec![];
    for line in stdin.lock().lines() {
        let op = line.unwrap().parse::<i32>().unwrap();
        ops.push(op);
    }
    let steps = process(ops);
    println!("{}", steps);
}

#[test]
fn it_works() {
    let ops = vec![0,3,0,1,-3];
    assert_eq!(process(ops), 10);
}

fn process(mut ops: Vec<i32>) -> u32 {
    let mut idx: i32 = 0;
    let mut count: u32 = 0;
    loop {
        let op = ops[idx as usize];
        if op >= 3 {
            ops[idx as usize] = op - 1;
        }
        else {
            ops[idx as usize] = op + 1;
        }
        idx += op;
        count = count + 1;
        if idx < 0 { break }
        if idx >= ops.len() as i32 { break }
    }
    return count;
}
