pub mod loader {
    use std::io::{self, BufRead};

    #[derive(Clone,Debug,Copy)]
    pub struct Letter {
        pub letter:  char,
        pub inverse: char
    }
    impl Letter {
        pub fn new(c: char) -> Letter {
            let mut inv: char = '0';
            // we're dealing with ascii, break should be safe
            if c.is_uppercase() {
                for i in c.to_lowercase() {
                    inv = i;
                    break;
                }
            }
            else {
                for i in c.to_uppercase() {
                    inv = i;
                    break;
                }
            }
            return Letter {
                letter:  c,
                inverse: inv,
            }
        }
    }
    
    pub fn load () -> Vec<Letter> {
        let stdin = io::stdin();
        let mut v: Vec<char> = Vec::new();
        // take string input and co
        for line in stdin.lock().lines() {
            let val = line.unwrap();
            let mut input: Vec<char> = val.chars().collect();
            v.append(&mut input);
        }
        let letters: Vec<Letter> = v.iter().map( |x| Letter::new(*x) ).collect();
        return letters;
    }
}
