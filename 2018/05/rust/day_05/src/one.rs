mod loader;

fn main() {
    let letters = loader::loader::load();
    let result = process(letters);
    println!("{}", result.len());
}

#[test]
fn aA () {
    let letters = "aA"
        .to_string()
        .chars()
        .map( |x| loader::loader::Letter::new(x) )
        .collect();
    let result = process(letters);
    assert_eq!(result, "".to_string());
}

#[test]
fn abBA () {
    let letters = "abBA"
        .to_string()
        .chars()
        .map( |x| loader::loader::Letter::new(x) )
        .collect();
    let result = process(letters);
    assert_eq!(result, "".to_string());
}

#[test]
fn abAB () {
    let letters = "abAB"
        .to_string()
        .chars()
        .map( |x| loader::loader::Letter::new(x) )
        .collect();
    let result = process(letters);
    assert_eq!(result, "abAB".to_string());
}

#[test]
fn aabAAB () {
    let letters = "aabAAB"
        .to_string()
        .chars()
        .map( |x| loader::loader::Letter::new(x) )
        .collect();
    let result = process(letters);
    assert_eq!(result, "aabAAB".to_string());
}

#[test]
fn dabAcCaCBAcCcaDA () {
    let letters = "dabAcCaCBAcCcaDA"
        .to_string()
        .chars()
        .map( |x| loader::loader::Letter::new(x) )
        .collect();
    let result = process(letters);
    assert_eq!(result, "dabCBAcaDA".to_string());
}

fn process ( mut letters: Vec<loader::loader::Letter> ) -> String {
    loop {
        let start = letters.len();
        for i in 0 .. letters.len() - 1 {
            if i >= letters.len() - 1 {
                // cutting from middle
                // so be sure not to run over the end
                break;
            }
            if letters[i].letter == letters[i+1].inverse {
                letters.remove(i);
                // same index, since first remove shifted everything left
                letters.remove(i);
            }
        }
        if start == letters.len() {
            // nothing removed in this pass, so done
            break;
        }
        if letters.len() < 2 {
            // not enough left to work with
            break;
        }
    }
    
    // convert Letter to char, and concat from there to a String
    let result: String = letters
        .iter()
        .map( |x| x.letter as char )
        .collect();
    return result;
}
