mod loader;

fn main() {
    let letters = loader::loader::load();
    let result = process(letters);
    println!("{}", result.len());
}

#[test]
fn dabAcCaCBAcCcaDA () {
    let letters = "dabAcCaCBAcCcaDA"
        .to_string()
        .chars()
        .map( |x| loader::loader::Letter::new(x) )
        .collect();
    let result = process(letters);
    assert_eq!(result.len(), 4);
}

fn process ( letters: Vec<loader::loader::Letter> ) -> String {
    // find unique lowercase letters in input
    let mut copy: Vec<char> = letters
        .to_vec().iter()
        .filter( |x| x.letter.is_lowercase() )
        .map( |x| x.letter )
        .collect();
    copy.sort();
    copy.dedup();

    // try removing all pairs for each given letter and then reducing
    let mut shortest = letters.to_vec();
    for c in copy.iter() {
        // remove all instances of c and upper(c)
        let temp: Vec<loader::loader::Letter> = letters
            .to_vec().iter()
            .filter( |x| x.letter != *c && x.inverse != *c )
            .map( |x| *x )
            .collect();    
        let reduced = reduce(temp);
        if reduced.len() < shortest.len() {
            shortest = reduced;
        }
    }
    
    
    // convert Letter to char, and concat from there to a String
    let result: String = shortest
        .iter()
        .map( |x| x.letter as char )
        .collect();
    return result;
}

fn reduce ( mut letters: Vec<loader::loader::Letter> ) -> Vec<loader::loader::Letter> {
    loop {
        let start = letters.len();
        for i in 0 .. letters.len() - 1 {
            if i >= letters.len() - 1 {
                // cutting from middle
                // so be sure not to run over the end
                break;
            }
            if letters[i].letter == letters[i+1].inverse {
                letters.remove(i);
                // same index, since first remove shifted everything left
                letters.remove(i);
            }
        }
        if start == letters.len() {
            // nothing removed in this pass, so done
            break;
        }
        if letters.len() < 2 {
            // not enough left to work with
            break;
        }
    }
    return letters;
}
