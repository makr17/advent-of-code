pub mod loader {
    use std::io::{self, BufRead};

    extern crate regex;
    use self::regex::Regex;

    #[derive(Clone,Debug)]
    pub struct Sleep {
        pub guard: u32,
        pub sleep: usize,
        pub wake:  usize,
    }
    
    pub fn load () -> Vec<Sleep> {
        let stdin = io::stdin();
        let mut v: Vec<Sleep> = Vec::new();
        let mut guard: u32 = 0;
        for line in stdin.lock().lines() {
            let val = line.unwrap();
            //println!("{}", val);
            let re = Regex::new(r"(\d+)\] (\w+) #?(\w+)").unwrap();
            for cap in re.captures_iter(&val) {
                if cap[2] == "Guard".to_string() {
                    guard = cap[3].parse::<u32>().unwrap();
                }
                if cap[2] == "falls".to_string() {
                    let mut sleep = Sleep {
                        guard: guard,
                        sleep: cap[1].parse::<usize>().unwrap(),
                        wake: 0
                    };
                    v.push(sleep);
                }
                if cap[2] == "wakes".to_string() {
                    let mut sleep = v.pop().unwrap();
                    sleep.wake = cap[1].parse::<usize>().unwrap();
                    //println!("{:?}", sleep);
                    v.push(sleep);
                }
            }
        }
        return v;
    }
}