mod loader;

use std::collections::HashMap;

fn main() {
    let vec = loader::loader::load();
    let num = process(vec);
    println!("{} is the num", num);
}

#[test]
fn example() {
    let mut v: Vec<loader::loader::Sleep> = Vec::new();
    v.push(loader::loader::Sleep { guard: 10, sleep: 05, wake: 25});
    v.push(loader::loader::Sleep { guard: 10, sleep: 30, wake: 55});
    v.push(loader::loader::Sleep { guard: 99, sleep: 40, wake: 50});
    v.push(loader::loader::Sleep { guard: 10, sleep: 24, wake: 29});
    v.push(loader::loader::Sleep { guard: 99, sleep: 36, wake: 46});
    v.push(loader::loader::Sleep { guard: 99, sleep: 45, wake: 55});
    assert_eq!(process(v), 240, "process should return {}", 240);
}

fn process (vec: Vec<loader::loader::Sleep>) -> u32 {
    let mut guards: HashMap<u32, Vec<u32>> = HashMap::new();
    for sleep in &vec {
        let guard = guards.entry(sleep.guard).or_insert(vec![0; 60]);
        for i in sleep.sleep .. sleep.wake {
            guard[i] += 1;
        }
    }

    let mut times: HashMap<u32, u32> = HashMap::new();
    let mut max_sleep = 0;
    let mut max_guard = 0;
    for (guard, sleep) in &guards {
        let time = sleep.iter().sum();
        times.insert(*guard, time);
        if time > max_sleep {
            max_sleep = time;
            max_guard = *guard;
        };
    }
    
    //println!("guard {} slept {} minutes", max_guard, max_sleep);

    let sleep = guards.get(&max_guard).unwrap();
    let mut max_minute: u32 = 0;
    max_sleep = 0;
    for mut i in 0 .. sleep.len() {
        if sleep[i] > max_sleep {
            max_sleep = sleep[i];
            max_minute = i as u32;
        };
    }

    //println!("{:?}", sleep);
    //println!("{} is max_minute", max_minute);
    
    return max_guard * max_minute as u32;
}