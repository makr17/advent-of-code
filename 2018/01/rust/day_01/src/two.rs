use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let mut vec = Vec::new();
    for line in stdin.lock().lines() {
        let val = line.unwrap();
        //println!("{}", val);
        let ival = val.parse::<i32>().unwrap();
        vec.push(ival)
    }
    let res = process( vec );
    println!("{}", res);
}

fn process( v:Vec<i32> ) -> i32 {
    let mut seen = Vec::new();
    let mut val: i32 = 0;
    seen.push(val);
    loop {
        for x in &v {
            val = val + x;
            if seen.contains(&val) {
                break;
            }
            seen.push(val);
        }
        if seen.contains(&val) && !seen.ends_with(&[val]) {
            break;
        }
    }
    return val;
}

#[test]
fn first_example() {
    let vec = vec![1, -2, 3, 1];
    assert_eq!(process(vec), 2, "sum should return {}", 2);
}
    
#[test]
fn second_example() {
    let vec = vec![1, -1];
    assert_eq!(process(vec), 0, "sum should return {}", 0);
}
    
#[test]
fn third_example() {
    let vec = vec![3, 3, 4, -2, -4];
    assert_eq!(process(vec), 10, "sum should return {}", 10);
}

#[test]
fn fourth_example() {
    let vec = vec![-6, 3, 8, 5, -6];
    assert_eq!(process(vec), 5, "sum should return {}", 5);
}

#[test]
fn fifth_example() {
    let vec = vec![7, 7, -2, -7, -4];
    assert_eq!(process(vec), 14, "sum should return {}", 14);
}
