use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let mut vec = Vec::new();
    for line in stdin.lock().lines() {
        let val = line.unwrap();
        //println!("{}", val);
        let ival = val.parse::<i32>().unwrap();
        vec.push(ival)
    }
    let sum = sum( vec );
    println!("{}", sum);
}

fn sum( v:Vec<i32> ) -> i32 {
    let mut val: i32 = 0;
    for x in &v {
        val = val + x;
    }
    return val;
}

#[test]
fn first_example() {
    let vec = vec![1, -2, 3, 1];
    assert_eq!(sum(vec), 3, "sum should return {}", 3);
}
    
#[test]
fn second_example() {
    let vec = vec![1, 1, 1];
    assert_eq!(sum(vec), 3, "sum should return {}", 3);
}
    
#[test]
fn third_example() {
    let vec = vec![1, 1, -2];
    assert_eq!(sum(vec), 0, "sum should return {}", 0);
}

#[test]
fn fourth_example() {
    let vec = vec![-1, -2, -3];
    assert_eq!(sum(vec), -6, "sum should return {}", -6);
}

