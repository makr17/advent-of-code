pub mod loader {
    use std::io::{self, BufRead};
    extern crate regex;
    use self::regex::Regex;
    
    #[derive(Clone,Debug)]
    pub struct Patch {
        pub id: u32,
        pub x:  u32,
        pub y:  u32,
        pub w:  u32,
        pub h:  u32,
    }

    pub fn load () -> Vec<Patch> {
        let stdin = io::stdin();
        let mut vec: Vec<Patch> = Vec::new();
        for line in stdin.lock().lines() {
            let val = line.unwrap();
            //println!("{}", val);
            let re = Regex::new(r"(\d+) @ (\d+),(\d+): (\d+)x(\d+)$").unwrap();
            for cap in re.captures_iter(&val) {
                let patch: Patch = Patch {
                    id: cap[1].parse::<u32>().unwrap(),
                    x:  cap[2].parse::<u32>().unwrap(),
                    y:  cap[3].parse::<u32>().unwrap(),
                    w:  cap[4].parse::<u32>().unwrap(),
                    h:  cap[5].parse::<u32>().unwrap(),
                };
                //println!("{:?}", patch);
                vec.push(patch);
            }
        }
        return vec;
    }
}