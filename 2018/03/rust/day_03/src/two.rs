mod loader;

extern crate ndarray;
use ndarray::Array2;

fn main() {
    let vec = loader::loader::load();
    let id = process(vec);
    println!("{} is the non-conflicting id", id);
}

#[test]
fn example() {
    let mut v: Vec<loader::loader::Patch> = Vec::new();
    v.push(loader::loader::Patch { id: 1, x: 1, y: 3, w: 4, h: 4});
    v.push(loader::loader::Patch { id: 2, x: 3, y: 1, w: 4, h: 4});
    v.push(loader::loader::Patch { id: 3, x: 5, y: 5, w: 2, h: 2});
    assert_eq!(process(v), 3, "process should return {}", 3);
}

fn process (v: Vec<loader::loader::Patch>) -> u32 {
    let mut m = Array2::<u32>::zeros((1000, 1000));
    for p in &v {
        for x in p.x .. p.x+p.w {
            for y in p.y .. p.y+p.h {
                m[[x as usize, y as usize]] += 1;
            }
        }
    }
    let mut id = 0;
    for p in &v {
        let mut ok = true;
        for x in p.x .. p.x+p.w {
            for y in p.y .. p.y+p.h {
                if m[[x as usize, y as usize]] > 1 {
                    ok = false;
                    break;
                }
            }
            if !ok {
                break;
            }
        
        }
        if ok {
            id = p.id;
            break;
        }
    }
    return id;
}