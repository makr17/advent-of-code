use std::collections::HashMap;

extern crate regex;
use regex::Regex;

fn main() {
    let input = include_str!("../input.txt");
    let points = parse(input);
    let thresh = 10_000;
    let area = process(points, thresh);
    println!("{}", area);
}

#[test]
fn example () {
    let input = include_str!("../example.txt");
    let points = parse(input);
    let thresh = 32;
    let area = process(points, thresh);
    assert_eq!(area, 16);
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

fn parse (input: &str) -> HashMap<Point, u32> {
    let mut points: HashMap<Point, u32> = HashMap::new();
    let re = Regex::new(r"^(\d+), (\d+)$").unwrap();
    for l in input.lines() {
        for cap in re.captures_iter(&l) {
            let point = Point {
                x: cap[1].parse::<i32>().unwrap(),
                y: cap[2].parse::<i32>().unwrap(),
            };
            points.insert(point, 0);
        }
    }
    return points;
}

fn process ( points: HashMap<Point, u32>, thresh: u32 ) -> u32 {
    let bound = bounding_box(&points);
    let buffer: i32 = thresh as i32 / points.len() as i32 * 2;
    let mut count: u32 = 0;
    for x in bound.min_x - buffer .. bound.max_x + buffer {
        for y in bound.min_y - buffer .. bound.max_y + buffer {
            let d = total_distance( Point { x: x, y: y }, &points );
            if d < thresh {
                count += 1;
            }
        }
    }
    return count;
}

fn total_distance ( point: Point, points: &HashMap<Point, u32> ) -> u32 {
    let mut d = 0;
    for (p, _) in points {
        d += metro_distance( *p, point );
    }
    return d;
}

#[test]
fn d_test_1 () {
    let p1 = Point { x: 1, y: 1 };
    let p2 = Point { x: 5, y: 4 };
    assert_eq!(metro_distance(p1, p2), 7);
}

fn metro_distance ( a: Point, b: Point ) -> u32 {
    let d_x = a.x - b.x;
    let d_y = a.y - b.y;
    let ret = d_x.abs() as u32 + d_y.abs() as u32;
    //println!("{:?} and {:?} -> {}", a, b, ret);
    return ret;
}

struct Bound {
    min_x: i32,
    max_x: i32,
    min_y: i32,
    max_y: i32,
}

fn bounding_box ( points: &HashMap<Point, u32> ) -> Bound {
    let mut min_x: i32 = 1_000_000;
    let mut max_x: i32 = 0;
    let mut min_y: i32 = 1_000_000;
    let mut max_y: i32 = 0;
    for (point, _) in points.iter() {
        if point.x < min_x {
            min_x = point.x;
        }
        if point.x > max_x {
            max_x = point.x;
        }
        if point.y < min_y {
            min_y = point.y;
        }
        if point.y > max_y {
            max_y = point.y;
        }
    }
    return Bound{ min_x: min_x, min_y: min_y, max_x: max_x, max_y: max_y };
}
