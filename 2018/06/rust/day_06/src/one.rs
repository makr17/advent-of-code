use std::collections::HashMap;

extern crate regex;
use regex::Regex;

fn main() {
    let input = include_str!("../input.txt");
    let points = parse(input);
    let area = process(points);
    println!("{}", area);
}

#[test]
fn example () {
    let input = include_str!("../example.txt");
    let points = parse(input);
    let area = process(points);
    assert_eq!(area, 17);
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

fn parse (input: &str) -> HashMap<Point, u32> {
    let mut points: HashMap<Point, u32> = HashMap::new();
    let re = Regex::new(r"^(\d+), (\d+)$").unwrap();
    for l in input.lines() {
        for cap in re.captures_iter(&l) {
            let point = Point {
                x: cap[1].parse::<i32>().unwrap(),
                y: cap[2].parse::<i32>().unwrap(),
            };
            points.insert(point, 0);
        }
    }
    return points;
}

fn process ( mut points: HashMap<Point, u32> ) -> u32 {
    let bound = bounding_box(&points);
    let buffer: i32 = 1000;
    for x in bound.min_x - buffer .. bound.max_x + buffer {
        for y in bound.min_y - buffer .. bound.max_y + buffer {
            let p = Point { x: x, y: y };
            let closest = closest_point( p, &points );
            // skip if more than one of our points is at the same min distance
            if closest.len() == 1 {
                //println!("for {:?}, {:?} is closest, d = {}", p, closest, min_d);
                if let Some(d) = points.get_mut(&closest[0]) {
                    *d += 1;
                }
            }
        }
    }
    let finite = exclude_infinite(&points, bound);
    let mut max_area = 0;
    for (_, area) in finite.iter() {
        // weed out the infinite areas on the corners
        if *area > max_area {
            max_area = *area;
        }
    }
    return max_area;
}

#[test]
fn d_test_1 () {
    let p1 = Point { x: 1, y: 1 };
    let p2 = Point { x: 5, y: 4 };
    assert_eq!(metro_distance(p1, p2), 7);
}

fn metro_distance ( a: Point, b: Point ) -> u32 {
    let d_x = a.x - b.x;
    let d_y = a.y - b.y;
    let ret = d_x.abs() as u32 + d_y.abs() as u32;
    //println!("{:?} and {:?} -> {}", a, b, ret);
    return ret;
}

struct Bound {
    min_x: i32,
    max_x: i32,
    min_y: i32,
    max_y: i32,
}

fn bounding_box ( points: &HashMap<Point, u32> ) -> Bound {
    let mut min_x: i32 = 1_000_000;
    let mut max_x: i32 = 0;
    let mut min_y: i32 = 1_000_000;
    let mut max_y: i32 = 0;
    for (point, _) in points.iter() {
        if point.x < min_x {
            min_x = point.x;
        }
        if point.x > max_x {
            max_x = point.x;
        }
        if point.y < min_y {
            min_y = point.y;
        }
        if point.y > max_y {
            max_y = point.y;
        }
    }
    return Bound{ min_x: min_x, min_y: min_y, max_x: max_x, max_y: max_y };
}

fn closest_point ( p: Point, points: &HashMap<Point, u32> ) -> Vec<Point> {
    let mut min_d: u32 = 1_000_000;
    let mut ret: Vec<Point> = Vec::new();
    for (point, _) in points.iter() {
        let d = metro_distance(*point, p );
        if d < min_d {
            min_d   = d;
            ret.truncate(0);
            ret.push(*point);
        }
        else if d == min_d {
            ret.push(*point);
        }
    }
    return ret;
}

fn exclude_infinite ( points: &HashMap<Point, u32>, bound: Bound ) -> HashMap<Point, u32> {
    // a point will have infinite close point
    // iff it is the closest of the set to a point on the bounding rectangle

    let mut ret = points.clone();
    for y in bound.min_y .. bound.max_y {
        let min = closest_point( Point { x: bound.min_x, y: y }, &points );
        if min.len() == 1 {
            ret.remove( &min[0] );
        }
        let max = closest_point( Point { x: bound.max_x, y: y }, &points );
        if max.len() == 1 {
            ret.remove( &max[0] );
        }
    }
    for x in bound.min_x .. bound.max_x {
        let min = closest_point( Point { x: x, y: bound.min_y }, &points );
        if min.len() == 1 {
            ret.remove( &min[0] );
        }
        let max = closest_point( Point { x: x, y: bound.max_y }, &points );
        if max.len() == 1 {
            ret.remove( &max[0] );
        }
    }

    //println!("{:?}", ret);
    
    return ret;
}
