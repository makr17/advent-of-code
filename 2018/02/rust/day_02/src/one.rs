use std::io::{self, BufRead};
use std::collections::HashMap;

fn main() {
    let stdin = io::stdin();
    let mut vec: Vec<std::string::String> = Vec::new();
    for line in stdin.lock().lines() {
        let val = line.unwrap();
        //println!("{}", val);
        vec.push(val)
    }
    let res = process( vec );
    println!("{}", res);
}

fn process (v: Vec<std::string::String>) -> u32 {
    let mut twos:   u32 = 0;
    let mut threes: u32 = 0;
    for label in &v {
        //println!("{}", label);
        let mut histo = HashMap::new();
        for c in label.chars() {
            let count = histo.entry(c).or_insert(0);
            *count += 1;
        }
        let mut two   = 0;
        let mut three = 0;
        for (_, val) in histo.iter() {
            if *val == 2 {
                two = 1;
            }
            if *val == 3 {
                three = 1;
            }
        }
        //println!("  two:  {}  three: {}", two, three);
        twos += two;
        threes += three;
        //println!("  twos: {} threes: {}", twos, threes);
    }
    return twos * threes;
}

#[test]
fn first_example() {
    let vec: Vec<std::string::String> = vec![
        "abcdef".to_string(),
        "bababc".to_string(),
        "abbcde".to_string(),
        "abcccd".to_string(),
        "aabcdd".to_string(),
        "abcdee".to_string(),
        "ababab".to_string()
    ];
    assert_eq!(process(vec), 12, "process should return {}", 12);
}
