use std::io::{self, BufRead};
//use std::collections::HashMap;

extern crate edit_distance;
use edit_distance::edit_distance;

fn main() {
    let stdin = io::stdin();
    let mut vec: Vec<std::string::String> = Vec::new();
    for line in stdin.lock().lines() {
        let val = line.unwrap();
        //println!("{}", val);
        vec.push(val)
    }
    let res = process( vec );
    println!("{}", res);
}

fn process (v: Vec<std::string::String>) -> std::string::String {
    // find the two strings in v that have edit distance == 1
    let mut seen: Vec<std::string::String> = Vec::new();
    let mut a = String::new();
    let mut b = String::new();
    for label in &v {
        for s in &seen {
            let x = label.clone();
            let y = s.clone();
            if edit_distance(&x, &y) == 1 {
                a = label.to_string();
                b = s.to_string();
                break;
            }
        }
        seen.push(label.to_string());
        if a.len() > 0 {
            break;
        }
    }
    println!("{}  {}", a, b);
    let a_chars: Vec<char> = a.chars().collect();
    let b_chars: Vec<char> = b.chars().collect();
    let mut res = String::new();
    let mut i: usize = 0;
    while i < a_chars.len() {
        if a_chars[i] == b_chars[i] {
            res.push(a_chars[i]);
        }
        i += 1;
    }
    return res;
}

#[test]
fn first_example() {
    let vec: Vec<std::string::String> = vec![
        "abcde".to_string(),
        "fghij".to_string(),
        "klmno".to_string(),
        "pqrst".to_string(),
        "fguij".to_string(),
        "axcye".to_string(),
        "wvxyz".to_string(),
    ];
    assert_eq!(process(vec), "fgij".to_string(), "process should return {}", "fgij");
}
