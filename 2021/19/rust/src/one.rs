#[macro_use]
extern crate lazy_static;

use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

#[allow(unused_imports)]
use nalgebra::{
    distance_squared
        , dmatrix
        , point
        , DMatrix
        , Matrix3
        , Point3
        , Rotation3
        , Vector3
};

#[allow(dead_code)]
#[derive(Debug)]
struct Beacon {
    // the original input position
    // relative to a scanner
    _position: Point3<i32>,
    // transform of input position to global
    // start with identity, adjust once we know more
    rotation: Matrix3<i32>,
    translation: Vector3<i32>,
}
impl Beacon {
    fn new(p: Point3<i32>) -> Self {
        Self {
            _position: p.clone(),
            rotation: Matrix3::identity(),
            translation: Vector3::identity(),
        }
    }

    fn position(&self) -> Point3<i32> {
        self.rotation * self._position + self.translation
    }
}

#[allow(dead_code)]
#[derive(Debug)]
struct Scanner {
    id: u32,
    _position: Point3<i32>,
    // transform of input position to global
    // start with identity, adjust once we know more
    rotation: Matrix3<i32>,
    translation: Vector3<i32>,
    beacons: Vec<Beacon>,
    distances: DMatrix::<i32>,
}
impl Scanner {
    fn new(id: u32) -> Self {
        Self {
            id,
            _position: Point3::origin(),
            rotation: Matrix3::identity(),
            translation: Vector3::identity(),
            beacons: vec![],
            distances: dmatrix![0],
        }
    }
    #[allow(dead_code)]
    fn position(&self) -> Point3<i32> {
        self.rotation * self._position + self.translation
    }
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let mut scanners: Vec<Scanner> = vec![];
    for line in input.lines() {
        let l = line?;
        if l.is_empty() {
            continue;
        }
        if l.contains("scanner") {
            // use length of scanners vec as id generator
            let scanner = Scanner::new(scanners.len() as u32);
            scanners.push(scanner);
            continue;
        }
        let coords: Vec<i32> = l.split(',')
            .map(|x| x.parse::<i32>().unwrap())
            .collect();
        let point = point!(coords[0], coords[1], coords[2]);
        let beacon = Beacon::new(point);

        let _ = &scanners.last_mut().unwrap().beacons.push(beacon);
    }
    for scanner in scanners.iter_mut() {
        let size = scanner.beacons.len();
        // resize now that we know how many beacons we (currently) have
        scanner.distances = scanner.distances.clone().resize(size, size, 0);
        for i in 0 .. size {
            for j in i+1 .. size {
                let a = &scanner.beacons[i]._position;
                let b = &scanner.beacons[j]._position;
                // store _squared_ distance
                // so we don't need to deal with sqrt and floats
                scanner.distances[(i,j)] =
                    (a.x - b.x).pow(2)
                    + (a.y - b.y).pow(2)
                    + (a.z - b.z).pow(2);
                scanner.distances[(j,i)] = scanner.distances[(i,j)];
            }
        }
    }

    let _iso = align(&mut scanners, 0, 1);
    
    Ok(0)
}

/*
[]
*/
lazy_static! {
    // inefficient, includes some dupes
    static ref ROTATIONS: Vec<Matrix3<i32>> = {
        // all possible unit rotations in x/y/z
        let r: Vec<Matrix3<i32>> = vec![
            Matrix3::new(0,1,0,0,0,1,1,0,0),
            Matrix3::new(-1,0,0,0,0,1,0,1,0),
            Matrix3::new(0,-1,0,0,0,1,-1,0,0),
            Matrix3::new(0,-1,0,0,0,-1,1,0,0),
            Matrix3::new(0,0,-1,1,0,0,0,-1,0),
            Matrix3::new(-1,0,0,0,-1,0,0,0,1),
            Matrix3::new(0,1,0,1,0,0,0,0,-1),
            Matrix3::new(0,0,1,0,-1,0,1,0,0),
            Matrix3::new(1,0,0,0,0,1,0,-1,0),
            Matrix3::new(-1,0,0,0,0,-1,0,-1,0),
            Matrix3::new(0,1,0,0,0,-1,-1,0,0),
            Matrix3::new(1,0,0,0,1,0,0,0,1),
            Matrix3::new(0,1,0,-1,0,0,0,0,1),
            Matrix3::new(0,0,1,-1,0,0,0,-1,0),
            Matrix3::new(1,0,0,0,-1,0,0,0,-1),
            Matrix3::new(0,0,1,0,1,0,-1,0,0),
            Matrix3::new(0,-1,0,-1,0,0,0,0,-1),
            Matrix3::new(0,-1,0,1,0,0,0,0,1),
            Matrix3::new(0,0,1,1,0,0,0,1,0),
            Matrix3::new(1,0,0,0,0,-1,0,1,0),
            Matrix3::new(0,0,-1,-1,0,0,0,1,0),
            Matrix3::new(0,0,-1,0,-1,0,-1,0,0),
            Matrix3::new(0,0,-1,0,1,0,1,0,0),
            Matrix3::new(-1,0,0,0,1,0,0,0,-1)
        ];
        r
    };
}

fn align(scanners: &mut Vec<Scanner>, a: usize, b: usize) -> bool {
    let mut candidates: Vec<(usize, usize)> = vec![];
    for i in 0 .. scanners[a].beacons.len() {
        for j in 0 .. scanners[b].beacons.len() {
            // compare distance column vectors for a[i] and b[j]
            let mut counts: HashMap<u32, u32> = HashMap::new();
            for d in scanners[a].distances.column(i).iter() {
                if *d == 0 { continue }
                *counts.entry(*d as u32).or_insert(0) += 1;
            }
            for d in scanners[b].distances.row(j).iter() {
                if *d == 0 { continue }
                *counts.entry(*d as u32).or_insert(0) += 1;
            }
            counts.retain(|_k,v| *v > 1);
            if counts.len() > 5 {
                //println!("{} {}:  {:?}", i, j, counts);
                candidates.push((i, j));
            }
        }
    }

    // take first two candidates
    let (i, ip) = candidates[0];
    let (j, jp) = candidates[1];
    // if our reasoning to this point is sound, then
    // i - j and ip - jp should be same vector, less a rotation
    let v = scanners[a].beacons[i].position() - scanners[a].beacons[j].position();
    //println!("{:?}", v);
    let vp = scanners[b].beacons[ip].position() - scanners[b].beacons[jp].position();
    //println!("{:?}", vp);
    let mut rot = Matrix3::identity();
    for r in ROTATIONS.iter() {
        if r * vp == v {
            //println!("{:?} <= {:?}", r * vp, r);
            rot = r.clone();
            break;
        }
    }
    // rotate, and then subtract to derive translation vector
    //println!("{:?}", a.beacons[i].position());
    //println!("{:?}", b.beacons[ip].position());
    let rb = rot * scanners[b].beacons[ip].position();
    let trans = scanners[a].beacons[i].position() - rb;
    //println!("{:?}", rb);
    //let tb = (rot * b.beacons[ip].position()) + trans;
    //println!("{:?}", tb);

    scanners[b].rotation = rot.clone();
    scanners[b].translation = trans.clone();
    for beacon in scanners[b].beacons.iter_mut() {
        beacon.rotation = rot.clone();
        beacon.translation = trans.clone();
    }

    for c in candidates {
        // to and from
        let (t, f) = c;
        if scanners[a].beacons[t].position() != scanners[b].beacons[f].position() {
            println!(
                "oops, something broke {:?} != {:?}",
                scanners[a].beacons[t].position(),
                scanners[b].beacons[f].position()
            );
            //return false;
        }
    }

    return true
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 79);
    Ok(())
}

