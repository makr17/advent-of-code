#!/usr/bin/env python3

from pathlib import Path
import unittest

from process import Process

class TestProcess(unittest.TestCase):
    def test_beacon_count(self):
        txt = Path("test.txt").read_text()
        p = Process(txt)
        self.assertEqual(p.beacon_count(), 79)


if __name__ == "__main__":
    unittest.main()
