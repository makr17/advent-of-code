import math
import numpy

class Scanner:
    def __init__(self, id):
        self.id = id
        self.beacons = []

    def build_distance_matrix(self):
        # initialize matrix with zero
        self.distance_matrix = []
        for b in self.beacons:
            self.distance_matrix.append([0 for x in self.beacons])
        for (x, a) in enumerate(self.beacons):
            for (y, b) in enumerate(self.beacons):
                if self.distance_matrix[x][y] > 0:
                    # already seen this pair
                    continue
                if x == y:
                    # distance to self is zero
                    continue
                self.distance_matrix[x][y] = self.distance_matrix[y][x] = a.distance(b)

    def match(self, other):
        for (y, beacon) in enumerate(self.beacons):
            dists = self.distance_matrix[y]
            for (oy, ob) in enumerate(other.beacons):
                distcounts = { d: 1 for d in dists if d != 0 }
                odists = other.distance_matrix[y]
                for d in odists:
                    if d == 0:
                        continue
                    if d in distcounts:
                        distcounts[d] += 1
                    else:
                        distcounts[d] = 1
                #print(distcounts)
                matches = [d for d in distcounts.keys() if distcounts[d] > 1]
                print(matches)
                if len(matches):
                    print(f"self[{y}] matches other[{oy}], {len(matches)} points")

class ScannerBeacon:
    def __init__(self, x, y, z):
        self.x = int(x)
        self.norm_x = None
        self.y = int(y)
        self.norm_y = None
        self.z = int(z)
        self.norm_z = None

    def __str__(self):
        return f"({self.x},{self.y},{self.z})"

    def distance(self, other):
        return math.sqrt(\
                         math.pow(self.x - other.x, 2)\
                         + math.pow(self.y - other.y, 2)\
                         + math.pow(self.z - other.z, 2)\
                         )

class Process:
    def __init__(self, txt):
        raw = txt.split("\n\n")
        scanners = []
        for rawscanner in raw:
            lines = rawscanner.split("\n")
            (_, _, scanner_id, _) = lines[0].split(" ")
            scanner = Scanner(id)
            scanners.append(scanner)
            for line in lines[1:]:
                try:
                    (x, y, z) = line.split(",")
                    scanner.beacons.append(ScannerBeacon(x, y, z))
                except:
                    pass
            scanner.build_distance_matrix()

        # set normalized x,y,z for beacons in reference scanner [0]
        for beacon in scanners[0].beacons:
            beacon.norm_x = beacon.x
            beacon.norm_y = beacon.y
            beacon.norm_z = beacon.z

        #for row in scanners[0].distance_matrix:
        #    print(",".join([str(x) for x in row]))

        # now we have distance matrix between all beacons for given scanner
        # need to find matching "constellations" of distances between scanners
        scanners[0].match(scanners[1])

    def beacon_count(self):
        return 0
