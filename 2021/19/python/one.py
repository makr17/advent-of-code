#!/usr/bin/env python3

from pathlib import Path
from process import Process

def run():
    txt = Path("input.txt").read_text()
    p = Process(txt)
    print(f"{p.beacon_count()} unique beacons")

if __name__ == "__main__":
    run()
