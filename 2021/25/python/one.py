#!/usr/bin/env python3

import fileinput

width = 0
height = 0

class Pos:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.x},{self.y})"


class Cuc:
    def __init__(self, direction, pos):
        self.pos = pos
        self.direction = direction

    def __str__(self):
        return f"{str(self.pos)} moving {self.direction}"

    def next(self):
        if self.direction == "E":
            return Pos((self.pos.x + 1) % width, self.pos.y)
        if self.direction == "S":
            return Pos(self.pos.x, (self.pos.y + 1) % height)


class Map:
    def __init__(self):
        self.filled = {}
        pass

    def add_cuc(self, cuc):
        self.filled[str(cuc.pos)] = cuc

    def move(self, cucs):
        for c in cucs:
            n = c.next()
            if str(n) in self.filled:
                # double-check
                panic(f"oops, {n} is taken")
            self.filled.pop(str(c.pos))
            c.pos = n
            self.filled[str(n)] = c


def run():
    global height
    global width
    y = 0
    m = Map()
    for line in fileinput.input():
        x = 0
        line = line.strip()
        for char in line:
            pos = Pos(x, y)
            if char == ">":
                cuc = Cuc('E', pos)
                m.add_cuc(cuc)
            elif char == "v":
                cuc = Cuc('S', pos)
                m.add_cuc(cuc)
            #print(cuc)
            x += 1
        if width == 0:
            width = x
        y += 1
    height = y

    step = 1
    print(f"{len(m.filled)} cucs total")
    while True:
        # East first
        ecan = [c for c in m.filled.values() if c.direction == "E" and str(c.next()) not in m.filled]
        print(f"step {step} E can: {len(ecan)}")
        m.move(ecan)
        # Then South
        scan =  [c for c in m.filled.values() if c.direction == "S" and str(c.next()) not in m.filled]
        print(f"step {step} S can: {len(scan)}")
        m.move(scan)
        if len(ecan) == 0 and len(scan) == 0:
            break
        step += 1

if __name__ == "__main__":
    run()
