#!/usr/bin/env python3

import fileinput

rows = []
for line in fileinput.input():
    points = [int(x) for x in line.strip()]
    #print(points)
    rows.append(points)

minimums = {}
for y in range(0, len(rows)):
    for x in range(0, len(rows[y])):
        # easy answers first
        # 9 is highest, can't be minima
        if rows[y][x] == 9:
            continue
        # 0 is lowest, must be minima
        if rows[y][x] == 0:
            minimums[(x,y)] = 0

        # now interrogate the neighbors
        neighbors = []
        if x - 1 >= 0:
            neighbors.append((x-1, y))
        if y - 1 >= 0:
            neighbors.append((x, y-1))
        if x + 1 < len(rows[y]):
            neighbors.append((x+1, y))
        if y + 1 < len(rows):
            neighbors.append((x, y+1))
            
        minima = True
        #print(f"({x},{y}): {neighbors}")
        for n in neighbors:
            (xn, yn) = n
            #print(f"({x},{y})[{rows[y][x]}] ~ {n}[{rows[yn][xn]}]")
            if rows[yn][xn] < rows[y][x]:
                minima = False
                #print("not minima")
                break

        if minima:
            #print("minima")
            minimums[(x,y)] = rows[y][x]

#print(minimums)

risk = sum([r + 1 for r in minimums.values()])
print(risk)
