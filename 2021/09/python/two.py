#!/usr/bin/env python3

import fileinput

rows = []
for line in fileinput.input():
    points = [int(x) for x in line.strip()]
    #print(points)
    rows.append(points)
seen = {}

def recurse(point, basin, seen):
    if point in seen:
        return
    
    (x,y) = point
    if rows[y][x] == 9:
        seen[(x,y)] = True
        return

    seen[point] = True
    basin.append(point)
    neighbors = []
    if x - 1 >= 0:
        neighbors.append((x-1, y))
    if y - 1 >= 0:
        neighbors.append((x, y-1))
    if x + 1 < len(rows[y]):
        neighbors.append((x+1, y))
    if y + 1 < len(rows):
        neighbors.append((x, y+1))

    for n in neighbors:
        recurse(n, basin, seen)

basins = []
while len(seen) < len(rows) * len(rows[0]):
    for y in range(0, len(rows)):
        for x in range(0, len(rows[y])):
            basin = []
            recurse((x,y), basin, seen)
            if len(basin) > 0:
                basins.append(basin)

#for row in rows:
#    print(row)
#print(len(basins))
lens = sorted([len(x) for x in basins], reverse=True)
print(lens)
sizes = lens[0] * lens[1] * lens[2]
print(sizes)
