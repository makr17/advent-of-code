use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

#[derive(Clone, Debug)]
struct Point {
    x: usize,
    y: usize,
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    // TODO: can I know the size upfront?
    let mut lines: Vec<String> = vec![];
    for line in input.lines() {
        let l = line?;
        lines.push(l);
    }
    let m = lines[0].len();
    let n = lines.len();

    let mut map: Vec<Vec<usize>> = vec![vec![0; m]; n];
    for (y, l) in lines.iter().enumerate() {
        for (x, c) in l.chars().enumerate() {
            map[y][x] = c.to_digit(10).ok_or("Failed to parse digit")? as usize;
        }
    }

    let mut minimums: Vec<Point> = vec![];
    for y in 0..n {
        for x in 0..m {
            // easy answers first
            // 9 is max, can't be a minima
            if map[y][x] == 9 {
                continue;
            }
            let p = Point { x, y };
            // 0 is min, must be minima
            if map[y][x] == 0 {
                minimums.push(p.clone());
                continue;
            }
            let ns = neighbors(p.clone(), Point { x: m, y: n });
            let mut minima = true;
            for n in ns {
                if map[n.y][n.x] < map[y][x] {
                    minima = false;
                    break;
                }
            }
            if minima {
                minimums.push(p.clone());
            }
        }
    }
    let mut risk = 0;
    for p in minimums {
        risk += map[p.y][p.x] + 1;
    }
    Ok(risk)
}

fn neighbors(p: Point, max: Point) -> Vec<Point> {
    let mut n: Vec<Point> = vec![];
    if p.x > 0 {
        n.push(Point { x: p.x-1, y: p.y });
    }
    if p.x < max.x - 1 {
        n.push(Point { x: p.x+1, y: p.y });
    }
    if p.y > 0 {
        n.push(Point { x: p.x, y: p.y-1 });
    }
    if p.y < max.y - 1 {
        n.push(Point { x: p.x, y: p.y+1 });
    }
    n
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 15);
    Ok(())
}

