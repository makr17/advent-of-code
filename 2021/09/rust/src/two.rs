use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct Point {
    x: usize,
    y: usize,
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    let mut grid: Vec<Vec<usize>> = vec![];
    for line in input.lines() {
        let l = line?;
        let row: Vec<usize> = l.chars().into_iter()
            .map(|c| c.to_digit(10).unwrap() as usize)
            .collect();
        grid.push(row);
    }

    let mut seen: HashMap<Point, bool> = HashMap::new();
    let mut basins: Vec<Vec<Point>> = vec![];
    while seen.len() < grid.len() * grid[0].len() {
        for (y, row) in grid.iter().enumerate() {
            for (x, _c) in row.iter().enumerate() {
                let mut basin: Vec<Point> = vec![];
                recurse(Point {x, y}, &mut basin, &grid, &mut seen);
                if !basin.is_empty() {
                    basins.push(basin);
                }
            }
        }
    }
    let mut lens: Vec<usize> = basins.into_iter().map(|b| b.len()).collect();
    lens.sort_unstable();
    lens.reverse();
    
    Ok(lens[0] * lens[1] * lens[2])
}

fn recurse(p: Point, basin: &mut Vec<Point>, grid: &[Vec<usize>], seen: &mut HashMap<Point, bool>) {
    if seen.contains_key(&p) {
        return;
    }
    seen.insert(p.clone(), true);
    if grid[p.y][p.x] == 9 {
        return;
    }
    basin.push(p.clone());
    for n in neighbors(p, Point { x: grid[0].len() - 1, y: grid.len() - 1 }) {
        recurse(n, basin, grid, seen);
    }
}

fn neighbors(p: Point, max: Point) -> Vec<Point> {
    let mut n: Vec<Point> = vec![];
    if p.x > 0 {
        n.push(Point { x: p.x-1, y: p.y });
    }
    if p.x < max.x {
        n.push(Point { x: p.x+1, y: p.y });
    }
    if p.y > 0 {
        n.push(Point { x: p.x, y: p.y-1 });
    }
    if p.y < max.y {
        n.push(Point { x: p.x, y: p.y+1 });
    }
    n
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 1134);
    Ok(())
}

