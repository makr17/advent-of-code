use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut h = 0;
    let mut d = 0;
    for line in input.lines() {
        let l = line?;
        let split: Vec<&str> = l.split(' ').collect();
        let cmd: &str = split[0];
        let val: i32 = split[1].parse::<i32>()?;
        match cmd {
            "forward" => h += val,
            "down" => d += val,
            "up" => d -= val,
            _ => println!("unknown command {}", cmd),
        }
    }

    Ok(h*d)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 150);
    Ok(())
}
