#!/usr/bin/env python3

import fileinput

h = 0
d = 0

for line in fileinput.input():
    parse = line.split(" ")
    command = parse[0]
    value = int(parse[1])
    if command == "forward":
        h += value
    elif command == "down":
        d += value
    elif command == "up":
        d -= value

print(f"horizontal = {h}  depth = {d}")
print(f"product = {h*d}")
