#!/usr/bin/env python3

import fileinput

h = 0
d = 0
a = 0

for line in fileinput.input():
    parse = line.split(" ")
    command = parse[0]
    value = int(parse[1])
    if command == "forward":
        h += value
        d += a * value
    elif command == "down":
        a += value
    elif command == "up":
        a -= value

print(f"horizontal = {h}  depth = {d}")
print(f"product = {h*d}")
