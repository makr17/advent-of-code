# 2021 AoC

Implementing in both python and rust this year.
Python because it is comfortable, and rust because it isn't.

Ended up with 45 stars:
* missed 19
* missed 23
* missed second half of 25
