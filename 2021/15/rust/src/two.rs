use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};


#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
struct Point {
    x: usize,
    y: usize,
}
impl Point {
    fn neighbors(&self, max: Point) -> Vec<Point> {
        let mut n: Vec<Point> = vec![];
        if self.x > 0 {
            n.push(Point{ x: self.x - 1, y: self.y });
        }
        if self.x < max.x {
            n.push(Point{ x: self.x + 1, y: self.y });
        }
        if self.y > 0 {
            n.push(Point{ x: self.x, y: self.y - 1 });
        }
        if self.y < max.y {
            n.push(Point{ x: self.x, y: self.y + 1 });
        }
        n
    }
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let mut subgrid: Vec<Vec<u32>> = vec![];
    for line in input.lines() {
        let l = line?;
        let row: Vec<u32> = l.chars().into_iter()
            .map(|c| c.to_digit(10).unwrap())
            .collect();
        subgrid.push(row);
    }
    let modulus = subgrid.len();

    // extend the grid 5x
    let mut grid: Vec<Vec<u32>> = vec![vec![0; 5*modulus]; 5 * modulus];
    // copy starter from subgrid
    for y in 0 .. modulus {
        for x in 0 .. modulus {
            grid[y][x] = subgrid[y][x];
        }
    }
    // copy across to fill rows to the right
    for y in 0 .. modulus {
        for x in modulus .. 5 * modulus {
            grid[y][x] = grid[y][x - modulus] + 1;
            if grid[y][x] > 9 {
                grid[y][x] = 1;
            }
        }
    }
    // then down to fill the rest
    for y in modulus .. 5 * modulus {
        for x in 0 .. 5 * modulus {
            grid[y][x] = grid[y - modulus][x] + 1;
            if grid[y][x] > 9 {
                grid[y][x] = 1;
            }
        }
    }
    //display(&grid);

    // simple-ish dijkstra
    let mut dist: HashMap<Point, HashMap<Point, u32>> = HashMap::new();
    let max = Point{ x: grid[0].len() - 1, y: grid.len() - 1 };
    let mut nodes: HashSet<Point> = HashSet::new();
    for y in 0 .. max.y + 1 {
        for x in 0 .. max.x + 1 {
            let p = Point { x, y };
            nodes.insert(p);
            let d = dist.entry(p).or_insert_with(HashMap::new);
            for n in p.neighbors(max) {
                *d.entry(n).or_insert(0) = grid[n.y][n.x];
            }
        }
    }
    //println!("{:?}", nodes);
    //println!("{:?}", dist);
    
    let mut unvisited: HashMap<Point, u32> = HashMap::new();
    let mut visited: HashMap<Point, u32> = HashMap::new();
    for p in nodes {
        *unvisited.entry(p).or_insert(0) = u32::MAX;
    }
    let mut current = Point{ x: 0, y: 0 };
    let mut current_distance = 0;
    *unvisited.entry(current).or_insert(0) = current_distance;

    loop {
        for (n, d) in dist[&current].iter() {
            //println!("{:+?}: {}", n, d);
            if !unvisited.contains_key(n) {
                continue;
            }
            let new_distance = current_distance + d;
            if unvisited[n] == u32::MAX || unvisited[n] > new_distance {
                *unvisited.entry(*n).or_insert(0) = new_distance;
            }
        }
        *visited.entry(current).or_insert(0) = current_distance;
        unvisited.remove(&current);
        if unvisited.is_empty() {
            break;
        }
        let mut candidates: Vec<(&Point, &u32)> =  unvisited.iter()
            .filter(|x| x.1 < &u32::MAX)
            .collect();
        candidates.sort_by(|a, b| a.1.partial_cmp(b.1).unwrap());
        current = *candidates[0].0;
        current_distance = *candidates[0].1;
    }
    
    Ok(visited[&max])
}

#[allow(dead_code)]
fn display(g: &[Vec<u32>]) {
    for row in g.iter() {
        println!("{}", row.iter().map(|i| i.to_string()).collect::<String>());
    }
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 315);
    Ok(())
}
