#!/usr/bin/env python3

import fileinput

def neighbors(x, y, mx, my):
    n = []
    if x > 0:
        n.append((x-1, y))
    if x < mx:
        n.append((x+1, y))
    if y > 0:
        n.append((x, y-11))
    if y < my:
        n.append((x, y+1))
    return n

def label(x, y):
    return str(x) + "," + str(y)

grid = []
for line in fileinput.input():
    line = line.strip()
    row = [int(c) for c in line]
    grid.append(row)
    print(row)

nodes = {}
for y in range(0, len(grid)):
    for x in range(0, len(grid[y])):
        nodes[label(x, y)] = (x, y)

dist = {}
for y in range(0, len(grid)):
    for x in range(0, len(grid[y])):
        dist[label(x, y)] = {
            label(nx, ny): grid[ny][nx]
            for (nx, ny) in neighbors(x, y, len(grid) - 1, len(grid[y]) - 1)}

unvisited = {node: None for node in nodes.keys()}
visited = {}
current = label(0, 0)
current_distance = 0
unvisited[current] = current_distance

while True:
    for neighbor, distance in dist[current].items():
        if neighbor not in unvisited: continue
        new_distance = current_distance + distance
        if unvisited[neighbor] is None or unvisited[neighbor] > new_distance:
            unvisited[neighbor] = new_distance
    visited[current] = current_distance
    del unvisited[current]
    if not unvisited: break
    candidates = [node for node in unvisited.items() if node[1]]
    current, current_distance = sorted(candidates, key = lambda x: x[1])[0]

#print(visited)

print(visited[label(len(grid[0])-1, len(grid)-1)])
