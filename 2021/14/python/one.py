#!/usr/bin/env python3

import fileinput
import re

template = ""
rules = {}
for line in fileinput.input():
    line = line.strip()
    if fileinput.isfirstline():
        template = line
        continue
    
    if "->" in line:
        # coordinates
        (match, ptr, add) = line.split(" ")
        #repl = match[0] + add + match[1]
        rules[match] = add
    else:
        pass


re_string = "(?=(" + "|".join(rules.keys()) + "))"
#print(re_string)
p = re.compile(re_string)
#print(template)
#print()
for step in range(0, 10):
    i = 0
    for match in p.finditer(template):
        (index, foo) = match.span()
        # since we're manipulating the string, match indexes need to be adjusted
        index += i
        # grab the key, two characters after the zero-width match
        key = template[index:index+2]
        #print(f"{key} => {rules[key]}")
        # slide over one from the zero-width match
        index += 1
        # and insert based on the rules
        template = template[:index] + rules[key] + template[index:]
        i += 1
#    print(template)
#    print()

occurs = {}
for char in template:
    if char in occurs:
        occurs[char] += 1
    else:
        occurs[char] = 1

#print(occurs)
counts = [v for v in occurs.values()]
counts.sort()
#print(counts)

# biggest count minus the smallest
print(counts[-1] - counts[0])
