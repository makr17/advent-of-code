use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());

    Ok(())
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let mut rules: HashMap<String, char> = HashMap::new();
    let mut template: Vec<char> = vec![];
    let mut first = true;
    for line in input.lines() {
        let l = line?;
        if first {
            template = l.chars().collect();
            first = false;
            continue;
        }
        if l.contains("->") {
            // a point, comma-separated dimensions
            let chunks: Vec<String> = l.trim().split(' ').map(|x| x.to_string()).collect();
            let key = &chunks[0];
            let repl: char = chunks[2].chars().next().unwrap();
            rules.insert(key.to_string(), repl);
        }
    }

    let mut pairs: HashMap<String, u64> = HashMap::new();
    for i in 0 .. template.len() - 1 {
        let pair: String = template[i .. i+2].iter().collect::<String>();
        *pairs.entry(pair).or_insert(0) += 1;
    }
    //println!("{:?}", pairs);

    for _j in 0 .. 40 {
        let mut newpairs: HashMap<String, u64> = HashMap::new();
        for test in pairs.keys() {
            if rules.contains_key(test) {
                let repl = rules.get(test).unwrap();
                let chars: Vec<char> = test.chars().collect();
                let left: String = [chars[0], *repl].iter().collect::<String>();
                let right: String = [*repl, chars[1]].iter().collect::<String>();
                let count = pairs.get(test).unwrap();
                *newpairs.entry(left).or_insert(0) += *count;
                *newpairs.entry(right).or_insert(0) += *count;
            }
            else {
                *newpairs.entry(test.to_string()).or_insert(0) +=  *pairs.get(test).unwrap();
            }
        }
        pairs = newpairs;
        //println!("{:?}", pairs);
    }

    let mut occurs: HashMap<char, u64> = HashMap::new();
    for (pair, count) in pairs {
        let chars: Vec<char> = pair.chars().collect();
        for c in chars {
            *occurs.entry(c).or_insert(0) += count;
        }
    }
    // first and last character weren't double counted
    *occurs.entry(template[0]).or_insert(0) += 1;
    *occurs.entry(template[template.len()-1]).or_insert(0) += 1;
    //println!("{:?}", occurs);

    let mut counts: Vec<u64> = occurs.values().into_iter().map(|x| *x/2).collect();
    counts.sort();
    //println!("{:?}", counts);

    Ok(counts[counts.len()-1] - counts[0])
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 2188189693529);
    Ok(())
}
