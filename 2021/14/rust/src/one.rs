use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let mut rules: HashMap<String, char> = HashMap::new();
    let mut template: Vec<char> = vec![];
    let mut first = true;
    for line in input.lines() {
        let l = line?;
        if first {
            template = l.chars().collect();
            first = false;
            continue;
        }
        if l.contains("->") {
            // a point, comma-separated dimensions
            let chunks: Vec<String> = l.trim().split(' ').map(|x| x.to_string()).collect();
            let key = &chunks[0];
            let repl: char = chunks[2].chars().next().unwrap();
            rules.insert(key.to_string(), repl);
        }
    }

    //println!("{:?}", template.iter().collect::<String>());
    for _j in 0 .. 10 {
    let mut i = 0;
        loop {
            if i == template.len() - 1 {
                break;
            }
            let key = &template[i .. i+2].iter().collect::<String>();
            //println!("{}: {}", i, key);
            if rules.contains_key(key) {
                template.insert(i+1, rules.get(key).unwrap().clone());
                i += 1;
            }
            i += 1;
        }
        //println!("{:?}", template.iter().collect::<String>());
    }

    let mut occurs: HashMap<char, u64> = HashMap::new();
    for c in template {
        *occurs.entry(c).or_insert(0) += 1;
    }
    let mut counts: Vec<u64> = occurs.values().map(|x| *x).collect();
    counts.sort();

    Ok(counts[counts.len()-1] - counts[0])
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 1588);
    Ok(())
}
