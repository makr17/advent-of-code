use std::cmp::{min, max};
use std::collections::HashSet;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

// realy an Axis-Aligned Plane
// but plain Plane is simpler to type
#[derive(Debug)]
struct Plane {
    align_to: char,
    align_value: i64,
}

// not really a _Cube_, more of a RectangularSolid
// but Cube is quicker to type...
#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
struct Cube {
    state: bool,
    x_start: i64,
    x_stop: i64,
    y_start: i64,
    y_stop: i64,
    z_start: i64,
    z_stop: i64,
}
impl Cube {
    fn new(txt: String) -> Self {
        let foo: Vec<&str> = txt.split(" ").collect();
        let state = if foo[0] == "on" { true } else { false };
        let rs: Vec<&str> = foo[1].split(",").collect();
        let mut xyz: Vec<i64> = vec![];
        for r in rs {
            let bar: Vec<&str> = r.split("=").collect();
            let mut st: Vec<i64> = bar[1].split("..")
                .map(|c| c.parse::<i64>().unwrap())
                .collect();
            xyz.append(&mut st);
        }

        Self {
            state,
            x_start: xyz[0],
            x_stop: xyz[1],
            y_start: xyz[2],
            y_stop: xyz[3],
            z_start: xyz[4],
            z_stop: xyz[5],
        }
    }

    fn count(&self) -> usize {
        ((self.x_stop - self.x_start + 1) * (self.y_stop - self.y_start + 1) * (self.z_stop - self.z_start + 1)) as usize
    }

    fn intersects(&self, other: &Self) -> bool {
        let x_start = max(self.x_start, other.x_start);
        let x_stop = min(self.x_stop, other.x_stop);
        if x_stop < x_start {
            return false;
        }
        let y_start = max(self.y_start, other.y_start);
        let y_stop = min(self.y_stop, other.y_stop);
        if y_stop < y_start {
            return false;
        }
        let z_start = max(self.z_start, other.z_start);
        let z_stop = min(self.z_stop, other.z_stop);
        if z_stop < z_start {
            return false;
        }
        ((x_stop - x_start + 1) * (y_stop - y_start + 1) * (z_stop - z_start + 1)) != 0
    }

    fn plane_intersects(&self, plane: &Plane) -> bool {
        if plane.align_to == 'x' {
            return self.x_start <= plane.align_value && self.x_stop >= plane.align_value;
        }
        else if plane.align_to == 'y' {
            return self.y_start <= plane.align_value && self.y_stop >= plane.align_value;
        }
        else if plane.align_to == 'z' {
            return self.z_start <= plane.align_value && self.z_stop >= plane.align_value;
        }
        false
    }

    fn split(&self, other: &Cube) -> Vec<Self> {
        let mut cubes: Vec<Self> = vec![];
        if !self.intersects(other) {
            cubes.push(*self);
            return cubes;
        }
        let mut remains = self.clone();
        // check 6 bounding planes of other
        // aligned with x_start
        if remains.plane_intersects(&Plane { align_to: 'x', align_value: other.x_start }) && other.x_start > remains.x_start {
            // chop off to the left of the intersecting plane
            let mut left = remains.clone();
            left.x_stop = other.x_start - 1;
            // and add it to the pieces we'll return
            cubes.push(left);
            // and truncate remains at the intersecting plane
            remains.x_start = other.x_start;            
        }
        // aligned with x_stop
        if remains.plane_intersects(&Plane { align_to: 'x', align_value: other.x_stop }) && other.x_stop < remains.x_stop {
            // same as above, but from the right rather than left
            let mut right = remains.clone();
            right.x_start = other.x_stop + 1;
            cubes.push(right);
            remains.x_stop = other.x_stop;            
        }
        // same as above, but reorient to y
        // aligned with y_start
        if remains.plane_intersects(&Plane { align_to: 'y', align_value: other.y_start }) && other.y_start > remains.y_start {
            let mut left = remains.clone();
            left.y_stop = other.y_start - 1;
            cubes.push(left);
            remains.y_start = other.y_start;            
        }
        // aligned with y_stop
        if remains.plane_intersects(&Plane { align_to: 'y', align_value: other.y_stop }) && other.y_stop < remains.y_stop {
            // same as above, but from the right rather than left
            let mut right = remains.clone();
            right.y_start = other.y_stop + 1;
            cubes.push(right);
            remains.y_stop = other.y_stop;            
        }
        // same as above but reorient to z
        // aligned with z_start
        if remains.plane_intersects(&Plane { align_to: 'z', align_value: other.z_start }) && other.z_start > remains.z_start {
            let mut left = remains.clone();
            left.z_stop = other.z_start - 1;
            cubes.push(left);
            remains.z_start = other.z_start;            
        }
        // aligned with z_stop
        if remains.plane_intersects(&Plane { align_to: 'z', align_value: other.z_stop }) && other.z_stop < remains.z_stop {
            // same as above, but from the right rather than left
            let mut right = remains.clone();
            right.z_start = other.z_stop + 1;
            cubes.push(right);
            remains.z_stop = other.z_stop;            
        }

        return cubes;
    }
}

#[test]
fn test_count() {
    let r1 = Cube{
        state: true,
        x_start: 5, x_stop: 10,
        y_start: 5, y_stop: 10,
        z_start: 5, z_stop: 10,
    };
    assert_eq!(r1.count(), 6 * 6 * 6);
    let r2 = Cube{
        state: true,
        x_start: -2, x_stop: 2,
        y_start: -2, y_stop: 2,
        z_start: -2, z_stop: 2,
    };
    assert_eq!(r2.count(), 5 * 5 * 5);
}

#[test]
fn test_intersects() {
    let r1 = Cube{
        state: true,
        x_start: 5, x_stop: 10,
        y_start: 5, y_stop: 10,
        z_start: 5, z_stop: 10,
    };
    let r2 = Cube{
        state: true,
        x_start: 5, x_stop: 6,
        y_start: 5, y_stop: 6,
        z_start: 5, z_stop: 6,
    };
    assert_eq!(r1.intersects(&r2), true);
    let r3 = Cube{
        state: true,
        x_start: 0, x_stop: 1,
        y_start: 0, y_stop: 1,
        z_start: 0, z_stop: 1,
    };
    assert_eq!(r1.intersects(&r3), false);
    assert_eq!(r2.intersects(&r3), false);    
}

#[test]
fn test_plane_intersects() {
    let r1 = Cube{
        state: true,
        x_start: 5, x_stop: 10,
        y_start: 5, y_stop: 10,
        z_start: 5, z_stop: 10,
    };
    assert_eq!(r1.plane_intersects(&Plane{ align_to: 'x', align_value: 7 }), true);
    assert_eq!(r1.plane_intersects(&Plane{ align_to: 'x', align_value: 5 }), true);
    assert_eq!(r1.plane_intersects(&Plane{ align_to: 'x', align_value: 4 }), false);
    assert_eq!(r1.plane_intersects(&Plane{ align_to: 'y', align_value: 7 }), true);
    assert_eq!(r1.plane_intersects(&Plane{ align_to: 'y', align_value: 5 }), true);
    assert_eq!(r1.plane_intersects(&Plane{ align_to: 'y', align_value: 4 }), false);}

#[test]
fn test_split() {
    let r1 = Cube{
        state: true,
        x_start: 5, x_stop: 10,
        y_start: 5, y_stop: 10,
        z_start: 5, z_stop: 10,
    };
    let r2 = Cube{
        state: true,
        x_start: 5, x_stop: 6,
        y_start: 5, y_stop: 6,
        z_start: 5, z_stop: 6,
    };
    let mut remains = r1.split(&r2);
    println!("{:?}", remains);
    assert_eq!(remains.len(), 3);
    let r3 = Cube{
        state: true,
        x_start: 7, x_stop: 8,
        y_start: 7, y_stop: 8,
        z_start: 7, z_stop: 8,
    };
    remains = r1.split(&r3);
    assert_eq!(remains.len(), 6);
}


fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    let mut cubes: HashSet<Cube> = HashSet::new();
    for line in input.lines() {
        let l = line?;
        if l.is_empty() {
            continue;
        }
        let cube = Cube::new(l);
        // can't mutate cubes while we iterate it
        let prevs: Vec<Cube> = cubes.iter().map(|c| c.clone()).collect();
        // remove any intersecions with existing objecs in our bag
        // replacing one object with N remaining after split
        for prev in prevs {
            if prev.intersects(&cube) {
                let remains = prev.split(&cube);
                cubes.remove(&prev);
                for new in remains {
                    cubes.insert(new);
                }
            }
        }
        cubes.insert(cube);
    }

    Ok(cubes.iter().filter(|r| r.state == true).map(|r| r.count()).sum())
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test-2758514936282235.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 2758514936282235);
    Ok(())
}

