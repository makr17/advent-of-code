use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

#[derive(Debug, Eq, PartialEq, Hash)]
struct Point {
    x: i32,
    y: i32,
    z: i32,
}

#[derive(Debug)]
struct Range {
    state: bool,
    x_start: i32,
    x_stop: i32,
    y_start: i32,
    y_stop: i32,
    z_start: i32,
    z_stop: i32,
}
impl Range {
    fn new(txt: String) -> Self {
        let foo: Vec<&str> = txt.split(" ").collect();
        let state = if foo[0] == "on" { true } else { false };
        let rs: Vec<&str> = foo[1].split(",").collect();
        let mut xyz: Vec<i32> = vec![];
        for r in rs {
            let bar: Vec<&str> = r.split("=").collect();
            let mut st: Vec<i32> = bar[1].split("..")
                .map(|c| c.parse::<i32>().unwrap())
                .collect();
            xyz.append(&mut st);
        }
        /*
        let normalized: Vec<i32> = xyz.iter()
            .map(|n| if n < &-50 { -50 } else if n > &50 { 50 } else { *n })
            .collect();
         */
        Range {
            state,
            x_start: xyz[0],
            x_stop: xyz[1],
            y_start: xyz[2],
            y_stop: xyz[3],
            z_start: xyz[4],
            z_stop: xyz[5],            
        }
    }
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let mut points: HashMap<Point, bool> = HashMap::new();
    for line in input.lines() {
        let l = line?;
        if l.is_empty() {
            continue;
        }
        //println!("{}", l);
        let range = Range::new(l);
        //println!("{:?}", range);
        for x in range.x_start ..= range.x_stop {
            if x > 50 || x < -50 { continue }
            for y in range.y_start ..= range.y_stop {
                if y > 50 || y < -50 { continue }
                for z in range.z_start ..= range.z_stop {
                    if z > 50 || z < -50 { continue }
                    *points.entry(Point{ x, y, z}).or_insert(false) = range.state;
                }
            }
        }
    }

    let mut on_count: u64 = 0;
    for v in points.values() {
        if *v {
            on_count += 1;
        }
    }
    Ok(on_count)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test-39.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 39);
    Ok(())
}

#[test]
fn test_input_2() -> io::Result<()> {
    let file = File::open("../test-590784.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 590784);
    Ok(())
}
