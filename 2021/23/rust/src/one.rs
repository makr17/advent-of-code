#[macro_use]
extern crate lazy_static;

use std::collections::{HashMap, HashSet};
#[allow(unused_imports)]
use std::fmt::{Display, Formatter, Result};

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
struct Position {
    x: usize,
    y: usize,
}
impl Position {
    fn distance(&self, other: &Self) -> u64 {
        ((self.x as i16 - other.x as i16).abs()
         + (self.y as i16 - other.y as i16).abs()) as u64
    }
}
impl std::fmt::Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug)]
struct Map {
    nodes: Vec<Position>,
    connections: HashMap<usize, HashSet<usize>>,
    units: Vec<Amphi>,
}
impl Map {
    fn new() -> Self {
        // all positions on the board
        let nodes = vec![
            Position { x: 0, y: 2 },
            Position { x: 1, y: 2 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: 1 },
            Position { x: 3, y: 2 },
            Position { x: 4, y: 0 },
            Position { x: 4, y: 1 },
            Position { x: 5, y: 2 },
            Position { x: 6, y: 0 },
            Position { x: 6, y: 1 },
            Position { x: 7, y: 2 },
            Position { x: 8, y: 0 },
            Position { x: 8, y: 1 },
            Position { x: 9, y: 2 },
            Position { x: 10, y: 2 }
        ];

        // empty map
        let mut map = Map {
            nodes,
            connections: HashMap::new(),
            units: vec![],
        };

        // initial open moves
        map.connect(&Position { x: 0, y: 2 }, &Position { x: 1, y: 2 });
        map.connect(&Position { x: 1, y: 2 }, &Position { x: 2, y: 1 });
        map.connect(&Position { x: 2, y: 1 }, &Position { x: 3, y: 2 });
        map.connect(&Position { x: 3, y: 2 }, &Position { x: 4, y: 1 });
        map.connect(&Position { x: 4, y: 1 }, &Position { x: 5, y: 2 });
        map.connect(&Position { x: 5, y: 2 }, &Position { x: 6, y: 1 });
        map.connect(&Position { x: 6, y: 1 }, &Position { x: 7, y: 2 });
        map.connect(&Position { x: 7, y: 2 }, &Position { x: 8, y: 1 });
        map.connect(&Position { x: 8, y: 1 }, &Position { x: 9, y: 2 });
        map.connect(&Position { x: 9, y: 2 }, &Position { x: 10, y: 2 });

        map
    }

    fn index_of(&self, n: &Position) -> usize {
        self.nodes.iter().position(|x| x == n).unwrap()
    }

    fn connect(&mut self, n1: &Position, n2: &Position) {
        let i = self.index_of(n1);
        let j = self.index_of(n2);
        self.connections.entry(i).or_insert(HashSet::new()).insert(j);
        self.connections.entry(j).or_insert(HashSet::new()).insert(i);
    }

    fn disconnect(&mut self, n1: &Position, n2: &Position) {
        let i = self.index_of(n1);
        let j = self.index_of(n2);
        self.connections.entry(i).or_insert(HashSet::new()).remove(&j);
        self.connections.entry(j).or_insert(HashSet::new()).remove(&i);
    }

    fn connections(&self, n: &Position) -> Vec<&Position> {
        let i = self.index_of(n);
        if !self.connections.contains_key(&i) {
            return vec![];
        }
        self.connections[&i].iter().map(|i| &self.nodes[*i]).collect()
    }

    fn complete(&self) -> bool {
        for p in self.units.iter() {
            if !p.home() {
                return false;
            }
        }
        true
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
struct Amphi {
    variety: char,
    pos: Position,
    // can't Copy a Vec
    target1: Position,
    target0: Position,
}
impl Amphi {
    fn new(variety: char, pos: Position, target: usize) -> Self {
       Self {
            variety,
            pos,
            target1: Position { x: target, y: 1 },
            target0: Position { x: target, y: 0 },
        }
    }

    fn home(&self) -> bool {
        if self.target1 == self.pos || self.target0 == self.pos {
            return true;
        }
        else {
            return false;
        }
    }
}

lazy_static! {
    static ref COSTS: HashMap<char, u64> = {
        let mut m = HashMap::new();
        m.insert('A', 1);
        m.insert('B', 10);
        m.insert('C', 100);
        m.insert('D', 1000);
        m
    };
}

#[derive(Clone, Copy, Debug)]
struct Move {
    unit: Amphi,
    pos: Position,
    cost: u64,
    adapted: u64
}
impl std::fmt::Display for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f,
               "move {} from {} -> {}, cost {}, adapted {}",
               self.unit.variety,
               self.unit.pos,
               self.pos,
               self.cost,
               self.adapted
        )
    }
}


fn main() {
    let mut map = Map::new();
    map.units.push(Amphi::new('C', Position { x: 2, y: 1 }, 6));
    map.units.push(Amphi::new('B', Position { x: 2, y: 0 }, 4));
    map.units.push(Amphi::new('D', Position { x: 4, y: 1 }, 8));
    map.units.push(Amphi::new('A', Position { x: 2, y: 0 }, 2));
    map.units.push(Amphi::new('A', Position { x: 6, y: 1 }, 2));
    map.units.push(Amphi::new('D', Position { x: 6, y: 0 }, 8));
    map.units.push(Amphi::new('B', Position { x: 8, y: 1 }, 4));
    map.units.push(Amphi::new('C', Position { x: 8, y: 0 }, 6));

    let energy = process(map);
    println!("{}", energy);
}

fn process(mut map: Map) -> u64 {
    let mut energy = 0_u64;
    let mut path: Vec<Move> = vec![];
    while !map.complete() {
        let mut moves: Vec<Move> = vec![];
        for p in &map.units {
            if p.home() {
                // already home
                continue;
            }
            for c in &map.connections(&p.pos) {
                // adapted cost by target
                let cost = p.pos.distance(c) * COSTS[&p.variety];
                let adapted = cost
                    + c.distance(&p.target1)
                    * COSTS[&p.variety];
                moves.push(Move { unit: *p, pos: **c, cost, adapted });
            }
        }
        // sort by cost of the move
        // use adapted cost as a tiebreaker
        moves.sort_unstable_by_key(|x| (x.cost, x.adapted));
        
        for m in moves.iter() {
            println!("{}", m);
        }
        
        let m = moves[0];
        println!("chosen move: {}", m);
        energy += m.cost;
        let mut unit = map.units.iter_mut().find(|x| **x == m.unit).unwrap();
        unit.pos = m.pos;
        let mut dis: Vec<&Position> = vec![];
        for c in map.connections(&m.pos).iter() {
            if **c != m.unit.target0 && **c != m.unit.target1 {
                dis.push(c);
            }
        }
        for c in dis.iter() {
            map.disconnect(&unit.pos, c);
        }
        
        path.push(m);
    }
    
    energy
}

/*
#################################################################
#####(0,2)(1,2)     (3,2)     (5,2)     (7,2)     (9,2)(a,2)#####
###############(2B1)#####(4C1)#####(6B1)#####(8D1)###############
          #####(2A0)#####(4D0)#####(6C0)#####(8A0)#####
          #############################################
*/
#[test]
fn test_input() {
    let mut map = Map::new();

    map.units.push(Amphi::new('B', Position { x: 2, y: 1 }, 4 ));
    map.units.push(Amphi::new('A', Position { x: 2, y: 0 }, 2 ));
    map.units.push(Amphi::new('C', Position { x: 4, y: 1 }, 6 ));
    map.units.push(Amphi::new('D', Position { x: 2, y: 0 }, 8 ));
    map.units.push(Amphi::new('B', Position { x: 6, y: 1 }, 4 ));
    map.units.push(Amphi::new('C', Position { x: 6, y: 0 }, 6 ));
    map.units.push(Amphi::new('D', Position { x: 8, y: 1 }, 8 ));
    map.units.push(Amphi::new('A', Position { x: 8, y: 0 }, 2 ));
        
    let energy = process(map);
    assert_eq!(energy, 12521);
}

