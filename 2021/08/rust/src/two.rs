use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use std::option::Option;


#[derive(Clone, Debug, Eq, PartialEq)]
struct Num {
    chars: String,
    num: Option<u32>,
    segments: HashSet<char>,
}
impl Num {
    fn new(input: &str) -> Num {
        let mut chars: Vec<char> = input.chars().collect();
        let segments: HashSet<char> = chars.clone().into_iter().collect();
        chars.sort();
        Num {
            chars: chars.iter().collect(),
            num: None,
            segments
        }
    }
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<i64, Box::<dyn Error>> {
    let total = 0;
    for line in input.lines() {
        let l = line?;
        let sides: Vec<&str> = l.trim().split("|").collect();
        let mut left: HashMap<String, Num> = HashMap::new();
        for x in sides[0].trim().split(" ") {
            let num = Num::new(x);
            left.insert(num.chars.clone(), num);
        }
        let right: Vec<Num> = sides[1].trim().split(" ")
            .map(|x| Num::new(x))
            .collect();

        let mut sets: Vec<HashSet<char>> = vec![HashSet::new(); 10];
        let tmpleft = left.clone();
        for key in left.keys() {
            let mut num = tmpleft.get(key).unwrap();
            // easy ones first
            num.num = match num.chars.len() {
                2 => Some(1),
                3 => Some(7),
                4 => Some(4),
                7 => Some(8),
                _ => None
            };
            if num.num.is_some() {
                sets[num.num.unwrap() as usize] = num.segments.clone();
            }
        }
        left = tmpleft.clone();

        // now we have enough data to match 0, 3, 6, 9
        let zero_test: HashSet<char> = sets[8].difference(&sets[4])
            .into_iter()
            .map(|x| *x)
            .collect();
        for key in left.keys() {
            let mut num = tmpleft.get(key).unwrap();
            if num.num.is_some() {
                continue;
            }
            num.num = match num.chars.len() {
                5 => {
                    let int1: HashSet<&char> = num.segments.intersection(&sets[1])
                        .into_iter()
                        .collect();
                    if int1.len() == sets[1].len() {
                        Some(3)
                    }
                    else {
                        None
                    }
                },
                6 => {
                    let int1: HashSet<&char> = num.segments.intersection(&sets[1])
                        .into_iter()
                        .collect();
                    if int1.len() == sets[1].len() {
                        let int0: HashSet<&char> = num.segments.intersection(&zero_test)
                            .into_iter()
                            .collect();
                        if int0.len() == zero_test.len() {
                            Some(0)
                        }
                        else {
                            Some(9)
                        }
                    }
                    else {
                        Some(6)
                    }
                },
                _ => None,                    
            };
            if num.num.is_some() {
                sets[num.num.unwrap() as usize] = num.segments.clone();
            }
        }
        left = tmpleft.clone();

        // finally 2 and 5
        let five_test: HashSet<char> = sets[9].difference(&sets[7])
            .into_iter()
            .map(|x| *x)
            .collect();
        for key in left.keys() {
            let mut num = tmpleft.get(key).unwrap();
            // skip things we've already mapped
            if num.num.is_some() {
                continue;
            }
            num.num = match num.chars.len() {
                5 => {
                    let int: HashSet<&char> = num.segments.intersection(&five_test)
                        .into_iter()
                        .collect();
                    if int.len() == five_test.len() {
                        Some(5)
                    }
                    else {
                        Some(2)
                    }   
                },
                _ => None,
            };
            if num.num.is_some() {
                sets[num.num.unwrap() as usize] = num.segments.clone();
            }
        }
        left = tmpleft.clone();
        //let digits: Vec<u32> = 
        break;
    }

    Ok(total)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 61229);
    Ok(())
}

