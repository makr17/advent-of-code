#!/usr/bin/env python3

import sys
import fileinput

def normalize(num):
    chars = [c for c in num]
    chars.sort()
    return chars

def chars(num):
    return set(normalize(num))

def found(mapping, revmap, num, value):
    # gah, can't hash a set
    l = list(num)
    l.sort()
    string = "".join(l)
    if string in mapping: return
    if value in revmap: return
    #print(f"{string}: {value}")
    mapping[string] = value
    revmap[value] = num

total = 0
for line in fileinput.input():
    (a,b) = line.strip().split('|')
    nums = [chars(num) for num in a.strip().split(' ')]
    mapping = {}
    revmap = {}
    # low hanging fruit first, these are distinct by len
    for num in nums:
        if len(num) == 2:
            found(mapping, revmap, num, 1)
        elif len(num) == 3:
            found(mapping, revmap, num, 7)
        elif len(num) == 4:
            found(mapping, revmap, num, 4)
        elif len(num) == 7:
            found(mapping, revmap, num, 8)
    zero_test = revmap[8] - revmap[4]
    # find mapping for 6 and 9 (9 intersects 1, 6 does not)
    for num in nums:
        if len(num) == 6:
            if num.intersection(revmap[1]) == revmap[1]:
                if num.intersection(zero_test) == zero_test:
                    found(mapping, revmap, num, 0)
                else:
                    found(mapping, revmap, num, 9)
            else:
                found(mapping, revmap, num, 6)
        if len(num) == 5:
            if num.intersection(revmap[1]) == revmap[1]:
                found(mapping, revmap, num, 3)
    #print(revmap)
    # segments in 9 minus segments in 7 will identify 5
    #print(f"{9}  {revmap[9]}")
    #print(f"{7}  {revmap[7]}")
    five_test = revmap[9] - revmap[7]
    #print(test)
    for num in nums:
        if len(num) == 5:
            if five_test.intersection(num) == five_test:
                found(mapping, revmap, num, 5)
            else:
                found(mapping, revmap, num, 2)

    digits = [mapping["".join(normalize(num))] for num in b.strip().split(" ")]
    value = int("".join([str(c) for c in digits]))
    print(value)
    total += value

print(total)
