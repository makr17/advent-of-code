#!/usr/bin/env python3

import fileinput

lens = {}
for line in fileinput.input():
    (a,b) = line.strip().split('|')
    #print(f"{a}    {b}")
    nums = b.strip().split(' ')
    #print(nums)
    for num in nums:
        l = len(num)
        if l in lens:
            lens[l] += 1
        else:
            lens[l] = 1

print(lens[2] + lens[3] + lens[4] + lens[7])

