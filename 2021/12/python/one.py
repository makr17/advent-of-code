#!/usr/bin/env python3

import fileinput

class Node:
    def __init__(self, label):
        self.label = label
        self.neighbors = []
        self.small = True
        for c in label:
            if c.isupper():
                self.small = False
                break
        #print(f"{label} small {self.small}")

    def connect(self, node):
        if node.label not in self.neighbors:
            self.neighbors.append(node.label)
        if self.label not in node.neighbors:
            node.neighbors.append(self.label)
            
    def neighbors(self):
        return self._ns.keys()


def traverse(nodes, path=["start"], paths={}):
    tail = path[-1]
    node = nodes[tail]
    for label in node.neighbors:
        n = nodes[label]
        if n.small and n.label in path:
            # can only visit small caves once
            continue
        # clone path for this branch
        newpath = path[:]
        newpath.append(n.label)
        if n.label == "end":
            paths[",".join(newpath)] = True
            continue
        else:
            paths = paths | traverse(nodes, newpath, paths)
    return paths
    
        
nodes = {}
for line in fileinput.input():
    # parse labels
    ns = line.strip().split('-')
    # create nodes if they don't already exist
    for label in ns:
        if not label in nodes:
            nodes[label] = Node(label)
    # and then connect them
    nodes[ns[0]].connect(nodes[ns[1]])

paths = traverse(nodes)
for path in paths.keys():
    print(path)
print(len(paths.keys()))
    
