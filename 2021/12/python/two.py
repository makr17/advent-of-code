#!/usr/bin/env python3

import fileinput

class Node:
    def __init__(self, label):
        self.label = label
        self.neighbors = []
        self.small = True
        for c in label:
            if c.isupper():
                self.small = False
                break
        #print(f"{label} small {self.small}")

    def connect(self, node):
        if node.label not in self.neighbors:
            self.neighbors.append(node.label)
        if self.label not in node.neighbors:
            node.neighbors.append(self.label)
            
    def neighbors(self):
        return self._ns.keys()


def traverse(nodes, path=["start"], paths={}):
    tail = path[-1]
    node = nodes[tail]
    for label in node.neighbors:
        n = nodes[label]
        if n.small and n.label in path:
            if n.label == "start" or n.label == "end":
                # start and end can only be visited once
                continue
            # are there any _other_ small caves in the path already?
            uniq = {}
            for l in path:
                if l in uniq:
                    uniq[l] += 1
                else:
                    uniq[l] = 1
            bail = False
            for l in uniq.keys():
                test = nodes[l]
                if test.small and uniq[l] > 1:
                    bail = True
                    break
            if bail:
                continue
        # clone path for this branch
        newpath = path[:]
        newpath.append(n.label)
        if n.label == "end":
            paths[",".join(newpath)] = True
            continue
        else:
            paths = paths | traverse(nodes, newpath, paths)
    return paths
    
        
nodes = {}
for line in fileinput.input():
    # parse labels
    ns = line.strip().split('-')
    # create nodes if they don't already exist
    for label in ns:
        if not label in nodes:
            nodes[label] = Node(label)
    # and then connect them
    nodes[ns[0]].connect(nodes[ns[1]])

paths = traverse(nodes)
for path in paths.keys():
    print(path)
print(len(paths.keys()))
    
