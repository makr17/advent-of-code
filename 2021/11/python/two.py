#!/usr/bin/env python3

import fileinput

class Point:
    def __init__(self, x, y, energy, grid):
        self.energy = energy
        self.flashed = False
        self.neighbors = []
        if x > 0:
            self.neighbors.append((x-1, y))
        if x < grid.width - 1:
            self.neighbors.append((x+1, y))
        if y > 0:
            self.neighbors.append((x, y-1))
        if y < grid.height - 1:
            self.neighbors.append((x, y+1))
        if x > 0 and y > 0:
            self.neighbors.append((x-1, y-1))
        if x > 0 and y < grid.height - 1:
            self.neighbors.append((x-1, y+1))
        if x < grid.width - 1 and y > 0:
            self.neighbors.append((x+1, y-1))
        if x < grid.width - 1 and y < grid.height - 1:
            self.neighbors.append((x+1, y+1))
        self.grid = grid

    def get_neighbors(self):
        return [self.grid.grid[y][x] for (x,y) in self.neighbors]
        
    def flash(self):
        if self.energy <= 9:
            return
        if self.flashed:
            return
        self.flashed = True
        for n in self.get_neighbors():
            n.energy += 1
            n.flash()
        self.grid.flashed += 1

    def reset(self):
        if self.flashed:
            self.energy = 0
            self.flashed = False


class Grid:
    def __init__(self,rows):
        self.flashed = 0
        self.height = len(rows)
        self.width = len(rows[0])
        grid = []
        for y in range(0, self.height):
            row = []
            for x in range(0, self.width):
                p = Point(x, y, rows[y][x], self)
                row.append(p)
            grid.append(row)
        self.grid = grid

    def get(self, x, y):
        return self.grid[y][x]

    def render(self):
        for row in self.grid:
            print("".join([str(p.energy) for p in row]))
        print(f"flashed={self.flashed}")
        print()

rows = []
for line in fileinput.input():
    points = [int(x) for x in line.strip()]
    #print(points)
    rows.append(points)
grid = Grid(rows)

#grid.render()
step = 0;
while True:
    step += 1
    start = grid.flashed
    # bump energy levels
    for y in range(0, grid.height):
        for x in range(0, grid.width):
            point = grid.get(x, y)
            point.energy += 1
    # then see who flashes
    for y in range(0, len(rows)):
        for x in range(0, len(rows[y])):
            point = grid.get(x, y)
            point.flash()
    # then reset all flashed cells to 0
    for y in range(0, len(rows)):
        for x in range(0, len(rows[y])):
            point = grid.get(x, y)
            point.reset()

    if grid.flashed - start == grid.width * grid.height:
        print(step)
        break
