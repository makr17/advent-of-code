use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

mod grid;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let lines: Vec<String> = input.lines().map(|line| line.unwrap()).collect();
    let mut grid = grid::Grid::new(
        lines[0].len(),
        lines.len(),
        grid::Point::new(0)
    );
    for (y, line) in lines.iter().enumerate() {
        for (x, c) in line.chars().enumerate() {
            let energy = c.to_digit(10).unwrap();
            let p = grid.get_mut((x,y)).unwrap();
            p.energy = energy;
        }
    }
    // bump energy levels
    for x in 0 .. grid.cols {
        for y in 0 .. grid.rows {
            let p = grid.get_mut((x,y)).unwrap();
            p.energy += 1
        }
    }

    // see who flashes
    for x in 0 .. grid.cols {
        for y in 0 .. grid.rows {
            let p = grid.get_mut((x,y)).unwrap();
            p.flash((x,y), &mut grid)
        }
    }
    
    // reset all flashed cells to 0 energy
    for x in 0 .. grid.cols {
        for y in 0 .. grid.rows {
            let p = grid.get_mut((x,y)).unwrap();
            if p.flashed {
                p.reset()
            }
        }
    }

    Ok(0)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 1656);
    Ok(())
}


