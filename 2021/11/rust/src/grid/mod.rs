use std::cmp;

pub type Coordinate = (usize, usize);

#[derive(Debug)]
pub struct Grid<T> {
    pub cols: usize,
    pub rows: usize,
    data: Vec<T>,
    pub flashed: u32,
}
impl<T> Grid<T>
where
    T: Copy,
{
    pub fn new(cols: usize, rows: usize, default: T) -> Self {
        Self {
            cols,
            rows,
            data: vec![default; cols * rows],
            flashed: 0,
        }
    }

    pub fn capacity(&self) -> usize {
        self.rows * self.cols
    }

    pub fn height(&self) -> usize {
        self.rows
    }

    pub fn width(&self) -> usize {
        self.cols
    }

    pub fn get_mut(&mut self, pos: Coordinate) -> Option<&mut T> {
        if self.valid(pos) {
            let idx = self.index(pos);
            return Some(&mut self.data[idx]);
        }
        None
    }

    pub fn get(&self, pos: Coordinate) -> Option<&T> {
        if self.valid(pos) {
            return Some(&self.data[self.index(pos)]);
        }
        None
    }

    pub fn set(&mut self, pos: Coordinate, val: T) {
        if !self.valid(pos) {
            return;
        }
        let idx = self.index(pos);
        self.data[idx] = val;
    }
    
    fn valid (&self, (x, y): Coordinate) -> bool {
        x < self.cols && y < self.rows
    }

    fn index(&self, (x, y): Coordinate) -> usize {
        return x + y * self.cols
    }

    fn neighbors(&self, (x, y): Coordinate) -> Vec<Coordinate> {
        let mut n: Vec<Coordinate> = vec![];
        for xn in cmp::max(x-1, 0) .. cmp::min(x+1, self.cols) {
            for yn in cmp::max(y-1, 0) .. cmp::min(y+1, self.rows) {
                n.push((xn, yn));
            }
        }
        n
    }
}

#[test]
fn test_new() {
    let mut grid = Grid::new(10, 15, 0);
    assert_eq!(150, grid.capacity());
    assert_eq!(15, grid.height());
    assert_eq!(10, grid.width());
}

#[derive(Copy, Clone, Debug)]
pub struct Point {
    pub energy: u32,
    pub flashed: bool,
}
impl Point {
    pub fn new(energy: u32) -> Self {
        Self {
            energy,
            flashed: false,
        }
    }

    pub fn flash(&mut self, pos: Coordinate, grid: &mut Grid<Point>) {
        // not enough energy
        if self.energy <= 9 {
            return;
        }
        // already flashed
        if self.flashed {
            return
        }
        self.flashed = true;
        for coord in grid.neighbors(pos) {
            let mut n = grid.get_mut(coord).unwrap();
            n.energy += 1;
            n.flash(coord, grid);
        }
        grid.flashed += 1;
    }

    pub fn reset(&mut self) {
        if self.flashed {
            self.energy = 0;
            self.flashed = false;
        }
    }
}
