#!/usr/bin/env python3

from pathlib import Path

from dirac import QuantumDie, QuantumGame, Player

def run():
        txt = Path("input.txt").read_text()
        die = QuantumDie()
        players = []
        for line in txt.split("\n"):
            if line == "":
                continue
            (_, pos) = line.split(": ")
            players.append(Player(die, int(pos)))
        #for player in players:
        #    print(str(player))

        game = QuantumGame(die, players)
        wins = game.play()

        print(wins)

    
if __name__ == "__main__":
    run()
