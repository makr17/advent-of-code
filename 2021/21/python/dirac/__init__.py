from collections import defaultdict

class DeterministicDie:
    def __init__(self):
        self.rolled = 0

    def roll(self):
        self.rolled += 1
        roll = (self.rolled - 1 % 100) + 1
        return roll
    
class Player:
    def __init__(self, die, pos):
        self.die = die
        self.pos = pos
        self.score = 0

    def play(self):
        move = 0
        for i in range(0, 3):
            move += self.die.roll()
        # ring indexes from 0
        # board indexes from 1
        # so subtract 1 from pos to get ring position
        # mod by 10
        # and add back 1 to get board position
        self.pos = ((self.pos - 1 + move) % 10) + 1
        self.score += self.pos

    def __str__(self):
        return f"pos: {self.pos}  score: {self.score}"

class Game:
    def __init__(self, die, players):
        self.die = die
        self.players = players

    def play(self):
        i = 0
        while max([p.score for p in self.players]) < 1000:
            self.players[i % len(self.players)].play()
            i += 1

        return

class QuantumDie:
    def __init__(self):
        self.rolled = 0
        self.rolls = [
            sum((x, y, z))
            for z in (1, 2, 3) for y in (1, 2, 3) for x in (1, 2, 3)
        ]

    def roll(self):
        return self.rolls

class QuantumGame:
    def __init__(self, die, players):
        # mostly doesn't matter
        # we're going to be tracking branching states locally in play()
        self.die = die
        self.players = players
        
    def play(self):
        states = defaultdict(int)
        # key by tuple of p1/p2 positions and p1/p2 scores
        states[(self.players[0].pos, self.players[1].pos, 0, 0)] = 1
        p1_wins = 0
        p2_wins = 0
        # we don't care about how many die rolls in this version
        # so inline to avoid umpteen method calls
        rolls = self.die.roll()
        print(rolls)
        while len(states) > 0:
           # player 1 goes first
            new_states = defaultdict(int)
            for state, count in states.items():
                start_tup = state
                for move in rolls:
                    (p1pos, p2pos, p1score, p2score) = start_tup
                    p1pos = ((p1pos - 1 + move) % 10) + 1
                    p1score += p1pos
                    if p1score >= 21:
                        p1_wins += count
                    else:
                        tup = (p1pos, p2pos, p1score, p2score)
                        new_states[tup] += count
            states = new_states
            # then player 2
            new_states = defaultdict(int)
            for state, count in states.items():
                start_tup = state
                for move in rolls:
                    (p1pos, p2pos, p1score, p2score) = start_tup
                    p2pos = ((p2pos - 1 + move) % 10) + 1
                    p2score += p2pos
                    if p2score >= 21:
                        p2_wins += count
                    else:
                        tup = (p1pos, p2pos, p1score, p2score)
                        new_states[tup] += count
            states = new_states

        max_wins = max(p1_wins, p2_wins)
        print(f"max wins: {max_wins}  p1 wins: {p1_wins}  p2 wins: {p2_wins}")
        return max_wins
