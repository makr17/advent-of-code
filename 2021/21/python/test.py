#!/usr/bin/env python3

from pathlib import Path
import unittest

from dirac import DeterministicDie, Game, Player, QuantumDie, QuantumGame

class TestDirac(unittest.TestCase):
    def test_deterministic(self):
        txt = Path("test.txt").read_text()
        die = DeterministicDie()
        players = []
        for line in txt.split("\n"):
            if line == "":
                continue
            (_, pos) = line.split(": ")
            players.append(Player(die, int(pos)))
        #for player in players:
        #    print(str(player))

        game = Game(die, players)
        game.play()

        loss = min([p.score for p in game.players])
        rolled = game.die.rolled

        self.assertEqual(loss * rolled, 739785)

    def test_quantum(self):
        txt = Path("test.txt").read_text()
        die = QuantumDie()
        players = []
        for line in txt.split("\n"):
            if line == "":
                continue
            (_, pos) = line.split(": ")
            players.append(Player(die, int(pos)))
        game = QuantumGame(die, players)
        wins = game.play()

        self.assertEqual(wins, 444356092776315)
        
   
if __name__ == "__main__":
    unittest.main()
