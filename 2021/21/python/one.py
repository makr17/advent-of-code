#!/usr/bin/env python3

from pathlib import Path

from dirac import DeterministicDie, Game, Player

def run():
        txt = Path("input.txt").read_text()
        die = DeterministicDie()
        players = []
        for line in txt.split("\n"):
            if line == "":
                continue
            (_, pos) = line.split(": ")
            players.append(Player(die, int(pos)))
        #for player in players:
        #    print(str(player))

        game = Game(die, players)
        game.play()

        loss = min([p.score for p in game.players])
        rolled = game.die.rolled

        print(loss * rolled)

    
if __name__ == "__main__":
    run()
