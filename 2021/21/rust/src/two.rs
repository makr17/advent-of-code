use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
struct Player {
    pos: u8,
    score: u8,
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let mut players: Vec<Player> = vec![];
    for line in input.lines() {
        let l = line?;
        if l.is_empty() {
            continue;
        }
        let pos: Vec<&str> = l.split(": ").collect();
        players.push(
            Player {
                pos: pos[1].parse::<u8>().unwrap(),
                score: 0
            }
        );
    }
    
    let mut wins: Vec<u64> = vec![0, 0];
    let mut states: HashMap<Vec<Player>, u64> = HashMap::new();
    states.insert(vec![players[0], players[1]], 1);

    let rolls: Vec<u8> = vec![
        3, 4, 5,  4, 5, 6,  5, 6, 7,
        4, 5, 6,  5, 6, 7,  6, 7, 8,
        5, 6, 7,  6, 7, 8,  7, 8, 9
    ];
    while !states.is_empty() {
        // player 0 first
        let mut new_states: HashMap<Vec<Player>, u64> = HashMap::new();
        for (state, count) in states {
            for roll in &rolls {
                let mut player = state[0];
                player.pos = ((player.pos - 1 + roll) % 10) + 1;
                player.score += player.pos;
                if player.score >= 21 {
                    wins[0] += count;
                }
                else {
                    *new_states.entry(vec![player, state[1]]).or_insert(0) += count;
                }
            }
        }
        states = new_states;
        // then player 1
        new_states = HashMap::new();
        for (state, count) in states {
            for roll in &rolls {
                let mut player = state[1];
                player.pos = ((player.pos - 1 + roll) % 10) + 1;
                player.score += player.pos;
                if player.score >= 21 {
                    wins[1] += count;
                }
                else {
                    *new_states.entry(vec![state[0], player]).or_insert(0) += count;
                }
            }
        }
        states = new_states;        
    }
    
    wins.sort();
    Ok(wins[1])
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 444356092776315);
    Ok(())
}

