use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let count = process(reader);
    println!("{}", count.unwrap());
    Ok(())
}

fn process(input: BufReader<File>) ->Result<i32, Box::<dyn Error>> {
    let mut incr = 0;
    let mut stack: Vec<i32> = vec![];
    for line in input.lines() {
        let val = line?.parse::<i32>()?;
        stack.push(val);
        if stack.len() == 3 {
            stack.remove(0);
        }
        if stack.len() == 2 && stack[1] > stack[0] {
            incr += 1;
        }
    }
    Ok(incr)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 7);
    Ok(())
}
