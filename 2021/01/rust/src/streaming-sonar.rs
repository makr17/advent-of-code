use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let count = process(reader);
    println!("{}", count.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut stack: Vec<i32> = vec![];
    let mut incr = 0;
    for line in input.lines() {
        let val = line?.trim().parse::<i32>()?;
        stack.push(val);
        // remove elements that we don't need anymore
        // essentially, make stack a fixed depth of 4
        if stack.len() == 5 {
            stack.remove(0);
        }
        // once we have 4 elements in the vec we can start comparing sums
        if stack.len() == 4 {
            let prev: i32 = stack[0..3].iter().sum();
            let curr: i32 = stack[1..4].iter().sum();
            if curr > prev {
                incr += 1;
            }
        }
    }

    Ok(incr)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 5);
    Ok(())
}
