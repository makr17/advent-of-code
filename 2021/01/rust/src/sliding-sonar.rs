use std::io;
use std::io::prelude::*;

fn main() {
    let stdin = io::stdin();

    let lines: Vec<i32> = stdin.lock().lines()
        .map(|x| x.unwrap().trim().parse::<i32>().unwrap())
        .collect();

    let mut incr = 0;
    for i in 3..lines.len() {
        let prev: i32 = lines[i-3..i].iter().sum();
        let curr: i32 = lines[i-2..i+1].iter().sum();
        //println!("{}  {}", prev, curr);
        if curr > prev {
            incr += 1;
        }
    }
    println!("{}", incr);
}
