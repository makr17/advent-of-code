#!/usr/bin/env python3

import fileinput

seen = []
increased = 0
for line in fileinput.input():
    line.rstrip()
    seen.append(int(line))
    size = len(seen)
    if size >= 4 and sum(seen[size-3 : size]) > sum(seen[size-4 : size-1]):
        increased += 1

print(increased)
