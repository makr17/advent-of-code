#!/usr/bin/env python3

import fileinput

last = None
increased = 0
for line in fileinput.input():
    line.rstrip()
    depth = int(line);
    if last and last < depth:
        increased += 1
    last = depth

print(increased)
