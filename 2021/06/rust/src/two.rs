use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(mut input: BufReader<File>) -> Result<i64, Box::<dyn Error>> {
    let mut line = String::new();
    input.read_line(&mut line)?;
    let ages: Vec<i32> = line.trim().split(',')
        .map(|x| x.parse::<i32>().unwrap())
        .collect();
    let mut pop: HashMap<i32, i64> = HashMap::new();
    for age in ages {
        let old = pop.remove(&age).unwrap_or(0);
        pop.insert(age, old + 1);
    }

    for _step in 0..256 {
        let mut new: HashMap<i32, i64> = HashMap::new();
        for (age, count) in pop.iter() {
            let newage = age - 1;
            new.insert(newage, *count);
        }
        pop = new;
        let birth = pop.remove(&-1).unwrap_or(0);
        pop.insert(8, birth);
        let curr = pop.remove(&6).unwrap_or(0);
        pop.insert(6, curr + birth);
    }

    let count = pop.values().sum();
    Ok(count)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 26984457539);
    Ok(())
}

