#!/usr/bin/env python3

import fileinput

detail = [int(x) for x in fileinput.input().readline().strip().split(",")]
fish = {}
for f in detail:
    if f not in fish:
        fish[f] = 0
    fish[f] += 1
print(fish)

for day in range(0, 256):
    new = {}
    for age in fish.keys():
        aged = age - 1
        new[aged] = fish[age]
    #print(f" new {new}")
    fish = new
    if -1 in fish:
        fish[8] = fish[-1]
        if 6 not in fish:
            fish[6] = 0
        fish[6] += fish.pop(-1)
    #print(f"fish {fish}")

population = 0
for count in fish.values():
    population += count
print(population)
