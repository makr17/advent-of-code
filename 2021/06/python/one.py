#!/usr/bin/env python3

import fileinput

fish = [int(x) for x in fileinput.input().readline().strip().split(",")]
print(f"Initial state: {fish}")

for day in range(1, 81):
    new = []
    for i in range(0, len(fish)):
        fish[i] -= 1
        if fish[i] == -1:
            fish[i] = 6
            new.append(8)
    fish += new
    #print(f"After {day} days: {fish}")

print(f"{len(fish)} fish")
