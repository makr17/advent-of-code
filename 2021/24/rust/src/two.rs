use std::cmp::Eq;
use std::collections::HashSet;
use std::error::Error;
use std::fs::File;
use std::hash::{Hash, Hasher};
use std::io::{self, prelude::*, BufReader};
use std::ops::{Index, IndexMut};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone, Debug)]
struct ALU {
    w: i64,
    x: i64,
    y: i64,
    z: i64,
    path: Vec<u8>,
}
impl PartialEq for ALU {
    fn eq(&self, other: &Self) -> bool {
        self.w == other.w && self.x == other.x && self.y == other.y && self.z == other.z
    }
}
impl Eq for ALU {}
impl Hash for ALU {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.w.hash(state);
        self.x.hash(state);
        self.y.hash(state);
        self.z.hash(state);
    }
}
// so we can index the structure dynamically by fieldname
impl Index<&str> for ALU {
    type Output = i64;
    fn index(&self, s: &str) -> &i64 {
        match s {
            "w" => &self.w,
            "x" => &self.x,
            "y" => &self.y,
            "z" => &self.z,
            _ => panic!("unknown field: {}", s),
        }
    }
}
impl IndexMut<&str> for ALU {
    fn index_mut(&mut self, s: &str) -> &mut i64 {
        match s {
            "w" => &mut self.w,
            "x" => &mut self.x,
            "y" => &mut self.y,
            "z" => &mut self.z,
            _ => panic!("unknown field: {}", s),
        }
    }
}
impl ALU {
    fn new() -> Self {
        ALU {
            w: 0,
            x: 0,
            y: 0,
            z: 0,
            path: vec![],
        }
    }
    // join the path to build 14-digit numeric value
    fn value(&self) -> u64 {
        if self.path.len() == 0 {
            return 0;
        }
        let digits: Vec<String> = self.path.iter()
            .map(|d| d.to_string())
            .collect();
        let prefix: &str = &digits.join("");
        let base = prefix.parse::<u64>().unwrap();
        let power = (14 - self.path.len()).try_into().unwrap();
        base * 10_u64.pow(power)
    }

    fn execute(&mut self, l: String) -> Vec<ALU> {
        let chunks: Vec<&str> = l.split(" ").collect();
        // inp a - Read an input value and write it to variable a.
        if chunks[0] == "inp" {
            let mut ret: Vec<ALU> = vec![];
            // can't be 0
            for d in 1 .. 10 {
                let mut a = self.clone();
                a[chunks[1]] = d;
                // add digit to the path
                a.path.push(d as u8);
                ret.push(a);
            }
            return ret;
        }

        let val: i64 = match chunks[2] {
            "w" => self.w,
            "x" => self.x,
            "y" => self.y,
            "z" => self.z,
            _ => chunks[2].parse::<i64>().unwrap(),
        };

        match chunks[0] {
            "add" => self[chunks[1]] += val,
            // mul a b - Multiply the value of a by the value of b, then store the result in variable a.
            "mul" => self[chunks[1]] *= val,
            // div a b - Divide the value of a by the value of b, truncate the result to an integer, then store the result in variable a. (Here, "truncate" means to round the value toward zero.)
            "div" => self[chunks[1]] /= val,
            // mod a b - Divide the value of a by the value of b, then store the remainder in variable a. (This is also called the modulo operation.)
            "mod" => self[chunks[1]] = self[chunks[1]] % val,
            // eql a b - If the value of a and b are equal, then store the value 1 in variable a. Otherwise, store the value 0 in variable a.
            "eql" => {
                if self[chunks[1]] == val {
                    self[chunks[1]] = 1
                }
                else {
                    self[chunks[1]] = 0
                }
            }
            _ => println!("unknown op: {}", chunks[1]),
        }

        return vec![self.clone()]
    }
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let mut states: HashSet<ALU> = HashSet::new();
    states.insert(ALU::new());
    let mut i = 0;
    for line in input.lines() {
        println!("line {}: {} ALU states", i, states.len());
        let l = line?;
        if l.is_empty() {
            continue;
        }
        let mut new_states: HashSet<ALU> = HashSet::new();
        for alu in states.iter() {
            let new_alus = alu.clone().execute(l.clone());
            for nalu in new_alus.iter() {
                let value = nalu.value();
                // replace the value if we have something smaller
                // tracking the smallest path/value that led to this state
                if match new_states.get(&nalu) {
                    Some(v) => v.value(),
                    None => u64::MAX,
                } > value {
                    new_states.remove(&nalu);
                    new_states.insert(nalu.clone());
                }
            }
        }
        states = new_states;
        i += 1;
    }

    let mut nums: Vec<u64> = states.iter()
        .filter(|k| k.z == 0)  // valid
        .map(|k| k.value())    // extract the "path" 14-digit sn
        .collect();
    nums.sort_unstable();      // sort them
    Ok(nums[0])                // and take the smallest (first) value
}

#[test]
fn test_alu() {
    let mut alu = ALU::new();
    alu.w = 2;
    alu.x = 4;
    alu.y = 8;
    alu.z = 16;
    alu.execute("mul w 4".to_string());
    assert_eq!(alu.w, 8);
    alu.execute("eql w y".to_string());
    assert_eq!(alu.w, 1);
    alu.execute("eql w z".to_string());
    assert_eq!(alu.w, 0);
    alu.execute("add w y".to_string());
    assert_eq!(alu.w, 8);
    alu.execute("div w x".to_string());
    assert_eq!(alu.w, 2);
    alu.execute("mod z 7".to_string());
    assert_eq!(alu.z, 2);
}

#[test]
fn test_alu_eq_hash() {
    let mut a1 = ALU::new();
    a1.x = 1;
    a1.z = 12;

    let mut a2 = ALU::new();
    a2.x = 1;
    a2.z = 12;
    println!("{:?}", a2);
    assert_eq!(a1, a2);
    a2.y = 7;
    assert_eq!(a1 == a2, false);
}
