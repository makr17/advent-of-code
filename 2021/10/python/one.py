#!/usr/bin/env python3

import fileinput

reps = [
    "[]",
    "{}",
    "()",
    "<>"
]

matches = {
    "[": "]",
    "{": "}",
    "(": ")",
    "<": ">"
}

points = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137
}

def reduce(line):
    l = len(line)
    #print(line)
    test = True
    while test:
        for rep in reps:
            line = line.replace(rep, "")
            #print(f"{rep}: {line}")
        if len(line) == l:
            test = False
        else:
            l = len(line)
    return line
            
total = 0
for line in fileinput.input():
    remain = reduce(line.strip())
    matched = {}
    for close in matches.values():
        pos = remain.find(close)
        if pos != -1:
            matched[close] = pos
    if len(matched):
        possible = list(matched.values())
        possible.sort()
        bad = remain[possible[0]]
        #sprint(bad)
        total += points[bad]
print(total)
