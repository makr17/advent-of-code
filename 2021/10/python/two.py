#!/usr/bin/env python3

import fileinput

reps = [
    "[]",
    "{}",
    "()",
    "<>"
]

matches = {
    "[": "]",
    "{": "}",
    "(": ")",
    "<": ">"
}

points = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4
}

def reduce(line):
    l = len(line)
    #print(line)
    test = True
    while test:
        for rep in reps:
            line = line.replace(rep, "")
            #print(f"{rep}: {line}")
        if len(line) == l:
            test = False
        else:
            l = len(line)
    return line

scores = []
for line in fileinput.input():
    remain = reduce(line.strip())
    matched = {}
    for close in matches.values():
        pos = remain.find(close)
        if pos != -1:
            matched[close] = pos
    if len(matched):
        # corrupt, skip
        continue

    score = 0
    for char in remain[::-1]:
        score = score * 5
        score += points[matches[char]]
    scores.append(score)
        
scores.sort()
#print(scores)
print(scores[int(len(scores)/2)])
