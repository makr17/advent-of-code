use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

//use regex::Regex;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct Point {
    x: usize,
    y: usize,
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());

    Ok(())
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let mut scores: Vec<u64> = vec![];
    for line in input.lines() {
        let l = line?;
        let reduced = reduce(l);
        //println!("{}", reduced);
        //println!();
        let closers: Vec<i32> = ["]","}",")",">"].iter().
            map(|c| match reduced.find(c) {
                Some(num) => num as i32,
                None => -1,
            })
            .filter(|&x| x != -1)
            .collect();
        if closers.len() > 0 {
            // corrupt, skip the line
            continue;
        }
        //println!("{}", reduced);
        let mut score = 0;
        for c in reduced.chars().rev() {
            score *= 5;
            score += match c {
                '(' => 1,
                '[' => 2,
                '{' => 3,
                '<' => 4,
                _ => 0,
            };
        }
        //println!("{}", score);
        scores.push(score);
    }
    scores.sort();
    Ok(scores[scores.len()/2 as usize])
}

fn reduce(mut line: String) -> String {
    let mut start = line.len();
    let test = true;
    //let re = Regex::new(r"(\[\]\|\{\}|\(\)|<>)").unwrap();
    while test {
        //line = re.replace_all(&line, "").to_string();
        line = str::replace(&line, "[]", "");
        line = str::replace(&line, "{}", "");
        line = str::replace(&line, "()", "");
        line = str::replace(&line, "<>", "");
        //println!("{}", line);
        if line.len() == start {
            break;
        }
        start = line.len()
    }
    line
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 288957);
    Ok(())
}


