use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

//use regex::Regex;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct Point {
    x: usize,
    y: usize,
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let mut score = 0;
    for line in input.lines() {
        let l = line?;
        let reduced = reduce(l);
        //println!("{}", reduced);
        //println!();
        let mut indexes: Vec<i32> = ["]","}",")",">"].iter().
            map(|c| match reduced.find(c) {
                Some(num) => num as i32,
                None => -1,
            })
            .filter(|&x| x != -1)
            .collect();
        indexes.sort();
        //println!("{}: {:?}", reduced, indexes);
        if indexes.len() > 0 {
            let idx = indexes[0] as usize;
            score += match &reduced[idx..idx+1] {
                ")" => 3,
                "]" => 57,
                "}" => 1197,
                ">" => 25137,
                _ => 0,
            };
        }
    }
    
    Ok(score)
}

fn reduce(mut line: String) -> String {
    let mut start = line.len();
    let test = true;
    //let re = Regex::new(r"(\[\]\|\{\}|\(\)|<>)").unwrap();
    while test {
        //line = re.replace_all(&line, "").to_string();
        line = str::replace(&line, "[]", "");
        line = str::replace(&line, "{}", "");
        line = str::replace(&line, "()", "");
        line = str::replace(&line, "<>", "");
        //println!("{}", line);
        if line.len() == start {
            break;
        }
        start = line.len()
    }
    line
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 26397);
    Ok(())
}


