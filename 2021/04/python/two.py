#!/usr/bin/env python3

import sys

class Spot:
    played = False
    
    def __init__(self, num):
        self.num = num

    def __str__(self):
        num = '{:2d}'.format(self.num)
        if self.played:
            return f"*{num}*"
        else:
            return f" {num} "


class Board:

    def __init__(self, nums):
        self.spotmap = {}
        for y in range(0,5):
            for x in range(0,5):
                #print(f"({x},{y})")
                num = nums[y][x]
                nums[y][x] = Spot(num)
                self.spotmap[num] = (x,y)
        self.nums = nums
        #print(str(self))

    def __str__(self):
        ret = ""
        for row in self.nums:
            ret += " ".join([str(x) for x in row])
            ret += "\n"
        return ret

    def play(self, num):
        if num in self.spotmap:
            (x,y) = self.spotmap[num]
            if self.nums[y][x].num != num:
                print(f"oops ({x},{y}) holds {self.nums[y][x].num} not {num}")
                sys.exit()
            self.nums[y][x].played = True

    def winner(self):
       # check horizontals
        for y in range(0,5):
            #print(f"y={y}")
            if self.nums[y][0].played and self.nums[y][1].played and self.nums[y][2].played and self.nums[y][3].played and self.nums[y][4].played:
                return True
        # check verticals
        for x in range(0,5):
            #print(f"x={x}")
            if self.nums[0][x].played and self.nums[1][x].played and self.nums[2][x].played and self.nums[3][x].played and self.nums[4][x].played:
                return True
        # check down diagonal
        #print("down diag")
        #if self.nums[0][0].played and self.nums[1][1].played and self.nums[2][2].played and self.nums[3][3].played and self.nums[4][4].played:
        #    return True
        # and the up diagonal
        #print("up diag")
        #if self.nums[0][4].played and self.nums[1][3].played and self.nums[2][2].played and self.nums[3][1].played and self.nums[4][0].played:
        #    return True
        return False
        
    def score(self, num):
        total = 0
        for y in range(0,5):
            for x in range(0,5):
                if self.nums[y][x].played:
                    continue
                total += self.nums[y][x].num
        print(total)
        print(num)
        return total * num                    

    
nums = sys.stdin.readline().rstrip()
nums = [int(x) for x in nums.split(",")]    

boards = []
while sys.stdin.readline():
    lines = []
    for i in range(0,5):
        line = sys.stdin.readline().rstrip()
        row = [int(line[0:2]), int(line[3:5]), int(line[6:8]), int(line[9:11]), int(line[12:14])]
        #print(row)
        lines.append(row)
    boards.append(Board(lines))

#print(boards)

for num in nums:
    print(num)
    for board in boards:
        board.play(num)
        #print(str(board))
        if board.winner():
            print("WINNER")
            print(str(board))
            print(board.score(num))
    # filter out winning boards
    boards = [x for x in boards if not x.winner()]
    
