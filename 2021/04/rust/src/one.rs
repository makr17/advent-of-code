use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

#[derive(Clone, Debug)]
struct Spot {
    played: bool,
    num: i32,
}

#[derive(Clone, Debug)]
struct Board {
    spots: Vec<Vec<Spot>>,
    spotmap: HashMap<i32, (usize, usize)>,
}
impl Board {
    fn new (nums: Vec<Vec<i32>>) -> Result<Board, Box<dyn Error>> {
        let mut spots: Vec<Vec<Spot>> = vec![];
        let mut spotmap: HashMap<i32, (usize, usize)> = HashMap::new();
        for (y, row) in nums.iter().enumerate() {
            let mut srow: Vec<Spot> = vec![];
            for (x, num) in row.iter().enumerate() {
                srow.push(Spot{ num: *num, played: false });
                spotmap.insert(*num, (x,y));
            }
            spots.push(srow);
        }
        Ok(Board{ spots, spotmap })
    }

    fn play(&mut self, num: i32) {
        let found = self.spotmap.get(&num).unwrap();
        self.spots[found.1][found.0].played = true;
    }

    fn winner(&mut self) -> bool {
        // check horizontals
        for y in 0..5 {
            if self.spots[y][0].played && self.spots[y][1].played && self.spots[y][2].played && self.spots[y][3].played && self.spots[y][4].played {
                return true;
            }
        }
        // check verticals
        for x in 0..5 {
            if self.spots[0][x].played && self.spots[1][x].played && self.spots[2][x].played && self.spots[3][x].played && self.spots[4][x].played {
                return true;
            }
        }
        false
    }

    fn score(&mut self, num: i32) -> i32 {
        let mut total = 0;
        for y in 0..5 {
            for x in 0..5 {
                if self.spots[y][x].played {
                    continue;
                }
                total += self.spots[y][x].num
            }
        }
        total * num
    }
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(mut input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut head = String::new();
    let _len = input.read_line(&mut head)?;
    let nums: Vec<i32> = head.trim().split(',')
        .map(|x| x.parse::<i32>().unwrap())
        .collect();
    //println!("{:?}", nums);

    let mut boards: Vec<Board> = vec![];
    let mut rows: Vec<Vec<i32>> = vec![];
    for line in input.lines() {
        let l = line?;
        // handle the blank line
        if l.len() == 0 {
            continue;
        }
        let vals: Vec<i32> = l.trim().split_whitespace()
            .filter(|&x| x.len() > 0)
            .map(|x| x.parse::<i32>().unwrap())
            .collect();
        rows.push(vals);
        if rows.len() == 5 {
            let board = Board::new(rows).unwrap();
            println!("{:?}", board);
            boards.push(board);
            rows = vec![];
        }
    }

    let mut score: i32 = 0;
    for num in nums {
        for mut board in boards {
            board.play(num);
            if board.winner() {
                //println!("WINNER");
                score = board.score(num);
            }
        }
    }
    
    Ok(score)
}



#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 4512);
    Ok(())
}
