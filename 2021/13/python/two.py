#!/usr/bin/env python3

import fileinput

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.x}, {self.y})"
        
points = {}
folds = []
for line in fileinput.input():
    line = line.strip()
    if "," in line:
        # coordinates
        (x, y) = line.split(",")
        p = Point(int(x), int(y))
        points[str(p)] = p
    elif "=" in line:
        # fold instructions
        folds.append(line)
    else:
        pass

for fold in folds:
    (foo, value) = fold.split("=")
    value = int(value)
    foldon = foo[-1]
#    print(f"{foldon} = {value}")
    pvals = list(points.values())
    for p in pvals:
        val = getattr(p, foldon)
        #print(f"{str(p)}: {val}")
        if val > value:
            offset = val - value
            new_val = value - offset
            #print(f"    {str(p)}")
            # remove old storage
            points.pop(str(p))
            # modify point address for the fold
            setattr(p, foldon, new_val)
            # put back into storage in new location
            points[str(p)] = p

# size the output matrix
ys = [p.y for p in points.values()]
ys.sort()
xs = [p.x for p in points.values()]
xs.sort()

#print(f"x: {xs[0]} .. {xs[-1]}")
#print(f"y: {ys[0]} .. {ys[-1]}")

output = []
for y in range(0, ys[-1] + 1):
    output.append([" " for x in range(0, xs[-1] + 1)])

for p in points.values():
    output[p.y][p.x] = "#"

for row in output:
    print("".join(row))
