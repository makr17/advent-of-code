use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use std::ops::{Index, IndexMut};

#[derive(Copy, Clone, Eq, Hash, PartialEq)]
struct Point {
    x: i32,
    y: i32,
}
impl Index<char> for Point {
    type Output = i32;
    fn index(&self, s: char) -> &i32 {
        match s {
            'x' => &self.x,
            'y' => &self.y,
            _ => panic!("unknown field: {}", s),
        }
    }
}
impl IndexMut<char> for Point {
    fn index_mut(&mut self, s: char) -> &mut i32 {
        match s {
            'x' => &mut self.x,
            'y' => &mut self.y,
            _ => panic!("unknown field: {}", s),
        }
    }
}

#[derive(Copy, Clone)]
struct Fold {
    dim: char,
    value: i32,
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let mut points: HashMap<Point, bool> = HashMap::new();
    let mut folds: Vec<Fold> = vec![];
    for line in input.lines() {
        let l = line?;
        if l.contains(',') {
            // a point, comma-separated dimensions
            let chunks: Vec<&str> = l.trim().split(',').collect();
            let p = Point {
                x: chunks[0].parse::<i32>().unwrap(),
                y: chunks[1].parse::<i32>().unwrap(),
            };
            points.insert(p, true);
        }
        else if l.contains('=') {
            // a fold
            let tmp: Vec<&str> = l.trim().split(' ').collect();
            let chunks: Vec<&str> = tmp[2].split('=').collect();
            let char_vec: Vec<char> = chunks[0].chars().collect();
            let fold = Fold {
                dim: char_vec[0],
                value: chunks[1].parse::<i32>().unwrap(),
            };
            folds.push(fold);
        }
    }

    for fold in folds {
        // need to iterate the points _and_ modify the HashMap as we go...
        let ps: Vec<Point> = points.keys().map(|x| *x).collect();
        for mut p in ps {
            // look for points "below" the fold
            if p[fold.dim] > fold.value {
                // remove the old entry
                points.remove(&p);
                // and map the value in the fold dimension across the fold
                let offset = p[fold.dim] - fold.value;
                p[fold.dim] = fold.value - offset;
                points.insert(p, true);
            }
        }
    }

    // find the bounds on our space
    let mut xs: Vec<i32> = points.keys().map(|p| p.x).collect();
    xs.sort();
    let mut ys: Vec<i32> = points.keys().map(|p| p.y).collect();
    ys.sort();

    // and initialize an "empty" 2d array
    let mut output: Vec<Vec<char>> = vec![
        vec![
            ' ';
            (*xs.last().unwrap() + 1) as usize
        ];
        (*ys.last().unwrap() + 1) as usize
    ];

    // then mark all our points
    for point in points.keys() {
        output[point.y as usize][point.x as usize] = '#'
    }
    
    for row in output {
        println!("{}", row.iter().cloned().collect::<String>());
    }

    Ok(0)
}
