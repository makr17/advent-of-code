#!/usr/bin/env python3

import copy
import fileinput


def common(input, index):
    counts = {0: 0, 1: 0}
    for val in input:
        counts[val[index]] += 1
    if counts[1] >= counts[0]:
        return 1
    else:
        return 0


o2_list = []
for line in fileinput.input():
    line = line.strip()
    digits = [int(char) for char in line]
    o2_list.append(digits)

#print(o2_list)
co2_list = copy.deepcopy(o2_list)

for digit in range(0, len(o2_list[0])):
    if len(o2_list) > 1:
        o2_common = common(o2_list, digit)
        o2_list = [x for x in o2_list if x[digit] == o2_common]
    if len(co2_list) > 1:
        co2_common = common(co2_list, digit)
        co2_list = [x for x in co2_list if x[digit] != co2_common]

#print(o2_list)
#print(co2_list)

o2 = int("".join([str(x) for x in o2_list[0]]), 2)
co2 = int("".join([str(x) for x in co2_list[0]]), 2)

print(o2*co2)

