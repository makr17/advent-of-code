#!/usr/bin/env python3

import fileinput

lines = 0
counts = []
for line in fileinput.input():
    line = line.strip()
    digits = [int(char) for char in line]
    # initialize length of array on first pass
    if len(counts) == 0:
        counts = [0 for digit in digits]
    #print(digits)
    for i in range(0, len(digits)):
        if digits[i] == 1:
            counts[i] += 1
    lines += 1

print(f"{lines}  {counts}")

gamma = ""
epsilon = ""
for count in counts:
    if count >= lines/2:
        gamma += "1"
        epsilon += "0"
    else:
        gamma += "0"
        epsilon += "1"

gamma = int(gamma, 2)
epsilon = int(epsilon, 2)
print(f"{gamma}, {epsilon}")
print(gamma * epsilon)
