use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut lines: Vec<i32> = vec![];
    let mut bits = 0;
    for line in input.lines() {
        let l = line?;
        // initialize bit length
        if bits == 0 {
            //println!("{}  {}", l.len(), l);
            bits = l.len();
        }
        let val = i32::from_str_radix(&l, 2).unwrap();
        lines.push(val);
    }
    let mut gamma = 0;
    let mut epsilon = 0;
    for index in 0..bits {
        let common = common_digit(lines.clone(), index);
        //println!("{}: {}", index, common);
        if common == 1 {
            gamma |= 1 << index;
        }
        else {
            epsilon |= 1 << index;
        }
    }
    //println!("gamma={}, epsilon={}", gamma, epsilon);
    
    Ok(gamma * epsilon)
}

fn common_digit(input: Vec<i32>, index: usize) -> i32 {
    let mask = 1 << index;
    let count: i32 = input.iter()
        .map(|x| if x & mask > 0 { 1 } else { 0 })
        .sum();
    if count >= (input.len()/2).try_into().unwrap() {
        return 1;
    }
    0
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 198);
    Ok(())
}

#[test]
fn test_common_digit() {
    let input: Vec<i32> = vec![4, 30, 22, 23, 21, 15, 7, 28, 16, 25, 2, 10];
    assert_eq!(common_digit(input.clone(), 0), 0);
    assert_eq!(common_digit(input.clone(), 1), 1);
    assert_eq!(common_digit(input.clone(), 2), 1);
    assert_eq!(common_digit(input.clone(), 3), 0);
    assert_eq!(common_digit(input.clone(), 4), 1);
}
