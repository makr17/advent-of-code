use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut lines: Vec<i32> = vec![];
    let mut bits = 0;
    for line in input.lines() {
        let l = line?;
        // initialize bit length
        if bits == 0 {
            bits = l.len();
        }
        let val = i32::from_str_radix(&l, 2).unwrap();
        lines.push(val);
    }

    let mut o2_vec = lines.clone();
    let mut co2_vec = lines.clone();
    for index in (0..bits).rev() {
        if o2_vec.len() > 1 {
            let (c, mask) = common_digit(o2_vec.clone(), index);
            o2_vec = o2_vec.iter()
                .filter(|x| *x & mask == c << index)
                .copied()
                .collect();
        }
        if co2_vec.len() > 1 {
            let (c, mask) = common_digit(co2_vec.clone(), index);
            co2_vec = co2_vec.iter()
                .filter(|x| *x & mask != c << index)
                .copied()
                .collect();
        }
    }

    let o2 = o2_vec[0];
    let co2 = co2_vec[0];

    Ok(o2 * co2)
}

fn common_digit(input: Vec<i32>, index: usize) -> (i32, i32) {
    let mask = 1 << index;
    let count: i32 = input.iter()
        .map(|x| if x & mask > 0 { 1 } else { 0 })
        .sum();
    if count as f32 >= input.len() as f32/2.0 {
        return (1, mask);
    }
    (0, mask)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 230);
    Ok(())
}

#[test]
fn test_common_digit() {
    let input: Vec<i32> = vec![4, 30, 22, 23, 21, 15, 7, 28, 16, 25, 2, 10];
    assert_eq!(common_digit(input.clone(), 0), (0, 1));
    assert_eq!(common_digit(input.clone(), 1), (1, 2));
    assert_eq!(common_digit(input.clone(), 2), (1, 4));
    assert_eq!(common_digit(input.clone(), 3), (0, 8));
    assert_eq!(common_digit(input.clone(), 4), (1, 16));
}

#[test]
fn test_common_digit_o2_2nd() {
    let input: Vec<i32> = vec![30, 22, 23, 21, 28, 16, 25];
    assert_eq!(common_digit(input.clone(), 3), (0, 8));
}


