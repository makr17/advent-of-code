#!/usr/bin/env python3

import fileinput

class Packet:
    def __init__(self, version, packet_type):
        self.version = version
        self.type = packet_type


class ValuePacket(Packet):
    def __init__(self, version, packet_type, value):
        super().__init__(version, packet_type)
        self.value = value

    def versum(self):
        return self.version


class OperatorPacket(Packet):
    def __init__(self, version, packet_type, subpackets):
        super().__init__(version, packet_type)
        self.subpackets = subpackets

    def versum(self):
        return self.version + sum([p.versum() for p in self.subpackets])

def packet_version(bits, offset):
    return int("".join(bits[offset:offset+3]), 2)

def packet_type(bits, offset):
    return int("".join(bits[offset:offset+3]), 2)

def parse(bits, offset=0):
    while offset < len(bits):
        ver = packet_version(bits, offset)
        #print(f"ver={ver}")
        offset += 3
        ptype = packet_type(bits, offset)
        offset += 3
        if ptype == 4:
            value = []
            while True:
                sub = bits[offset:offset+5]
                offset += 5
                for bit in sub[1:5]:
                    value.append(bit)
                if sub[0] == "0":
                    # last set of bits
                    break
            packet = ValuePacket(ver, ptype, int("".join(value), 2))
            return packet, offset
        else:
            ltype = bits[offset]
            offset += 1
            subpackets = []
            if ltype == "0":
                # parse by bitlength
                bitlength = int("".join(bits[offset:offset+15]), 2)
                offset += 15
                #print(f"bitlen={bitlength}")
                stop = offset + bitlength
                #print(f"offset={offset} stop={stop}")
                while offset < stop:
                    #print(offset, ": ", bits[offset:])
                    subpacket, offset = parse(bits, offset)
                    subpackets.append(subpacket)
            elif ltype == "1":
                # parse by count
                count = int("".join(bits[offset:offset+11]), 2)
                offset += 11
                for i in range(count):
                    subpacket, offset = parse(bits, offset)
                    subpackets.append(subpacket)
            packet = OperatorPacket(ver, ptype, subpackets)
            return packet, offset


bits = []
for line in fileinput.input():
    line = line.strip()
    for char in line:
        for bit in bin(int(char, 16))[2:].zfill(4):
            bits.append(bit)

root, offset = parse(bits)


print(f"version sum = {root.versum()}")
