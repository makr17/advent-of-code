I hate binary protocal dissection enough that I'm not inclined to
reimplement the problem for today in rust.  It was painful enough to
get it right in python.

It _did_ make me wish I was doing the problems in Erlang, but I
suppose earlier days would have produced more pain in that case...
