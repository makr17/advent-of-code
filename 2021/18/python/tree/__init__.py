import json
import math

class Tree:
    def __init__(self, string=""):
        depth = 0
        if isinstance(string, str):
            j = json.loads(string)
        else:
            j = string
        self.left = None
        self.right = None
        self.value = None
        self.parent = None
        if isinstance(j, list):
            self.left = Tree(j[0])
            self.left.parent = self
            self.right = Tree(j[1])
            self.right.parent = self
        elif isinstance(j, int):
            self.value = j

    def __str__(self):
        if self.left and self.right:
            return "[" + str(self.left) + "," + str(self.right) + "]"
        else:
            return str(self.value)
        
    def reduce(self):
        #print(str(self))
        while self.explode(0) or self.split():
            #print(str(self))
            continue

    def explode(self, depth):
        if self.left:
            # parent node
            if depth >= 4 \
               and isinstance(self.left.value, int) \
               and isinstance(self.right.value, int):
                #print(f"exploding [{self.left.value},{self.right.value}] at depth={depth}")
                # both children are leaf nodes
                
                # walk up the tree to the left
                # until the left node is not our previous
                #   to avoid backtracking
                left = self.parent
                prev = self
                while left and left.left == prev:
                    prev = left
                    left = left.parent
                if left:
                    left = left.left
                # otherwise start walking down to the _right_
                while left and left.right:
                    left = left.right
                # and mirrored to the right
                # walk up until right is not prev node
                right = self.parent
                prev = self
                while right and right.right == prev:
                    prev = right
                    right = right.parent
                if right:
                    right = right.right
                # walk down to the _left_
                while right and right.left:
                    right = right.left
                # add child values into left and right
                #print(f"  self.left.value={self.left.value}")
                if left and isinstance(left.value, int):
                    #print(f"  left.value={left.value}")
                    left.value += self.left.value
                #print(f"  self.right.value={self.right.value}")
                if right and isinstance(right.value, int):
                    #print(f"  right.value={right.value}")
                    right.value += self.right.value
                # remove child nodes
                self.left = None
                self.right = None
                self.value = 0
                #print("explode True")
                return True
            return self.left.explode(depth+1) or self.right.explode(depth+1)
        #print("explode False")
        return False
    
    def split(self):
        if self.value:
            # leaf node
            if self.value > 9:
                #print(f"splitting {self.value}");
                # splittable
                # turn it into a parent node with left/right
                # left is half value rounded down
                self.left = Tree(math.floor(self.value/2))
                # rigth is half value rounded up
                self.right = Tree(math.ceil(self.value/2))
                # parent the newly-created nodes
                self.left.parent = self
                self.right.parent = self
                # and unset our value
                self.value = None
                #print(f"  result [{self.left.value},{self.right.value}]")
                #print("split True")
                return True
        if self.left:
            # parent node, try to split the children
            # but only _one_ operation per reduce...
            return self.left.split() or self.right.split()
        #print("split True")
        return False

    def add(self, tree):
        newtree = Tree("[" + str(self) + "," + str(tree) + "]")
        #print(str(newtree))
        newtree.reduce()
        return newtree

    def magnitude(self):
        if isinstance(self.value, int):
            return self.value
        return self.left.magnitude() * 3 + self.right.magnitude() * 2
