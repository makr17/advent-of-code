#!/usr/bin/env python3

import fileinput
import json
import math

from tree import Tree

def run():
    strings = []
    for line in fileinput.input():
        line = line.strip()
        strings.append(line)

    maxmag = 0
    for (i, one) in enumerate(strings):
        for two in strings[i+1:]:
            tree = Tree(one).add(Tree(two))
            mag = tree.magnitude()
            if mag > maxmag:
                maxmag = mag

    print(maxmag)
                
if __name__ == "__main__":
    run()
