#!/usr/bin/env python3

import unittest

from tree import Tree

class TestTree(unittest.TestCase):
    def test_explode_1(self):
        string = "[[[[[9,8],1],2],3],4]"
        tree = Tree(string)
        tree.explode(0)
        self.assertEqual(str(tree), "[[[[0,9],2],3],4]")

    def test_explode_2(self):
        string = "[7,[6,[5,[4,[3,2]]]]]"
        tree = Tree(string)
        tree.explode(0)
        self.assertEqual(str(tree), "[7,[6,[5,[7,0]]]]")

    def test_explode_3(self):
        string = "[[6,[5,[4,[3,2]]]],1]"
        tree = Tree(string)
        tree.explode(0)
        self.assertEqual(str(tree), "[[6,[5,[7,0]]],3]")

    def test_explode_4(self):
        string = "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"
        tree = Tree(string)
        tree.explode(0)
        self.assertEqual(str(tree), "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]")

    def test_explode_5(self):
        string = "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
        tree = Tree(string)
        tree.explode(0)
        self.assertEqual(str(tree), "[[3,[2,[8,0]]],[9,[5,[7,0]]]]")


    def test_split_1(self):
        string = "[10,1]"
        tree = Tree(string)
        tree.split()
        self.assertEqual(str(tree), "[[5,5],1]")

    def test_add_1(self):
        one = Tree("[[[[4,3],4],4],[7,[[8,4],9]]]")
        two = Tree("[1,1]")
        three = one.add(two)
        three.reduce()
        self.assertEqual(str(three), "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")

    def test_list_add_1(self):
        stack = [
            "[1,1]",
            "[2,2]",
            "[3,3]",
            "[4,4]"
        ]
        #print(stack[0])
        tree = Tree(stack[0])
        for string in stack[1:]:
            #print(string)
            adder = Tree(string)
            tree = tree.add(adder)
        self.assertEqual(str(tree), "[[[[1,1],[2,2]],[3,3]],[4,4]]")

    def test_list_add_2(self):
        stack = [
            "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]",
            "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]",
            "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]",
            "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]",
            "[7,[5,[[3,8],[1,4]]]]",
            "[[2,[2,2]],[8,[8,1]]]",
            "[2,9]",
            "[1,[[[9,3],9],[[9,0],[0,7]]]]",
            "[[[5,[7,4]],7],1]",
            "[[[[4,2],2],6],[8,7]]"
        ]
        #print(stack[0])
        tree = Tree(stack[0])
        for string in stack[1:]:
            #print(string)
            adder = Tree(string)
            tree = tree.add(adder)
        self.assertEqual(str(tree), "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")

    def test_magnitude_1(self):
        string = "[9,1]"
        tree = Tree(string)
        self.assertEqual(tree.magnitude(), 29)

    def test_magnitude_2(self):
        string = "[1,9]"
        tree = Tree(string)
        self.assertEqual(tree.magnitude(), 21)

    def test_magnitude_3(self):
        string = "[[9,1],[1,9]]"
        tree = Tree(string)
        self.assertEqual(tree.magnitude(), 129)

    def test_magnitude_4(self):
        string = "[[1,2],[[3,4],5]]"
        tree = Tree(string)
        self.assertEqual(tree.magnitude(), 143)

    def test_magnitude_5(self):
        string = "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"
        tree = Tree(string)
        self.assertEqual(tree.magnitude(), 1384)

    def test_magnitude_6(self):
        string = "[[[[1,1],[2,2]],[3,3]],[4,4]]"
        tree = Tree(string)
        self.assertEqual(tree.magnitude(), 445)

    def test_magnitude_7(self):
        string = "[[[[3,0],[5,3]],[4,4]],[5,5]]"
        tree = Tree(string)
        self.assertEqual(tree.magnitude(), 791)

    def test_magnitude_8(self):
        string = "[[[[5,0],[7,4]],[5,5]],[6,6]]"
        tree = Tree(string)
        self.assertEqual(tree.magnitude(), 1137)

    def test_magnitude_9(self):
        string = "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
        tree = Tree(string)
        self.assertEqual(tree.magnitude(), 3488)

    def test_homework(self):
        stack = [
            "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]",
            "[[[5,[2,8]],4],[5,[[9,9],0]]]",
            "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
            "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
            "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
            "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
            "[[[[5,4],[7,7]],8],[[8,3],8]]",
            "[[9,3],[[9,9],[6,[4,9]]]]",
            "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
            "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"
        ]
        tree = Tree(stack[0])
        for string in stack[1:]:
            #print(string)
            adder = Tree(string)
            tree = tree.add(adder)
        self.assertEqual(str(tree), "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]")
        self.assertEqual(tree.magnitude(), 4140)


if __name__ == '__main__':
    unittest.main()
