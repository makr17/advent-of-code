#!/usr/bin/env python3

import fileinput
import json
import math

from tree import Tree

def run():
    stack = []
    for line in fileinput.input():
        line = line.strip()
        print(line)
        stack.append(Tree(line))

    tree = stack[0]
    for adder in stack[1:]:
        tree = tree.add(adder)
    print(tree.magnitude())


if __name__ == "__main__":
    run()
