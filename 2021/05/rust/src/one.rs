use std::cmp::{max, min};
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

#[derive(Debug, Eq, Hash, PartialEq)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug)]
struct Segment {
    a: Point,
    b: Point,
}
impl Segment {
    fn points (&self) -> Vec<Point> {
        let mut points: Vec<Point> = vec![];
        // vertical, x values are the same
        if self.a.x == self.b.x {
            let ymin = min(self.a.y, self.b.y);
            let ymax = max(self.a.y, self.b.y);
            for i in ymin .. ymax + 1 {
                points.push(Point {x: self.a.x, y: i});
            }
        }
        // horizontal
        else if self.a.y == self.b.y {
            let xmin = min(self.a.x, self.b.x);
            let xmax = max(self.a.x, self.b.x);
            for i in xmin .. xmax + 1 {
                points.push(Point {x: i, y: self.a.y });
            }
        }
        points
    }
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<usize, Box::<dyn Error>> {
    let mut segments: Vec<Segment> = vec![];
    for line in input.lines() {
        let l = line?;
        let chunks: Vec<&str> = l.trim().split(' ').collect();
        let a: Vec<i32> = chunks[0].split(',')
            .map(|i| i.parse::<i32>().unwrap())
            .collect();
        let pa = Point { x: a[0], y: a[1] };
        let b: Vec<i32> = chunks[2].split(',')
            .map(|i| i.parse::<i32>().unwrap())
            .collect();
        let pb = Point { x: b[0], y: b[1] };
        // only horizontal or vertical
        if pa.x == pb.x || pa.y == pb.y {
            segments.push(Segment { a: pa, b: pb });
        }
    }
    let mut points: HashMap<Point, i32> = HashMap::new();
    for segment in segments {
        for point in segment.points() {
            let count = points.remove(&point).unwrap_or(0);
            points.insert(point, count + 1);
        }
    }
    //println!("{:?}", points);
    let overlap: Vec<&i32> = points.values().filter(|&x| x > &1).collect();
        
    Ok(overlap.len())
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 5);
    Ok(())
}
