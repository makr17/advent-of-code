#!/usr/bin/env python3

import fileinput

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.x}, {self.y})"

class Segment:
    def __init__(self, ax, ay, bx, by):
        self.a = Point(ax, ay)
        self.b = Point(bx, by)

    def __str__(self):
        return f"{str(self.a)} => {str(self.b)}"

    def points(self):
        points = []
        if self.a.x == self.b.x:
            ymin = min(self.a.y, self.b.y)
            ymax = max(self.a.y, self.b.y)
            for i in range(ymin, ymax + 1):
                points.append(Point(self.a.x, i))
        elif self.a.y == self.b.y:
            xmin = min(self.a.x, self.b.x)
            xmax = max(self.a.x, self.b.x)
            for i in range(xmin, xmax + 1):
                points.append(Point(i, self.a.y))
        else:
            if self.a.x > self.b.x:
                x = [i for i in reversed(range(self.b.x, self.a.x + 1))]
            else:
                x = [i for i in range(self.a.x, self.b.x + 1)]
            if self.a.y > self.b.y:
                y = [i for i in reversed(range(self.b.y, self.a.y + 1))]
            else:
                y = [i for i in range(self.a.y, self.b.y + 1)]
            for i in range(0, len(x)):
                points.append(Point(x[i], y[i]))
        return points


segments = []
for line in fileinput.input():
    line = line.strip()
    (a,ptr,b) = line.split(' ')
    (ax, ay) = [int(i) for i in a.split(',')]
    (bx, by) = [int(i) for i in b.split(',')]
    segments.append(Segment(ax, ay, bx, by))


counts = {}
for segment in segments:
    #print(f"{str(segment)}: {[str(p) for p in segment.points()]}")
    for point in segment.points():
        s = str(point)
        if s in counts:
            counts[s] += 1
        else:
            counts[s] = 1

crossed = [p for p in counts.keys() if counts[p] > 1]
print(len(crossed))
