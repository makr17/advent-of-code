#!/usr/bin/env python3

import fileinput

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.x}, {self.y})"

class Segment:
    def __init__(self, ax, ay, bx, by):
        self.a = Point(ax, ay)
        self.b = Point(bx, by)

    def __str__(self):
        return f"{str(self.a)} => {str(self.b)}"

    def points(self):
        points = []
        if self.a.x == self.b.x:
            ymin = min(self.a.y, self.b.y)
            ymax = max(self.a.y, self.b.y)
            for i in range(ymin, ymax + 1):
                points.append(Point(self.a.x, i))
        elif self.a.y == self.b.y:
            xmin = min(self.a.x, self.b.x)
            xmax = max(self.a.x, self.b.x)
            for i in range(xmin, xmax + 1):
                points.append(Point(i, self.a.y))
        return points
    

segments = []
for line in fileinput.input():
    line = line.strip()
    (a,ptr,b) = line.split(' ')
    (ax, ay) = [int(i) for i in a.split(',')]
    (bx, by) = [int(i) for i in b.split(',')]
    if ax == bx or ay == by:
        segments.append(Segment(ax, ay, bx, by))


counts = {}
for segment in segments:
    for point in segment.points():
        s = str(point)
        if s in counts:
            counts[s] += 1
        else:
            counts[s] = 1

crossed = [p for p in counts.keys() if counts[p] > 1]
print(len(crossed))
