use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use rayon::prelude::*;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(mut input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut line = String::new();
    input.read_line(&mut line)?;
    let mut pos: Vec<i32> = line.trim().split(',')
        .map(|x| x.parse::<i32>().unwrap())
        .collect();
    pos.sort_unstable();
    let mut costs: Vec<i32> = (pos[0] .. *pos.last().unwrap())
        .into_par_iter()
        .map(|x| cost(&pos, x))
        .collect();
    costs.sort_unstable();
    let optimal = costs[0];
    
    Ok(optimal)
}

fn cost(pos: &[i32], test: i32) -> i32 {
    pos.par_iter().map(|x| (1 .. (test - x).abs() + 1).into_par_iter().sum::<i32>()).sum()
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 168);
    Ok(())
}

