use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(mut input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut line = String::new();
    input.read_line(&mut line)?;
    let mut pos: Vec<i32> = line.trim().split(',')
        .map(|x| x.parse::<i32>().unwrap())
        .collect();
    pos.sort_unstable();
    let median = pos[pos.len()/2];
    let cost: i32 = pos.iter().map(|x| (x - median).abs()).sum();
    Ok(cost)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 37);
    Ok(())
}

