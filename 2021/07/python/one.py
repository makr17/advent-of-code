#!/usr/bin/env python3

import fileinput
from statistics import median

pos = [int(x) for x in fileinput.input().readline().strip().split(",")]
#print(f"Initial positions: {pos}")

best = median(pos)
delta = [abs(x - best) for x in pos]
print(sum(delta))
