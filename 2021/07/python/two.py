#!/usr/bin/env python3

import fileinput
import sys

pos = [int(x) for x in fileinput.input().readline().strip().split(",")]
#print(f"Initial positions: {pos}")

def cost(start, test):
    return sum([sum(range(1, abs(x - test) + 1)) for x in start])

start = min(pos)
stop = max(pos)
optimal = sys.maxsize;
for test in range(start, stop + 1):
    c = cost(pos, test)
    if c < optimal:
        optimal = c

print(optimal)
