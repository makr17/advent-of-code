use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

mod enhancer;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u32, Box::<dyn Error>> {
    let mut enhancer_bits: Vec<u8> = vec![];
    let mut image_bits: Vec<Vec<u8>> = vec![];
    let mut mode = "enhancer";
    for line in input.lines() {
        let l = line?;
        if l.is_empty() {
            mode = "image";
            continue;
        }
        let mut bits: Vec<u8> = l.chars().into_iter()
            .map(|c| if c == '#' { 1 } else { 0 })
            .collect();
        if mode == "enhancer" {
            // append to the vec
            enhancer_bits.append(&mut bits);
        }
        else {
            // push row onto the image
            image_bits.push(bits)
        }
    }
    let enhancer = enhancer::Enhancer{ lookup: enhancer_bits };
    let mut image = enhancer::Image{ bits: image_bits };
    //image.visualize();
    //println!("dim={:?}, {} lit pixels", image.dimensions(), image.lit_pixels());

    for step in 1..51 {
        image = enhancer.enhance(image, step);
        //image.visualize();
        //println!("dim={:?}, {} lit pixels", image.dimensions(), image.lit_pixels());
    }
    
    Ok(image.lit_pixels())
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 3351);
    Ok(())
}

