#[allow(dead_code, unused_variables)]
pub struct Enhancer {
    pub lookup: Vec<u8>,
}
impl Enhancer {
    #[allow(dead_code, unused_variables)]
    // clunky...  need to allow negatives, but vec indexing is usize...
    pub fn enhance(&self, image: Image, step: u32) -> Image {
        let odim = image.dimensions();
        let nx: i32 = (odim.0 + 2) as i32;
        let ny: i32 = (odim.1 + 2) as i32;
        let mut bits: Vec<Vec<u8>> = vec![vec![0; nx as usize]; ny as usize];

        for y in 0 .. ny {
            for x in 0 .. nx {
                bits[y as usize][x as usize] = self.enhanced_pixel(
                    &image,
                    step,
                    x - 1,
                    y - 1
                );
            }
        }

        Image { bits }
    }

    #[allow(dead_code, unused_variables)]
    // clunky...  need to allow negatives, but vec indexing is usize...
    fn enhanced_pixel(&self, image: &Image, step: u32, x: i32, y: i32) -> u8 {
        let dim = image.dimensions();
        let mut binkey: Vec<u8> = vec![];
        for sy in y-1 .. y+2 {
            for sx in x-1 .. x+2 {
                if sy < 0 || sy > dim.1 as i32 - 1 || sx < 0 || sx > dim.0 as i32 - 1 {
                    if self.lookup[0] == 1 {
                        binkey.push(if step % 2 == 1 { 0 } else { 1 });
                    }
                    else {
                        binkey.push(0);
                    }
                    continue;
                }
                binkey.push(image.bits[sy as usize][sx as usize]);
            }
        }
        let strkey: Vec<String> = binkey.iter()
            .map(|x| x.to_string())
            .collect();
        let key = usize::from_str_radix(&strkey.join(""), 2).unwrap();
        self.lookup[key]
    }
}

#[allow(dead_code, unused_variables)]
pub struct Image {
    pub bits: Vec<Vec<u8>>,
}
impl Image {
    #[allow(dead_code, unused_variables)]
    pub fn dimensions(&self) -> (usize, usize) {
        if self.bits.len() == 0 {
            return (0, 0)
        }
        (self.bits[0].len(), self.bits.len())
    }

    #[allow(dead_code, unused_variables)]
    pub fn lit_pixels(&self) -> u32 {
        let mut count: u32 = 0;
        for row in self.bits.iter() {
            for bit in row.iter() {
                count += *bit as u32;
            }
        }
        count
    }

    #[allow(dead_code, unused_variables)]
    pub fn visualize(&self) {
        println!();
        for row in self.bits.iter() {
            let strow: Vec<&str> = row.iter()
                .map(|x| if *x == 1 { "#" } else { "." })
                .collect();
            println!("{}", strow.join(""))
        }
    }
}
