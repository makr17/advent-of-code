class Image:
    def __init__(self, lines=None):
        image = []
        self.image = image
        if lines:
            for line in lines.split("\n"):
                if line == "":
                    continue
                line = line.strip()
                row = [1 if x == "#" else 0 for x in line]
                image.append(row)

    def dimensions(self):
        if len(self.image) == 0:
            return (0,0)
        return (len(self.image[0]), len(self.image))

    def lit_pixels(self):
        count = 0
        for row in self.image:
            for bit in row:
                count += bit
        return count

    def visualize(self):
        print()
        for row in self.image:
            print("".join(["#" if x == 1 else "." for x in row]))
        print()
