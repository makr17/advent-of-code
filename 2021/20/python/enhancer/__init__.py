import sys
sys.path.append("..")
from image import Image

class Enhancer:
    def __init__(self, bits):
        lookup = []
        for c in bits:
            bit = 1 if c == "#" else 0
            lookup.append(bit)
        self.lookup = lookup

    def enhance(self, image, step):
        enhanced = Image()
        # pad the dimensions on each side by 1
        (ox, oy) = image.dimensions()
        nx = ox + 2
        ny = oy + 2
        rows = []
        for y in range(0, ny):
            row = []
            for x in range(0, nx):
                row.append(self.enhanced_pixel(image, step, x-1, y-1))
            rows.append(row)
        enhanced.image = rows
        
        return enhanced

    def enhanced_pixel(self, image, step, x, y):
        (dx, dy) = image.dimensions()
        binkey = []
        for sy in range(y-1, y+2):
            for sx in range(x-1, x+2):
                if sy < 0 or sy > dy - 1 or sx < 0 or sx > dx - 1:
                    if self.lookup[0] == 1:
                        binkey.append(0 if step % 2 == 1 else 1)
                    else:
                        binkey.append(0)
                    continue
                binkey.append(image.image[sy][sx])
        key = int("".join([str(x) for x in binkey]), 2)
        return self.lookup[key]
