#!/usr/bin/env python3

from pathlib import Path
import unittest

from enhancer import Enhancer
from image import Image

class TestEnhancer(unittest.TestCase):
    def test_enhancer(self):
        txt = Path("test.txt").read_text()
        (enhancer_bits, image_bits) = txt.split("\n\n")
        enhancer_bits = enhancer_bits.replace("\n", "")

        enhancer = Enhancer(enhancer_bits)
        self.assertEqual(len(enhancer.lookup), 512)
        self.assertEqual(enhancer.lookup[0], 0)
        self.assertEqual(enhancer.lookup[2], 1)
        self.assertEqual(enhancer.lookup[34], 1)

    def test_enhanced_pixel(self):
        txt = Path("test.txt").read_text()
        (enhancer_bits, image_bits) = txt.split("\n\n")
        enhancer_bits = enhancer_bits.replace("\n", "")
        enhancer = Enhancer(enhancer_bits)
        input_image = Image(image_bits)
        self.assertEqual(enhancer.enhanced_pixel(input_image, 1, 2, 2), 1)
        self.assertEqual(
            enhancer.enhanced_pixel(input_image, 1, -1, -1),
            enhancer.lookup[0]
        )
        self.assertEqual(
            enhancer.enhanced_pixel(input_image, 2, -1, -1),
            enhancer.lookup[0]
        )

    def test_two_pass(self):
        txt = Path("test.txt").read_text()
        (enhancer_bits, image_bits) = txt.split("\n\n")
        enhancer_bits = enhancer_bits.replace("\n", "")

        enhancer = Enhancer(enhancer_bits)

        input_image = Image(image_bits)
        #input_image.visualize()
        
        image_1 = enhancer.enhance(input_image, 1)
        #image_1.visualize()
        
        image_2 = enhancer.enhance(image_1, 2)
        #image_2.visualize()

        self.assertEqual(image_2.lit_pixels(), 35)

    def test_fifty_passes(self):
        txt = Path("test.txt").read_text()
        (enhancer_bits, image_bits) = txt.split("\n\n")
        enhancer_bits = enhancer_bits.replace("\n", "")

        enhancer = Enhancer(enhancer_bits)

        image = Image(image_bits)

        for step in range(1, 51):
            image = enhancer.enhance(image, step)

        self.assertEqual(image.lit_pixels(), 3351)


if __name__ == "__main__":
    unittest.main()
