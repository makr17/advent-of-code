#!/usr/bin/env python3

from pathlib import Path

from enhancer import Enhancer
from image import Image

def run():
    txt = Path("input.txt").read_text()
    (enhancer_bits, image_bits) = txt.split("\n\n")
    enhancer_bits = enhancer_bits.replace("\n", "")

    enhancer = Enhancer(enhancer_bits)
    image = Image(image_bits)

    for step in range(1, 51):
        image = enhancer.enhance(image, step)

    print(f"{image.lit_pixels()} pixels lit")


if __name__ == "__main__":
    run()
