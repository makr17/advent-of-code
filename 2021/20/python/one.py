#!/usr/bin/env python3

from pathlib import Path

from enhancer import Enhancer
from image import Image

def run():
    txt = Path("input.txt").read_text()
    (enhancer_bits, image_bits) = txt.split("\n\n")
    enhancer_bits = enhancer_bits.replace("\n", "")

    enhancer = Enhancer(enhancer_bits)
    input_image = Image(image_bits)
    print(input_image.dimensions())
    print(f"{input_image.lit_pixels()} pixels lit")
    #input_image.visualize()

    image_1 = enhancer.enhance(input_image, 1)
    print(image_1.dimensions())
    #image_1.visualize()
    print(f"{image_1.lit_pixels()} pixels lit")

    image_2 = enhancer.enhance(image_1, 2)
    print(image_2.dimensions())
    #image_2.visualize()

    print(f"{image_2.lit_pixels()} pixels lit")
    
if __name__ == "__main__":
    run()
