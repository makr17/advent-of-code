use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};


#[derive(Copy, Clone, Debug)]
struct Point {
    x: i32,
    y: i32,
}

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(mut input: BufReader<File>) -> Result<i32, Box::<dyn Error>> {
    let mut line = String::new();
    input.read_line(&mut line)?;
    let l = line.trim();
    
    // I feel like there should be a simpler way to mask/parse input
    let chunks: Vec<&str> = l.split(' ').collect();
    //println!("{:?}", chunks);
    let mut foo: Vec<&str> = chunks[2].split('=').collect();
    //println!("{:?}", foo);
    let comma_offset = foo[1].find(',').unwrap();
    let mut xfoo = foo[1].to_string();
    xfoo.replace_range(comma_offset .. comma_offset + 1, "");
    //println!("{:?}", xfoo);
    let xs: Vec<&str> = xfoo.split('.').collect();
    let xmin = xs[0].parse::<i32>().unwrap();
    let xmax = xs[2].parse::<i32>().unwrap();
    foo = chunks[3].split('=').collect();
    //println!("{:?}", foo);
    let ys: Vec<&str> = foo[1].split('.').collect();
    let ymax = ys[0].parse::<i32>().unwrap();
    let ymin = ys[2].parse::<i32>().unwrap();
    let target_max = Point { x: xmax, y: ymax };
    let target_min = Point { x: xmin, y: ymin };
    println!("target min: {:?}  max: {:?}", target_min, target_max);

    let mut max = Point{ x: 0, y: 0 };

    for x in 0 .. target_max.x + 1 {
        for y in 0 .. 1000 {
            let mut v = Point { x, y };
            let _init_v = v.clone();
            let mut current = Point{ x: 0, y: 0 };
            let mut peak = Point{ x: 0, y: 0 };
            loop {
                // adjust current position
                current.x += v.x;
                current.y += v.y;

                // modify velocity for drag and gravity
                if v.x > 0 {
                    v.x -= 1;
                }
                else if v.x < 0 {
                    v.x += 1;
                }
                v.y -= 1;

                // modify peak if we're higher
                if current.y > peak.y {
                    peak = current.clone();
                }
                // do we hit the target
                if current.x >= target_min.x
                    && current.x <= target_max.x
                    && current.y <= target_min.y
                    && current.y >= target_max.y {
                        // only flag max if we are _in_ the are on a step
                        if peak.y > max.y {
                            max = peak.clone();
                            //println!("{:?}", init_v);
                            //println!("  * peak: {:?}  current: {:?}", peak, current);
                            //println!("  # max: {:?}", max);
                        }
                    }
                if current.x > target_max.x {
                    // went too far
                    break;
                }
                if current.y < target_max.y {
                    // too deep
                    break;
                }
            }
        }
    }
    
    Ok(max.y)
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 45);
    Ok(())
}
