#!/usr/bin/env python3

import sys
from copy import deepcopy

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.x}, {self.y})"

def parse_range(xs, ys):
    (_, r) = xs.split("=")
    (xmin, _, xmax) = r.split(".")
    (_, r) = ys.split("=")
    (ymax, _, ymin) = r.split(".")
    return Point(int(xmin), int(ymin)), Point(int(xmax), int(ymax))

line = sys.stdin.readline()
line = line.strip()
(_, _, xs, ys) = line.split(" ")
xs = xs.strip(",")
        
target_min, target_max = parse_range(xs, ys)

hit = 0
for x in range(0, target_max.x + 2):
    for y in range(target_max.y, 1000):
        v = Point(x, y)
        init_v = deepcopy(v)
        #print(init_v)
        current = Point(0, 0)
        peak = Point(0, 0)
        while True:
            current.x += v.x
            current.y += v.y
            #print("  ", current)

            if v.x > 0:
                v.x -= 1
            elif v.x < 0:
                v.x += 1
            v.y -= 1

            if (current.y > peak.y):
                peak = deepcopy(current)
            # do we hit the target?
            if current.x >= target_min.x and current.x <= target_max.x and current.y <= target_min.y and current.y >= target_max.y:
                # only flag max if we are _in_ the area on a step
                hit += 1
                print(init_v)
                #print(init_v)
                #print("  * ", peak, current)
                #print("  # ", max_point)
                break

            if current.x > target_max.x:
                # went to far
                break
            if current.y < target_max.y:
                # too deep
                break

print(hit)


