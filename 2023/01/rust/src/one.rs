use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let mut nums: Vec<u64> = vec![];
    for s in input.lines() {
        let digits: Vec<char> = s.unwrap().chars()
            .filter(|c| c.is_ascii_digit())
            .collect();
        if digits.is_empty() {
            continue
        }
        let mut numstr = String::new();
        numstr.push(*digits.first().unwrap());
        numstr.push(*digits.last().unwrap());
        //println!("{numstr}");
        nums.push(numstr.parse::<u64>().unwrap());
    }
    //println!("{:?}", nums);
    Ok(nums.iter().sum())
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 142);
    Ok(())
}
