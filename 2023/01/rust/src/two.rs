use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use regex::Regex;
use std::collections::HashMap;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let mut nums: Vec<u64> = vec![];
    // replacements for spelled-out string digits
    // TODO: something like "eightwone" could still break us
    //       is there a general solution for "entangled" digits?
    let map: HashMap<String, String> = HashMap::from([
        ("twone".to_owned(), "21".to_owned()),
        ("sevenine".to_owned(), "79".to_owned()),
        ("oneight".to_owned(), "18".to_owned()),
        ("threeight".to_owned(), "38".to_owned()),
        ("nineight".to_owned(), "98".to_owned()),
        ("fiveight".to_owned(), "58".to_owned()),
        ("eighthree".to_owned(), "83".to_owned()),
        ("eightwo".to_owned(), "82".to_owned()),
        ("one".to_owned(), "1".to_owned()),
        ("two".to_owned(), "2".to_owned()),
        ("three".to_owned(), "3".to_owned()),
        ("four".to_owned(), "4".to_owned()),
        ("five".to_owned(), "5".to_owned()),
        ("six".to_owned(), "6".to_owned()),
        ("seven".to_owned(), "7".to_owned()),
        ("eight".to_owned(), "8".to_owned()),
        ("nine".to_owned(), "9".to_owned()),
    ]);
    // "entangled" digits
    let re1 = Regex::new(r"(twone|sevenine|oneight|threeight|nineight|fiveight|eighthree|eightwo)").unwrap();
    // plain digits
    let re2 = Regex::new(r"(one|two|three|four|five|six|seven|eight|nine)").unwrap();
    for s in input.lines() {
        let mut line = s.unwrap();
        //println!("{line}");
        // process twice, once for entangled digits
        // and once for plain
        for re in [re1.clone(), re2.clone()] {
            // I had a hard time getting this to work as a map from the find_iter()
            // Also had issues with just Vec<Match>
            // so breaking out the bits I need and being explicit
            let mut matches: Vec<(usize, usize, String)> = vec![];
            for m in re.find_iter(&line.clone()) {
                matches.push((m.start(), m.end(), m.as_str().to_owned()));
            }
            // reverse the matches so that replacements don't alter offsets
            // if we went left to right, say "three" => "3" shifts everything 4 positions
            // can avoid that by processing right to left
            matches.reverse();
            //println!("{:?}", matches);
            for (start, end, m) in matches {
                //println!("{start}, {end}, {m}");
                // we control regex and map, so know that it will be there
                // so unwrap() is safe(-ish)
                let rep = map.get(&m).unwrap();
                line.replace_range(start..end, rep);
            }
            //println!("{line}");
        }

        let digits: Vec<char> = line.chars()
            .filter(|c| c.is_ascii_digit())
            .collect();
        // in case we have a line with no digits at all
        // or if an empty line finds it's way into the end
        if digits.is_empty() {
            continue
        }
        let mut numstr = String::new();
        // at this point we know we have at least one digit, so unwrap() should be safe
        numstr.push(*digits.first().unwrap());
        numstr.push(*digits.last().unwrap());
        //println!("  {numstr}");
        // similiarly, we know we have digits, so we must have a base-10 number to convert
        nums.push(numstr.parse::<u64>().unwrap());
    }
    //println!("{:?}", nums);
    Ok(nums.iter().sum())
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test2.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 281);
    Ok(())
}
