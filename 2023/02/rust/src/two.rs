use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use regex::Regex;
use std::collections::HashMap;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    //let game_re = Regex::new(r"Game\s+(\d+)").unwrap();
    let plays_re = Regex::new(r"(\s+(\d+)\s+(\w+),?)+").unwrap();
    let play_re = Regex::new(r"(\d+)\s+(\w+)").unwrap();
    let mut powers: Vec<u64> = vec![];
    for s in input.lines() {
        let line = s.unwrap();
        //let c = game_re.captures(&line).ok_or("no game captured")?;
        //let game_num = u64::from_str_radix(c.get(1).ok_or("no game number")?.as_str(), 10).unwrap();
        //println!("game: {game_num}");
        let mut maxes: HashMap<String, u64> = HashMap::new();
        for c in plays_re.captures_iter(&line) {
            let play = c.get(0).unwrap().as_str();
            //println!("  {play}");
            for cap in play_re.captures_iter(play) {
                //println!("    {:?}", cap);
                let count = cap.get(1).unwrap().as_str().parse::<u64>().unwrap();
                let key = cap.get(2).unwrap().as_str().to_owned();
                let test = maxes.get(&key);
                if let Some(max) = test {
                    if max < &count {
                        maxes.insert(key, count);
                    }
                }
                else {
                    maxes.insert(key, count);
                }
            }
        }
        let mut power = 1;
        for val in maxes.values() {
            power *= val;
        }
        powers.push(power);
    }
    
    Ok(powers.iter().sum())
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 2286);
    Ok(())
}
