use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use regex::Regex;
use std::collections::HashMap;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let limits: HashMap<String, u64> = HashMap::from([
        ("red".to_owned(), 12),
        ("green".to_owned(), 13),
        ("blue".to_owned(), 14),
    ]);
    let mut good_games: Vec<u64> = vec![];
    let game_re = Regex::new(r"Game\s+(\d+)").unwrap();
    let plays_re = Regex::new(r"(\s+(\d+)\s+(\w+),?)+").unwrap();
    let play_re = Regex::new(r"(\d+)\s+(\w+)").unwrap();
    for s in input.lines() {
        let line = s.unwrap();
        let c = game_re.captures(&line).ok_or("no game captured")?;
        let game_num = c.get(1).ok_or("no game number")?.as_str().parse::<u64>().unwrap();
        //println!("game: {game_num}");
        let mut good = true;
        for c in plays_re.captures_iter(&line) {
            let play = c.get(0).unwrap().as_str();
            //println!("  {play}");
            let mut cubes: HashMap<String, u64> = HashMap::new();
            for cap in play_re.captures_iter(play) {
                //println!("    {:?}", cap);
                let count = cap.get(1).unwrap().as_str().parse::<u64>().unwrap();
                let key = cap.get(2).unwrap().as_str().to_owned();
                cubes.insert(key, count);
            }
            //println!("{:?}", cubes);
            for (key, val) in limits.iter() {
                let test = cubes.get(key);
                //println!("{key}: {val}: {:?}", test);
                if test.is_some() && test.unwrap() > val {
                    good = false;
                    //println!("no good");
                    break;
                }
            }
            if !good {
                break;
            }
        }
        if good {
            good_games.push(game_num);
        }
        //println!("good games {:?}", good_games);
    }
    Ok(good_games.iter().sum())
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 8);
    Ok(())
}
