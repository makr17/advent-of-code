use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use regex::Regex;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();

    println!("Part One: {}", part_one(&data));

    println!("Part Two: {}", part_two(&data));

    Ok(())
}

#[derive(Debug)]
#[allow(dead_code)]
struct StuffMap {
    source: String,
    dest: String,
    start: i64,
    end: i64,
    offset: i64,
}

#[derive(Debug)]
struct Almanac {
    seeds: Vec<i64>,
    maps: Vec<Vec<StuffMap>>,
}

fn parse_input(input: BufReader<File>) -> Result<Almanac, Box::<dyn Error>> {
    let tag_re = Regex::new(r"(\w+)-to-(\w+)\smap")?;
    let id_re = Regex::new(r"(\d+)")?;
    let mut almanac = Almanac {
        seeds: vec![],
        maps: vec![],
    };
    let mut source = "".to_owned();
    let mut dest = "".to_owned();
    for s in input.lines() {
        let line = s?;
        if line.contains(':') {
            if line.contains("seeds") {
                almanac.seeds = id_re.captures_iter(&line)
                    .map(|n| n.get(0).unwrap().as_str().parse::<i64>().unwrap())
                    .collect()
            }
            else {
                let cap = tag_re.captures(&line).ok_or("failed to parse header line")?;
                source = cap.get(1).unwrap().as_str().to_owned();
                dest = cap.get(2).unwrap().as_str().to_owned();
            }
        }
        else if line.is_empty() {
            almanac.maps.push(vec![]);
        }
        else {
            let nums = id_re.captures_iter(&line)
                .map(|n| n.get(0).unwrap().as_str().parse::<i64>().unwrap())
                .collect::<Vec<i64>>();
            let idx = almanac.maps.len() - 1;
            almanac.maps[idx].push(
                StuffMap {
                    source: source.clone(),
                    dest: dest.clone(),
                    start: nums[1],
                    end: nums[1] + nums[2] - 1,
                    offset: nums[0] - nums[1],
                }
            );
        }
    }

    //println!("{:?}", almanac);

    Ok(almanac)
}

fn part_one(data: &Almanac) -> i64 {
    let mut locs: Vec<i64> = vec![];
    for seed in data.seeds.iter() {
        //println!("seed {seed}");
        let mut id = *seed;
        for maps in data.maps.iter() {
            for map in maps.iter() {
                //println!("{:?}", map);
                if (map.start ..= map.end).contains(&id) {
                    id += map.offset;
                    //println!("  {} {id}", map.dest);
                    break;
                }
            }
        }
        locs.push(id);
    }
    locs.sort();
    locs[0]
}

#[test]
fn test_part_one() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_one(&data), 35);
    Ok(())
}

fn part_two(data: &Almanac) -> i64 {
    let mut ranges = (0..data.seeds.len()/2)
        .map(|i| i*2)
        .map(|i| (data.seeds[i], data.seeds[i] + data.seeds[i+1] - 1))
        .collect::<Vec<(i64, i64)>>();
    ranges.sort();
    //println!("seeds: {:?}", ranges);

    for maps in data.maps.iter() {
        let mut map_ranges = maps.iter()
            .map(|m| (m.start, m.end, m.offset))
            .collect::<Vec<(i64, i64, i64)>>();
        map_ranges.sort();
        let mut new: Vec<(i64, i64)> = vec![];
        for r in ranges.iter() {
            let mut found = false;
            // short-circuit if we find a range that fits
            for m in map_ranges.iter() {
                if m.0 <= r.0 && m.1 >= r.1 {
                    new.push((r.0 + m.2, r.1 + m.2));
                    found = true;
                    break;
                }
            }
            if found {
                continue
            }
            // fall back to piecemeal
            let mut start = map_id(r.0, &map_ranges);
            let mut prev = start;
            for id in r.0+1 ..= r.1 {
                let mapped = map_id(id, &map_ranges);
                if mapped != prev + 1 {
                    new.push((start, prev));
                    start = mapped;
                }
                prev = mapped;
            }
            new.push((start, map_id(r.1, &map_ranges)));
        }
        new.sort();
        ranges = new;
        //println!("{} {:?}", maps[0].dest, ranges);
    }

    // low bound of first tuple
    ranges[0].0
}

fn map_id(id: i64, maps: &[(i64, i64, i64)]) -> i64 {
    let map = maps.iter()
        .filter(|m| m.0 <= id && m.1 >= id)
        .copied()
        .collect::<Vec<(i64, i64, i64)>>();
    //println!("{id}: {:?}", map);
    if map.is_empty() {
        id
    }
    else {
        id + map[0].2
    }
}

#[test]
fn test_part_two() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_two(&data), 46);
    Ok(())
}
