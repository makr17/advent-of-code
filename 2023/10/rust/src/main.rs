use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashMap;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();

    println!("Part One: {}", part_one(&data));

    println!("Part Two: {}", part_two(&data));

    Ok(())
}

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
// (x, y)
struct Pos(i64, i64);
impl Pos {
    fn neighbors(&self) -> Vec<Pos> {
        vec![Pos(self.0-1,self.1), Pos(self.0+1,self.1), Pos(self.0,self.1-1), Pos(self.0,self.1-1)]
    }

    fn routes(&self, pipe: char) -> Vec<Pos> {
        let mut r = vec![];
        match pipe {
            '|' => { // is a vertical pipe connecting north and south.
                r.push(Pos(self.0, self.1-1));
                r.push(Pos(self.0, self.1+1));
            },
            '-' => { // is a horizontal pipe connecting east and west.
                r.push(Pos(self.0-1, self.1));
                r.push(Pos(self.0+1, self.1));
            },
            'L' => { // is a 90-degree bend connecting north and east.
                r.push(Pos(self.0, self.1-1));
                r.push(Pos(self.0+1, self.1));
            },
            'J' => { // is a 90-degree bend connecting north and west.
                r.push(Pos(self.0, self.1-1));
                r.push(Pos(self.0-1, self.1));
            },
            '7' => { // is a 90-degree bend connecting south and west.
                r.push(Pos(self.0, self.1+1));
                r.push(Pos(self.0-1, self.1));
            },
            'F' => { // is a 90-degree bend connecting south and east.
                r.push(Pos(self.0, self.1+1));
                r.push(Pos(self.0+1, self.1));
            },
            _ => (),
        };
        r
    }

    fn successors(&self, map: &Map) -> Vec<(Pos, u32)> {
        let &Pos(x,y) = self;
        let mut succ: Vec<(Pos, u32)> = vec![];
        for tx in [x-1, x+1] {
            let pos = Pos(tx,y);
            let test = map.edges.get(&pos);
            if test == Some(&'.') || !test.is_some() {
                succ.push((pos.clone(), 1));
            }
        }
        for ty in [y-1, y+1] {
            let pos = Pos(x,ty);
            let test = map.edges.get(&pos);
            if test == Some(&'.') || !test.is_some() {
                succ.push((pos.clone(), 1));
            }
        }
        succ
    }
}

#[derive(Clone, Debug)]
struct Map {
    start: Pos,
    edges: HashMap<Pos,char>,
}

fn parse_input(input: BufReader<File>) -> Result<Map, Box::<dyn Error>> {
    let mut edges: HashMap<Pos,char> = HashMap::new();
    let mut start: Pos = Pos(0,0);
    for (y,s) in input.lines().enumerate() {
        for (x,c) in s?.chars().enumerate() {
            match c {
                'S' => {
                    start = Pos(x as i64, y as i64);
                },
                _ => {
                    edges.insert(Pos(x as i64, y as i64), c);
                }
            };
        }
    }
    Ok(Map {
        start,
        edges,
    })
}

fn part_one(map: &Map) -> usize {
    let pos = loop_pos(map);
    pos.len() / 2
}

fn loop_pos(map: &Map) -> Vec<Pos> {
    let routes = map.start.neighbors().iter()
        .map(|r| (r, map.edges.get(r)))
        .filter(|(_,e)| e.is_some())
        .filter(|(r,e)| r.routes(*e.unwrap()).contains(&map.start))
        .map(|(r,_)| r)
        .cloned()
        .collect::<Vec<Pos>>();
    let mut visited = vec![map.start.clone()];
    let mut pos = routes[0].clone();
    let mut prev = map.start.clone();
    loop {
        let routes = pos.routes(*map.edges.get(&pos).unwrap()).iter()
            .filter(|r| **r != prev)
            .cloned()
            .collect::<Vec<Pos>>();
        prev = pos;
        pos = routes[0].clone();
        visited.push(pos.clone());
        if pos == map.start {
            break
        }
    }
    visited
}

#[test]
fn test_part_one() -> io::Result<()> {
    let file0 = File::open("../test0.txt")?;
    let reader0 = BufReader::new(file0);
    let data0 = parse_input(reader0).unwrap();
    assert_eq!(part_one(&data0), 4);

    let file1 = File::open("../test1.txt")?;
    let reader1 = BufReader::new(file1);
    let data1 = parse_input(reader1).unwrap();
    assert_eq!(part_one(&data1), 8);

    Ok(())
}

fn part_two(map: &Map) -> usize {
    let mut enclosed = 0;
    let mut xs = map.edges.keys()
        .map(|p| p.0)
        .collect::<Vec<i64>>();
    xs.sort();
    let max_x = *xs.last().unwrap();
    let mut ys = map.edges.keys()
        .map(|p| p.1)
        .collect::<Vec<i64>>();
    ys.sort();
    let max_y = *ys.last().unwrap();

    for y in 0..=max_y {
        let mut parity = 0;
        for x in 0..=max_x {
            let test = map.edges.get(&Pos(x,y));
            if test == Some(&'|') {
                parity = (parity + 1) % 2;
            }
            if parity == 1 && test == Some(&'.') {
                enclosed += 1;
            }
            
        }
    }

    enclosed
}

#[test]
fn test_part_two() -> io::Result<()> {
    let file2 = File::open("../test2.txt")?;
    let reader2 = BufReader::new(file2);
    let data2 = parse_input(reader2).unwrap();
    assert_eq!(part_two(&data2), 4);

    let file3 = File::open("../test3.txt")?;
    let reader3 = BufReader::new(file3);
    let data3 = parse_input(reader3).unwrap();
    assert_eq!(part_two(&data3), 8);

    Ok(())
}
