use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::{HashMap, HashSet};

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let result = process(reader);
    println!("{}", result.unwrap());
    
    Ok(())
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash, Ord, PartialOrd)]
struct Point (usize, usize);

fn process(input: BufReader<File>) -> Result<u64, Box::<dyn Error>> {
    let mut symbols: HashMap<Point, char> = HashMap::new();
    let mut nums: HashMap<Vec<Point>, u64> = HashMap::new();

    // iterate over input, enumerated for x,y
    for (y, s) in input.lines().enumerate() {
        let mut numstr = String::new();
        let line = s.unwrap();
        for (x, c) in line.chars().enumerate() {
            // store chars based on value
            // ignore '.'
            match c {
                '0'..='9' => {
                    numstr.push(c);
                },
                _ => {
                    if !numstr.is_empty() {
                        // convert string to numeric and stash populated points
                        let num = numstr.parse::<u64>().unwrap();
                        let points = (x-numstr.len()..x).map(|xc| Point(xc, y)).collect();
                        nums.insert(points, num);
                        // and reset our string collector
                        numstr.truncate(0);
                    }
                    if c != '.' {
                        symbols.insert(Point(x,y), c);
                    }
                },
            };
        }
        // handle numbers bounded by newline
        if !numstr.is_empty() {
            let num = numstr.parse::<u64>().unwrap();
            let x = line.len() - 1;
            let points = (x-numstr.len()..x).map(|xc| Point(xc, y)).collect();
            nums.insert(points, num);
        }
    }
    //println!("{:?}", nums);
    //println!("{:?}", symbols);

    // look for gears
    let mut full_ratio = 0;
    for (point, c) in symbols.iter() {
        if *c != '*' {
            continue
        }
        // and points adjacent to our point
        let adjacent = adjacent_points(&[*point]);
        let mut count = 0;
        let mut ratio = 1;
        for (points, num) in nums.iter() {
            for p in points.iter() {
                if adjacent.contains(p) {
                    count += 1;
                    ratio *= num;
                    break;
                }
            }
        }
        if count == 2 {
            full_ratio += ratio;
        }
    }


    Ok(full_ratio)
}

fn adjacent_points(points: &[Point]) -> HashSet<Point> {
    let xl = points.first().unwrap().0 as i128 - 1;
    let xr = points.last().unwrap().0 as i128 + 1;
    let yu = points.first().unwrap().1 as i128 - 1;
    let yd = points.first().unwrap().1 as i128 + 1;
    let mut adj = HashSet::new();
    //println!("{:?}", points);
    for x in xl..=xr {
        if x < 0 {
            continue
        }
        for y in yu..=yd {
            if y < 0 {
                continue
            }
            let pnt = Point(x as usize, y as usize);
            //println!("  {:?}", pnt);
            // don't return digit points, only adjacent
            if points.contains(&pnt) {
                continue
            }
            //println!("    {:?}", pnt);
            adj.insert(pnt);
        }
    }
    adj
}

#[test]
fn test_adjacent_points() -> io::Result<()> {
    let mut adj = adjacent_points(&vec![Point(0,0), Point(1,0), Point(2,0)]);
    let mut expect = HashSet::from([
        Point(3,0),
        Point(0,1),
        Point(1,1),
        Point(2,1),
        Point(3,1),
    ]);
    assert_eq!(
        adj,
        expect
    );

    adj = adjacent_points(&vec![Point(6,2), Point(7,2), Point(8,2)]);
    expect = HashSet::from([
        Point(5,1),
        Point(6,1),
        Point(7,1),
        Point(8,1),
        Point(9,1),
        Point(5,2),
        Point(9,2),
        Point(5,3),
        Point(6,3),
        Point(7,3),
        Point(8,3),
        Point(9,3),
    ]);
    assert_eq!(
        adj,
        expect
    );

    Ok(())
}

#[test]
fn test_input() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    assert_eq!(process(reader).unwrap(), 467835);
    Ok(())
}
