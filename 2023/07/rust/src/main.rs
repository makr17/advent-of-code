use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::cmp::Ordering;
use std::collections::HashMap;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();

    println!("Part One: {}", part_one(&data));

    //println!("Part Two: {}", part_two(&data));

    Ok(())
}

#[derive(Clone, Debug)]
struct Card {
    value: char,
}
impl Card {
    fn strength(&self) -> u32 {
        match self.value {
            'A' => 14,
            'K' => 13,
            'Q' => 12,
            'J' => 11,
            'T' => 10,
            '2'..='9' => self.value.to_digit(10).unwrap(),
            _ => 0,
        }
    }
}

#[derive(Clone, Debug)]
struct Hand {
    cards: Vec<Card>,
    bid: u64,
}
impl Hand {
    fn strength(&self) -> u32 {
        let (count, _) = self.count_same();
        if count == 5 { // five of a kind
            7 
        }
        else if count == 4 { // four of a kind
            6
        }
        else if self.full_house() {
            5
        }
        else if count == 3 { // three of a kind
            4
        }
        else if self.two_pair() {
            3
        }
        else if count == 2 { // one pair
            2
        }
        else { // all distinct
            1
        }
    }

    fn count_same(&self) -> (u32, char) {
        let uniq = self.unique_counts();
        *uniq.last().unwrap()
    }

    fn unique_counts(&self) -> Vec<(u32, char)> {
        let mut uniq: HashMap<char, u32> = HashMap::new();
        for card in self.cards.iter() {
            *uniq.entry(card.value).or_default() += 1;
        }
        let mut sorted: Vec<(u32, char)> = uniq.iter()
            .map(|(c, n)| (*n, *c))
            .collect();
        sorted.sort();
        sorted
    }

    fn full_house(&self) -> bool {
        let mut three = false;
        let mut two = false;
        for uniq in self.unique_counts().iter() {
            if uniq.0 == 3 {
                three = true;
            }
            if uniq.0 == 2 {
                two = true;
            }
        }
        three && two
    }

    fn two_pair(&self) -> bool {
        let count_pairs = self.unique_counts().iter()
            .filter(|(n, _)| n == &2)
            .count();
        count_pairs == 2
    }
}
impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        (self.strength(), self.cards[0].strength(), self.cards[1].strength(), self.cards[2].strength(), self.cards[3].strength(), self.cards[4].strength()) == (other.strength(), other.cards[0].strength(), other.cards[1].strength(), other.cards[2].strength(), other.cards[3].strength(), other.cards[4].strength())
    }
}
impl Eq for Hand { }
impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.strength(), self.cards[0].strength(), self.cards[1].strength(), self.cards[2].strength(), self.cards[3].strength(), self.cards[4].strength()).cmp(&(other.strength(), other.cards[0].strength(), other.cards[1].strength(), other.cards[2].strength(), other.cards[3].strength(), other.cards[4].strength()))
    }
}
impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn parse_input(input: BufReader<File>) -> Result<Vec<Hand>, Box::<dyn Error>> {
    let mut hands = vec![];
    for s in input.lines() {
        let line = s.unwrap();
        let parts: Vec<&str> = line.split(' ').collect();
        let card_str = parts[0];
        let cards = card_str.chars()
            .map(|value| Card { value })
            .collect();
        let bid = parts[1].parse::<u64>().unwrap();
        hands.push(
            Hand {
                cards,
                bid
            }
        );
    }

    Ok(hands)
}

fn part_one(data: &[Hand]) -> u64 {
    let mut hands: Vec<Hand> = data.to_vec();
    hands.sort();
    //println!("{:?}", hands);
    hands.iter().enumerate()
        .map(|(idx, h)| (idx as u64 + 1) * h.bid )
        .sum()
}

#[test]
fn test_part_one() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_one(&data), 6440);
    Ok(())
}
