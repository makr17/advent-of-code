use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::cmp::Ordering;
use std::collections::HashMap;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();

    println!("Part Two: {}", part_two(&data));

    Ok(())
}

#[derive(Clone, Debug)]
struct Card {
    value: char,
}
impl Card {
    fn strength(&self) -> u32 {
        match self.value {
            'A' => 14,
            'K' => 13,
            'Q' => 12,
            'J' => 1,
            'T' => 10,
            '2'..='9' => self.value.to_digit(10).unwrap(),
            _ => 0,
        }
    }
}

#[allow(dead_code)]
#[derive(Clone, Debug)]
struct Hand {
    cards: Vec<Card>,
    bid: u64,
    card_string: String,
}
impl Hand {
    fn new(card_str: &str, bid: u64) -> Self {
        let cards: Vec<Card> = card_str.chars()
            .map(|value| Card { value })
            .collect();
        Self {
            cards,
            bid,
            card_string: card_str.to_owned(),
        }
    }
    fn strength(&self) -> u32 {
        let (count, _) = self.count_same();
        if count == 5 { // five of a kind
            7 
        }
        else if count == 4 { // four of a kind
            6
        }
        else if self.full_house() {
            5
        }
        else if count == 3 { // three of a kind
            4
        }
        else if self.two_pair() {
            3
        }
        else if count == 2 { // one pair
            2
        }
        else { // all distinct
            1
        }
    }

    fn count_same(&self) -> (u32, char) {
        let uniq = self.unique_counts();
        if uniq[0].1 == 'J' {
            // more jokers than anything else
            // what if it is five jokers?
            if uniq[0].0 == 5 {
                uniq[0]
            }
            else {
                // take next-most-common and add the jokers
                (uniq[1].0 + uniq[0].0, uniq[1].1)
            }
        }
        else {
            (uniq[0].0 + self.count_jokers(), uniq[0].1)
        }
    }

    fn count_jokers(&self) -> u32 {
        self.unique_counts().iter()
            .filter(|(_, c)| c == &'J')
            .map(|(n, _)| n)
            .sum()
    }

    fn unique_counts(&self) -> Vec<(u32, char)> {
        let mut uniq: HashMap<char, u32> = HashMap::new();
        for card in self.cards.iter() {
            *uniq.entry(card.value).or_default() += 1;
        }
        let mut sorted: Vec<(u32, char)> = uniq.iter()
            .map(|(c, n)| (*n, *c))
            .collect();
        sorted.sort();
        sorted.reverse();
        sorted
    }

    fn full_house(&self) -> bool {
        let uniq = self.unique_counts();
        let mut no_jokers = uniq.iter()
            .filter(|(_, c)| c != &'J')
            .map(|(n, c)| (*n, *c))
            .collect::<Vec<(u32, char)>>();
        no_jokers[0].0 += self.count_jokers();
        no_jokers[0].0 == 3 && no_jokers[1].0 == 2
    }

    // I don't see any way to get to two pair with jokers
    // if we had one pair and then one joker, it would be three of a kind
    // if we had two jokers and three other distinct, also three of a kind
    fn two_pair(&self) -> bool {
        let count_pairs = self.unique_counts().iter()
            .filter(|(n, _)| n == &2)
            .count();
        count_pairs == 2
    }
}
impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        (self.strength(), self.cards[0].strength(), self.cards[1].strength(), self.cards[2].strength(), self.cards[3].strength(), self.cards[4].strength()) == (other.strength(), other.cards[0].strength(), other.cards[1].strength(), other.cards[2].strength(), other.cards[3].strength(), other.cards[4].strength())
    }
}
impl Eq for Hand { }
impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.strength(), self.cards[0].strength(), self.cards[1].strength(), self.cards[2].strength(), self.cards[3].strength(), self.cards[4].strength()).cmp(&(other.strength(), other.cards[0].strength(), other.cards[1].strength(), other.cards[2].strength(), other.cards[3].strength(), other.cards[4].strength()))
    }
}
impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[test]
fn test_hand_strength() {
    let four_with_joker = Hand::new("KTJJT", 0);
    assert_eq!(four_with_joker.strength(), 6);
    let full_house_with_joker = Hand::new("KKJQQ", 0);
    assert_eq!(full_house_with_joker.strength(), 5);
    let pair_with_joker = Hand::new("2345J", 0);
    assert_eq!(pair_with_joker.strength(), 2);
    let five_with_joker = Hand::new("JJJJ2", 0);
    assert_eq!(five_with_joker.strength(), 7);
    let five_jokers = Hand::new("JJJJJ", 0);
    assert_eq!(five_jokers.strength(), 7);
    assert_eq!(five_jokers.cards[0].strength(), 1);
    let three_with_jokers = Hand::new("JJ857", 0);
    assert_eq!(three_with_jokers.strength(), 4);
    let three_twos_with_joker = Hand::new("J2T2Q", 4);
    assert_eq!(three_twos_with_joker.strength(), 4);
}

fn parse_input(input: BufReader<File>) -> Result<Vec<Hand>, Box::<dyn Error>> {
    let mut hands = vec![];
    for s in input.lines() {
        let line = s.unwrap();
        let parts: Vec<&str> = line.split(' ').collect();
        let card_str = parts[0];
        let bid = parts[1].parse::<u64>().unwrap();
        hands.push(Hand::new(card_str, bid));
    }

    Ok(hands)
}

fn part_two(data: &[Hand]) -> u64 {
    let mut hands: Vec<Hand> = data.to_vec();
    hands.sort();
    //println!("{:?}", hands);
    hands.iter().enumerate()
        .map(|(idx, h)| (idx as u64 + 1) * h.bid )
        .sum()
}

#[test]
fn test_part_two() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_two(&data), 5905);
    Ok(())
}
