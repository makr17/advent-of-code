# 2023 AoC

Implementing in rust this year.

Only 19 stars this year:
* missed second half of day 10, just couldn't get it to work while sitting in an airport lounge
* missed 11-25 due to travel and time constraints, and generally lack of motivation to get out my laptop and attempt anything.
