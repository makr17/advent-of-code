use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashMap;

use num::integer::lcm;
use rayon::prelude::*;
use regex::Regex;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();

    println!("Part One: {}", part_one(&data));

    println!("Part Two: {}", part_two(&data));

    Ok(())
}

#[derive(Debug)]
struct Map {
    moves: Vec<char>,
    routes: HashMap<String, (String, String)>,
}

fn parse_input(input: BufReader<File>) -> Result<Map, Box::<dyn Error>> {
    let mut moves = vec![];
    let mut routes = HashMap::new();
    let line_re = Regex::new(r"(\w+)\s=\s\((\w+),\s(\w+)\)").unwrap();
    for s in input.lines() {
        let line = s.unwrap();
        let cap = line_re.captures(&line);
        if cap.is_some() {
            let c = cap.unwrap();
            let from = c.get(1).unwrap().as_str().to_owned();
            let left = c.get(2).unwrap().as_str().to_owned();
            let right = c.get(3).unwrap().as_str().to_owned();
            routes.insert(from, (left, right));
        }
        else if !line.is_empty() {
            moves = line.chars().collect::<Vec<char>>();
        }
    }
    let map = Map { moves, routes };
    //println!("{:?}", map);
    Ok(map)
}

fn part_one(map: &Map) -> usize {
    let mut pos = "AAA";
    let mut count = 0;
    while pos != "ZZZ" {
        let lr = map.routes.get(pos).unwrap();
        let dir = map.moves[count % map.moves.len()];
        //println!("at {}, routes {:?}, choosing {}", pos, lr, dir);
        pos = match dir {
            'L' => &lr.0,
            'R' => &lr.1,
            _ => &lr.0, // but should not happen
        };
        count += 1;
    }
    count
}

#[test]
fn test_part_one() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_one(&data), 6);
    Ok(())
}

fn part_two(map: &Map) -> usize {
    let start_re = Regex::new(r"..A").unwrap();
    let pos = map.routes.keys()
        .filter(|n| start_re.is_match(n))
        .cloned()
        .collect::<Vec<String>>();
    // count steps down each path
    let counts = pos.par_iter()
        .map(|p| route_moves(p.clone(), map))
        .collect::<Vec<usize>>();
    //println!("{:?}", counts);

    // then the cycles all resolve with lcm (least common multiple) across the vec of counts
    let mut count = counts[0];
    for c in counts[1..].iter() {
        count = lcm(count, *c);
    }
        
    count
}

fn route_moves(node: String, map: &Map) -> usize {
    let end_re: Regex = Regex::new(r"..Z").unwrap();
    let mut pos = node.clone();
    let mut count = 0;
    while !end_re.is_match(&pos) {
        let dir = map.moves[count % map.moves.len()];
        pos = pick_move(dir, pos, map);
        count += 1;
    }
    count
}

fn pick_move(dir: char, node: String, map: &Map) -> String {
    let lr = map.routes.get(&node).unwrap();
    match dir {
        'L' => lr.0.clone(),
        'R' => lr.1.clone(),
        _ => lr.0.clone(), // but should not happen
    }
}

#[test]
fn test_part_two() -> io::Result<()> {
    let file = File::open("../test2.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_two(&data), 6);
    Ok(())
}

