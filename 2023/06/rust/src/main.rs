use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use regex::Regex;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();

    println!("Part One: {}", part_one(&data));

    println!("Part Two: {}", part_two(&data));

    Ok(())
}

#[derive(Debug)]
struct Race {
    time: u64,
    distance: u64,
}

fn parse_input(mut input: BufReader<File>) -> Result<Vec<Race>, Box::<dyn Error>> {
    let mut line1 = String::new();
    let mut _len = input.read_line(&mut line1)?;
    let times = parse_line(&line1);
    let mut line2 = String::new();
    _len = input.read_line(&mut line2)?;
    let distances = parse_line(&line2);
    let races = times.iter().zip(distances.iter())
        .map(|(t,d)| Race { time: *t, distance: *d })
        .collect();
        
    Ok(races)
}

fn parse_line(line: &str) -> Vec<u64> {
    let nums_re = Regex::new(r"(\d+)").unwrap();
    nums_re.captures_iter(line)
        .map(|n| n.get(0).unwrap().as_str().parse::<u64>().unwrap())
        .collect()
}

fn part_one(data: &[Race]) -> u64 {
    let mut result = 1;
    for race in data.iter() {
        let mut wins = 0;
        for t in 1..race.time {
            let d = t * (race.time - t);
            if d > race.distance {
                wins += 1;
            }
        }
        result *= wins;
    }

    result
}

#[test]
fn test_part_one() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_one(&data), 288);
    Ok(())
}

fn part_two(data: &[Race]) -> u64 {
    let time = data.iter()
        .map(|n| n.time.to_string())
        .collect::<String>()
        .parse::<u64>().unwrap();
    let distance = data.iter()
        .map(|n| n.distance.to_string())
        .collect::<String>()
        .parse::<u64>().unwrap();

    let mut wins = 0;
    for t in 1..time {
        let d = t * (time - t);
        if d > distance {
            wins += 1;
        }
    }
    
    wins
}

#[test]
fn test_part_two() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_two(&data), 71503);
    Ok(())
}
