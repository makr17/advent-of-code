use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use regex::Regex;
use std::collections::HashSet;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();

    println!("Part One: {}", part_one(&data));

    println!("Part Two: {}", part_two(&data));

    Ok(())
}

fn parse_input(input: BufReader<File>) -> Result<Vec<Vec<HashSet<u32>>>, Box::<dyn Error>> {
    let mut data: Vec<Vec<HashSet<u32>>> = vec![];

    let card_re = Regex::new(r"([\d\s]+)|([\d\s]+)").unwrap();
    let nums_re = Regex::new(r"(\d+)").unwrap();

    for s in input.lines() {
        let line = s.unwrap();
        let mut card = vec![];
        let parts = line.split(':');
        for c in card_re.captures_iter(parts.last().unwrap()) {
            //println!("{:?}", c);
            let cap = c.get(0).unwrap().as_str();
            //println!("{cap}");
            let nums = nums_re.captures_iter(cap)
                .map(|n| n.get(0).unwrap().as_str().parse::<u32>().unwrap())
                .collect::<HashSet<u32>>();
            //println!("{:?}", nums);
            card.push(nums);
        }
        data.push(card);
    }

    //println!("{:?}", data);
    Ok(data)
}

fn part_one(data: &[Vec<HashSet<u32>>]) -> u64 {
    let mut points = 0;
    for card in data.iter() {
        let winners = card.first().unwrap();
        let numbers = card.last().unwrap();
        let matched = winners.intersection(numbers).collect::<Vec<&u32>>().len();
        //println!("{:?} | {:?} => {:?}", winners, numbers, matched);
        if matched == 0 {
            continue
        }
        points += 2_u64.pow(matched as u32 - 1);
    }
    points
}

#[test]
fn test_part_one() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_one(&data), 13);
    Ok(())
}

fn part_two(data: &[Vec<HashSet<u32>>]) -> u64 {
    // how many follow-on cards does each card win
    let winnings = data.iter()
        .map(|c| c.first().unwrap().intersection(c.last().unwrap()).collect::<Vec<&u32>>().len())
        .collect::<Vec<usize>>();
    //println!("{:?}", winnings);

    // how many of each card do we have
    // starts at 1, since that's what we have
    let mut counts: Vec<usize> = vec![1; winnings.len()];
    for (idx, wins) in winnings.iter().enumerate() {
        // add the next N cards based on wins
        for i in idx+1..=idx+wins {
            // and add one for each of the current card that we have
            counts[i] += counts[idx];
        }
    }
    
    counts.iter().map(|c| *c as u64).sum()
}

#[test]
fn test_part_two() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_two(&data), 30);
    Ok(())
}
