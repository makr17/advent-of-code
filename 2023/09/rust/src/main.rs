use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

use std::collections::HashSet;

use rayon::prelude::*;

fn main() -> io::Result<()> {
    let file = File::open("../input.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();

    println!("Part One: {}", part_one(&data));

    println!("Part Two: {}", part_two(&data));

    Ok(())
}

fn parse_input(input: BufReader<File>) -> Result<Vec<Vec<i64>>, Box::<dyn Error>> {
    let mut series: Vec<Vec<i64>> = vec![];
    for s in input.lines() {
        let line = s.unwrap();
        let nums = line.split(' ')
            .map(|n| n.parse::<i64>().unwrap())
            .collect::<Vec<i64>>();
        series.push(nums);
    }
    Ok(series)
}

fn part_one(series: &[Vec<i64>]) -> i64 {
    series.par_iter()
        .map(|s| next_value(s))
        .sum()
}

fn next_value(series: &[i64]) -> i64 {
    //println!("{:?}", series);

    let mut prev = series.to_vec();
    let mut stack: Vec<Vec<i64>> = vec![prev.clone()];
    loop {
        let diff = prev.windows(2)
            .map(|w| w[1] - w[0])
            .collect::<Vec<i64>>();
        let uniq = diff.iter().collect::<HashSet<&i64>>();
        if uniq.len() == 1 && uniq.contains(&0) {
            break;
        }
        stack.push(diff.clone());
        prev = diff;
    }

    //println!("{:?}", stack);

    let mut next_diff = 0;
    stack.reverse();
    for s in stack.iter() {
        next_diff = s[s.len()-1] + next_diff;
        //println!("{:?} => {}", s, next_diff);
    }
    
    next_diff
}

#[test]
fn test_part_one() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_one(&data), 114);
    Ok(())
}

fn part_two(series: &[Vec<i64>]) -> i64 {
    series.par_iter()
        .map(|s| prev_value(s))
        .sum()
}

fn prev_value(series: &[i64]) -> i64 {
    //println!("{:?}", series);

    let mut prev = series.to_vec();
    let mut stack: Vec<Vec<i64>> = vec![prev.clone()];
    loop {
        let diff = prev.windows(2)
            .map(|w| w[1] - w[0])
            .collect::<Vec<i64>>();
        let uniq = diff.iter().collect::<HashSet<&i64>>();
        if uniq.len() == 1 && uniq.contains(&0) {
            break;
        }
        stack.push(diff.clone());
        prev = diff;
    }

    //println!("{:?}", stack);

    let mut prev_diff = 0;
    stack.reverse();
    for s in stack.iter() {
        prev_diff = s[0] - prev_diff;
        //println!("{:?} => {}", s, prev_diff);
    }
    
    prev_diff
}

#[test]
fn test_part_two() -> io::Result<()> {
    let file = File::open("../test.txt")?;
    let reader = BufReader::new(file);
    let data = parse_input(reader).unwrap();
    assert_eq!(part_two(&data), 2);
    Ok(())
}
