use std::collections::{HashMap, HashSet};
use std::ops::{Add, Sub};

use itertools::{iproduct, Itertools};
use lazy_static::lazy_static;
use pathfinding::prelude::astar_bag;

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input));
}

// points in the maze
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

lazy_static! {
    static ref MOVES: HashMap<Point, char> = HashMap::from([
        (Point { x:  0, y:  1 }, 'v'),
        (Point { x:  0, y: -1 }, '^'),
        (Point { x:  1, y:  0 }, '>'),
        (Point { x: -1, y:  0 }, '<'),
    ]);
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Arm {
    pos: Point,
    dir: Option<char>,
}
impl Arm {
    fn neighbors(self, map: &HashMap<Point, char>) -> HashSet<Self> {
        MOVES.iter()
            .map(|(p,dir)| Self { pos: self.pos + *p, dir: Some(*dir) })
            .filter(|arm| map.contains_key(&arm.pos))
            .collect()
    }

    // rough distance metric to guide A*
    fn distance(&self, other: &Point) -> usize {
        (self.pos.x.abs_diff(other.x) + self.pos.y.abs_diff(other.y)) as usize
    }

    // find reachable points
    // need to penalize turns
    // e.g. v<< ends up being more efficient than <v<
    //   not on the initial conversion, they appear isomorphic
    // but once we move up the stack of pads
    //   v<< becomes <vA<AA
    //   <v< becomes <v<A>A<A, two extra presses
    // but can't just sort the paths, because that could put us over gaps
    fn successors(self, map: &HashMap<Point, char>) -> Vec<(Self, usize)> {
        let s = self.neighbors(map).iter()
            .map(|arm|
                 // no inital direction
                 // after that penalize movement in another direction
                 (*arm, match self.dir {
                     Some(_) => {
                         // preference same moves together
                         if arm.dir == self.dir {
                             1
                         }
                         else {
                             10
                         }
                     },
                     None => 1,
                 })
            )
            .collect();
        //println!("{:?}: {:?}", self, s);
        s
    }
}

lazy_static! {
    /*
+---+---+---+
| 7 | 8 | 9 |
+---+---+---+
| 4 | 5 | 6 |
+---+---+---+
| 1 | 2 | 3 |
+---+---+---+
    | 0 | A |
    +---+---+
    */
    static ref NUM_MAP: HashMap<Point, char> = HashMap::from([
        (Point { x: 0, y: 0 }, '7'),
        (Point { x: 1, y: 0 }, '8'),
        (Point { x: 2, y: 0 }, '9'),
        (Point { x: 0, y: 1 }, '4'),
        (Point { x: 1, y: 1 }, '5'),
        (Point { x: 2, y: 1 }, '6'),
        (Point { x: 0, y: 2 }, '1'),
        (Point { x: 1, y: 2 }, '2'),
        (Point { x: 2, y: 2 }, '3'),
        (Point { x: 1, y: 3 }, '0'),
        (Point { x: 2, y: 3 }, 'A'),
    ]);

    /*
    +---+---+
    | ^ | A |
+---+---+---+
| < | v | > |
+---+---+---+
    */
    static ref DIR_MAP: HashMap<Point, char> = HashMap::from([
        (Point { x: 1, y: 0 }, '^'),
        (Point { x: 2, y: 0 }, 'A'),
        (Point { x: 0, y: 1 }, '<'),
        (Point { x: 1, y: 1 }, 'v'),
        (Point { x: 2, y: 1 }, '>'),
    ]);
}

fn process(input: &str) -> usize {
    let codes = input.split("\n").filter(|l| !l.is_empty())
        .collect::<Vec<&str>>();
    let mut paths = derive_paths(&NUM_MAP);
    let dir_paths = derive_paths(&DIR_MAP);
    paths.extend(dir_paths);
    let mut sequences: HashMap<String, Vec<String>> = HashMap::new();
    let mut cmplx = 0;
    for code in codes {
        // deal with sequences and ocurrence counts
        let mut expansion: HashMap<String, usize> = HashMap::new();
        for seq in expand(code.to_string(), &paths, &mut sequences) {
            expansion.entry(seq)
                .and_modify(|v| *v += 1)
                .or_insert(1);
        }
        // work through sequence expansion from level to level
        for _i in 0..25 {
            //println!("{:?}", expansion);
            let mut new: HashMap<String, usize> = HashMap::new();
            for (seq, count) in expansion.iter() {
                for c in expand(seq.clone(), &paths, &mut sequences) {
                    new.entry(c)
                        .and_modify(|v| *v += count)
                        .or_insert(*count);
                }
            }
            expansion = new;
        }
        //println!("{:?}", expansion);
        let numeric = code.chars()
            .filter(|c| c.is_ascii_digit())
            .collect::<String>()
            .parse::<usize>().unwrap();
        // now use each key sequence length times the number of times it occurs
        let length: usize = expansion.iter().map(|(s,c)| s.len() * c).sum();
        println!("{code}:  complexity = {length} * {numeric}");
        cmplx += length * numeric;
    }

    cmplx
}

fn expand(code: String, paths: &HashMap<(char, char), Vec<char>>, cache: &mut HashMap<String, Vec<String>>) -> Vec<String> {
    match cache.get(&code) {
        // cached sequence of keys
        Some(s) => s.clone(),
        None => {
            // not cached (yet)
            // build sequence from paths
            let mut key = 'A';
            let mut presses: Vec<String> = vec![];
            for c in code.chars() {
                //println!("{key} : {c}");
                let mut path = paths.get(&(key, c)).unwrap().clone();
                path.push('A');
                presses.push(path.iter().collect::<String>());
                key = c;
            }
            // every sequence always ends with enter/A
            cache.insert(code.to_string(), presses.clone());
            //println!("  new {}: {:?}", code, presses.clone());
            presses
        },
    }
}

fn derive_paths(map: &HashMap<Point, char>) -> HashMap<(char, char), Vec<char>> {
    let invert = map.iter()
        .map(|(p, c)| (*c, *p))
        .collect::<HashMap<char, Point>>();
    let mut paths = HashMap::new();
    // optimal paths from one key to all others
    for (from, to) in iproduct!(map.values(), map.values()) {
        let start = Arm {
            pos: *invert.get(from).unwrap(),
            dir: None,
        };
        let goal = invert.get(to).unwrap();
        let bag = astar_bag(&start, |a: &Arm| a.successors(map), |a| a.distance(goal)/3, |a| a.pos == *goal);
        let solutions = bag.iter().map(|(b, _s)| b.clone().collect::<Vec<Vec<Arm>>>()).collect::<Vec<Vec<Vec<Arm>>>>();
        let charvecs = solutions[0].iter()
            .map(|v|
                 v.iter().tuple_windows()
                 .map(|(this, next)| {
                     // turn succession of nodes into chain of moves
                     let m = next.pos - this.pos;
                     // and then lookup the char for that move
                     *MOVES.get(&m).unwrap()
                 })
                 .collect::<Vec<char>>()
            )
            .collect::<Vec<Vec<char>>>();
        let path = if charvecs.len() == 1 {
            // only one path option
            charvecs[0].clone()
        }
        else {
            // more than one path option
            // preference the one that starts with '<'
            // since '<' at the end of a movement chain can be expensive
            // at the next iteration, since '<':'A' is so far
            //   can hopefully catch the other keys on the way back to 'A'
            let lefties = charvecs.iter()
                .filter(|v| v[0] == '<')
                .cloned()
                .collect::<Vec<Vec<char>>>();
            let downers = charvecs.iter()
                .filter(|v| v[0] == 'v')
                .cloned()
                .collect::<Vec<Vec<char>>>();
            // preference early left moves
            if !lefties.is_empty() {
                lefties[0].clone()
            }
            // followed by early down moves
            else if !downers.is_empty() {
                downers[0].clone()
            }
            // up and right are inverses, one move from A and one inverse move back
            else {
                // shrug
                // until I have another metric/preference
                charvecs[0].clone()
            }
        };
        paths.insert((*from, *to), path);
    }
    paths
}

/*
// no expected result given for 25 internal iterations
#[test]
fn test() {
    assert_eq!(process(include_str!("../../test.txt")), 126384);
}
*/
