#![recursion_limit = "1024"]
use std::collections::{HashMap, HashSet};
use std::ops::Add;

use lazy_static::lazy_static;
use pathfinding::prelude::astar_bag;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

// possible directions
#[allow(dead_code)]
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
enum Direction {
    N,
    S,
    E,
    W,
}

// points in the maze
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Point {
    // moves in all possible directions
    // but only if there is no wall blocking
    fn moves(self, walls: &HashSet<Self>) -> HashMap<Direction, Self> {
        MOVES.iter()
            .map(|(d, p)| (*d, self + *p))
            .filter(|(_d, p)| !walls.contains(p))
            .collect()
    }
}

lazy_static! {
    // directions and associated point translations
    static ref MOVES: HashMap<Direction, Point> = HashMap::from([
        (Direction::N, Point { x: 0, y: -1 }),
        (Direction::E, Point { x: 1, y: 0 }),
        (Direction::S, Point { x: 0, y: 1 }),
        (Direction::W, Point { x: -1, y: 0 }),
    ]);

    // right turn from current direction
    static ref RIGHT: HashMap<Direction, Direction> = HashMap::from([
        (Direction::N, Direction::E),
        (Direction::E, Direction::S),
        (Direction::S, Direction::W),
        (Direction::W, Direction::N),
    ]);

    // left turn from current direction
    static ref LEFT: HashMap<Direction, Direction> = HashMap::from([
        (Direction::N, Direction::W),
        (Direction::W, Direction::S),
        (Direction::S, Direction::E),
        (Direction::E, Direction::N),
    ]);
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Deer {
    pos: Point,
    facing: Direction,
}
impl Deer {
    // rough distance metric to guide A*
    fn distance(&self, other: &Point) -> usize {
        (self.pos.x.abs_diff(other.x) + self.pos.y.abs_diff(other.y)) as usize
    }

    // work out possible moves, and associated cost
    // given reindeer with current direction and position of walls
    fn successors(&self, walls: &HashSet<Point>) -> Vec<(Self, usize)> {
        self.pos.moves(walls).iter()
            .filter(|(_d, p)| !walls.contains(p))
            .map(|(d, p)| {
                let mut deer = *self;
                deer.pos = *p;
                if *d == deer.facing {
                    (deer, MOVE_COST)
                }
                else if d == RIGHT.get(&deer.facing).unwrap() {
                    deer.facing = *RIGHT.get(&deer.facing).unwrap();
                    (deer, TURN_COST + MOVE_COST)
                }
                else if d == LEFT.get(&deer.facing).unwrap() {
                    deer.facing = *LEFT.get(&deer.facing).unwrap();
                    (deer, TURN_COST + MOVE_COST)
                }
                else {
                    deer.facing = *d;
                    (deer, 2*TURN_COST + MOVE_COST)
                }
            })
            .collect::<Vec<(Self, usize)>>()
    }
}
#[test]
fn test_successors() {
    let deer = Deer {
        pos: Point { x: 1, y: 1},
        facing: Direction::E,
    };
    // test straigh ahead and a turn
    let mut walls = HashSet::from([
        Point { x: 0, y: 0 },
        Point { x: 0, y: 1 },
        Point { x: 0, y: 2 },
        Point { x: 1, y: 0 },
        Point { x: 2, y: 0 },
        Point { x: 2, y: 2 },
    ]);
    visualize(&walls, deer, Point { x: 2, y: 1 });
    // use a HashSet for order-independent equality check
    let mut successors = deer.successors(&walls).iter().cloned().collect::<HashSet<(Deer, usize)>>();
    let mut expect = HashSet::from([
        (Deer { pos: Point { x: 2, y: 1 }, facing: Direction::E }, 1),
        (Deer { pos: Point { x: 1, y: 2 }, facing: Direction::S }, 1001),
    ]);
    assert_eq!(successors, expect);
    // test 180
    walls.remove(&Point { x: 0, y: 1 });
    visualize(&walls, deer, Point { x: 2, y: 1 });
    successors = deer.successors(&walls).iter().cloned().collect::<HashSet<(Deer, usize)>>();
    expect = HashSet::from([
        (Deer { pos: Point { x: 2, y: 1 }, facing: Direction::E }, 1),
        (Deer { pos: Point { x: 1, y: 2 }, facing: Direction::S }, 1001),
        (Deer { pos: Point { x: 0, y: 1 }, facing: Direction::W }, 2001),
    ]);
    assert_eq!(successors, expect);
}

const MOVE_COST: usize = 1;
const TURN_COST: usize = 1000;

fn process(input: &str) -> usize {
    let mut walls: HashSet<Point> = HashSet::new();
    let mut deer = Deer {
        pos: Point { x: -1, y: -1 },
        facing: Direction::E,
    };
    let mut end = Point { x: -1, y: -1 };
    for (y, line) in input.split("\n").filter(|line| !line.is_empty()).enumerate() {
        for (x,c) in line.chars().enumerate() {
            if c == '.' {
                continue;
            }
            let p = Point { x: x as i64, y: y as i64 };
            if c == '#' {
                walls.insert(p);
            }
            else if c == 'S' {
                deer.pos = p;
            }
            else if c == 'E' {
                end = p;
            }
        }
    }
    let bag = astar_bag(&deer, |d| d.successors(&walls), |d| d.distance(&end)/3, |d| d.pos == end);
    let onpath = bag.iter()
        .flat_map(|(path, _cost)| path.clone().flatten())
        .map(|deer| deer.pos)
        .collect::<HashSet<Point>>();
    onpath.len()
}

#[allow(dead_code)]
fn visualize(walls: &HashSet<Point>, deer: Deer, end: Point) {
    #[allow(unused_variables)]
    let mut xs = walls.iter().map(|Point { x, y }| x).collect::<Vec<&i64>>();
    xs.sort_by(|a, b| b.cmp(a));
    #[allow(unused_variables)]
    let mut ys = walls.iter().map(|Point { x, y }| y).collect::<Vec<&i64>>();
    ys.sort_by(|a, b| b.cmp(a));
    for y in 0..=*ys[0] {
        for x in 0..=*xs[0] {
            let p = Point { x, y };
            print!("{}",
                   if p == deer.pos {
                       'S'
                   }
                   else if p == end {
                       'E'
                   }
                   else if walls.contains(&p) {
                       '#'
                   }
                   else {
                       '.'
                   }
            );
        }
        println!();
    }
    println!();
}

#[test]
fn test1() {
    assert_eq!(process(include_str!("test1.txt")), 45);
}

#[test]
fn test2() {
    assert_eq!(process(include_str!("test2.txt")), 64);
}

// https://www.reddit.com/r/adventofcode/comments/1hfhgl1/2024_day_16_part_1_alternate_test_case/
#[test]
fn test3() {
    assert_eq!(process(include_str!("test3.txt")), 149);
}
