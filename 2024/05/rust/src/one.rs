use std::collections::{HashMap, HashSet};

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> u64 {
    let mut rules: HashMap::<u64,HashSet<u64>> = HashMap::new();
    let mut updates: Vec<Vec<u64>> = vec![];
    for line in input.split("\n").filter(|s| !s.is_empty()) {
        if line.contains("|") {
            let rule: Vec<u64> = line.split("|")
                .map(|n| n.parse::<u64>().unwrap())
                .collect();
            rules.entry(rule[1])
                .and_modify(|r| { r.insert(rule[0]); })
                .or_insert(HashSet::from([rule[0]]));
        }
        else if line.contains(",") {
            let pages: Vec<u64> = line.split(",")
                .map(|n| n.parse::<u64>().unwrap())
                .collect();
            updates.push(pages);
        }
    }

    let mut sum = 0;
    for update in updates {
        if correct(&update, &rules) {
            sum += update[update.len()/2];
        }
    }

    sum
}

fn correct(update: &[u64], rules: &HashMap<u64, HashSet<u64>>) -> bool {
    for (idx, page) in update.iter().enumerate() {
        if rules.contains_key(page) {
            let rule = rules.get(page).unwrap();
            if rule.intersection(&(update[idx+1..].iter().cloned().collect())).count() > 0 {
                return false;
            }
        }
    }

    true
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 143);
}
