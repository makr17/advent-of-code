use std::collections::{HashMap, HashSet};

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> u64 {
    let mut rules: HashMap::<u64,HashSet<u64>> = HashMap::new();
    let mut updates: Vec<Vec<u64>> = vec![];
    for line in input.split("\n").filter(|s| !s.is_empty()) {
        if line.contains("|") {
            let rule: Vec<u64> = line.split("|")
                .map(|n| n.parse::<u64>().unwrap())
                .collect();
            rules.entry(rule[1])
                .and_modify(|r| { r.insert(rule[0]); })
                .or_insert(HashSet::from([rule[0]]));
        }
        else if line.contains(",") {
            let pages: Vec<u64> = line.split(",")
                .map(|n| n.parse::<u64>().unwrap())
                .collect();
            updates.push(pages);
        }
    }

    let mut sum = 0;
    for update in updates {
        if !correct(&update, &rules) {
            let proper = reorder(&update, &rules);
            sum += proper[proper.len()/2];
        }
    }

    sum
}

fn correct(update: &[u64], rules: &HashMap<u64, HashSet<u64>>) -> bool {
    for (idx, page) in update.iter().enumerate() {
        if rules.contains_key(page) {
            let rule = rules.get(page).unwrap();
            if rule.intersection(&(update[idx+1..].iter().cloned().collect())).count() > 0 {
                return false;
            }
        }
    }

    true
}

fn reorder(update: &[u64], rules: &HashMap<u64, HashSet<u64>>) -> Vec<u64> {
    let mut new = update.to_vec();
    // if a page is blocked by rule, swap with the blocker
    for (idx, page) in update.iter().enumerate() {
        if rules.contains_key(page) {
            let rule = rules.get(page).unwrap();
            let tail = update[idx+1..].iter().cloned().collect::<HashSet<u64>>();
            let intersect: HashSet<&u64> = rule
                .intersection(&tail)
                .collect();
            if !intersect.is_empty() {
                let swap = intersect.iter().collect::<Vec<&&u64>>()[0];
                let swap_idx = new.iter().position(|&r| r == **swap).unwrap();
                new[idx] = **swap;
                new[swap_idx] = *page;
                break;
            }
        }
    }
    // if the swap produces a correct ordering, we are done
    // otherwise recurse to fix the next blocker
    if correct(&new, rules) {
        new
    }
    else {
        reorder(&new, rules)
    }
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 123);
}
