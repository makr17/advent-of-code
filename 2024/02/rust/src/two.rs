fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> usize {
    let mut count = 0;
    for line in input.split("\n").filter(|s| !s.is_empty()) {
        // parse input nums
        let nums = line.split(" ")
            .filter(|s| !s.is_empty())
            .map(|s| s.parse::<i64>().unwrap() )
            .collect::<Vec<i64>>();

        //println!("{:?}", nums);
        // try dropping each index in turn
        for i in 0..nums.len() {
            let mut filtered = nums.clone();
            filtered.remove(i);
            //println!("  {}: {:?}", i, filtered);

            // collect piece-wise difference between pairs of numbers
            let diffs = filtered.iter().enumerate()
                .skip(1)
                .map(|(i, n)| n - filtered[i-1])
                .collect::<Vec<i64>>();
            // found a safe line, count it and move on
            if safe(diffs) {
                //println!("    safe");
                count += 1;
                break
            }
            //println!("    unsafe");
        }
    }
    
    count
}

fn safe(diffs: Vec<i64>) -> bool {
    // can't be zero
    if diffs.contains(&0) {
        //println!("  contains 0");
        return false
    }

    // diffs must be either all positive or all negative
    let pos = diffs.iter()
        .filter(|n| n > &&0)
        .count();
    if pos != 0 && pos != diffs.len() {
        return false
    }

    // and at most a difference of 3
    let safe: bool = diffs.iter()
        .filter(|n| n != &&0 && n.abs() > 3)
        .count() == 0;

    safe
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 4);
}
