fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> usize {
    let mut count = 0;
    for line in input.split("\n").filter(|s| !s.is_empty()) {
        // parse input nums
        let nums = line.split(" ")
            .filter(|s| !s.is_empty())
            .map(|s| s.parse::<i64>().unwrap() )
            .collect::<Vec<i64>>();
        // collect piece-wise difference between pairs of numbers
        let diffs = nums.iter().enumerate()
            .skip(1)
            .map(|(i, n)| n - nums[i-1])
            .collect::<Vec<i64>>();

        // can't be zero
        if diffs.contains(&0) {
            //println!("  contains 0");
            continue
        }

        // diffs must be either all positive or all negative
        let pos = diffs.iter()
            .filter(|n| n > &&0)
            .count();
        if pos != 0 && pos != diffs.len() {
            continue
        }

        // and at most a difference of 3
        let safe: bool = diffs.iter()
            .filter(|n| n != &&0 && n.abs() > 3)
            .count() == 0;

        if safe {
            count += 1
        }
    }
    
    count
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 2);
}
