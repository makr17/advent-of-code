#![recursion_limit = "1024"]
use std::collections::{HashMap, HashSet};
use std::ops::Add;

use itertools::iproduct;
use lazy_static::lazy_static;
use pathfinding::prelude::astar;

// possible directions
#[allow(dead_code)]
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
enum Direction {
    N,
    S,
    E,
    W,
}

// points in the maze
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Point {
    fn neighbors(self, max: &Self) -> HashSet<Self> {
        MOVES.iter()
            .map(|(_d, p)| self + *p)
            .filter(|p| p.x >= 0 && p.x <= max.x && p.y >= 0 && p.y <= max.y)
            .collect()
    }

    fn manhattan_neighborhood(self, max: &Self, size: i64) -> HashSet<Self> {
        iproduct!(-size..=size, -size..=size)
            .map(|(x,y)| self + Self { x, y })
            // leave out our position
            .filter(|p| *p != self)
            // stay on the map
            .filter(|p| p.x >= 0 && p.y >= 0 && p.x <= max.x && p.y <= max.y)
            .filter(|p| self.distance(p) <= size as usize)
            .collect::<HashSet<Self>>()
    }

    // moves in all possible directions
    // but only if there is no wall blocking
    fn moves(self, walls: &HashSet<Self>, max: &Self) -> HashSet<Self> {
        self.neighbors(max).iter()
            .filter(|p| !walls.contains(p))
            .cloned()
            .collect()
    }

    // rough distance metric to guide A*
    fn distance(&self, other: &Point) -> usize {
        (self.x.abs_diff(other.x) + self.y.abs_diff(other.y)) as usize
    }

    // reachable points that are not walls (or corrupted bytes)
    fn successors(self, walls: &HashSet<Point>, max: &Self) -> Vec<(Self, usize)> {
        self.moves(walls, max).iter()
            .map(|p| (*p, 1))
            .collect()
    }
}
#[test]
fn test_manhattan_neighborhood() {
    let start = Point { x: 3, y: 3 };
    let max = Point { x: 7, y: 7 };
    assert_eq!(
        start.manhattan_neighborhood(&max, 1),
        start.moves(&HashSet::new(), &max)
    );
    let upper = Point { x: 0, y: 0 };
    assert_eq!(
        upper.manhattan_neighborhood(&max, 2),
        HashSet::from([
            Point { x: 1, y: 0 },
            Point { x: 2, y: 0 },
            Point { x: 0, y: 1 },
            Point { x: 1, y: 1 },
            Point { x: 0, y: 2 },
        ])
    );
    let lower = max;
    assert_eq!(
        lower.manhattan_neighborhood(&max, 2),
        HashSet::from([
            Point { x: 7, y: 5 },
            Point { x: 6, y: 6 },
            Point { x: 7, y: 6 },
            Point { x: 5, y: 7 },
            Point { x: 6, y: 7 },
        ])
    );
}

lazy_static! {
    // directions and associated point translations
    static ref MOVES: HashMap<Direction, Point> = HashMap::from([
        (Direction::N, Point { x: 0, y: -1 }),
        (Direction::E, Point { x: 1, y: 0 }),
        (Direction::S, Point { x: 0, y: 1 }),
        (Direction::W, Point { x: -1, y: 0 }),        
    ]);
}

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input, 100));
}

fn process(input: &str, limit: usize) -> usize {
    let mut walls: HashSet<Point> = HashSet::new();
    let mut start = Point { x: -1, y: -1 };
    let mut end = Point { x: -1, y: -1 };
    for (y, line) in input.split("\n").filter(|l| !l.is_empty()).enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                walls.insert(Point { x: x as i64, y: y as i64 });
            }
            else if c == 'S' {
                start = Point { x: x as i64, y: y as i64 };
            }
            else if c == 'E' {
                end = Point { x: x as i64, y: y as i64 };
            }
        }
    }
    // find max x,y value
    let mut xs = walls.iter().map(|p| p.x).collect::<Vec<i64>>();
    xs.sort_by(|a, b| b.cmp(a));
    let mut ys = walls.iter().map(|p| p.y).collect::<Vec<i64>>();
    ys.sort_by(|a, b| b.cmp(a));
    let max = Point { x: xs[0], y: ys[0] };    

    // find our baseline route score to beat
    let (basepathvec, _baseline) = astar(&start, |p| p.successors(&walls, &max), |p| p.distance(&end)/3, |p| *p == end).unwrap();

    // hash from point on the path to position on the path
    let basepath = basepathvec.iter().enumerate()
        .map(|(i,p)| (*p,i))
        .collect::<HashMap<Point, usize>>();
    let mut cheats = 0;
    // for each point along our optimal path
    for (cheat_start, start_pos) in basepath.iter() {
        // find all points in the manhattan neighborhood sized 20 around that point
        let endpoints = cheat_start.manhattan_neighborhood(&max, 20).iter()
            // that are also on our path
            .filter(|p| basepath.contains_key(p))
            // add in position on path
            .map(|p| (*p, *basepath.get(p).unwrap()))
            // no backtracking, that would defeat our objective
            .filter(|(_p,i)| i > start_pos)
            .collect::<Vec<(Point, usize)>>();
        // for each possible cheat endpoint
        for (cheat_end, end_pos) in endpoints.iter() {
            // find manhattan distance from our starting point
            let mdistance = cheat_start.distance(cheat_end);
            /*
            if mdistance > 20 {
                println!("{:?} -> {:?}: {} oops", cheat_start, cheat_end, mdistance);
            }
             */
            // savings is our original subpath length - manhattan distance
            let savings = *end_pos as i64 - *start_pos as i64 - mdistance as i64;
            // path distance - manhattan distance saves more than limit
            if savings >= limit as i64 {
                cheats += 1;
            }
        }
    }

    cheats
}

#[allow(dead_code)]
fn visualize(walls: &HashSet<Point>, max: &Point, start: &Point, end: &Point, cheat: &[Point]) {
    for y in 0..=max.x {
        for x in 0..=max.y {
            let p = Point { x, y };
            if p == *start {
                print!("S");
            }
            else if p == *end {
                print!("E");
            }
            else if cheat.contains(&p) {
                print!("C");
            }
            else if walls.contains(&p) {
                print!("#");
            }
            else {
                print!(".");
            }
        }
        println!();
    }
    println!();
}

#[test]
fn test() {
    assert_eq!(process(include_str!("../../test.txt"), 50), 285);
}
