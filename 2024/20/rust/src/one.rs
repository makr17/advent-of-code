#![recursion_limit = "1024"]
use std::collections::{HashMap, HashSet};
use std::ops::Add;

use lazy_static::lazy_static;
use pathfinding::prelude::astar;

// possible directions
#[allow(dead_code)]
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
enum Direction {
    N,
    S,
    E,
    W,
}

// points in the maze
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Point {
    fn neighbors(self, max: &Self) -> HashSet<Self> {
        MOVES.iter()
            .map(|(_d, p)| self + *p)
            .filter(|p| p.x >= 0 && p.x <= max.x && p.y >= 0 && p.y <= max.y)
            .collect()
    }

    // moves in all possible directions
    // but only if there is no wall blocking
    fn moves(self, walls: &HashSet<Self>, max: &Self) -> HashSet<Self> {
        self.neighbors(max).iter()
            .filter(|p| !walls.contains(p))
            .cloned()
            .collect()
    }

    // rough distance metric to guide A*
    fn distance(&self, other: &Point) -> usize {
        (self.x.abs_diff(other.x) + self.y.abs_diff(other.y)) as usize
    }

    // reachable points that are not walls (or corrupted bytes)
    fn successors(self, walls: &HashSet<Point>, max: &Self) -> Vec<(Self, usize)> {
        self.moves(walls, max).iter()
            .map(|p| (*p, 1))
            .collect()
    }
}

lazy_static! {
    // directions and associated point translations
    static ref MOVES: HashMap<Direction, Point> = HashMap::from([
        (Direction::N, Point { x: 0, y: -1 }),
        (Direction::E, Point { x: 1, y: 0 }),
        (Direction::S, Point { x: 0, y: 1 }),
        (Direction::W, Point { x: -1, y: 0 }),        
    ]);
}

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input, 100));
}

fn process(input: &str, limit: usize) -> usize {
    let mut walls: HashSet<Point> = HashSet::new();
    let mut start = Point { x: -1, y: -1 };
    let mut end = Point { x: -1, y: -1 };
    for (y, line) in input.split("\n").filter(|l| !l.is_empty()).enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                walls.insert(Point { x: x as i64, y: y as i64 });
            }
            else if c == 'S' {
                start = Point { x: x as i64, y: y as i64 };
            }
            else if c == 'E' {
                end = Point { x: x as i64, y: y as i64 };
            }
        }
    }
    // find max x,y value
    let mut xs = walls.iter().map(|p| p.x).collect::<Vec<i64>>();
    xs.sort_by(|a, b| b.cmp(a));
    let mut ys = walls.iter().map(|p| p.y).collect::<Vec<i64>>();
    ys.sort_by(|a, b| b.cmp(a));
    let max = Point { x: xs[0], y: ys[0] };    

    // find our baseline route score to beat
    let (_, baseline) = astar(&start, |p| p.successors(&walls, &max), |p| p.distance(&end)/3, |p| *p == end).unwrap();
    //println!("{baseline}");

    // hash from wall to remove to resulting score
    let mut cheats: HashMap<Point, usize> = HashMap::new();
    // ignore the bounding walls
    for cheat in walls.iter()
        // ignore the bounding walls
        .filter(|w| w.x > 0 && w.y > 0 && w.x < max.x && w.y < max.y ) {
            if !cheats.contains_key(cheat) {
                // copy of the walls, but remove our candidate
                let test = walls.iter()
                    .filter(|w| *w != cheat)
                    .cloned()
                    .collect::<HashSet<Point>>();
                let(_, score) = astar(&start, |p| p.successors(&test, &max), |p| p.distance(&end)/3, |p| *p == end).unwrap();
                //println!("{:?}: {}", cheat, score);
                cheats.insert(*cheat, score);
            }
    }
    let mut bucketed: HashMap<usize, Vec<Point>> = HashMap::new();
    for (cheat, score) in cheats.iter() {
        bucketed.entry(*score)
            .and_modify(|v| v.push(*cheat))
            .or_insert(vec![*cheat]);
    }
        
    bucketed.iter()
        .filter(|(score, _cheats)| baseline - **score >= limit)
        .map(|(_score, cheats)| cheats.len())
        .sum()
}

#[allow(dead_code)]
fn visualize(walls: &HashSet<Point>, max: &Point, start: &Point, end: &Point, cheat: &Point) {
    for y in 0..=max.x {
        for x in 0..=max.y {
            let p = Point { x, y };
            if p == *start {
                print!("S");
            }
            else if p == *end {
                print!("E");
            }
            else if p == *cheat {
                print!("C");
            }
            else if walls.contains(&p) {
                print!("#");
            }
            else {
                print!(".");
            }
        }
        println!();
    }
    println!();
}

#[test]
fn test1() {
    assert_eq!(process(include_str!("../../test.txt"), 25), 4);
}
