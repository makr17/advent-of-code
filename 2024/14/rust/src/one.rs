use std::collections::HashSet;
use std::ops::Add;

use regex::Regex;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input, Point { x: 101, y: 103 }));
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Robot {
    pos: Point,
    v: Point,
}

fn process(input: &str, max: Point) -> usize {
    let bot_re = Regex::new(r"p=(\d+),(\d+)\s+v=([-\d]+),([-\d]+)").unwrap();
    let mut bots = bot_re.captures_iter(input)
        .map(|c| c.extract())
        .map(|(_, [bx, by, vx, vy])|
             Robot {
                 pos: Point { x: bx.parse::<i64>().unwrap(), y: by.parse::<i64>().unwrap() },
                 v: Point { x: vx.parse::<i64>().unwrap(), y: vy.parse::<i64>().unwrap() },
             }
        )
        .collect::<HashSet<Robot>>();

    let min = Point { x: 0, y: 0 };
    //for _i in 0..100 {
    for _i in 0..100 {
        //visualize(&bots, &max);
        bots = bots.into_iter()
            .map(|mut b| {
                let mut new = b.pos + b.v;
                if new.x < min.x {
                    new.x = max.x + new.x;
                }
                if new.y < min.y {
                    new.y = max.y + new.y;
                }
                if new.x >= max.x {
                    new.x = new.x % max.x;
                }
                if new.y >= max.y {
                    new.y = new.y % max.y;
                }
                b.pos = new;
                b
            })
            .collect::<HashSet<Robot>>();
    }
    //visualize(&bots, &max);

    let q1 = bots.iter()
        .filter(|b| b.pos.x < max.x/2 && b.pos.y < max.y/2)
        .count();
    let q2 = bots.iter()
        .filter(|b| b.pos.x > max.x/2 && b.pos.y < max.y/2)
        .count();
    let q3 = bots.iter()
        .filter(|b| b.pos.x < max.x/2 && b.pos.y > max.y/2)
        .count();
    let q4 = bots.iter()
        .filter(|b| b.pos.x > max.x/2 && b.pos.y > max.y/2)
        .count();
    q1 * q2 * q3 * q4
}

#[allow(dead_code)]
fn visualize(bots: &HashSet<Robot>, max: &Point) {
    for y in 0..max.y {
        for x in 0..max.x {
            let count = bots.iter()
                .filter(|b| b.pos == Point { x, y })
                .count();
            print!(
                "{}",
                if count > 0 {
                    format!("{}", count)
                }
                else {
                    ".".to_string()
                }
            );
        }
        println!();
    }
    println!();
}

/*
#[test]
fn test0() {
    let test = "p=2,4 v=2,-3";
    assert_eq!(process(test, Point { x: 11, y: 7 }), 12);
}
 */

#[test]
fn test() {
    assert_eq!(process(include_str!("test.txt"), Point { x: 11, y: 7 }), 12);
}

