fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> usize {
    let definition = input.chars()
        .filter(|c| c.is_ascii_digit())
        .map(|c| c.to_digit(10).unwrap())
        .enumerate()
        .collect::<Vec<(usize, u32)>>();

    let files = definition.iter()
        .filter(|(idx, _size)| idx % 2 == 0)
        .map(|(idx, size)| (idx/2, *size))
        .collect::<Vec<(usize, u32)>>();
    //println!("{:?}", files);

    let mut disk = definition.iter()
        .flat_map(|(i, count)| {
            if i % 2 == 0 {
                let id = i/2;
                vec![Some(id); *count as usize]
            }
            else {
                vec![None; *count as usize]
            }
        })
        .collect::<Vec<Option<usize>>>();

    //print!("    ");
    //visualize(&disk);
    // files by id in descending order
    for (id, size) in files.iter().rev() {
        // map freespace
        let free = freespace(&disk);
        // find free spots that the file will fit
        let fits = free.iter()
            .filter(|(_i, s)| s >= size)
            .cloned()
            .collect::<Vec<(usize, u32)>>();
        // oops, no candidate spots
        if fits.is_empty() {
            continue;
        }
        // find where we are on disk now
        let current = disk.iter()
            .enumerate()
            .filter(|(_i, b)| **b == Some(*id))
            .map(|(i, _b)| i)
            .collect::<Vec<usize>>();
        // can only move left
        if fits[0].0 < current[0] {
            // write into new location
            for spot in disk.iter_mut()
                .skip(fits[0].0)
                .take(*size as usize) {
                    *spot = Some(*id);
                }
            // clear out old blocks
            for i in current.iter() {
                disk[*i] = None;
            }
        }
        //print!("{id}:  ");
        //visualize(&disk);
    }

    //visualize(&disk);

    let checksum = disk.iter()
        .enumerate()
        .filter(|(_i, b)| b.is_some())
        .map(|(i,b)| i * b.unwrap())
        .sum();

    checksum
}

fn freespace(disk: &[Option<usize>]) -> Vec<(usize, u32)> {
    let mut free = vec![];
    let mut start_idx = 0;
    let mut interior = false;
    for (idx, block) in disk.iter().enumerate() {
        if block.is_none() {
            if !interior {
                start_idx = idx;
                interior = true;
            }
        }
        else if interior {
            free.push((start_idx, (idx - start_idx) as u32));
            interior = false;
        }
    }
    free
}

#[allow(dead_code)]
fn visualize(disk: &[Option<usize>]) {
    for block in disk {
        if block.is_some() {
            print!("{}", block.unwrap());
        }
        else {
            print!(".");
        }
    }
    println!();
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 2858);
}
