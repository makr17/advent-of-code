fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> usize {
    let mut disk = input.chars()
        .filter(|c| c.is_ascii_digit())
        .enumerate()
        .flat_map(|(i,c)| {
            let count = c.to_digit(10).unwrap();
            if i % 2 == 0 {
                let id = i/2;
                vec![Some(id); count as usize]
            }
            else {
                vec![None; count as usize]
            }
        })
        .collect::<Vec<Option<usize>>>();

    loop {
        let free = disk.iter()
            .enumerate()
            .filter(|(_i, b)| b.is_none())
            .map(|(i, _b)| i)
            .take(1)
            .collect::<Vec<usize>>();
        let idx = free[0];
        let block = disk.iter()
            .enumerate()
            .rev()
            .filter(|(_i, b)| b.is_some())
            .take(1)
            .collect::<Vec<(usize,&Option<usize>)>>();
        let (pos, id) = block[0];
        if pos > idx {
            disk[idx] = *id;
            disk[pos] = None;
        }
        else {
            break;
        }
        //visualize(&disk);
    }

    let checksum = disk.iter()
        .enumerate()
        .filter(|(_i, b)| b.is_some())
        .map(|(i,b)| i * b.unwrap())
        .sum();

    checksum
}

#[allow(dead_code)]
fn visualize(disk: &[Option<usize>]) {
    for block in disk {
        if block.is_some() {
            print!("{}", block.unwrap());
        }
        else {
            print!(".");
        }
    }
    println!();
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 1928);
}
