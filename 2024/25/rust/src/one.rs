use std::collections::HashSet;
use std::iter::zip;

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> usize {
    let mut locks: Vec<Vec<usize>> = vec![];
    let mut keys: Vec<Vec<usize>> = vec![];

    for schem in input.split("\n\n") {
        if schem.split("\n").next().unwrap() == "#####" {
            // lock
            locks.push(parse_heights(schem));
        }
        else {
            // key
            keys.push(parse_heights(schem));
        }
    }
    let mut fits = 0;
    for lock in locks.iter() {
        for key in keys.iter() {
            let good = zip(key, lock)
                .filter(|(k,l)| **k + **l <= 5)
                .count();
            //println!("{:?} + {:?} => {}", lock, key, good == 5);
            if good == 5 {
                fits += 1;
            }
        }
    }

    fits
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: usize,
    y: usize,
}

fn parse_heights(input: &str) -> Vec<usize> {
    let schem = input.split("\n").enumerate()
        .flat_map(
            |(y, line)|
            line.chars().enumerate()
                .filter(|(_x, c)| *c == '#')
                .map(|(x, _c)| Point { x, y })
                .collect::<Vec<Point>>()
        )
        .collect::<HashSet<Point>>();
    (0..5)
        .map(|x| schem.iter().filter(|p| p.x == x).count() - 1)
        .collect()
}

#[test]
fn test() {
    assert_eq!(process(include_str!("../../test.txt")), 3);
}
