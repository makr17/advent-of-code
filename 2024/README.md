# 2024 AoC

First attempt this year will be in rust.
I'll fall back to other tools as needed/expedient, but trying rust first.

Finally made it to 50/50 stars for the year.
The last holdout was Day 21, which took me about a week, off and on.
I got finally got Part 01 working the morning of the 28th.
Part 02 was a fairly easy abstraction from there.
