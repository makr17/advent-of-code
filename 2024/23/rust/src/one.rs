use std::collections::HashSet;

use itertools::iproduct;

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> usize {
    let mut edges = input.split("\n").filter(|l| !l.is_empty())
        .map(|l| {
            let ab = l.split("-").collect::<Vec<&str>>();
            (ab[0], ab[1])
        })
        .collect::<HashSet<(&str,&str)>>();
    // add returning edges, just in case
    let invert = edges.iter().map(|(a,b)| (*b,*a)).collect::<HashSet<(&str,&str)>>();
    edges.extend(&invert);

    let cliques = three_cliques(&edges);

    // count of cliques with a member that starts with 't'
    cliques.iter()
        // at least one element in the set starts with 't'
        .filter(|c| c.iter().filter(|n| n.starts_with('t')).count() > 0)
        .count()
}

// I _want_ a HashSet<HashSet<&str>>, but HashSet doesn't implement Hash
// so settle for sorted HashSet<Vec<&str>> where the Vec is sorted
fn three_cliques<'a>(graph: &'a HashSet<(&'a str, &'a str)>) -> HashSet<Vec<&'a str>> {
    // start with 2-cliques, that's simple edge enumeration
    let two_cliques = graph.iter()
        .map(|(a,b)| {
            let mut c = vec![*a, *b];
            c.sort();
            c
        })
        .collect::<HashSet<Vec<&str>>>();
    //println!("2-cliques: {:?}", two_cliques);

    let three_cliques = iproduct!(two_cliques.iter(), two_cliques.clone().iter())
        .filter_map(|(a,b)| {
            // turn both sides into hashsets
            let mut ah = a.iter().copied().collect::<HashSet<&str>>();
            let bh = b.iter().copied().collect::<HashSet<&str>>();
            if ah != bh {
                // and take the intersection
                let int = ah.intersection(&bh).copied().collect::<HashSet<&str>>();
                if !int.is_empty() {
                    //println!("ah: {:?}, bh: {:?}, int: {:?}", ah, bh, int);
                    let ad = ah.difference(&int).copied().collect::<Vec<&str>>();
                    let bd = bh.difference(&int).copied().collect::<Vec<&str>>();
                    //println!("{:?} :: {:?}", ad, bd);
                    if graph.contains(&(ad[0], bd[0])) {
                        ah.insert(bd[0]);
                        let mut v = ah.iter().copied().collect::<Vec<&str>>();
                        v.sort();
                        Some(v)
                    }
                    else {
                        None
                    }
                }
                else {
                    None
                }
            }
            else {
                // both sides are the same 2-clique
                None
            }
        })
        .collect::<HashSet<Vec<&str>>>();
    //println!("3-cliques: {:?}, count={}", three_cliques, three_cliques.len());

    three_cliques
}

#[test]
fn test() {
    assert_eq!(process(include_str!("../../test.txt")), 7);
}
