use std::collections::{HashMap, HashSet};

use itertools::Itertools;

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> String {
    let mut graph: HashMap<&str, Vec<&str>> = HashMap::new();
    for l in input.split("\n").filter(|l| !l.is_empty()) {
        let ab = l.split("-").collect::<Vec<&str>>();
        graph.entry(ab[0])
            .and_modify(|v| v.push(ab[1]))
            .or_insert(vec![ab[1]]);
        graph.entry(ab[1])
            .and_modify(|v| v.push(ab[0]))
            .or_insert(vec![ab[0]]);
    }

    let clique = k_cliques(&graph);

    clique.join(",")
}


fn k_cliques<'a>(graph: &'a HashMap<&'a str, Vec<&'a str>>) -> Vec<&'a str> {
    // 2-cliques
    let mut cliques = graph.iter()
        .flat_map(|(a,v)| {
            v.iter()
                .map(|b| {
                    let mut c = vec![*a,*b];
                    c.sort();
                    c
                })
                .collect::<Vec<Vec<&str>>>()
        })
        .collect::<HashSet<Vec<&str>>>();
    let mut k = 3;
    while cliques.len() > 1 {
        // hashset to handle deduping
        let mut cliques_1: HashSet<Vec<&str>> = HashSet::new();
        // compare each clique to all cliques to the right
        // list so we can index into it to only look to the right
        // and HashSet as the element so we only convert once per pass
        let list = cliques.into_iter()
            .map(|h| h.into_iter().collect::<HashSet<&str>>())
            .collect::<Vec<HashSet<&str>>>();
        for (idx,uh) in list.iter().enumerate() {
            for vh in list[idx+1..].iter() {
                let diff  = uh.symmetric_difference(vh)
                    .copied()
                    .collect::<Vec<&str>>();
                // diff is minimal, and edge exists
                if diff.len() == 2 && graph.get(&diff[0]).unwrap().contains(&diff[1]) {
                    //println!("{:?} : {:?} => {:?}", u, v, diff);
                    let mut new = uh.clone();
                    // one of these will already exist
                    new.insert(diff[0]);
                    // but the other extends our clique by one element
                    new.insert(diff[1]);
                    // into a sorted vec for storage
                    let newvec = new.into_iter().sorted().collect::<Vec<&str>>();
                    //println!("  {:?}", newvec);
                    cliques_1.insert(newvec);
                }
            }
        }

        cliques = cliques_1;
        println!("k={}, count={}", k, cliques.len());
        // rinse/repeat
        k += 1;            
    }

    let v = cliques.into_iter().collect::<Vec<Vec<&str>>>();
    // already sorted by the clique search
    v[0].to_vec()
}

#[test]
fn test() {
    assert_eq!(process(include_str!("../../test.txt")), "co,de,ka,ta");
}
