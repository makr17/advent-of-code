use std::collections::{HashMap, HashSet};

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Point {
    fn reachable(self, map: &HashMap<Point, u32>) -> Vec<Self> {
        let height = map.get(&self).unwrap();
        self.neighbors().iter()
            .filter(|p| map.contains_key(p) && *map.get(p).unwrap() == height + 1)
            .cloned()
            .collect::<Vec<Point>>()
    }

    fn neighbors(self) -> Vec<Self> {
        vec![
            Point { x: self.x, y: self.y - 1 }, // N
            Point { x: self.x, y: self.y + 1 }, // S
            Point { x: self.x - 1, y: self.y }, // W
            Point { x: self.x + 1, y: self.y }, // E
        ]
    }
}

fn process(input: &str) -> usize {
    let mut map: HashMap<Point, u32> = HashMap::new();
    let mut trailheads: Vec<Point> = vec![];
    for (y, line) in input.split("\n").filter(|s| !s.is_empty()).enumerate() {
        for (x, c) in line.chars().enumerate() {
            if !c.is_ascii_digit() {
                continue;
            }
            let p = Point { x: x as i64, y: y as i64 };
            let digit = c.to_digit(10).unwrap();
            map.insert(p, digit);
            if digit == 0 {
                trailheads.push(p);
            }
        }
    }

    trailheads.iter()
        .map(|p| traverse(p, &map))
        .map(|v| {
            let unique = v.iter().collect::<HashSet<&Point>>();
            unique.len()
        })
        .sum()
}

// return vec of reachable height==9 points
// we'll unique later
// TODO: use HashSet as return type here?
fn traverse (p: &Point, map: &HashMap<Point, u32>) -> Vec<Point> {
    let reach = p.reachable(map);
    //println!("{:?}: {:?}", p, reach);

    if reach.is_empty() {
        if map.get(p).unwrap() == &9 {
            //println!("  {:?}, h = 9", p);
            // we're at the top, count this path
            vec![*p]
        }
        else {
            //println!("  {:?}, h != 9", p);
            // dead-end, not at height==9, don't count this path
            vec![]
        }
    }
    else {
        // sum values for reachable non-terminating paths
        reach.iter()
            .flat_map(|p| traverse(p, map))
            .collect::<Vec<Point>>()
    }
}

#[test]
fn test0() {
    let input = include_str!("test0.txt");
    assert_eq!(process(input), 1);
}

#[test]
fn test1() {
    let input = include_str!("test1.txt");
    assert_eq!(process(input), 36);
}
