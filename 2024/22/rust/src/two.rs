use std::collections::{HashMap, HashSet};

use itertools::Itertools;
use rayon::prelude::*;

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input, 2000));
}

fn process(input: &str, rounds: usize) -> i64 {
    let secrets = input.split("\n").filter(|l| !l.is_empty())
        .map(|l| l.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();
    // find all prices
    let allprices: Vec<Vec<i64>> = secrets.par_iter()
        .map(|s| {
            let mut prices: Vec<i64> = vec![*s%10];
            let mut val = *s;
            for _ in 0..rounds {
                val = evolve(val);
                prices.push(val%10);
            }
            prices
        })
        .collect::<Vec<Vec<i64>>>();
    // find interstitial price changes
    let changes = allprices.par_iter()
        .map(|v|
             v.iter().tuple_windows()
             .map(|(a,b)| b - a)
             .collect::<Vec<i64>>()
        )
        .collect::<Vec<Vec<i64>>>();
    // find unique set of all 4-element sequential price changes
    let sequences = changes.par_iter()
        .flat_map(|v|
                  v.iter().tuple_windows()
                  .collect::<Vec<(&i64, &i64, &i64, &i64)>>()
        )
        .collect::<HashSet<(&i64, &i64, &i64, &i64)>>();
    // for each vec of price changes
    // find the first occurrence of the sequence in that vec
    let sequence_offsets = changes.par_iter()
        .map(|v| {
            let seqs = v.iter().tuple_windows()
                .collect::<Vec<(&i64, &i64, &i64, &i64)>>();
            sequences.iter()
                .map(|seq| (*seq, seqs.iter().position(|s| s == seq)))
                .collect::<HashMap<(&i64, &i64, &i64, &i64), Option<usize>>>()
        })
        .collect::<Vec<HashMap<(&i64, &i64, &i64, &i64), Option<usize>>>>();
    // now find out the price at those offsets for each sequence and vec to get gains
    let gains = sequences.par_iter()
        .map(|seq| {
            let gain = sequence_offsets.iter().enumerate()
                .map(|(idx, m)| {
                    // get offset in the idx-th change vector
                    match m.get(seq) {
                        // and then get the price in corresponding price vector
                        // +4 to get the price _after_ the sequence of changes
                        // that's the gain for the single seller
                        Some(Some(i)) => allprices[idx][*i+4],
                        // sequence doesn't exist for this vec
                        Some(None) => 0,
                        // shouldn't happen, but...
                        None => 0,
                    }
                })
                // sum gains across all sellers for the gain at _this_ sequence
                .sum();
            (*seq, gain)
        })
        .collect::<HashMap<(&i64, &i64, &i64, &i64), i64>>();
    // sort descending to find max
    let maximize = gains.values()
        .sorted_by(|a,b| b.cmp(a))
        .copied()
        .collect::<Vec<i64>>();
    let matching = gains.iter()
        .filter(|(_s, n)| **n == maximize[0])
        .map(|(s,_)| *s)
        .collect::<Vec<(&i64, &i64, &i64, &i64)>>();
    println!("{:?}", matching);
    maximize[0]
}

fn evolve(input: i64) -> i64 {
    let mut secret = input;
    // multiply 64
    let mut new = secret * 64;
    // mix
    new ^= secret;
    // prune
    new %= 16777216;
    // divide 32, round down (truncate, should be integer default)
    secret = new;
    new /= 32;
    // mix
    new ^= secret;
    // prune
    new %= 16777216;
    // multiply 2048
    secret = new;
    new *= 2048;
    // mix
    new ^= secret;
    // prune
    new %= 16777216;

    new
}

#[test]
fn test_evolve() {
    assert_eq!(evolve(123), 15887950);
    assert_eq!(evolve(15887950), 16495136);
}

#[test]
fn test() {
    assert_eq!(process(include_str!("../../test2.txt"), 2000), 23);
}
