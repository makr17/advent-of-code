fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input, 2000));
}

fn process(input: &str, rounds: usize) -> u64 {
    let secrets = input.split("\n").filter(|l| !l.is_empty())
        .map(|l| l.parse::<u64>().unwrap())
        .collect::<Vec<u64>>();
    let mut total = 0;
    for secret in secrets.iter() {
        let mut val = *secret;
        for _ in 0..rounds {
            val = evolve(val);
        }
        total += val;
    }

    total
}

fn evolve(input: u64) -> u64 {
    let mut secret = input;
    // multiply 64
    let mut new = secret * 64;
    // mix
    new ^= secret;
    // prune
    new %= 16777216;
    // divide 32, round down (truncate, should be integer default)
    secret = new;
    new /= 32;
    // mix
    new ^= secret;
    // prune
    new %= 16777216;
    // multiply 2048
    secret = new;
    new *= 2048;
    // mix
    new ^= secret;
    // prune
    new %= 16777216;

    new
}

#[test]
fn test_evolve() {
    assert_eq!(evolve(123), 15887950);
    assert_eq!(evolve(15887950), 16495136);
}

#[test]
fn test() {
    assert_eq!(process(include_str!("../../test.txt"), 2000), 37327623);
}
