use std::collections::{HashMap, HashSet};
use std::ops::Add;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq)]
enum Directions {
    N,
    S,
    E,
    W,
}

#[derive(Clone, Copy, Debug)]
struct Guard {
    direction: Directions,
    position: Point,
}
impl Guard {
    fn turn(mut self) -> Self {
        if self.direction == Directions::N {
            self.direction = Directions::E;
        }
        else if self.direction == Directions::E {
            self.direction = Directions::S;
        }
        else if self.direction == Directions::S {
            self.direction = Directions::W;
        }
        else if self.direction == Directions::W {
            self.direction = Directions::N;
        }

        self
    }
}

fn process(input: &str) -> usize {
    let moves = HashMap::from([
        (Directions::N, Point { x: 0, y: -1 }),
        (Directions::S, Point { x: 0, y: 1 }),
        (Directions::E, Point { x: 1, y: 0 }),
        (Directions::W, Point { x: -1, y: 0 }),
    ]);
    let min = Point { x: 0, y: 0 };
    let mut max = min;
    let mut guard = Guard { direction: Directions::N, position: Point { x: -1, y: -1 }};
    let mut obstacles: HashSet<Point> = HashSet::new();
    for (y, line) in input.split("\n").filter(|s| !s.is_empty()).enumerate() {
        if y as i64 > max.y {
            max.y = y as i64
        }
        for (x, c) in line.chars().enumerate() {
            if x as i64 > max.x {
                max.x = x as i64
            }
            if c == '^' {
                guard.position = Point { x: x as i64, y: y as i64 };
            }
            if c == '#' {
                obstacles.insert(Point { x: x as i64, y: y as i64 });
            }
        }
    }

    let mut positions: HashSet<Point> = HashSet::new();
    loop {
        positions.insert(guard.position);
        let new_pos = guard.position + *(moves.get(&guard.direction).unwrap());
        if obstacles.contains(&new_pos) {
            // blocked, so turn
            guard = guard.turn();
        }
        else {
            //println!("    {:?} guard moves to {:?}", guard.position, new_pos);
            // guard moves
            guard.position = new_pos;
            if guard.position.x < min.x || guard.position.y < min.y ||
                guard.position.x > max.x || guard.position.y > max.y {
                    // off the map, we're done
                    break;
                }
        }
        
    }
    
    positions.len()
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 41);
}
