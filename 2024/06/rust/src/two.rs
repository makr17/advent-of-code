use std::collections::{HashMap, HashSet};
use std::ops::Add;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq)]
enum Direction {
    N,
    S,
    E,
    W,
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq)]
struct Guard {
    direction: Direction,
    position: Point,
}
impl Guard {
    fn turn(mut self) -> Self {
        if self.direction == Direction::N {
            self.direction = Direction::E;
        }
        else if self.direction == Direction::E {
            self.direction = Direction::S;
        }
        else if self.direction == Direction::S {
            self.direction = Direction::W;
        }
        else if self.direction == Direction::W {
            self.direction = Direction::N;
        }

        self
    }
}

fn process(input: &str) -> usize {
    let moves = HashMap::from([
        (Direction::N, Point { x: 0, y: -1 }),
        (Direction::S, Point { x: 0, y: 1 }),
        (Direction::E, Point { x: 1, y: 0 }),
        (Direction::W, Point { x: -1, y: 0 }),
    ]);
    let min = Point { x: 0, y: 0 };
    let mut max = min;
    let mut init_guard = Guard { direction: Direction::N, position: Point { x: -1, y: -1 }};
    let mut init_obstacles: HashSet<Point> = HashSet::new();
    for (y, line) in input.split("\n").filter(|s| !s.is_empty()).enumerate() {
        if y as i64 > max.y {
            max.y = y as i64
        }
        for (x, c) in line.chars().enumerate() {
            if x as i64 > max.x {
                max.x = x as i64
            }
            if c == '^' {
                init_guard.position = Point { x: x as i64, y: y as i64 };
            }
            if c == '#' {
                init_obstacles.insert(Point { x: x as i64, y: y as i64 });
            }
        }
    }

    // test all possible positions for a new obstacle
    // if we end up in a loop, count that
    // if we exit the map, move onto the next possible
    let mut loop_count = 0;
    for x in 0..=max.x {
        for y in 0..=max.y {
            let pos = Point { x, y };
            // can't place in an initially-occupied position
            if init_obstacles.contains(&pos) || pos == init_guard.position {
                continue
            }
            let mut positions: HashSet<Guard> = HashSet::new();
            let mut guard = init_guard;
            let mut obstacles = init_obstacles.clone();
            obstacles.insert(pos);
            loop {
                if positions.contains(&guard) {
                    loop_count += 1;
                    break;
                }
                positions.insert(guard);
                let new_pos = guard.position + *(moves.get(&guard.direction).unwrap());
                if obstacles.contains(&new_pos) {
                    // blocked, so turn
                    guard = guard.turn();
                }
                else {
                    guard.position = new_pos;
                    if guard.position.x < min.x || guard.position.y < min.y ||
                        guard.position.x > max.x || guard.position.y > max.y {
                            // off the map, we're done
                            // and it wasn't a loop
                            break;
                        }
                }
            }
        }
    }
    
    loop_count
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 6);
}
