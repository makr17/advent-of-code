fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> usize {
    let lines = input.split("\n")
        .filter(|line| !line.is_empty())
        .collect::<Vec<&str>>();
    let towels = lines[0].split(", ")
        .map(|s| s.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();
    let patterns = lines.iter().skip(1)
        .map(|s| s.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();

    patterns.iter()
        .map(|p| build_pattern(p, &towels))
        .filter(|p| *p > 0)
        .count()
}

// recurse through branches of matching candidates for pattern
fn build_pattern(pattern: &[char], towels: &[Vec<char>]) -> usize {
    if pattern.is_empty() {
        return 1;
    }

    // check all towels that match a prefix of our remaining pattern
    for towel in towels.iter()
        .filter(|t| t.len() <= pattern.len() && **t == pattern[0..t.len()]) {
            if build_pattern(&pattern[towel.len()..], towels) > 0 {
                return 1;
            }
        }

    0
}

#[test]
fn test() {
    assert_eq!(process(include_str!("../../test.txt")), 6);
}
