use std::collections::HashMap;

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> usize {
    let lines = input.split("\n")
        .filter(|line| !line.is_empty())
        .collect::<Vec<&str>>();
    let towels = lines[0].split(", ")
        .map(|s| s.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();
    let patterns = lines.iter().skip(1)
        .map(|s| s.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();

    let mut cache: HashMap<Vec<char>, usize> = HashMap::new();
    patterns.iter()
        .map(|p| build_pattern(p, &towels, &mut cache))
        .sum()
}

// recurse through branches of matching candidates for pattern
fn build_pattern(pattern: &[char], towels: &[Vec<char>], cache: &mut HashMap<Vec<char>, usize>) -> usize {
    if pattern.is_empty() {
        return 1;
    }
    if cache.contains_key(pattern) {
        return *cache.get(pattern).unwrap();
    }
    // back to enumerating all possible combinations
    // let's try rayon and throwing some cpu/parallelization at it
    // check all towels that match a prefix of our remaining pattern
    let count = towels.iter()
        .filter(|t| t.len() <= pattern.len() && **t == pattern[0..t.len()])
        .map(|t| build_pattern(&pattern[t.len()..], towels, cache))
        .sum();
    cache.insert(pattern.to_vec(), count);
    count
}

#[test]
fn test() {
    assert_eq!(process(include_str!("../../test.txt")), 16);
}
