use std::collections::{HashMap, HashSet};

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

// i64 so we don't need to worry about boundaries
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Point {
    fn diags(self) -> Vec<(Self, Self)> {
        vec![
            (Point { x: self.x-1, y: self.y-1 }, Point { x: self.x+1, y: self.y+1 }),
            (Point { x: self.x-1, y: self.y+1 }, Point { x: self.x+1, y: self.y-1 }),
        ]
    }
}

fn process(input: &str) -> usize {
    let mut points = HashMap::<Point, char>::new();
    for (y, line) in input.split("\n").filter(|s| !s.is_empty()).enumerate() {
        for (x, c) in line.chars().enumerate() {
            let p = Point { x: x as i64, y: y as i64 };
            points.insert(p, c);
        }
    }

    let good = HashSet::from(['M', 'S']);
    let mut found: Vec<&Point> = vec![];
    for a in points.iter()
        .filter(|(_p, c)| **c == 'A')
        .map(|(p, _c)| p) {
            let diags = a.diags();
            let a_left = points.get(&diags[0].0).unwrap_or(&'.');
            let a_right = points.get(&diags[0].1).unwrap_or(&'.');
            let b_left = points.get(&diags[1].0).unwrap_or(&'.');
            let b_right = points.get(&diags[1].1).unwrap_or(&'.');
            if a_left != a_right && good.contains(a_left) && good.contains(a_right)
                && b_left != b_right && good.contains(b_left) && good.contains(b_right) {
                    found.push(a);
                }
        }

    // visualize for test
    /*
    let unique = found.iter()
        .collect::<HashSet<&&Point>>();
    visualize(&unique, &points);
     */

    found.len()
}

#[allow(dead_code)]
fn visualize(unique: &HashSet<&&Point>, points: &HashMap::<Point, char>) {
    let mut xs = unique.iter().map(|p| p.x).collect::<Vec<i64>>();
    xs.sort();
    let mut ys = unique.iter().map(|p| p.y).collect::<Vec<i64>>();
    ys.sort();
    for y in 0 ..= ys[ys.len()-1] {
        for x in 0 ..= xs[xs.len()-1] {
            let p = Point { x, y };
            if unique.contains(&&p) {
                print!("{}", points.get(&p).unwrap());
            }
            else {
                print!(".");
            }
        }
        println!();
    }
    println!();
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 9);
}
