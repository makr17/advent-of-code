use std::collections::{HashMap, HashSet};

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

// i64 so we don't need to worry about boundaries
#[derive(Clone, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Point {
    // enumerate all radials from given point
    // N, S, E, W, NE, SE, NW, SW
    fn radials(self) -> Vec<Vec<Self>> {
        let radials = vec![
            // W
            (self.x-3 ..= self.x)
                .rev()
                .map(|x| Point { x, y: self.y })
                .collect::<Vec<Self>>(),
            // E
            (self.x ..= self.x+3)
                .map(|x| Point { x, y: self.y })
                .collect::<Vec<Self>>(),
            // N
            (self.y-3 ..= self.y)
                .rev()
                .map(|y| Point { x: self.x, y })
                .collect::<Vec<Self>>(),
            // S
            (self.y ..= self.y+3)
                .map(|y| Point { x: self.x, y })
                .collect::<Vec<Self>>(),
            // NW
            (-3..=0)
                .rev()
                .map(|off| Point { x: self.x + off, y: self.y + off })
                .collect::<Vec<Self>>(),
            // SE
            (0..=3)
                .map(|off: i64| Point { x: self.x + off, y: self.y + off })
                .collect::<Vec<Self>>(),
            // NE
            (-3..=0)
                .rev()
                .map(|off: i64| Point { x: self.x + off.abs(), y: self.y + off })
                .collect::<Vec<Self>>(),
            // SW
            (-3..=0)
                .rev()
                .map(|off: i64| Point { x: self.x + off, y: self.y + off.abs() })
                .collect::<Vec<Self>>(),
        ];

        radials
    }
}

fn process(input: &str) -> usize {
    let target = vec!['X', 'M', 'A', 'S'];
    let mut points = HashMap::<Point, char>::new();
    for (y, line) in input.split("\n").filter(|s| !s.is_empty()).enumerate() {
        for (x, c) in line.chars().enumerate() {
            let p = Point { x: x as i64, y: y as i64 };
            points.insert(p, c);
        }
    }

    // testing that I didn't twist things parsing input
    /*
    let all = points.iter().map(|(p, _c)| p).to_owned().collect::<HashSet<&Point>>();
    visualize(&all, &points);
     */

    let mut found: Vec<Vec<Point>> = vec![];
    for x in points.iter()
        .filter(|(_p, c)| **c == 'X')
        .map(|(p, _c)| p) {
            //println!("x: {:?}", x);
            for path in x.clone().radials() {
                let word = path.iter()
                    .map(|p| match points.get(p) {
                        Some(c) => *c,
                        None => '.',
                    })
                    .collect::<Vec<char>>();
                //println!("{:?}: {:?}", word, path);
                if word == target {
                    found.push(path);
                }
            }
        }

    // visualize for test
    /*
    let unique = found.iter()
        .flat_map(|f| f)
        .to_owned()
        .collect::<HashSet<&Point>>();
    visualize(&unique, &points);
     */

    found.len()
}

#[allow(dead_code)]
fn visualize(unique: &HashSet<&Point>, points: &HashMap::<Point, char>) {
    let mut xs = unique.iter().map(|p| p.x).collect::<Vec<i64>>();
    xs.sort();
    let mut ys = unique.iter().map(|p| p.y).collect::<Vec<i64>>();
    ys.sort();
    for y in ys[0] ..= ys[ys.len()-1] {
        for x in xs[0] ..= xs[xs.len()-1] {
            let p = Point { x, y };
            if unique.contains(&p) {
                print!("{}", points.get(&p).unwrap());
            }
            else {
                print!(".");
            }
        }
        println!();
    }
    println!();
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 18);
}
