use itertools::iproduct;
use regex::Regex;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

#[derive(Debug)]
struct Point {
    x: u64,
    y: u64,
}

#[derive(Debug)]
struct Claw {
    a: Point,
    b: Point,
    prize: Point,
}

fn process(input: &str) -> u64 {
    let claw_re = Regex::new(r"(?ms)(Button A: X\+(?<button_a_x>\d+), Y\+(?<button_a_y>\d+)\nButton B: X\+(?<button_b_x>\d+), Y\+(?<button_b_y>\d+)\nPrize: X=(?<prize_x>\d+), Y=(?<prize_y>\d+))+").unwrap();

    let claws = claw_re.captures_iter(input)
        .map(|c| c.extract())
        .map(|(_, [_, button_a_x, button_a_y, button_b_x, button_b_y, prize_x, prize_y])|
             Claw {
                 a: Point {
                     x: button_a_x.parse::<u64>().unwrap(),
                     y: button_a_y.parse::<u64>().unwrap()
                 },
                 b: Point {
                     x: button_b_x.parse::<u64>().unwrap(),
                     y: button_b_y.parse::<u64>().unwrap()
                 },
                 prize: Point {
                     x: prize_x.parse::<u64>().unwrap(),
                     y: prize_y.parse::<u64>().unwrap()
                 },
             }
        )
        .collect::<Vec<Claw>>();
    claws.iter().map(min_cost).sum()
}

/*
  prize.x = A * a.x + B * b.x
  prize.y = A * a.y + B * b.y
  A <= 100
  B <= 100
  minimize: 3A + 1B
*/
fn min_cost (claw: &Claw) -> u64 {
    let r = 0..100;
    let mut costs = iproduct!(r.clone(), r)
        // make sure we can actually get there
        .filter(
            |(a,b)|
            a * claw.a.x + b * claw.b.x == claw.prize.x
                && a * claw.a.y + b * claw.b.y == claw.prize.y
        )
        // then calculate the cost
        .map(|(a,b)| 3*a + b)
        .collect::<Vec<u64>>();
    if !costs.is_empty() {
        // take least cost
        costs.sort();
        costs[0]
    }
    else {
        0
    }
}

#[test]
fn test() {
    assert_eq!(process(include_str!("test.txt")), 480);
}
