use regex::Regex;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

#[derive(Debug)]
struct Point {
    x: i64,
    y: i64,
}

#[derive(Debug)]
struct Claw {
    a: Point,
    b: Point,
    prize: Point,
}

const PRIZE_SCALE: i64 = 10000000000000;

fn process(input: &str) -> i64 {
    let claw_re = Regex::new(r"(?ms)(Button A: X\+(?<button_a_x>\d+), Y\+(?<button_a_y>\d+)\nButton B: X\+(?<button_b_x>\d+), Y\+(?<button_b_y>\d+)\nPrize: X=(?<prize_x>\d+), Y=(?<prize_y>\d+))+").unwrap();

    let claws = claw_re.captures_iter(input)
        .map(|c| c.extract())
        .map(|(_, [_, button_a_x, button_a_y, button_b_x, button_b_y, prize_x, prize_y])|
             Claw {
                 a: Point {
                     x: button_a_x.parse::<i64>().unwrap(),
                     y: button_a_y.parse::<i64>().unwrap()
                 },
                 b: Point {
                     x: button_b_x.parse::<i64>().unwrap(),
                     y: button_b_y.parse::<i64>().unwrap()
                 },
                 prize: Point {
                     x: prize_x.parse::<i64>().unwrap() + PRIZE_SCALE,
                     y: prize_y.parse::<i64>().unwrap() + PRIZE_SCALE,
                 },
             }
        )
        .collect::<Vec<Claw>>();
    claws.iter().map(cost).sum()
}

/*
  prize.x = A * a.x + B * b.x
  prize.y = A * a.y + B * b.y

  Cramer's Rule:
  A = (prize.x * b.y - prize.y * b.x) / (a.x * b.y - a.y * b.x)
  B = (a.x * prize.y - a.y * prize.x) / (a.x * b.y - a.y * b.x)
*/
fn cost (claw: &Claw) -> i64 {
    let a = (claw.prize.x * claw.b.y - claw.prize.y * claw.b.x) / (claw.a.x * claw.b.y - claw.a.y * claw.b.x);
    let b = (claw.a.x * claw.prize.y - claw.a.y * claw.prize.x) / (claw.a.x * claw.b.y - claw.a.y * claw.b.x);

    // check that the integer solution works
    if claw.prize.x == a * claw.a.x + b * claw.b.x
        && claw.prize.y == a * claw.a.y + b * claw.b.y {
            3 * a + b
        }
    else {
        0
    }
}

#[test]
fn test() {
    assert_eq!(process(include_str!("test.txt")), 875318608908);
}
