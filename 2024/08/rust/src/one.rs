use std::collections::{HashMap, HashSet};
use std::ops::{Add, Sub};

use itertools::Itertools;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

fn process(input: &str) -> usize {
    let mut antennas: HashMap<Point, char> = HashMap::new();
    let mut chars: HashSet<char> = HashSet::new();
    let mut max = Point { x: 0, y: 0 };
    for (y, line) in input.split("\n").filter(|s| !s.is_empty()).enumerate() {
        if y as i64 > max.y {
            max.y = y as i64;
        }
        for (x, c) in line.chars().enumerate() {
            if x as i64 > max.x {
                max.x = x as i64;
            }
            if !c.is_alphanumeric() {
                continue;
            }
            antennas.insert(Point { x: x as i64, y: y as i64 }, c);
            chars.insert(c);
        }
    }
    println!("max: {:?}", max);
    let mut antinodes: HashSet<Point> = HashSet::new();
    for c in chars.iter() {
        let ants = antennas.iter()
            .filter(|(_p, a)| a == &c)
            .map(|(p, _c)| p)
            .collect::<Vec<&Point>>();
        // iterate over unique pairings of matching antennas
        for (a, b) in ants.clone().iter()
            .cartesian_product(ants)
            .filter(|(a, b)| *a != b && *a > b) {
                let d = **a - *b;
                //println!("{}: {:?} - {:?} = {:?}", c, a, b, d);
                let bigger = **a + d;
                if bigger.x <= max.x && bigger.y <= max.y
                    && bigger.x >= 0 && bigger.y >= 0 {
                    //&& !antennas.contains_key(&bigger) {
                        antinodes.insert(bigger);
                    }
                let smaller = *b - d;
                if smaller.x <= max.x && smaller.y <= max.y
                    && smaller.x >= 0 && smaller.y >= 0 {
                    //&& !antennas.contains_key(&smaller) {
                        antinodes.insert(smaller);
                    }
            }
    }

    println!("{:?}", antinodes);
    visualize(&antennas, &antinodes, max);

    antinodes.len()
}

#[allow(dead_code)]
fn visualize(ants: &HashMap<Point, char>, antis: &HashSet<Point>, max: Point) {
    for y in 0..=max.y {
        for x in 0..=max.x {
            let p = Point { x, y };
            if ants.contains_key(&p) {
                print!("{}", ants.get(&p).unwrap());
            }
            else if antis.contains(&p) {
                print!("#");
            }
            else {
                print!(".");
            }
        }
        println!();
    }
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 14);
}
