use regex::Regex;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> i64 {
    let dont_re = Regex::new(r"(don't\(\).*?)do\(\)").unwrap();
    let clean = dont_re.replace_all(input, "");
    println!("{clean}");

    let valid_re = Regex::new(r"(mul\(\d+,\d+\))+?").unwrap();
    let nums_re = Regex::new(r"(\d+)").unwrap();

    let mut total = 0;
    for f in valid_re.find_iter(&clean) {
        let nums = nums_re.find_iter(f.as_str())
            .map(|n| n.as_str().parse::<i64>().unwrap())
            .collect::<Vec<i64>>();
        total += nums[0] * nums[1];
    }

    total
}

#[test]
fn test() {
    let input = include_str!("test2.txt");
    assert_eq!(process(input), 48);
}
