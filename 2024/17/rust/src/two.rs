use regex::Regex;

fn main() {
    let out = process(include_str!("input.txt"));
    println!("{}", out);
}

#[derive(Clone, Debug)]
struct State {
    pointer: usize,
    registers: Registers,
    output: Vec<u32>,
    halt: bool,
}

// "any integer", let's go as big as possible
#[derive(Clone, Copy, Debug)]
struct Registers {
    a: u128,
    b: u128,
    c: u128,
}

fn process(input: &str) -> u128 {
    let mut state = State {
        pointer: 0,
        registers: Registers {
            a: 0,
            b: 0,
            c: 0,
        },
        output: vec![],
        halt: false,
    };

    // read register settings from input
    let reg_re = Regex::new(r"(Register (\w): (\d+))+").unwrap();
    for c in reg_re.captures_iter(input) {
        let (_, [_, reg, val]) = c.extract();
        match reg {
            "A" => state.registers.a = val.parse::<u128>().unwrap(),
            "B" => state.registers.b = val.parse::<u128>().unwrap(),
            "C" => state.registers.c = val.parse::<u128>().unwrap(),
            _ => {}
        };
    }
    // read program "tape" from input
    let prog_re = Regex::new(r"Program: ([\d,]+)").unwrap();
    let program = prog_re.captures_iter(input)
        .map(|c| c.extract())
        .flat_map(|(_, [val])| val.split(",").map(|n| n.parse::<u32>().unwrap()))
        .collect::<Vec<u32>>();

    // need 48 bits, start at 44
    // there might be leading zeroes......
    //let mut val = 2_u128.pow(44);
    // first prefix hit during the hunt
    let mut val = 17592241400722;
    //println!("starting search at: {val}");
    //let mut prev = 0;
    loop {
        let mut s = state.clone();
        s.registers.a = val;
        loop {
            // map opcodes to functions, passing operand and mutable s
            match program[s.pointer] {
                0 => adv(program[s.pointer + 1], &mut s),
                1 => bxl(program[s.pointer + 1], &mut s),
                2 => bst(program[s.pointer + 1], &mut s),
                3 => jnz(program[s.pointer + 1], &mut s),
                4 => bxc(program[s.pointer + 1], &mut s),
                5 => out(program[s.pointer + 1], &mut s),
                6 => bdv(program[s.pointer + 1], &mut s),
                7 => cdv(program[s.pointer + 1], &mut s),
                _ => {}
            };
            // as soon as non-empty output deviates from the program, move on
            if !s.output.is_empty() && s.output.as_slice() != &program[0..s.output.len()] {
                break;
            }
            // ran off the end of the tape, or hit a halt (combo 7)
            if s.pointer >= program.len() || s.halt {
                break;
            }
        }
        // hunting for a cycle/pattern
        // find value(s) for which a nice portion of our output matches the program
        // looking at the interval between these values
        /*
        if s.output.len() >= 7 && &s.output[0..7] == &program[0..7] {
            println!("{}: {:?}, {}", val, s.output, val-prev);
            prev = val;
        }
         */
        // running program outputs itself
        if program == s.output {
            break;
        }
        // from observation, step =? 2097152
        // hmmmm, that's 2.pow(21)...  meaningful?
        //val += 1;
        val += 2097152;
    }
    
    val
}

fn combo_operand(op: u32, state: &State) -> u128 {
    match op {
        0 => 0,
        1 => 1,
        2 => 2,
        3 => 3,
        4 => state.registers.a,
        5 => state.registers.b,
        6 => state.registers.c,
        _ => 0 // should not happen?!?
    }
}

// opcode 0
fn adv(op: u32, state: &mut State) {
    // combo operand 7 is illegal
    // can't pass state as mutable, so need to catch it at the call site
    if op == 7 {
        state.halt = true;
        return;
    }
    state.registers.a /= 2_u128.pow(combo_operand(op, state) as u32);
    state.pointer += 2;
}

// opcode 1
fn bxl(op: u32, state: &mut State) {
    state.registers.b ^= op as u128;
    state.pointer += 2;
}

#[test]
fn test_opcode_1() {
    let mut state = State {
        pointer: 0,
        registers: Registers {
            a: 0,
            b: 29,
            c: 0,
        },
        output: vec![],
        halt: false,
    };
    bxl(7, &mut state);
    assert_eq!(state.registers.b, 26);
}

// opcode 2
fn bst(op: u32, state: &mut State) {
    // combo operand 7 is illegal
    // can't pass state as mutable, so need to catch it at the call site
    if op == 7 {
        state.halt = true;
        return;
    }
    state.registers.b = combo_operand(op, state) % 8;
    state.pointer += 2;
}

#[test]
fn test_opcode_2() {
    let mut state = State {
        pointer: 0,
        registers: Registers {
            a: 0,
            b: 0,
            c: 9,
        },
        output: vec![],
        halt: false,
    };
    bst(6, &mut state);
    assert_eq!(state.registers.b, 1);
}

// opcode 3
fn jnz(op: u32, state: &mut State) {
    if state.registers.a != 0 {
        state.pointer = op as usize;
    }
    else {
        // advance pointer only if we didn't explicitly jump
        state.pointer += 2;
    }
}

// opcode 4
fn bxc(_op: u32, state: &mut State) {
    state.registers.b ^= state.registers.c;
    state.pointer += 2;
}

#[test]
fn test_opcode_4() {
    let mut state = State {
        pointer: 0,
        registers: Registers {
            a: 0,
            b: 2024,
            c: 43690,
        },
        output: vec![],
        halt: false,
    };
    bxc(0, &mut state);
    assert_eq!(state.registers.b, 44354);
}

// opcode 5
fn out(op: u32, state: &mut State) {
    // combo operand 7 is illegal
    // can't pass state as mutable, so need to catch it at the call site
    if op == 7 {
        state.halt = true;
        return;
    }
    state.output.push(combo_operand(op, state) as u32 % 8);
    state.pointer += 2;
}

// opcode 6
fn bdv(op: u32, state: &mut State) {
    // combo operand 7 is illegal
    // can't pass state as mutable, so need to catch it at the call site
    if op == 7 {
        state.halt = true;
        return;
    }
    state.registers.b = state.registers.a/2_u128.pow(combo_operand(op, state) as u32);
    state.pointer += 2;
}

// opcode 7
fn cdv(op: u32, state: &mut State) {
    // combo operand 7 is illegal
    // can't pass state as mutable, so need to catch it at the call site
    if op == 7 {
        state.halt = true;
        return;
    }
    state.registers.c = state.registers.a/2_u128.pow(combo_operand(op, state) as u32);
    state.pointer += 2;
}

#[test]
fn test() {
    assert_eq!(process(include_str!("test3.txt")), 117440);
}
