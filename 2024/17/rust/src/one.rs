use regex::Regex;

fn main() {
    let out = process(include_str!("input.txt"));
    println!("{}", out.0);
}

#[derive(Clone, Debug)]
struct State {
    pointer: usize,
    registers: Registers,
    output: Vec<u32>,
}

#[derive(Clone, Copy, Debug)]
struct Registers {
    a: u32,
    b: u32,
    c: u32,
}

fn process(input: &str) -> (String, State) {
    let mut state = State {
        pointer: 0,
        registers: Registers {
            a: 0,
            b: 0,
            c: 0,
        },
        output: vec![],
    };

    // read register settings from input
    let reg_re = Regex::new(r"(Register (\w): (\d+))+").unwrap();
    for c in reg_re.captures_iter(input) {
        let (_, [_, reg, val]) = c.extract();
        match reg {
            "A" => state.registers.a = val.parse::<u32>().unwrap(),
            "B" => state.registers.b = val.parse::<u32>().unwrap(),
            "C" => state.registers.c = val.parse::<u32>().unwrap(),
            _ => {}
        };
    }
    // read program "tape" from input
    let prog_re = Regex::new(r"Program: ([\d,]+)").unwrap();
    let program = prog_re.captures_iter(input)
        .map(|c| c.extract())
        .flat_map(|(_, [val])| val.split(",").map(|n| n.parse::<u32>().unwrap()))
        .collect::<Vec<u32>>();
    //println!("{:?}", state);
    //println!("Program: {:?}", program);
    // run the program
    loop {
        // map opcodes to functions, passing operand and mutable state
        match program[state.pointer] {
            0 => adv(program[state.pointer + 1], &mut state),
            1 => bxl(program[state.pointer + 1], &mut state),
            2 => bst(program[state.pointer + 1], &mut state),
            3 => jnz(program[state.pointer + 1], &mut state),
            4 => bxc(program[state.pointer + 1], &mut state),
            5 => out(program[state.pointer + 1], &mut state),
            6 => bdv(program[state.pointer + 1], &mut state),
            7 => cdv(program[state.pointer + 1], &mut state),
            _ => {}
        };
        if state.pointer >= program.len() {
            break;
        }
    }
    
    let strings = state.output.iter().map(|n| format!("{n}")).collect::<Vec<String>>();
    (strings.join(","), state)
}

fn combo_operand(op: u32, state: &State) -> u32 {
    match op {
        0 => 0,
        1 => 1,
        2 => 2,
        3 => 3,
        4 => state.registers.a,
        5 => state.registers.b,
        6 => state.registers.c,
        _ => 0 // shold not happen?!?
    }
}

// opcode 0
fn adv(op: u32, state: &mut State) {
    state.registers.a /= 2_u32.pow(combo_operand(op, state));
    state.pointer += 2;
}

// opcode 1
fn bxl(op: u32, state: &mut State) {
    state.registers.b ^= op;
    state.pointer += 2;
}

#[test]
fn test_opcode_1() {
    let mut state = State {
        pointer: 0,
        registers: Registers {
            a: 0,
            b: 29,
            c: 0,
        },
        output: vec![],
    };
    bxl(7, &mut state);
    assert_eq!(state.registers.b, 26);
}

// opcode 2
fn bst(op: u32, state: &mut State) {
    state.registers.b = combo_operand(op, state) % 8;
    state.pointer += 2;
}

#[test]
fn test_opcode_2() {
    let mut state = State {
        pointer: 0,
        registers: Registers {
            a: 0,
            b: 0,
            c: 9,
        },
        output: vec![],
    };
    bst(6, &mut state);
    assert_eq!(state.registers.b, 1);
}

// opcode 3
fn jnz(op: u32, state: &mut State) {
    if state.registers.a != 0 {
        state.pointer = op as usize;
    }
    else {
        // advance pointer only if we didn't explicitly jump
        state.pointer += 2;
    }
}

// opcode 4
fn bxc(_op: u32, state: &mut State) {
    state.registers.b ^= state.registers.c;
    state.pointer += 2;
}

#[test]
fn test_opcode_4() {
    let mut state = State {
        pointer: 0,
        registers: Registers {
            a: 0,
            b: 2024,
            c: 43690,
        },
        output: vec![],
    };
    bxc(0, &mut state);
    assert_eq!(state.registers.b, 44354);
}

// opcode 5
fn out(op: u32, state: &mut State) {
    state.output.push(combo_operand(op, state) % 8);
    state.pointer += 2;
}

// opcode 6
fn bdv(op: u32, state: &mut State) {
    state.registers.b = state.registers.a/2_u32.pow(combo_operand(op, state));
    state.pointer += 2;
}

// opcode 7
fn cdv(op: u32, state: &mut State) {
    state.registers.c = state.registers.a/2_u32.pow(combo_operand(op, state));
    state.pointer += 2;
}

#[test]
fn test() {
    assert_eq!(process(include_str!("test.txt")).0, "4,6,3,5,6,3,5,2,1,0");
}

#[test]
fn test1() {
    assert_eq!(process(include_str!("test1.txt")).0, "0,1,2");
}

#[test]
fn test2() {
    let out = process(include_str!("test2.txt"));
    assert_eq!(out.0, "4,2,5,6,7,7,7,7,3,1,0");
    assert_eq!(out.1.registers.a, 0);
}
