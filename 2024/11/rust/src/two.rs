use std::collections::HashMap;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input, 75));
}

fn process(input: &str, blinks: usize) -> u64 {
    let mut stones = input.split(" ")
        .map(|s|
            match s.strip_suffix("\n") {
                Some(r) => (r.parse::<u64>().unwrap(), 1),
                None => (s.parse::<u64>().unwrap(), 1),
            }
        )
        .collect::<HashMap<u64, u64>>();

    //println!("{:?}", stones);
    for _i in 0..blinks {
        let mut new = HashMap::new();
        for (stone, count) in stones.into_iter() {
            let s = format!("{stone}");
            let vals = if stone == 0 {
                vec![1]
            }
            else if s.len() % 2 == 0 {
                let(l, r) = s.split_at(s.len() / 2);
                vec![l.parse::<u64>().unwrap(), r.parse::<u64>().unwrap()]
            }
            else {
                vec![stone * 2024]
            };
            for v in vals.iter() {
                new.entry(*v)
                    .and_modify(|value| * value += count)
                    .or_insert(count);
            }
        }
        stones = new;
        //println!("{:?}", stones);
    }

    stones.values().sum()
}

#[test]
fn test0() {
    let input = include_str!("test0.txt");
    assert_eq!(process(input, 1), 7);
}

#[test]
fn test1() {
    let input = include_str!("test1.txt");
    assert_eq!(process(input, 6), 22);
    assert_eq!(process(input, 25), 55312);
}
