fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input, 25));
}

fn process(input: &str, blinks: usize) -> usize {
    let mut stones = input.split(" ")
        .map(|s|
            match s.strip_suffix("\n") {
                Some(r) => r.parse::<u64>().unwrap(),
                None => s.parse::<u64>().unwrap(),
            }
        )
        .collect::<Vec<u64>>();
    //println!("{:?}", stones);
    for _i in 0..blinks {
        stones = stones.into_iter()
            .flat_map(
                |n|
                {
                    let s = format!("{n}");
                    if n == 0 {
                        // 0 => 1
                        vec![1]
                    }
                    else if s.len() % 2 == 0 {
                        // even number of digits, split in half
                        let (l, r) = s.split_at(s.len() / 2);
                        vec![l.parse::<u64>().unwrap(), r.parse::<u64>().unwrap()]
                    }
                    else {
                        vec![n * 2024]
                    }
                }
            )
            .collect::<Vec<u64>>();
        //println!("{:?}", stones);
    }

    stones.len()
}

#[test]
fn test0() {
    let input = include_str!("test0.txt");
    assert_eq!(process(input, 1), 7);
}

#[test]
fn test1() {
    let input = include_str!("test1.txt");
    assert_eq!(process(input, 6), 22);
    assert_eq!(process(input, 25), 55312);
}
