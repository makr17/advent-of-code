use std::collections::{HashMap, HashSet};
use std::ops::Add;

use lazy_static::lazy_static;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Box {
    left: Point,
    right: Point,
}

#[derive(Clone, Debug)]
struct Map {
    robot: Point,
    walls: HashSet<Point>,
    boxes: HashSet<Box>,
    max: Point,
}

lazy_static! {
    static ref TRANSLATIONS: HashMap<char, Point> = HashMap::from([
        ('^', Point { x: 0, y: -1 }),
        ('v', Point { x: 0, y: 1 }),
        ('<', Point { x: -1, y: 0 }),
        ('>', Point { x: 1, y: 0 }),
    ]);
}

fn process(input: &str) -> i64 {
    let mut map = Map {
        robot: Point { x: -1, y: -1 },
        walls: HashSet::new(),
        boxes: HashSet::new(),
        max: Point { x: -1, y: -1 },
    };
    let mut moves: Vec<char> = vec![];
    let mut map_complete = false;
    for (y, line) in input.split("\n").enumerate() {
        if line.is_empty() {
            map_complete = true;
            continue;
        }
        if !map_complete {
            if y as i64 > map.max.y {
                map.max.y = y as i64;
            }
            for (x, c) in line.chars().enumerate() {
                if (x*2 + 1) as i64 > map.max.x {
                    map.max.x = (x*2 + 1) as i64;
                }
                let p = Point { x: (x*2) as i64, y: y as i64 };
                if c == '#' {
                    map.walls.insert(p);
                    map.walls.insert(p + Point { x: 1, y: 0 });
                }
                else if c == 'O' {
                    map.boxes.insert(
                        Box {
                            left: p,
                            right: p + Point { x: 1, y: 0 },
                        }
                    );
                }
                else if c == '@' {
                    map.robot = p;
                }
            }
        }
        else {
            let mut m = line.chars().collect::<Vec<char>>();
            moves.append(&mut m);
        }
    }
    //visualize(&map);

    for m in moves {
        //println!("Move {}:", m);
        do_move(&mut map, m);

        //visualize(&map);
    }

    map.boxes.iter()
        .map(|b| 100 * b.left.y + b.left.x)
        .sum()
}

fn do_move(map: &mut Map, m: char) {
    let new = map.robot + *TRANSLATIONS.get(&m).unwrap();
    //println!("do_move({}): {:?}", m, new);
    if map.walls.contains(&new) {
        return
    }
    if boxes_contains(&map.boxes, &new).is_some() {
        if do_box_move(map, boxes_contains(&map.boxes, &new).unwrap(), m) {
            map.robot = new;
        }
    }
    else {
        map.robot = new;
    }
}

fn can_box_vertical_move(map: &mut Map, b: Box, m: char) -> bool {
    let mut moved = b;
    moved.left = moved.left + *TRANSLATIONS.get(&m).unwrap();
    moved.right = moved.right + *TRANSLATIONS.get(&m).unwrap();
    if map.walls.contains(&moved.left) || map.walls.contains(&moved.right) {
        return false
    }

    if m == '^' || m == 'v' {
        let left_blocker = boxes_contains(&map.boxes, &moved.left);
        let right_blocker = boxes_contains(&map.boxes, &moved.right);
        let left_success = match left_blocker {
            Some(blk) => can_box_vertical_move(map, blk, m),
            None => true,
        };
        let right_success = if right_blocker.is_some() && left_blocker != right_blocker {
            can_box_vertical_move(map, right_blocker.unwrap(), m)
        }
        else {
            true
        };
        if left_success && right_success {
            // box is in the way, but we can move it
            true
        }
        else {
            false
        }
    }
    else {
        true
    }
}

fn do_box_move(map: &mut Map, b: Box, m: char) -> bool {
    let mut moved = b;
    moved.left = moved.left + *TRANSLATIONS.get(&m).unwrap();
    moved.right = moved.right + *TRANSLATIONS.get(&m).unwrap();
    if map.walls.contains(&moved.left) || map.walls.contains(&moved.right) {
        return false
    }
    
    if m == '^' || m == 'v' {
        let left_blocker = boxes_contains(&map.boxes, &moved.left);
        let right_blocker = boxes_contains(&map.boxes, &moved.right);
        let left_success = match left_blocker {
            Some(blk) => can_box_vertical_move(map, blk, m),
            None => true,
        };
        let right_success = if right_blocker.is_some() && left_blocker != right_blocker {
            can_box_vertical_move(map, right_blocker.unwrap(), m)
        }
        else {
            true
        };
        if left_success && right_success {
            if let Some(blk) = left_blocker {
                do_box_move(map, blk, m);
            }
            if left_blocker != right_blocker {
                if let Some(blk) = right_blocker {
                    do_box_move(map, blk, m);
                }
            }
            // box was in the way, but we moved it
            map.boxes.remove(&b);
            map.boxes.insert(moved);
            true
        }
        else {
            false
        }
    }
    else if m == '<' {
        let blocker = boxes_contains(&map.boxes, &moved.left);
        if let Some(blk) = blocker {
            if do_box_move(map, blk, m) {
                map.boxes.remove(&b);
                map.boxes.insert(moved);
                true
            }
            else {
                false
            }
        }
        else {
            map.boxes.remove(&b);
            map.boxes.insert(moved);
            true
        }
    }
    else if m == '>' {
        let blocker = boxes_contains(&map.boxes, &moved.right);
        if let Some(blk) = blocker {
            if do_box_move(map, blk, m) {
                map.boxes.remove(&b);
                map.boxes.insert(moved);
                true
            }
            else {
                false
            }
        }
        else {
            map.boxes.remove(&b);
            map.boxes.insert(moved);
            true
        }
    }
    else {
        false
    }
}

fn boxes_contains(boxes: &HashSet<Box>, p: &Point) -> Option<Box> {
    let matched = boxes.iter()
        .filter(|b| b.left == *p || b.right == *p)
        .cloned()
        .collect::<Vec<Box>>();
    if !matched.is_empty() {
        Some(matched[0])
    }
    else {
        None
    }
}

#[allow(dead_code)]
fn visualize(map: &Map) {
    for y in 0..=map.max.y {
        for x in 0..=map.max.x {
            let p = Point { x, y };
            // 'box' is a reserved keyword
            let boxx = boxes_contains(&map.boxes, &p);
            print!(
                "{}",
                if p == map.robot {
                    "@"
                }
                else if map.walls.contains(&p) {
                    "#"
                }
                else if boxx.is_some() {
                    if boxx.unwrap().left == p {
                        "["
                    }
                    else {
                        "]"
                    }
                }
                else {
                    "."
                }                
            );
        }
        println!();
    }
    println!();
}

#[test]
fn test0_2() {
    assert_eq!(process(include_str!("test0_2.txt")), 105 + 207 + 306);
}

#[test]
fn test() {
    assert_eq!(process(include_str!("test.txt")), 9021);
}
