use std::collections::{HashMap, HashSet};
use std::ops::Add;

use lazy_static::lazy_static;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Clone, Debug)]
struct Map {
    robot: Point,
    walls: HashSet<Point>,
    boxes: HashSet<Point>,
    max: Point,
}

lazy_static! {
    static ref TRANSLATIONS: HashMap<char, Point> = HashMap::from([
        ('^', Point { x: 0, y: -1 }),
        ('v', Point { x: 0, y: 1 }),
        ('<', Point { x: -1, y: 0 }),
        ('>', Point { x: 1, y: 0 }),
    ]);
}

fn process(input: &str) -> i64 {
    let mut map = Map {
        robot: Point { x: -1, y: -1 },
        walls: HashSet::new(),
        boxes: HashSet::new(),
        max: Point { x: -1, y: -1 },
    };
    let mut moves: Vec<char> = vec![];
    let mut map_complete = false;
    for (y, line) in input.split("\n").enumerate() {
        if line.is_empty() {
            map_complete = true;
            continue;
        }
        if !map_complete {
            if y as i64 > map.max.y {
                map.max.y = y as i64;
            }
            for (x, c) in line.chars().enumerate() {
                if x as i64 > map.max.x {
                    map.max.x = x as i64;
                }
                let p = Point { x: x as i64, y: y as i64 };
                if c == '#' {
                    map.walls.insert(p);
                }
                else if c == 'O' {
                    map.boxes.insert(p);
                }
                else if c == '@' {
                    map.robot = p;
                }
            }
        }
        else {
            let mut m = line.chars().collect::<Vec<char>>();
            moves.append(&mut m);
        }
    }
    //visualize(&map);

    for m in moves {
        //println!("Move {}:", m);
        do_move(&mut map, m);

        //visualize(&map);
    }

    map.boxes.iter()
        .map(|Point { x, y }| 100 * y + x)
        .sum()
}

fn do_move(map: &mut Map, m: char) {
    let new = map.robot + *TRANSLATIONS.get(&m).unwrap();
    //println!("do_move({}): {:?}", m, new);
    if map.walls.contains(&new) {
        return
    }
    if map.boxes.contains(&new) {
        if do_box_move(map, new, m) {
            map.robot = new;
        }
    }
    else {
        map.robot = new;
    }
}


fn do_box_move(map: &mut Map, p: Point, m: char) -> bool {
    let new = p + *TRANSLATIONS.get(&m).unwrap();
    if map.walls.contains(&new) {
        // wall is in the way, return false back up the stack
        false
    }
    else if map.boxes.contains(&new) {
        if do_box_move(map, new, m) {
            // box is in the way, but we moved it
            map.boxes.remove(&p);
            map.boxes.insert(new);
            true
        }
        else {
            false
        }
    }
    else {
        // the way is clear, make the move
        map.boxes.remove(&p);
        map.boxes.insert(new);
        true
    }
}

#[allow(dead_code)]
fn visualize(map: &Map) {
    for y in 0..=map.max.y {
        for x in 0..=map.max.x {
            let p = Point { x, y };
            print!(
                "{}",
                if p == map.robot {
                    "@"
                }
                else if map.walls.contains(&p) {
                    "#"
                }
                else if map.boxes.contains(&p) {
                    "O"
                }
                else {
                    "."
                }                
            );
        }
        println!();
    }
    println!();
}

#[test]
fn test0() {
    assert_eq!(process(include_str!("test0.txt")), 2028);
}

#[test]
fn test() {
    assert_eq!(process(include_str!("test.txt")), 10092);
}
