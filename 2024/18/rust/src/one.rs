#![recursion_limit = "1024"]
use std::collections::{HashMap, HashSet};
use std::ops::Add;

use lazy_static::lazy_static;
use pathfinding::prelude::astar;

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input, 70, 1024));
}

// possible directions
#[allow(dead_code)]
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
enum Direction {
    N,
    S,
    E,
    W,
}

// points in the maze
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Point {
    // moves in all possible directions
    // but only if there is no wall blocking
    fn moves(self, walls: &HashSet<Self>, size: usize) -> HashSet<Self> {
        MOVES.iter()
            .map(|(_d, p)| self + *p)
            .filter(|p| !walls.contains(p))
            .filter(|p| p.x >= 0 && p.y >= 0 && p.x <= size as i64 && p.y <= size as i64)
            .collect()
    }

    // rough distance metric to guide A*
    fn distance(&self, other: &Point) -> usize {
        (self.x.abs_diff(other.x) + self.y.abs_diff(other.y)) as usize
    }

    // reachable points that are not walls (or corrupted bytes)
    fn successors(self, walls: &HashSet<Point>, size: usize) -> Vec<(Self, usize)> {
        self.moves(walls, size).iter()
            .map(|p| (*p, 1))
            .collect()
    }
}

lazy_static! {
    // directions and associated point translations
    static ref MOVES: HashMap<Direction, Point> = HashMap::from([
        (Direction::N, Point { x: 0, y: -1 }),
        (Direction::E, Point { x: 1, y: 0 }),
        (Direction::S, Point { x: 0, y: 1 }),
        (Direction::W, Point { x: -1, y: 0 }),        
    ]);
}

fn process(input: &str, size: usize, bytes: usize) -> usize {
    let corrupted = input.split("\n")
        .filter(|line| !line.is_empty())
        .map(|line| {
            let ns = line.split(",")
                .map(|s| s.parse::<i64>().unwrap())
                .collect::<Vec<i64>>();
            Point { x: ns[0], y: ns[1] }
        })
        .collect::<Vec<Point>>();
    // take the specified number of corrupted bytes
    let map = corrupted[0..bytes].iter().cloned().collect::<HashSet<Point>>();

    let start = Point { x: 0, y: 0 };
    let end = Point { x: size as i64, y: size as i64 };
    let (_path, cost) = astar(&start, |p| p.successors(&map, size), |p| p.distance(&end)/3, |p| *p == end).unwrap();

    cost
}

#[test]
fn test1() {
    assert_eq!(process(include_str!("../../test.txt"), 6, 12), 22);
}
