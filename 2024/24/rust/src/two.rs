use std::collections::{HashMap, HashSet};

use itertools::Itertools;

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input));
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Op {
    AND,
    OR,
    XOR,
    UNKNOWN, // shouldn't happen, but need something for the _ branch
}

#[derive(Debug, Clone, Copy)]
struct Node<'a> {
    name: &'a str,
    value: Option<bool>,
}

#[derive(Debug, Clone, Copy)]
struct Gate<'a> {
    op: Op,
    left: &'a str,
    right: &'a str,
    dest: &'a str,
}

fn process<'a>(input: &'a str) -> String {
    let mut nodes: HashMap<&str, Node<'a>> = HashMap::new();
    let mut gates: Vec<Gate> = vec![];

    for line in input.split("\n").filter(|l| !l.is_empty()) {
        if line.contains(":") {
            // initial value, e.g.
            //   x00: 1
            let pieces = line.split(": ").collect::<Vec<&str>>();
            let value = match pieces[1] {
                "1" => true,
                "0" => false,
                _ => false, // shouldn't happen, but need to put something in _
            };
            nodes.insert(pieces[0], Node{ name: pieces[0], value: Some(value) });
        }
        else {
            // gate mapping, e.g.
            // ntg XOR fgs -> mjb
            let pieces = line.split(" ").collect::<Vec<&str>>();
            let left = match nodes.get(&pieces[0]) {
                Some(n) => *n,
                None => {
                    let n = Node{ name: pieces[0], value: None };
                    nodes.insert(n.name, n);
                    n
                }
            };
            let right = match nodes.get(&pieces[2]) {
                Some(n) => *n,
                None => {
                    let n = Node{ name: pieces[2], value: None };
                    nodes.insert(n.name, n);
                    n
                }
            };
            let dest = match nodes.get(&pieces[4]) {
                Some(n) => *n,
                None => {
                    let n = Node{ name: pieces[4], value: None };
                    nodes.insert(n.name, n);
                    n
                }
            };
            let op = match pieces[1] {
                "OR" => Op::OR,
                "AND" => Op::AND,
                "XOR" => Op::XOR,
                _ => Op::UNKNOWN,
            };
            gates.push(Gate { op, left: left.name, right: right.name, dest: dest.name });
        }
    }

    // find highest z
    let mut zs = nodes.iter()
        .filter_map(|(name, _)| {
            if name.starts_with("z") {
                Some(*name)
            }
            else {
                None
            }
        })
        .collect::<Vec<&str>>();
    zs.sort_by(|a,b| b.cmp(a));
    let max_z = zs[0];

    // analyze the circuit (ripple carry adder)
    // find gate dest that break the rules
    let xyz = vec!['x','y','z'];
    let mut wrong: HashSet<&str> = HashSet::new();
    for g in gates.iter() {
        // ripple carry adder
        // 1. If the output of a gate is z,
        //    then the operation has to be XOR unless it is the last bit.
        if g.dest.starts_with("z") && g.dest != max_z && g.op != Op::XOR {
            wrong.insert(g.dest);
        }
        // 2. If the output of a gate is not z and the inputs are not x, y
        //    then it has to be AND / OR, but not XOR.
        if g.op == Op::XOR &&
            !xyz.contains(&g.left.chars().next().unwrap()) &&
            !xyz.contains(&g.right.chars().next().unwrap()) &&
            !xyz.contains(&g.dest.chars().next().unwrap()) {
                wrong.insert(g.dest);
            }
        if g.op == Op::AND && g.left != "x00" && g.right != "x00" {
            for sub in gates.iter() {
                if (g.dest == sub.left || g.dest == sub.right) && sub.op != Op::OR {
                    wrong.insert(g.dest);
                }
            }
        }
        if g.op == Op::XOR {
            for sub in gates.iter() {
                if (g.dest == sub.left || g.dest == sub.right) && sub.op == Op::OR {
                    wrong.insert(g.dest);
                }
            }
        }
        
    }
    
    let sorted = wrong.into_iter().sorted().collect::<Vec<&str>>();
    sorted.join(",")
}

#[test]
fn test3() {
    assert_eq!(process(include_str!("../../test3.txt")), "z00,z01,z02,z05".to_string());
}
