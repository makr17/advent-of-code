use std::collections::HashMap;

fn main() {
    let input = include_str!("../../input.txt");
    println!("{}", process(input));
}

#[derive(Debug, Clone, Copy)]
enum GateType {
    AND,
    OR,
    XOR,
    UNKNOWN, // shouldn't happen, but need something for the _ branch
}

#[derive(Debug, Clone, Copy)]
struct Node<'a> {
    name: &'a str,
    value: Option<bool>,
}

#[derive(Debug, Clone, Copy)]
struct Gate<'a> {
    gate_type: GateType,
    left: &'a str,
    right: &'a str,
    dest: &'a str,
}

fn process<'a>(input: &'a str) -> u64 {
    let mut nodes: HashMap<&str, Node<'a>> = HashMap::new();
    let mut gates: Vec<Gate> = vec![];

    for line in input.split("\n").filter(|l| !l.is_empty()) {
        if line.contains(":") {
            // initial value, e.g.
            //   x00: 1
            let pieces = line.split(": ").collect::<Vec<&str>>();
            let value = match pieces[1] {
                "1" => true,
                "0" => false,
                _ => false, // shouldn't happen, but need to put something in _
            };
            nodes.insert(pieces[0], Node{ name: pieces[0], value: Some(value) });
        }
        else {
            // gate mapping, e.g.
            // ntg XOR fgs -> mjb
            let pieces = line.split(" ").collect::<Vec<&str>>();
            let left = match nodes.get(&pieces[0]) {
                Some(n) => *n,
                None => {
                    let n = Node{ name: pieces[0], value: None };
                    nodes.insert(n.name, n);
                    n
                }
            };
            let right = match nodes.get(&pieces[2]) {
                Some(n) => *n,
                None => {
                    let n = Node{ name: pieces[2], value: None };
                    nodes.insert(n.name, n);
                    n
                }
            };
            let dest = match nodes.get(&pieces[4]) {
                Some(n) => *n,
                None => {
                    let n = Node{ name: pieces[4], value: None };
                    nodes.insert(n.name, n);
                    n
                }
            };
            let gate_type = match pieces[1] {
                "OR" => GateType::OR,
                "AND" => GateType::AND,
                "XOR" => GateType::XOR,
                _ => GateType::UNKNOWN,
            };
            gates.push(Gate { gate_type, left: left.name, right: right.name, dest: dest.name });
        }
    }

    let mut empty = nodes.iter()
        .filter(|(_, node)| node.value.is_none())
        .map(|(name, _)| *name)
        .collect::<Vec<&str>>();
    while !empty.is_empty() {
        //println!("empty count {:?}", empty.len());
        for name in empty {
            // can we have more than one?
            for g in gates.iter_mut().filter(|g| g.dest == name) {
                let left = nodes.get(&g.left).unwrap();
                let right = nodes.get(&g.right).unwrap();
                if left.value.is_some() && right.value.is_some() {
                    let value = Some(match g.gate_type {
                        GateType::OR => left.value.unwrap() | right.value.unwrap(),
                        GateType::AND => left.value.unwrap() & right.value.unwrap(),
                        GateType::XOR => left.value.unwrap() ^ right.value.unwrap(),
                        GateType::UNKNOWN => false,
                    });
                    nodes.entry(name).and_modify(|n| n.value = value);
                }
            }
        }
        empty = nodes.iter()
            .filter(|(_, node)| node.value.is_none())
            .map(|(name, _)| *name)
            .collect::<Vec<&str>>();
    }
    // collect z nodes
    let mut zs = nodes.iter()
        .filter(|(name, _)| name.starts_with("z"))
        .map(|(_, node)| *node)
        .collect::<Vec<Node>>();
    // sort descending
    zs.sort_by(|a,b| b.name.cmp(a.name));
    // build binary string
    let numstr = zs.iter()
        .map(|z| match z.value {
            Some(true) => "1",
            Some(false) => "0",
            _ => "z",  // shouldn't happen
        })
        .collect::<String>();
    //println!("{numstr}");
    // and convert to integer
    u64::from_str_radix(&numstr, 2).unwrap()
}

#[test]
fn test1() {
    assert_eq!(process(include_str!("../../test1.txt")), 4);
}

#[test]
fn test2() {
    assert_eq!(process(include_str!("../../test2.txt")), 2024);
}
