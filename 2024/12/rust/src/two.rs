use std::collections::{HashMap, HashSet};

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}


#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Point {
    fn neighbors(self) -> Vec<Self> {
        vec![
            Point { x: self.x, y: self.y - 1 }, // N
            Point { x: self.x, y: self.y + 1 }, // S
            Point { x: self.x - 1, y: self.y }, // W
            Point { x: self.x + 1, y: self.y }, // E
        ]
    }
}

fn process(input: &str) -> usize {
    let mut plots: HashMap<Point, char> = HashMap::new();
    let mut max = Point { x: 0, y: 0 };
    for (y, line) in input.split("\n").filter(|s| !s.is_empty()).enumerate() {
        if y as i64 > max.y {
            max.y = y as i64;
        }
        for (x, c) in line.chars().enumerate() {
            if x as i64 > max.x {
                max.x = x as i64;
            }
            if !c.is_alphanumeric() {
                continue;
            }
            let p = Point { x: x as i64, y: y as i64 };
            plots.insert(p, c);
        }
    }

    let mut connected: Vec<HashSet<Point>> = vec![];
    let mut seen: HashSet<Point> = HashSet::new();
    for x in 0..=max.x {
        for y in 0..=max.y {
            let p = Point { x, y };
            // already connected to an existing connected region
            if seen.contains(&p) {
                continue;
            }
            let mut reachable: HashSet<Point> = HashSet::new();
            contiguous(&p, &plots, &mut reachable);
            for s in reachable.iter() {
                seen.insert(*s);
            }
            connected.push(reachable);
        }
    }

    // cost for each region is the area: r.len() times the perimeter()
    // then sum the costs for all regions
    let mut total = 0;
    for region in connected.iter() {
        let area = region.len();
        let sides = sides(region);
        //println!("{} * {}: {:?}", area, sides, region);
        total += area * sides;
    }

    total
}

fn contiguous(plot: &Point, plots: &HashMap<Point, char>, connected: &mut HashSet<Point>) {
    let plant = plots.get(plot).unwrap();
    connected.insert(*plot);
    for n in plot.neighbors() {
        if !plots.contains_key(&n) {
            // walked off the map
            continue;
        }
        if connected.contains(&n) {
            // already found it
            continue;
        }
        if plots.get(&n).unwrap() == plant {
            // only walk to plots with the same plant
            contiguous(&n, plots, connected);
        }
    }
}

fn sides(region: &HashSet<Point>) -> usize {
    // sides == corners
    // so count the corner pieces
    region.iter()
        .map(|p| corners(p, region))
        .sum()
}

// TODO: there must be a cleaner way to build these...
fn corners(plot: &Point, region: &HashSet<Point>) -> usize {
    let mut count = 0;

    // NW (x-1, y), (x-1, y-1), (x, y-1)
    let nw = vec![Point { x: plot.x-1, y: plot.y }, Point { x: plot.x-1, y: plot.y-1 }, Point { x: plot.x, y: plot.y-1 }];
    if is_convex_corner(&nw, region) || is_concave_corner(&nw, Point { x: plot.x-1, y: plot.y-1 }, region) {
        count += 1;
    }
    // NE (x, y-1), (x+1, y-1), (x+1, y)
    let ne = vec![Point { x: plot.x, y: plot.y-1 }, Point { x: plot.x+1, y: plot.y-1 }, Point { x: plot.x+1, y: plot.y }];
    if is_convex_corner(&ne, region) || is_concave_corner(&ne, Point { x: plot.x+1, y: plot.y-1 }, region) {
        count += 1;
    }
    // SE (x+1, y), (x+1, y+1), (x, y+1)
    let se = vec![Point { x: plot.x+1, y: plot.y }, Point { x: plot.x+1, y: plot.y+1 }, Point { x: plot.x, y: plot.y+1 }];
    if is_convex_corner(&se, region) || is_concave_corner(&se, Point { x: plot.x+1, y: plot.y+1 }, region) {
        count += 1;
    }
    // SW (x-1, y), (x-1, y+1), (x, y+1)
    let sw = vec![Point { x: plot.x-1, y: plot.y }, Point { x: plot.x-1, y: plot.y+1 }, Point { x: plot.x, y: plot.y+1 }];
    if is_convex_corner(&sw, region) || is_concave_corner(&sw, Point { x: plot.x-1, y: plot.y+1 }, region) {
        count += 1;
    }

    count
}

fn is_convex_corner(test: &[Point], region: &HashSet<Point>) -> bool {
    // in a convex corner, all three points wrapping the diagonal are not in the region
    matching_count(test, region) == 0
}

fn matching_count(test: &[Point], region: &HashSet<Point>) -> usize {
    test.iter()
        .filter(|p| region.contains(p))
        .count()
}

fn is_concave_corner(test: &[Point], diag: Point, region: &HashSet<Point>) -> bool {
    // in a concave corner, two points wrapping the diagonal are in the region
    // but the diagonal is not
    let matching = matching_count(test, region);
    if matching == 2 && !region.contains(&diag) {
        true
    }
    else if matching == 1 && region.contains(&diag) {
        // edge case
        // diag is in-region, but the others are not
        // test4: (2,2) SE and (3,3) NW
        true
    }
    else {
        false
    }
}

#[test]
fn test() {
    assert_eq!(process(include_str!("test0.txt")), 80);
    assert_eq!(process(include_str!("test1.txt")), 436);
    assert_eq!(process(include_str!("test2.txt")), 1206);
    assert_eq!(process(include_str!("test3.txt")), 236);
}

#[test]
fn test4() {
    assert_eq!(process(include_str!("test4.txt")), 368);
}
