use std::collections::{HashMap, HashSet};

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}


#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
    x: i64,
    y: i64,
}
impl Point {
    fn neighbors(self) -> Vec<Self> {
        vec![
            Point { x: self.x, y: self.y - 1 }, // N
            Point { x: self.x, y: self.y + 1 }, // S
            Point { x: self.x - 1, y: self.y }, // W
            Point { x: self.x + 1, y: self.y }, // E
        ]
    }
}

fn process(input: &str) -> usize {
    let mut plots: HashMap<Point, char> = HashMap::new();
    let mut max = Point { x: 0, y: 0 };
    for (y, line) in input.split("\n").filter(|s| !s.is_empty()).enumerate() {
        if y as i64 > max.y {
            max.y = y as i64;
        }
        for (x, c) in line.chars().enumerate() {
            if x as i64 > max.x {
                max.x = x as i64;
            }
            if !c.is_alphanumeric() {
                continue;
            }
            let p = Point { x: x as i64, y: y as i64 };
            plots.insert(p, c);
        }
    }

    let mut connected: Vec<HashSet<Point>> = vec![];
    let mut seen: HashSet<Point> = HashSet::new();
    for x in 0..=max.x {
        for y in 0..=max.y {
            let p = Point { x, y };
            // already connected to an existing connected region
            if seen.contains(&p) {
                continue;
            }
            let mut reachable: HashSet<Point> = HashSet::new();
            contiguous(&p, &plots, &mut reachable);
            for s in reachable.iter() {
                seen.insert(*s);
            }
            connected.push(reachable);
        }
    }

    // cost for each region is the area: r.len() times the perimeter()
    // then sum the costs for all regions
    connected.iter()
        .map(|r| r.len() * perimeter(r, &plots))
    .sum()
}

fn contiguous(plot: &Point, plots: &HashMap<Point, char>, connected: &mut HashSet<Point>) {
    let plant = plots.get(plot).unwrap();
    connected.insert(*plot);
    for n in plot.neighbors() {
        if !plots.contains_key(&n) {
            // walked off the map
            continue;
        }
        if connected.contains(&n) {
            continue;
        }
        if plots.get(&n).unwrap() == plant {
            // only walk to plots with the same plant
            contiguous(&n, plots, connected);
        }
    }
}

fn perimeter(region: &HashSet<Point>, plots: &HashMap<Point, char>) -> usize {
    let samples = region.iter().collect::<Vec<&Point>>();
    let plant = plots.get(samples[0]).unwrap();
    // perimeter is all edges that don't have the same plant on the other side
    // including the edge of the map (where there is nothing on the other side)
    region.iter()
        .map(|p| {
            p.neighbors().iter()
                .filter(|n|
                    !plots.contains_key(n) || plots.get(n).unwrap() != plant
                )
                .count()
        })
        .sum()
}

#[test]
fn test() {
    assert_eq!(process(include_str!("test0.txt")), 140);
    assert_eq!(process(include_str!("test1.txt")), 772);
    assert_eq!(process(include_str!("test2.txt")), 1930);
}
