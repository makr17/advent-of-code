fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> u64 {
    // read/parse the input
    let mut left: Vec<u64> = vec![];
    let mut right: Vec<u64> = vec![];
    for line in input.split("\n").filter(|s| !s.is_empty()) {
        let nums = line.split(" ")
            .filter(|s| !s.is_empty())
            .map(|s| s.parse::<u64>().unwrap() )
            .collect::<Vec<u64>>();
        left.push(nums[0]);
        right.push(nums[1]);
    }

    // count the times each left id appears in the right
    // and multiply by the left id
    // and then sum those
    // ids in one left and not in right fall out with count == 0
    let sim = left.iter()
        .map(|l| right.iter()
             .filter(|r| l == *r)
             .count() as u64 * l
        )
        .sum();

    sim
}

#[test]
fn test() {
    let input = include_str!("one.test.txt");
    assert_eq!(process(input), 31);
}
