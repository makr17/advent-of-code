fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> u64 {
    // read/parse the input
    let mut left: Vec<u64> = vec![];
    let mut right: Vec<u64> = vec![];
    for line in input.split("\n").filter(|s| !s.is_empty()) {
        let nums = line.split(" ")
            .filter(|s| !s.is_empty())
            .map(|s| s.parse::<u64>().unwrap() )
            .collect::<Vec<u64>>();
        left.push(nums[0]);
        right.push(nums[1]);
    }

    // sort the lists
    left.sort();
    right.sort();

    // and piece-wise compare/sum distance
    let mut d: u64 = 0;
    for (i, n) in left.iter().enumerate() {
        d += (*n as i128 - right[i] as i128).unsigned_abs() as u64;
    }

    d
}

#[test]
fn test() {
    let input = include_str!("one.test.txt");
    assert_eq!(process(input), 11);
}
