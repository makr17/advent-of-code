use radix_fmt::radix;
use regex::Regex;

fn main() {
    let input = include_str!("input.txt");
    println!("{}", process(input));
}

fn process(input: &str) -> u64 {
    let mut total = 0;
    let parse_re = Regex::new(r"(\d+)").unwrap();
    for line in input.split("\n").filter(|s| !s.is_empty()) {
        let mut nums = parse_re.find_iter(line)
            .map(|s| s.as_str().parse::<u64>().unwrap())
            .collect::<Vec<u64>>();
        let values = nums.split_off(1);
        let target = nums[0];
        //println!("{} {:?}", target, values);
        // enumerate all possible operator configurations for N values
        //   0 => add
        //   1 => multiply
        //   2 => concatenate
        // stop if we find a combination that works
        for mask in 0 .. 3_u64.pow((values.len()-1) as u32) {
            let rad = radix(mask, 3);
            let mut trinary = format!("{}", rad);
            // pad with zeroes out to needed len
            while trinary.len() < values.len() - 1 {
                trinary = format!("0{trinary}");
            }
            //println!("  {trinary}");
            let mut value = values[0];
            for (num, op) in values[1..].iter()
                .zip(trinary.chars().rev()) {
                    let new = match op {
                        '0' => add(value, num),
                        '1' => mul(value, num),
                        '2' => concat(value, num),
                        _ => value, // shouldn't happen, but need to satisfy match
                    };
                    //println!("    {value} {op} {num} = {new}");
                    value = new;
                }
            if value == target {
                total += target;
                //println!("success!");
                break;
            }
        }
    }

    total
}

fn add(l: u64, r: &u64) -> u64 {
    l + r
}

fn mul(l: u64, r: &u64) -> u64 {
    l * r
}

fn concat(l: u64, r: &u64) -> u64 {
    let s = format!("{l}{r}");
    s.parse::<u64>().unwrap()
}

#[test]
fn test_concat() {
    assert_eq!(concat(12, &34), 1234);
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(process(input), 11387);
}
